package com.github.andrdev.bolservice.clients.edit;

import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ClientEditPresenter extends MvpBasePresenter<ClientEditView> {

    public abstract void getData();

    abstract void deleteItem(Client client);

    public abstract void deleteRequestFailed(Throwable throwable);

    public abstract void deleteRequestSuccess(SimpleResponse simpleResponse);

    public abstract void updateItem(ClientUpdateRequest request);

    public abstract void onUpdateSuccess(ClientUpdateRequest request);

    public abstract void onUpdateFailed(Throwable throwable);

    @SuppressWarnings("ConstantConditions")
    abstract void clientSaved(boolean saved);
}
