package com.github.andrdev.bolservice.current.originEbol.damages.chooseDamageType;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginCdtView extends MvpView {

    VehiclePhoto getVehiclePhoto();

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);

    Context getViewContext();

    void setMarkedDamage(MarkedDamage markedDamage);

    void initViews();
}
