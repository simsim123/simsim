package com.github.andrdev.bolservice.model.deserializers;

import android.util.Log;

import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;


public class ProfileResponseTypedFactory extends CustomizedTypeAdapterFactory<ProfileResponse> {

    public ProfileResponseTypedFactory(Class<ProfileResponse> customizedClass) {
        super(customizedClass);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected ProfileResponse deserialize(JsonElement deserialized, Gson gson) {
        ProfileResponse response = null;
        try {
            Log.d("dree", deserialized.toString());
            JsonObject profileJson = deserialized.getAsJsonObject().getAsJsonArray("profile").get(0).getAsJsonObject();
            response = new ProfileResponse();
            String status = deserialized.getAsJsonObject().getAsJsonPrimitive("status").getAsString();
            response.setStatus(status);
            Profile profile;
            profile = gson.getDelegateAdapter(this, TypeToken.get(Profile.class))
                    .fromJson(String.valueOf(profileJson));
            response.setProfile(profile);
        } catch (Throwable e) {
            Log.d("ss", e.getMessage());
            e.printStackTrace();
        }
        return response;
    }
}
