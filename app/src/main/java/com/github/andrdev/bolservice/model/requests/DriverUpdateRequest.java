package com.github.andrdev.bolservice.model.requests;


public class DriverUpdateRequest {
    String did;
    String firstName;
    String lastName;
    String driverEmail;
    String driverPhone;

    public DriverUpdateRequest(String did, String driverEmail, String driverPhone, String firstName, String lastName) {
        this.did = did;
        this.driverEmail = driverEmail;
        this.driverPhone = driverPhone;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
