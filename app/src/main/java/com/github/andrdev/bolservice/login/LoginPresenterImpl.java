package com.github.andrdev.bolservice.login;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class LoginPresenterImpl extends LoginPresenter {

    LoginDataProvider dataProvider;

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new LoginDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @SuppressWarnings("ConstantConditions")
    public void sendLoginRequest(final LoginRequest loginRequest) {
        if (!isViewAttached()) {
            return;
        }
        getView().showProgress();
        dataProvider.setAuthenticatedRestWorker(loginRequest);
        dataProvider.getProfile(
                this::loginSuccess,
                this::loginFailed);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void loginSuccess(ProfileResponse response) {
        if (!isViewAttached()) {
            return;
        }
        if (!response.getStatus().equals("true")) {
            getView().hideProgress();
            getView().showLoginErrorToast();
            return;
        }
        dataProvider.saveProfile(response.getProfile(), this::profileSaved);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void loginFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().hideProgress();
        getView().showLoginErrorToast();
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void profileSaved(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().hideProgress();
        getView().saveLoginInfo(EbolNetworkWorker2.getLoginInfo());
        getView().openDashBoardActivity();
    }
}
