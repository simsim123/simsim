package com.github.andrdev.bolservice.colleagues.create;

import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;


public class ColleagueNewPresenterImpl extends ColleagueNewPresenter {

    ColleagueNewDataProvider dataProvider;

    @Override
    public void attachView(ColleagueNewView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ColleagueNewDataProvider();
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void createDriver(CreateDriverRequest request) {
        dataProvider.createColleague(request, this::requestSuccess, this::requestFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestSuccess(CreateDriverResponse simpleResponse) {
        if (!isViewAttached()) {
            return;
        }

        if (simpleResponse.getStatus().equals("true")) {
            getView().backToColleagues();
        } else {
            getView().showFailedToast();
        }
    }
}
