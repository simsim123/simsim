package com.github.andrdev.bolservice.model.responses;

import java.util.List;


public class DamagesGetResponse {
    String status;
    List<DamagesG> damages;

    public List<DamagesG> getDamages() {
        return damages;
    }

    public void setDamages(List<DamagesG> damages) {
        this.damages = damages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
