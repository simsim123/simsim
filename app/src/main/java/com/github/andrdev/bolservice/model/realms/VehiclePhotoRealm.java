package com.github.andrdev.bolservice.model.realms;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class VehiclePhotoRealm extends RealmObject{

    public static final String ID = "id";
    public static final String PHOTO_TYPE = "photoType";
    public static final String PHOTO_PATH = "photoPath";
    public static final String IS_TEMP = "isTemp";
    public static final String NOTE = "note";
    public static final String MARKED_DAMAGES = "markedDamages";
    public static final String MARKED_DAMAGES_DELIVERY = "markedDamagesDelivery";

    @PrimaryKey
    private int id;
    private String photoType;
    private String photoPath;
    private boolean isTemp;
    private String note;
    private RealmList<MarkedDamageRealm> markedDamages;
    private RealmList<MarkedDamageRealm> markedDamagesDelivery;

    public VehiclePhotoRealm() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTemp() {
        return isTemp;
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp;
    }

    public RealmList<MarkedDamageRealm> getMarkedDamages() {
        return markedDamages;
    }

    public void setMarkedDamages(RealmList<MarkedDamageRealm> markedDamages) {
        this.markedDamages = markedDamages;
    }

    public RealmList<MarkedDamageRealm> getMarkedDamagesDelivery() {
        return markedDamagesDelivery;
    }

    public void setMarkedDamagesDelivery(RealmList<MarkedDamageRealm> markedDamagesDelivery) {
        this.markedDamagesDelivery = markedDamagesDelivery;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }
}
