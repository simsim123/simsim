package com.github.andrdev.bolservice.newEbol;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.requests.CarCreateRequest;
import com.github.andrdev.bolservice.model.requests.CarPhotoCreateRequest;
import com.github.andrdev.bolservice.model.requests.NewDamageRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.github.andrdev.bolservice.newEbol.trs.NewEbolDataProvider;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;

public class NewFFebolPresenterImpl extends NewFFebolPresenter {

    NewFFebolDataProvider dataProvider;

    @Override
    public void attachView(NewFFebolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new NewFFebolDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getEbol(this::setEbol);
    }

    private void setEbol(NewEbol newEbol) {
        if (!isViewAttached()) {
            return;
        }
        Log.v("dree", newEbol.getOrderId()!=null?newEbol.getOrderId():"nyull");
        getView().setData(newEbol);
    }

    @Override
    public void saveEbol(NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSave();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }

    @Override
    public void finishEbolCreation() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openSendActivity);
    }

    private void openSendActivity(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().finishEbol();
    }

    @Override
    public void addNewVehicle() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::addNewVehicleEbolSaved);
    }

    private void addNewVehicleEbolSaved(boolean saved) {
        dataProvider.removeTempVehicle(this::openVehicleCreation);
    }

    private void openVehicleCreation(boolean cleared) {
        if (!isViewAttached()) {
            return;
        }
        getView().openNewVehicleInfoActivity();
    }

    @Override
    public void detailsClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showVehicleDetails(vehicleId));
    }

    @Override
    public void justSaveEbol() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> proceedWithExit());
    }

    @Override
    public void justSaveEbolDb() {
        if (!isViewAttached()) {
            return;
        }
        saveEbolDb((saved) -> proceedWithExit());
    }

    private void saveEbolDb(NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSaveDb();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }

    private void proceedWithExit() {
//        getView().proceedWithExit();
    }

    public void showVehicleDetails(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showVehicleDetails(vehicleId);
    }

    @Override
    public void damagesClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showDamages(vehicleId));
    }

    public void showDamages(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showDamages(vehicleId);
    }

    @Override
    public void customerSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openCustomerSignature);
    }

    private void openCustomerSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openCustomerSignature();
    }

    @Override
    public void driverSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openDriverSignature);
    }

    private void openDriverSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openDriverSignature();
    }

    @Override
    public void unableToSignClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::showUnableToSign);
    }

    public void showUnableToSign(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().showUnableToSign();
    }

    void sendEbol() {
        NewEbol ne = new NewEbol();
        Observable.from(ne.getVehicleInfos()).forEach(
                vehicleInfo -> EbolNetworkWorker2.getInstance()
                        .createVehicle(new CarCreateRequest(vehicleInfo, ne.getId()))
                        .doOnCompleted(
                                Observable.just(vehicleInfo.getDamages().getFront(),
                                        vehicleInfo.getDamages().getBack(),
                                        vehicleInfo.getDamages().getRightSide(),
                                        vehicleInfo.getDamages().getLeftSide()).forEach(
                                        photo->Observable.from(photo.getMarkedDamages()).doOnEach(
                                                damage ->EbolNetworkWorker2.getInstance()
                                                        .postDamages(new NewDamageRequest(damage, photo.getPhotoType())))
                                                .doOnCompleted(
                                                EbolNetworkWorker2.getInstance().uploadPhoto(new CarPhotoCreateRequest(photo, vehicleInfo.getId())));
                                             }


        }
    }
}

