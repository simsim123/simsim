package com.github.andrdev.bolservice.messages;

import android.content.Context;

import com.github.andrdev.bolservice.model.Message;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;


public interface ChatView extends MvpView {
    void sendRequest(long conversationId);

    void setData(List<Message> data);

    void showChatList(int id);

    Context getViewContext();
}
