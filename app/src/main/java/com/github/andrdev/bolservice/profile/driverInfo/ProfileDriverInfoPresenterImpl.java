package com.github.andrdev.bolservice.profile.driverInfo;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class ProfileDriverInfoPresenterImpl extends ProfileDriverInfoPresenter {

    ProfileDriverInfoDataProvider dataProvider;

    @Override
    public void attachView(ProfileDriverInfoView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ProfileDriverInfoDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getSavedProfile(this::setData);
    }

    public void setData(Profile profile) {
        if (!isViewAttached()) {
            return;
        }
        getView().setProfile(profile);
        getView().initFields();
    }

    public void saveData(SaveProfileRequest profileRequest) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.saveProfile(profileRequest, this::requestSuccess, this::requestFailed);
    }

    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
    }

    public void requestSuccess(SaveProfileResponse saveProfileResponse) {
        if (!isViewAttached()) {
            return;
        }
            if (saveProfileResponse.getStatus().equals("true")
                    && saveProfileResponse.getProfile() != null) {
                getView().setProfile(saveProfileResponse.getProfile());
            }
    }
}
