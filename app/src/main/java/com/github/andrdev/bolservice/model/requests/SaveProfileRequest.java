package com.github.andrdev.bolservice.model.requests;


public class SaveProfileRequest {

    String firstName;
    String lastName;
    String billingFirstName;
    String billingLastName;
    String billingAddress;
    String billingCity;
    String billingState;
    String billingZip;
    String billingPhone;
    String billingFax;
    String billingEmail;
    String companyName;
    String companyAddress;
    String companyCity;
    String companyState;
    String companyZip;
    String companyPhone;
    String companyFax;
    String companyEma;
    String companyWebsite;
    String companyUsdot;
    String companyMc;
    String driverEmail;
    String driverPhone;
    String usertype;

    public SaveProfileRequest(String billingAddress, String billingCity, String billingEmail,
                              String billingFax, String billingFirstName, String billingLastName,
                              String billingPhone, String billingState, String billingZip,
                              String companyAddress, String companyCity, String companyEma,
                              String companyFax, String companyMc, String companyName,
                              String companyPhone, String companyState, String companyUsdot,
                              String companyWebsite, String companyZip, String driverEmail,
                              String driverPhone, String firstName, String lastName,
                              String usertype) {
        this.billingAddress = billingAddress;
        this.billingCity = billingCity;
        this.billingEmail = billingEmail;
        this.billingFax = billingFax;
        this.billingFirstName = billingFirstName;
        this.billingLastName = billingLastName;
        this.billingPhone = billingPhone;
        this.billingState = billingState;
        this.billingZip = billingZip;
        this.companyAddress = companyAddress;
        this.companyCity = companyCity;
        this.companyEma = companyEma;
        this.companyFax = companyFax;
        this.companyMc = companyMc;
        this.companyName = companyName;
        this.companyPhone = companyPhone;
        this.companyState = companyState;
        this.companyUsdot = companyUsdot;
        this.companyWebsite = companyWebsite;
        this.companyZip = companyZip;
        this.driverEmail = driverEmail;
        this.driverPhone = driverPhone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.usertype = usertype;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingFax() {
        return billingFax;
    }

    public void setBillingFax(String billingFax) {
        this.billingFax = billingFax;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingZip() {
        return billingZip;
    }

    public void setBillingZip(String billingZip) {
        this.billingZip = billingZip;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public String getCompanyEma() {
        return companyEma;
    }

    public void setCompanyEma(String companyEma) {
        this.companyEma = companyEma;
    }

    public String getCompanyFax() {
        return companyFax;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    public String getCompanyMc() {
        return companyMc;
    }

    public void setCompanyMc(String companyMc) {
        this.companyMc = companyMc;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyState() {
        return companyState;
    }

    public void setCompanyState(String companyState) {
        this.companyState = companyState;
    }

    public String getCompanyUsdot() {
        return companyUsdot;
    }

    public void setCompanyUsdot(String companyUsdot) {
        this.companyUsdot = companyUsdot;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyZip() {
        return companyZip;
    }

    public void setCompanyZip(String companyZip) {
        this.companyZip = companyZip;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
}
