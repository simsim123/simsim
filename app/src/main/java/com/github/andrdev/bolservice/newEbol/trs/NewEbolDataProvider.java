package com.github.andrdev.bolservice.newEbol.trs;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.GpsAddressProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.util.List;

public class NewEbolDataProvider implements DataProvider {

    GpsAddressProvider mapsGpsHandler;
    DbHelper dbHelper;

    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public GpsAddressProvider getMapsGpsHandler() {
        return mapsGpsHandler;
    }

    public void setMapsGpsHandler(GpsAddressProvider mapsGpsHandler) {
        this.mapsGpsHandler = mapsGpsHandler;
    }

    public void saveOrUpdateEbol(NewEbol newEbol, DataCb<Boolean> callback) {
//        realmDbData.updateOrSaveEbol(newEbol, callback::returnData);
    }

    public void removeTempVehicle(DataCb<Boolean> callback) {
//        realmDbData.clearVehicleInfoTable(callback::returnData);
    }

    public void getEbol(DataCb<NewEbol> callback) {
//        realmDbData.getEbol(callback::returnData);
    }

    public void checkForGpsPermission(DataCb<Boolean> callback) {

        mapsGpsHandler.hasGpsPermissions(callback::returnData);
    }

    public void checkGpsIsTurnedOn(DataCb<Boolean> callback) {
        mapsGpsHandler.checkGpsTurnedOn(callback::returnData);
    }

    public void getGpsCoordinates(DataCb<Location> callback) {
        mapsGpsHandler.getCurrentGpsLocationOv(callback::returnData);
    }

    public void getAddressFromCoordinates(Location location, DataCb<List<Address>> callback) {
        mapsGpsHandler.getAddressGJs(location, callback::returnData);
    }

    public void deInit() {
        dbHelper.deInit();
        mapsGpsHandler.deInit();
    }
}
