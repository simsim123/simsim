package com.github.andrdev.bolservice.newEbol.damages;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolDamagesFragment  {
//
//    public static final String PHOTO_TYPE = "photoType";
//    public static final String FRONT_PHOTO = "frontPhoto";
//    public static final String ADDITIONAL_PHOTO_FIRST = "additionalPhotoFirst";
//    public static final String ADDITIONAL_PHOTO_SECOND = "additionalPhotoSecond";
//    public static final String CarPhotosWidget = "backPhoto";
//    public static final String LEFT_SIDE_PHOTO = "leftSidePhoto";
//    public static final String RIGHT_SIDE_PHOTO = "rightSidePhoto";
//
//    @Bind(R.id.front)
//    ImageView front;
//    @Bind(R.id.back)
//    ImageView back;
//    @Bind(R.id.leftSide)
//    ImageView leftSide;
//    @Bind(R.id.rightSide)
//    ImageView rightSide;
//    @Bind(R.id.additionalFir)
//    ImageView additionalFir;
//    @Bind(R.id.additionalSec)
//    ImageView additionalSec;
//
//    @Bind(R.id.keysCount)
//    EditText keysCount;
//    @Bind(R.id.headRestCount)
//    EditText headRestCount;
//    @Bind(R.id.remotesCount)
//    EditText remotesCount;
//    @Bind(R.id.floorMatsCount)
//    EditText floorMatsCount;
//    @Bind(R.id.frontDamagesCount)
//    TextView frontDamagesCount;
//    @Bind(R.id.backDamagesCount)
//    TextView backDamagesCount;
//    @Bind(R.id.rightsideDamagesCount)
//    TextView rightsideDamagesCount;
//    @Bind(R.id.leftSideDamagesCount)
//    TextView leftSideDamagesCount;
//    @Bind(R.id.additionalFirDamagesCount)
//    TextView additionalFirDamagesCount;
//    @Bind(R.id.additionalSecDamagesCount)
//    TextView additionalSecDamagesCount;
//
//
//    @Bind(R.id.radioSw)
//    CarSwitch radio;
//
//    @Bind(R.id.spareTires)
//    CarSwitch spareTires;
//
//    @Bind(R.id.manuals)
//    CarSwitch manuals;
//
//    @Bind(R.id.cargoCovers)
//    CarSwitch cargoCovers;
//
//    Repo repo;
//    int selectedVehicleId;
//    VehicleInfoRealm currentCar;
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_damages, container, false);
//        ButterKnife.bind(this, view);
//        repo = new Repo(getContext());
//        selectedVehicleId = getArguments().getInt(NewEbolFragment.SELECTED_VEHICLE_ID);
//        return view;
//    }
//
//    private void setFieldsFromDb(DamagesRealm damages) {
//        remotesCount.setText(String.valueOf(damages.getRemotes()));
//        floorMatsCount.setText(String.valueOf(damages.getFloorMats()));
//        headRestCount.setText(String.valueOf(damages.getHeadrest()));
//        keysCount.setText(String.valueOf(damages.getKeys()));
//
//        radio.setState(damages.isRadio());
//        cargoCovers.setState(damages.isCargoCovers());
//        spareTires.setState(damages.isSpareTires());
//        manuals.setState(damages.isManuals());
//        if (damages.getFront() != null
//                || damages.getAdditionalFirst() != null
//                || damages.getAdditionalSecond() != null) {
//            setCapturedImages(damages);
//        }
//
//    }
//
//    @Override
//    public MvpPresenter createPresenter() {
//        return null;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        currentCar = repo.repoVehicleInfo.getVehicleInfoById(selectedVehicleId);
//        ((NewEbolCarDamagesActivity)getActivity()).setTitle(currentCar);
//        DamagesRealm damages = currentCar.getDamages();
//        if (damages != null) {
//            setFieldsFromDb(damages);
//        } else {
//            DamagesRealm damagesTemp = new DamagesRealm();
//            repo.repoDamages.createDamages(damagesTemp);
//            currentCar.setDamages(damagesTemp);
//            repo.repoVehicleInfo.updateVehicleInfo(currentCar);
//        }
//    }
//
//    private void setCapturedImages(DamagesRealm damages) {
//        Picasso picasso = Picasso.with(getContext());
//        File file;
//        if (damages.getFront() != null) {
//            file = new File(damages.getFront().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(front);
//            front.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int frontDamage = damages.getFront().getMarkedDamages().size();
//            frontDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, frontDamage, frontDamage));
//        }
//        if (damages.getBack() != null) {
//            file = new File(damages.getBack().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(back);
//            back.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int damagesCounted = damages.getBack().getMarkedDamages().size();
//            backDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
//        }
//        if (damages.getLeftSide() != null) {
//            file = new File(damages.getLeftSide().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(leftSide);
//            leftSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int damagesCounted = damages.getLeftSide().getMarkedDamages().size();
//            leftSideDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
//        }
//        if (damages.getRightSide() != null) {
//            file = new File(damages.getRightSide().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(rightSide);
//            rightSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int damagesCounted = damages.getRightSide().getMarkedDamages().size();
//            rightsideDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
//        }
//        if (damages.getAdditionalFirst() != null) {
//            file = new File(damages.getAdditionalFirst().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(additionalFir);
//            additionalFir.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int damagesCounted = damages.getAdditionalFirst().getMarkedDamages().size();
//            additionalFirDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
//        }
//        if (damages.getAdditionalSecond() != null) {
//            file = new File(damages.getAdditionalSecond().getPhotoPath());
//            picasso.invalidate(file);
//            picasso.load(file).fit().centerCrop().into(additionalSec);
//            additionalSec.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
//            int damagesCounted = damages.getAdditionalSecond().getMarkedDamages().size();
//            additionalSecDamagesCount.setText(getResources().getQuantityString(
//                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
//        }
//    }
//
////    @OnClick(R.id.front)
////    void frontClick(View view) {
////        Intent intent = new Intent(getActivity(), NewEbolCameraAcitvity.class);
////        intent.putExtra(PHOTO_TYPE, FRONT_PHOTO);
////
////        intent.putExtra(NewEbolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
////        startActivity(intent);
////    }
//
//    @OnClick({R.id.front, R.id.back, R.id.rightSide, R.id.leftSide, R.id.additionalFir, R.id.additionalSec})
//    void imageClick(View view) {
//        String phototype;
//        String action;
//        switch (view.getId()){
//            case R.id.front:
//                phototype = FRONT_PHOTO;
//                if(currentCar.getDamages().getFront() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            case R.id.back:
//                if(!hasPhoto(currentCar.getDamages().getFront())) return;
//                phototype = BACK_PHOTO;
//                if(currentCar.getDamages().getBack() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            case R.id.rightSide:
//                if(!hasPhoto(currentCar.getDamages().getBack())) return;
//                phototype = RIGHT_SIDE_PHOTO;
//                if(currentCar.getDamages().getRightSide() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            case R.id.leftSide:
//                if(!hasPhoto(currentCar.getDamages().getRightSide())) return;
//                phototype = LEFT_SIDE_PHOTO;
//                if(currentCar.getDamages().getLeftSide() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            case R.id.additionalFir:
//                phototype = ADDITIONAL_PHOTO_FIRST;
//                if(currentCar.getDamages().getAdditionalFirst() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            case R.id.additionalSec:
//                if(!hasPhoto(currentCar.getDamages().getAdditionalFirst())) return;
//                phototype = ADDITIONAL_PHOTO_SECOND;
//                if(currentCar.getDamages().getAdditionalSecond() == null){
//                    action = "create";
//                } else {
//                    action = "edit";
//                }
//                break;
//            default:
//                phototype = "";
//                action = "";
//                break;
//        }
//        Intent intent = new Intent(getActivity(), NewEbolCameraAcitvity.class);
//        intent.putExtra(PHOTO_TYPE, phototype);
//
//        intent.putExtra("action", action);
//        intent.putExtra(NewEbolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
//        startActivity(intent);
//    }
//
//    private boolean hasPhoto(CarPhotoRealm photo) {
//        return photo != null
//                && photo.getPhotoPath() != null
//                && !TextUtils.isEmpty(photo.getPhotoPath());
//    }
//
////        @OnClick({R.id.additionalFir, R.id.additionalSec})
////    void plusClick(View v) {
////        String phototype;
////        String action;
////        switch (v.getId()){
////            case R.id.additionalFir:
////                phototype = ADDITIONAL_PHOTO_FIRST;
////                if(currentCar.getDamages().getAdditionalFirst() == null){
////                    action = "create";
////                } else {
////                    action = "edit";
////                }
////                break;
////            case R.id.additionalSec:
////                phototype = ADDITIONAL_PHOTO_SECOND;
////                if(currentCar.getDamages().getAdditionalSecond() == null){
////                    action = "create";
////                } else {
////                    action = "edit";
////                }
////                break;
////            default:
////                phototype = "";
////                action = "";
////                break;
////        }
////        Intent intent = new Intent(getActivity(), NewEbolCameraAcitvity.class);
////        intent.putExtra(PHOTO_TYPE, phototype);
////        intent.putExtra("action", action);
////        intent.putExtra(NewEbolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
////        startActivity(intent);
////    }
//
//    @OnClick(R.id.save)
//    void saveClick(View view) {
//        currentCar.getDamages().setCargoCovers(cargoCovers.getState());
//        currentCar.getDamages().setSpareTires(spareTires.getState());
//        currentCar.getDamages().setManuals(manuals.getState());
//        currentCar.getDamages().setRadio(radio.getState());
//
//        int tempRemotesCount = 0;
//        if (!TextUtils.isEmpty(Utils.getTextFromEditText(remotesCount))) {
//            tempRemotesCount = Integer.valueOf(Utils.getTextFromEditText(remotesCount));
//        }
//        currentCar.getDamages().setRemotes(tempRemotesCount);
//
//        int tempKeysCount = 0;
//        if (!TextUtils.isEmpty(Utils.getTextFromEditText(keysCount))) {
//            tempKeysCount = Integer.valueOf(Utils.getTextFromEditText(keysCount));
//        }
//        currentCar.getDamages().setKeys(tempKeysCount);
//
//        int tempHeadRestCount = 0;
//        if (!TextUtils.isEmpty(Utils.getTextFromEditText(headRestCount))) {
//            tempHeadRestCount = Integer.valueOf(Utils.getTextFromEditText(headRestCount));
//        }
//        currentCar.getDamages().setHeadrest(tempHeadRestCount);
//
//        int tempFloorMatsCount = 0;
//        if (!TextUtils.isEmpty(Utils.getTextFromEditText(floorMatsCount))) {
//            tempFloorMatsCount = Integer.valueOf(Utils.getTextFromEditText(floorMatsCount));
//        }
//        currentCar.getDamages().setFloorMats(tempFloorMatsCount);
//
//        repo.repoDamages.updateDamages(currentCar.getDamages());
//
//        getActivity().onBackPressed();
//    }
}
