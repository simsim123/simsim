package com.github.andrdev.bolservice.notifications;

import com.github.andrdev.bolservice.model.Notification;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;


public interface NotificationsEditView extends MvpView {
    void setData(List<Notification> data);

    void showFailedToast();

    void getData();
}
