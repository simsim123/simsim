package com.github.andrdev.bolservice.current.originEbol.vehicleInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginVehicleInfoView extends MvpView {
    void setVehicleInfo(VehicleInfo info);

    Context getViewContext();
}
