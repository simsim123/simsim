package com.github.andrdev.bolservice.view.customer;

import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.github.andrdev.bolservice.dialogs.AddressAutocompleteDialogFragment;
import com.github.andrdev.bolservice.dialogs.DialogFactory;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.dialogs.SelectClientDialogFrament;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
import com.google.android.gms.location.places.Place;
import com.hannesdorfmann.mosby.mvp.layout.MvpRelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerNewEbolWidget
        extends MvpRelativeLayout<CustomerNewEbolView, CustomerNewEbolPresenter>
        implements CustomerNewEbolView {

    @Bind(R.id.customerName)
    protected EditText customerName;

    @Bind(R.id.address)
    protected EditText address;

    @Bind(R.id.city)
    protected EditText city;

    @Bind(R.id.zip)
    protected EditText zip;

    @Bind(R.id.state)
    protected EditText state;

    @Bind(R.id.phone)
    protected EditText phone;

    @Bind(R.id.cell)
    protected EditText cell;

    FragmentManager fragmentManager;

    public CustomerNewEbolWidget(Context context) {
        super(context);
        init(context);
    }

    public CustomerNewEbolWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomerNewEbolWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CustomerNewEbolWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_customer_a, this);
        ButterKnife.bind(this, view);
    }

//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        getPresenter().getData();
//    }

    @Override
    public CustomerNewEbolPresenter createPresenter() {
        return new CustomerNewEbolPresenterImpl();
    }


    @OnClick(R.id.locationButton)
    void locationButtonClick() {
        getPresenter().getAddress();
    }

    @OnClick(R.id.customerNameButton)
    void customerNameButtonClick() {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
        newFragment.setCallback((client)->{
                customerName.setText(client.getCustomerName());
                address.setText(client.getAddress());
                city.setText(client.getCity());
                state.setText(client.getState());
                zip.setText(client.getZIP());
                phone.setText(client.getPhone());
        });
        newFragment.show(ft, "dialog");
    }

    @Override
    public void setAddressFromPrediction(Address fullAddress) {
        address.setText(fullAddress.getThoroughfare());
        city.setText(fullAddress.getAdminArea());
        state.setText(fullAddress.getLocality());
        zip.setText(fullAddress.getPostalCode());
    }

    @Override
    public void buildAlertMessageNoGps() {
        DialogInterface.OnClickListener clickListener = (dialog, which)
                -> selectAddressFromGooglePlaces();
        DialogFactory.createAskToTurnOnGpsDialog(getContext(), clickListener).show();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void selectAddressFromGooglePlaces() {
        AddressAutocompleteDialogFragment.SelectedCallback selectedCallback =
                this::setAddressFromPrediction;
        DialogFactory.createSelectFromPlacesDialog(getContext(), selectedCallback)
                .show(fragmentManager, "addressPicker");
    }

    private void setAddressFromPlaces(Place place) {
//        address.setText(place.get);
//        city.setText(place.getAdminArea());
//        state.setText(place.getLocality());
//        zip.setText(place.getPostalCode());
    }

    @Override
    public Customer getCustomer(Customer customer) {
        if (customer == null) {
            customer = new Customer();
        }
        customer.setCustomerName(Utils.getTextFromEditText(customerName));
        customer.setAddress(Utils.getTextFromEditText(address));
        customer.setCity(Utils.getTextFromEditText(city));
        customer.setState(Utils.getTextFromEditText(state));
        customer.setZip(Utils.getTextFromEditText(zip));
        customer.setPhone(Utils.getTextFromEditText(phone));
        customer.setCell(Utils.getTextFromEditText(cell));
        return customer;
    }

    @Override
    public void setCustomerFields(Customer customer) {
        customerName.setText(customer.getCustomerName());
        address.setText(customer.getAddress());
        city.setText(customer.getCity());
        state.setText(customer.getState());
        zip.setText(customer.getZip());
        phone.setText(customer.getPhone());
        cell.setText(customer.getCell());
    }

    @Override
    public void showPlacesOrGpsDialog() {
        DialogFactory.createAskGpsLocationOrPlacesDialog(getContext(),
                this::getAddressFromGps,
                this::selectAddressFromGooglePlaces).show();
    }

    private void getAddressFromGps(DialogInterface dialogInterface, int i) {
        getPresenter().getAddressFromGps();
    }

    private void selectAddressFromGooglePlaces(DialogInterface dialogInterface, int i) {
        selectAddressFromGooglePlaces();
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
}
