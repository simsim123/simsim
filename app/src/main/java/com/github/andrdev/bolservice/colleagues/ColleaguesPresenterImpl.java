package com.github.andrdev.bolservice.colleagues;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;


public class ColleaguesPresenterImpl extends ColleaguesPresenter {

    ColleaguesDataProvider dataProvider;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ColleaguesDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getColleagues(this::requestSuccess, this::requestFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showRequestFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestSuccess(ColleaguesResponse colleaguesResponse) {
        if (!isViewAttached()) {
            return;
        }

        if (colleaguesResponse.getStatus().equals("true")
                && colleaguesResponse.getColleagues() != null && !colleaguesResponse.getColleagues().isEmpty()) {
            getView().setData(colleaguesResponse.getColleagues());
        } else {
            getView().showNoDataText();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void listItemClicked(Colleague colleague) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.saveColleague(colleague,
                this::colleagueSaved);
    }

    @SuppressWarnings("ConstantConditions")
    public void colleagueSaved(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openListItem();
    }
}
