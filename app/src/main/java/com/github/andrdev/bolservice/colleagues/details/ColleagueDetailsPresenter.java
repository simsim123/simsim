package com.github.andrdev.bolservice.colleagues.details;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ColleagueDetailsPresenter extends MvpBasePresenter<ColleagueDetailsView> {

    public abstract void getData();
}
