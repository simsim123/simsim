package com.github.andrdev.bolservice.helpcenter;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.model.HelpCenter;

import butterknife.Bind;


public class HelpCenterAdapter extends BaseAdapter<HelpCenter, HelpCenterAdapter.HelpCenterHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_helpcenter;
    }

    @Override
    protected HelpCenterHolder getHolder(View view) {
        return new HelpCenterHolder(view);
    }

    protected class HelpCenterHolder extends BaseAdapter.BaseViewHolder<HelpCenter> {

        @Bind(R.id.itemText)
        TextView itemText;

        public HelpCenterHolder(View itemView) {
            super(itemView);
        }

        protected void bind(HelpCenter helpCenter) {
            itemText.setText(helpCenter.getTitle());
        }
    }
}
