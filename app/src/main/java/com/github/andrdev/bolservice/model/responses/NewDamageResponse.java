package com.github.andrdev.bolservice.model.responses;


public class NewDamageResponse {

    String status;
    String msg;
    DamagesG damage;

    public NewDamageResponse() {
    }

    public DamagesG getDamage() {
        return damage;
    }

    public void setDamage(DamagesG damage) {
        this.damage = damage;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
