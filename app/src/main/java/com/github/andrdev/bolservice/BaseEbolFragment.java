package com.github.andrdev.bolservice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;


public abstract class BaseEbolFragment<V extends MvpView, P extends MvpPresenter<V>,
        VT extends TextView> extends MvpFragment<V, P> {

//    public static final String SELECTED_VEHICLE_ID = "selectedVehicleId";
    public static final String TEMP = "temp";
    public static final String TYPE = "type";

    @Bind(R.id.originOrderId)
    protected VT originOrderId;

    @Bind(R.id.originDate)
    protected TextView originDate;

//    @Bind(R.id.originCustomerName)
//    protected VT originCustomerName;
//
//    @Bind(R.id.originAddress)
//    protected VT originAddress;
//
//    @Bind(R.id.originCity)
//    protected VT originCity;
//
//    @Bind(R.id.originZip)
//    protected VT originZip;
//
//    @Bind(R.id.originState)
//    protected VT originState;
//
//    @Bind(R.id.originPhone)
//    protected VT originPhone;
//
//    @Bind(R.id.originCell)
//    protected VT originCell;
//
//    @Bind(R.id.destinationCustomerName)
//    protected VT destinationCustomerName;
//
//    @Bind(R.id.destinationAddress)
//    protected VT destinationAddress;
//
//    @Bind(R.id.destinationCity)
//    protected VT destinationCity;
//
//    @Bind(R.id.destinationZip)
//    protected VT destinationZip;
//
//    @Bind(R.id.destinationState)
//    protected VT destinationState;
//
//    @Bind(R.id.destinationPhone)
//    protected VT destinationPhone;
//
//    @Bind(R.id.destinationCell)
//    protected VT destinationCell;

    @Bind(R.id.vehiclesHost)
    LinearLayout vehicleHost;

    protected NewEbol currentEbol;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    protected abstract int getLayoutResource();

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    protected abstract void getData();

    protected void showData() {
        if (currentEbol == null) {
            return;
        }
        Log.d("dreesd", currentEbol.toString());
        vehicleHost.removeAllViews();
        setCommonFields(currentEbol);
        if(currentEbol.getDestination() != null) {
            setDestinationCustomerFields(currentEbol.getDestination());
        }
        if(currentEbol.getOrigin() != null) {
            setOriginCustomerFields(currentEbol.getOrigin());
        }
        drawVehicles(currentEbol);
        setSignImages(currentEbol);
    }

    protected abstract void setSignImages(NewEbol ebol);

    private void drawVehicles(NewEbol ebol) {
        Log.d("dree", "sz" + ebol.getVehicleInfos().size());
        for (final VehicleInfo vehInfo : ebol.getVehicleInfos()) {
            View damagedVehicle = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_damaged_car, null);

            setVehicleClickers(vehInfo, damagedVehicle);
            setVehicleText(vehInfo, damagedVehicle);

            vehicleHost.addView(damagedVehicle);

            LinearLayout.LayoutParams params =(LinearLayout.LayoutParams) damagedVehicle.getLayoutParams();
            params.bottomMargin = 8;
        }
    }

    private void setVehicleText(VehicleInfo vehInfo, View damagedVehicle) {
        TextView vehicleInfo = (TextView) damagedVehicle.findViewById(R.id.carInfo);
        TextView vin = (TextView) damagedVehicle.findViewById(R.id.vin);
        TextView damagesCount = (TextView) damagedVehicle.findViewById(R.id.damagesCount);

        vehicleInfo.setText(String.format(
                "%s %s %s", vehInfo.getYearOfMake(), vehInfo.getMake(), vehInfo.getModel()));
        vin.setText(vehInfo.getVinNumber());
        Damages vehDamages =  vehInfo.getDamages();
        int markedDamagesCount = 0;
        if(vehDamages != null) {
            markedDamagesCount = getMarkedDamagesCount(vehDamages, markedDamagesCount);
        }

        damagesCount.setText(getResources().getQuantityString(
                R.plurals.damagesMarked, markedDamagesCount, markedDamagesCount));
    }

    private void setVehicleClickers(final VehicleInfo vehInfo, View damagedVehicle) {
        damagedVehicle.findViewById(R.id.damages)
                .setOnClickListener(v -> damagesClick(vehInfo));
        damagedVehicle.findViewById(R.id.details)
                .setOnClickListener(v -> detailsClick(vehInfo.getId()));
    }

    private int getMarkedDamagesCount(Damages vehDamages, int markedDamagesCount) {
        if(vehDamages.getFront()!=null) {
            markedDamagesCount += vehDamages.getFront().getMarkedDamages().size();
        }
        if(vehDamages.getBack()!=null) {
            markedDamagesCount += vehDamages.getBack().getMarkedDamages().size();
        }
        if(vehDamages.getRightSide()!=null) {
            markedDamagesCount += vehDamages.getRightSide().getMarkedDamages().size();
        }
        if(vehDamages.getLeftSide()!=null) {
            markedDamagesCount += vehDamages.getLeftSide().getMarkedDamages().size();
        }
        if(vehDamages.getAdditionalFirst()!=null) {
            markedDamagesCount += vehDamages.getAdditionalFirst().getMarkedDamages().size();
        }
        if(vehDamages.getAdditionalSecond()!=null) {
            markedDamagesCount += vehDamages.getAdditionalSecond().getMarkedDamages().size();
        }
        return markedDamagesCount;
    }

    private void setCommonFields(NewEbol ebol) {
        originOrderId.setText(ebol.getOrderId());
        originDate.setText(ebol.getDate());
//        try {
//            Calendar cal=Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//            Date d = sdf.parse(ebol.getDate());
//            cal.setTime(d);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }

    protected abstract void detailsClick(int vehicleId);

    protected abstract void damagesClick(VehicleInfo vehInfo);


    public abstract void setOriginCustomerFields(Customer origin);
//        originCustomerName.setText(origin.getCustomerName());
//        originAddress.setText(origin.getAddress());
//        originCity.setText(origin.getCity());
//        originState.setText(origin.getState());
//        originZip.setText(origin.getZip());
//        originPhone.setText(origin.getPhone());
//        originCell.setText(origin.getCell());
//    }

    public abstract void setDestinationCustomerFields(Customer destination);
//        destinationCustomerName.setText(destination.getCustomerName());
//        destinationAddress.setText(destination.getAddress());
//        destinationCity.setText(destination.getCity());
//        destinationState.setText(destination.getState());
//        destinationZip.setText(destination.getZip());
//        destinationPhone.setText(destination.getPhone());
//        destinationCell.setText(destination.getCell());
//    }
}