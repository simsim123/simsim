package com.github.andrdev.bolservice.messages;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.ConversationSmall;
import com.github.andrdev.bolservice.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;


public class MessagesAdapter extends BaseAdapter<ConversationSmall, MessagesAdapter.MessagesHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_messages;
    }

    @Override
    protected MessagesHolder getHolder(View view) {
        return new MessagesHolder(view);
    }

    protected static class MessagesHolder extends BaseAdapter.BaseViewHolder<ConversationSmall> {

        @Bind(R.id.newMessagesSign)
        View newMessagesSign;
        @Bind(R.id.bossText)
        TextView bossText;
        @Bind(R.id.timeLine)
        TextView timeLine;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.name)
        TextView author;
        @Bind(R.id.messageText)
        TextView messageText;

        public MessagesHolder(View itemView) {
            super(itemView);
        }

        protected void bind(ConversationSmall conversation) {
            Message message = conversation.getLastMessage();
            timeLine.setVisibility(View.INVISIBLE);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yy, HH:mm a");
                Date d = sdf.parse(message.getCreatedAt());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            author.setText(conversation.getInterlocutorFullname());
            messageText.setText(message.getMessageText());
            if (conversation.getLastMessage().getStatus().equals("new")) {
                newMessagesSign.setVisibility(View.VISIBLE);
            } else {
                newMessagesSign.setVisibility(View.INVISIBLE);
            }
        }
    }
}
