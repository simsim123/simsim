package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;

public class InvoiceRequestBuilder {
    private String address;
    private String amount;
    private String cell;
    private String city;
    private String fullName;
    private String invoiceId;
    private String notes;
    private String orderId;
    private String paymentTerms;
    private String phone;
    private String state;
    private String vCount;
    private String zip;

    public InvoiceRequestBuilder setAddress(EditText address) {
        if(Utils.hasText(address)){
            this.address = Utils.getTextFromEditText(address);
        }
        return this;
    }

    public InvoiceRequestBuilder setAmount(EditText amount) {
        if(Utils.hasText(amount)){
            this.amount = Utils.getTextFromEditText(amount);
        }
        return this;
    }

    public InvoiceRequestBuilder setCell(EditText cell) {
        if(Utils.hasText(cell)){
            this.cell = Utils.getTextFromEditText(cell);
        }
        return this;
    }

    public InvoiceRequestBuilder setCity(EditText city) {
        if(Utils.hasText(city)){
            this.city = Utils.getTextFromEditText(city);
        }
        return this;
    }

    public InvoiceRequestBuilder setFullName(EditText fullName) {
        if(Utils.hasText(fullName)){
            this.fullName = Utils.getTextFromEditText(fullName);
        }
        return this;
    }

    public InvoiceRequestBuilder setInvoiceId(EditText invoiceId) {
        if(Utils.hasText(invoiceId)){
            this.invoiceId = Utils.getTextFromEditText(invoiceId);
        }
        return this;
    }

    public InvoiceRequestBuilder setNotes(EditText notes) {
        if(Utils.hasText(notes)){
            this.notes = Utils.getTextFromEditText(notes);
        }
        return this;
    }

    public InvoiceRequestBuilder setOrderId(EditText orderId) {
        if(Utils.hasText(orderId)){
            this.orderId = Utils.getTextFromEditText(orderId);
        }
        return this;
    }

    public InvoiceRequestBuilder setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
        return this;
    }

    public InvoiceRequestBuilder setPhone(EditText phone) {
        if(Utils.hasText(phone)){
            this.phone = Utils.getTextFromEditText(phone);
        }
        return this;
    }

    public InvoiceRequestBuilder setState(EditText state) {
        if(Utils.hasText(state)){
            this.state = Utils.getTextFromEditText(state);
        }
        return this;
    }

    public InvoiceRequestBuilder setvCount(EditText vCount) {
        if(Utils.hasText(vCount)){
            this.vCount = Utils.getTextFromEditText(vCount);
        }
        return this;
    }

    public InvoiceRequestBuilder setZip(EditText zip) {
        if(Utils.hasText(zip)){
            this.zip = Utils.getTextFromEditText(zip);
        }
        return this;
    }

    public InvoiceRequest createInvoiceRequest() {
        return new InvoiceRequest(address, amount, cell, city, fullName, invoiceId, notes, orderId, paymentTerms, phone, state, vCount, zip);
    }
}