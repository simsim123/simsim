package com.github.andrdev.bolservice.notifications;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.model.Notification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;


public class NotificationsEditAdapter extends
        BaseAdapter<Notification, NotificationsEditAdapter.NotificationsHolder> {

    boolean[] selectedElems;

    @Override
    protected int getLayoutResource(int viewType) {
        if(viewType==1) {
            return R.layout.list_item_notification_pickup_edit;
        } else {
            return R.layout.list_item_notification_delivery_edit;
        }
    }

    @Override
    protected NotificationsHolder getHolder(View view) {
        return new NotificationsHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationsHolder messagesHolder, int i) {
        messagesHolder.bind(data.get(i), i);
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getAddressType().equals("pendingPickup")?1:2;
    }

    @Override
    public void setData(List<Notification> notifications) {
        super.setData(notifications);
        selectedElems = new boolean[notifications.size()];
    }

    public void flipSelector(int position) {
        selectedElems[position] = !selectedElems[position];
        notifyDataSetChanged();
    }

    public List<Long> getSelected() {
        List<Long> tempSelected = new ArrayList<>();
        for (int i = 0; i < selectedElems.length; i++) {
            if(selectedElems[i]) {
                tempSelected.add(data.get(i).getId());
            }
        }
        return tempSelected;
    }

    protected class NotificationsHolder extends BaseAdapter.BaseViewHolder<Notification> {

        @Bind(R.id.orderId)
        TextView orderId;
        @Bind(R.id.timeLine)
        TextView timeLine;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.newSign)
        TextView newSign;
        @Bind(R.id.customerName)
        TextView customerName;
        @Bind(R.id.address)
        TextView address;
        @Bind(R.id.vehiclesCount)
        TextView vehiclesCount;
        @Bind(R.id.selected)
        View selected;
        @Bind(R.id.notSelected)
        View notSelected;

        public NotificationsHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(Notification item) {
            timeLine.setVisibility(View.INVISIBLE);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yy, HH:mm a");
                Date d = sdf.parse(item.getItemDate());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            orderId.setText(String.valueOf(item.getSubmitOrderId()));
            customerName.setText(item.getItemName());
            address.setText(item.getSubmitAddress());
            vehiclesCount.setText(item.getItemString());
            if (item.getStatus().equals("new")) {
                newSign.setVisibility(View.VISIBLE);
            } else {
                newSign.setVisibility(View.INVISIBLE);
            }

        }

        protected void bind(Notification notification, int position) {
            bind(notification);
            if (selectedElems[position]) {
                selected.setVisibility(View.VISIBLE);
                notSelected.setVisibility(View.INVISIBLE);
            } else {
                selected.setVisibility(View.INVISIBLE);
                notSelected.setVisibility(View.VISIBLE);
            }
        }
    }
}