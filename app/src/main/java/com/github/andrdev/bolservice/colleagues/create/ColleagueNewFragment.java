package com.github.andrdev.bolservice.colleagues.create;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.requests.CreateDriverRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ColleagueNewFragment extends MvpFragment<ColleagueNewView, ColleagueNewPresenter>
        implements ColleagueNewView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @BindString(R.string.wrongEmail)
    String wrongEmail;

    @BindString(R.string.passwordsNotIdentical)
    String passwordsNotIdentical;

    @Bind(R.id.firstName)
    EditText firstName;

    @Bind(R.id.lastName)
    EditText lastName;

    @Bind(R.id.email)
    EditText email;

    @Bind(R.id.phoneNumber)
    EditText phoneNumber;

    @Bind(R.id.username)
    EditText username;

    @Bind(R.id.password)
    EditText password;

    @Bind(R.id.repeatPassword)
    EditText repeatPassword;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colleague_new, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        if(!enteredInfoIsOk()) return;

        CreateDriverRequest request = new CreateDriverRequestBuilder()
                .setEmail(email)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPassword(password)
                .setPhoneNumber(phoneNumber)
                .setUserName(username).createCreateDriverRequest();

        getPresenter().createDriver(request);
    }

    private boolean enteredInfoIsOk() {
        return Utils.hasText(errorText, email, firstName, lastName, password, username, phoneNumber)
                && Utils.isValidEmail(wrongEmail, email)
                && Utils.isCorrectRepeatPassword(passwordsNotIdentical, password, repeatPassword);
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void backToColleagues() {

    }
    @Override
    public void showFailedToast() {
    }

    @Override
    public ColleagueNewPresenter createPresenter() {
        return new ColleagueNewPresenterImpl();
    }
}
