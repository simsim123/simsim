package com.github.andrdev.bolservice.current;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.pendingDelivery.PendingDeliveryActivity;
import com.github.andrdev.bolservice.current.pendingPickup.PendingPickupActivity;
import com.github.andrdev.bolservice.database.RealmDbData;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class CurrentEbolsActivity extends AppCompatActivity {

    static final String TAG_PENDING_PICKUP = "tagPendingPickup";
    static final String TAG_PENDING_DELIVERY = "tagPendingDelivery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_ebols);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showEbolChooseFragment();
        }
    }

    private void showEbolChooseFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new EbolChooseFragment()).commit();
    }

    public void showtPendingDeliveryFragment() {
        Intent intent = new Intent(this, PendingDeliveryActivity.class);
        startActivity(intent);
    }

    public void showPendingPickupFragment() {
        Intent intent = new Intent(this, PendingPickupActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        onBackPressed();
    }
}