package com.github.andrdev.bolservice.history.details.damages;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class HistoryDamagesDataProvider implements DataProvider {

    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getVehicleInfoById(int selectedVehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(selectedVehicleId, callback::returnData);
    }
}
