package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Submit;

import java.util.List;


public class SubmitsResponse {
    String status;
    String mode;
    List<Submit> submits;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Submit> getSubmits() {
        return submits;
    }

    public void setSubmits(List<Submit> submits) {
        this.submits = submits;
    }

    public String getMode() {

        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
