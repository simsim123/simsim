package com.github.andrdev.bolservice.profile.driverInfo;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ProfileDriverInfoDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }


    public void getSavedProfile(DataCb<Profile> successCb) {
        dbHelper.getSavedProfile(successCb::returnData);
    }

    public void saveProfile(SaveProfileRequest profileRequest,
                            DataCb<SaveProfileResponse> successCb,
                            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().saveProfile(profileRequest,
                successCb::returnData,
                failedCb::returnData);
    }
}
