package com.github.andrdev.bolservice.colleagues.edit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ColleagueEditFragment extends MvpFragment<ColleagueEditView, ColleagueEditPresenter>
        implements ColleagueEditView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @BindString(R.string.wrongEmail)
    String wrongEmail;

    @Bind(R.id.firstName)
    EditText firstName;

    @Bind(R.id.lastName)
    EditText lastName;

    @Bind(R.id.email)
    EditText email;

    @Bind(R.id.phoneNumber)
    EditText phoneNumber;

    @Bind(R.id.username)
    TextView username;

    @Bind(R.id.titleTool)
    TextView titleTool;

    Colleague colleague;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colleague_edit, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public ColleagueEditPresenter createPresenter() {
        return new ColleagueEditPresenterImpl();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void initFields() {
        firstName.setText(colleague.getFirstname());
        lastName.setText(colleague.getLastname());
        email.setText(colleague.getDriverEmail());
        phoneNumber.setText(colleague.getDriverPhone());
        username.setText(colleague.getUsername());
        titleTool.setText(colleague.getFullNameFormatted());
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        if(!enteredInfoIsOk()) return;

        DriverUpdateRequest request = new DriverUpdateRequestBuilder()
                .setDid(String.valueOf(colleague.getId()))
                .setDriverEmail(email)
                .setDriverPhone(phoneNumber)
                .setFirstName(firstName)
                .setLastName(lastName)
                .createDriverUpdateRequest();

        getPresenter().updateColleague(request);
    }

    @OnClick(R.id.delete)
    void deleteClick(View view) {
        getPresenter().deleteItem(colleague);
    }

    private boolean enteredInfoIsOk() {
        return Utils.hasText(errorText, firstName, lastName, email, phoneNumber)
                && Utils.isValidEmail(wrongEmail, email);
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void setData(Colleague colleague) {
        this.colleague = colleague;
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToColleagueDetails() {
        getActivity().onBackPressed();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
