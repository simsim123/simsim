package com.github.andrdev.bolservice.current.originEbol;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;


public class OriginEbolAcitivty extends AppCompatActivity {

    public static final String EBOL_ID = "ebolId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            showNewEbolFragment();
        }
    }

    void showNewEbolFragment() {
        OriginFEbolFragment fragment = new OriginFEbolFragment();
        Bundle bundle = new Bundle();
        if(getIntent().hasExtra(EBOL_ID)) {
            bundle.putInt(EBOL_ID, getIntent().getIntExtra(EBOL_ID, 0));
        }
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();
    }
}
