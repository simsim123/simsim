package com.github.andrdev.bolservice.colleagues;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ColleaguesDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getColleagues(DataCb<ColleaguesResponse> successCb, DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getColleagues(successCb::returnData, failedCb::returnData);
    }

    public void saveColleague(Colleague colleague, DataCb<Boolean> successCb) {
        dbHelper.saveColleague(colleague, successCb::returnData);
    }
}
