//package com.github.andrdev.bolservice.history;
//
//import android.os.Bundle;
//
//import com.github.andrdev.bolservice.NucleusActivity;
//import com.github.andrdev.bolservice.R;
//
//
//public class HistoryEbolAcitivty extends NucleusActivity {
//
//    public static final String EBOL_ID = "ebolId";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        if (savedInstanceState == null) {
//            showNewEbolFragment();
//        }
//    }
//
//    void showNewEbolFragment() {
//        HistoryFEbolFragment fragment = new HistoryFEbolFragment();
//        Bundle bundle = new Bundle();
//        if(getIntent().hasExtra(EBOL_ID)) {
//            bundle.putInt(EBOL_ID, getIntent().getIntExtra(EBOL_ID, 0));
//        }
//        fragment.setArguments(bundle);
//        getSupportFragmentManager().beginTransaction()
//                .add(android.R.id.content, fragment).commit();
//    }
//}
