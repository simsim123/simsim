package com.github.andrdev.bolservice.database;

import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

import java.util.List;

public interface DbHelper {

    RealmDbData getRealmDbData();

    void setRealmDbData(RealmDbData realmDbData);

    void saveProfile(Profile profile, DataCallback<Boolean> callback);

    void getSavedProfile(DataCallback<Profile> callback);

    void getIsUserBoss(DataCallback<Boolean> callback);

    void saveClient(Client client, DataCallback<Boolean> callback);

    void getSavedClient(DataCallback<Client> callback);

    void saveColleague(Colleague colleague, DataCallback<Boolean> callback);

    void getSavedColleague(DataCallback<Colleague> callback);

    void saveInvoice(Invoice invoice, DataCallback<Boolean> callback);

    void getSavedInvoice(DataCallback<Invoice> callback);

    void updateOrSaveEbol(NewEbol ebol, DataCallback<Boolean> callback);

    void updateSaveEbol(NewEbol ebol, DataCallback<Boolean> callback);

    void getEbol(DataCallback<NewEbol> callback);

    void getOrCreateEbol(DataCallback<NewEbol> callback);

    void getTempEbol(DataCallback<NewEbol> callback);

    void clearVehicleInfoTable(DataCallback<Boolean> callback);

    void clearTempVehicleInfo(DataCallback<Boolean> callback);

    void saveTempVehicleOrUpdate(VehicleInfo info, DataCallback<Boolean> callback);

    void updateVehicle(VehicleInfo info, DataCallback<Boolean> callback);

    void getTempVehicle(DataCallback<VehicleInfo> callback);

    void getVehicleById(int vehicleId, DataCallback<VehicleInfo> callback);

    void updateDamages(Damages damages, int vehicleId, DataCallback<Boolean> callback);

    void deInit();

    void deleteVehiclePhotoTempAndSave(VehiclePhoto photoToDelete, VehiclePhoto tempPhoto,
                                       DataCallback<Boolean> callback);

    void updateVehiclePhoto(VehiclePhoto vehiclePhoto, DataCallback<Boolean> callback);

    void deleteTempVehiclePhoto(DataCallback<Boolean> callback);

    void deleteVehiclePhotoById(int photoId, DataCallback<Boolean> callback);

    void saveVehiclePhoto(VehiclePhoto tempPhoto, int vehicleId, DataCallback<Boolean> callback);

    void deleteTempEbol(DataCallback<Boolean> callback);

    void deleteNetTempEbols(DataCallback<Boolean> callback);

    void getDbEbols(DataCallback<List<NewEbol>> returnData);

    void deleteNetSaveLocalEbol();

    interface SaveCallback{
        void requestSuccess();
    }

    interface FailedCallback{
        void requestFailed();
    }

    interface DataCallback<T>{
        void requestSuccess(T data);
    }
}
