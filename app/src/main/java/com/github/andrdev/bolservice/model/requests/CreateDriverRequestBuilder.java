package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;

public class CreateDriverRequestBuilder {
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String phoneNumber;
    private String userName;

    public CreateDriverRequestBuilder setEmail(EditText email) {
        if(Utils.hasText(email)){
            this.email = Utils.getTextFromEditText(email);
        }
        return this;
    }

    public CreateDriverRequestBuilder setFirstName(EditText firstName) {
        if(Utils.hasText(firstName)){
            this.firstName = Utils.getTextFromEditText(firstName);
        }
        return this;
    }

    public CreateDriverRequestBuilder setLastName(EditText lastName) {
        if(Utils.hasText(lastName)){
            this.lastName = Utils.getTextFromEditText(lastName);
        }
        return this;
    }

    public CreateDriverRequestBuilder setPassword(EditText password) {
        if(Utils.hasText(password)){
            this.password = Utils.getTextFromEditText(password);
        }
        return this;
    }

    public CreateDriverRequestBuilder setPhoneNumber(EditText phoneNumber) {
        if(Utils.hasText(phoneNumber)){
            this.phoneNumber = Utils.getTextFromEditText(phoneNumber);
        }
        return this;
    }

    public CreateDriverRequestBuilder setUserName(EditText userName) {
        if(Utils.hasText(userName)){
            this.userName = Utils.getTextFromEditText(userName);
        }
        return this;
    }

    public CreateDriverRequest createCreateDriverRequest() {
        return new CreateDriverRequest(email, firstName, lastName, password, phoneNumber, userName);
    }
}