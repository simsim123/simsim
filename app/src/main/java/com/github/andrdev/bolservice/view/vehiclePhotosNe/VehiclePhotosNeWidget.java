package com.github.andrdev.bolservice.view.vehiclePhotosNe;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.MvpPercentRelativeLayout;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.NewEbolCameraAcitvity;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehiclePhotosNeWidget extends MvpPercentRelativeLayout<VehiclePhotosNeView, VehiclePhotosNePresenter>
        implements VehiclePhotosNeView {

    public static final String PHOTO_TYPE = "photoType";
    public static final String FRONT_PHOTO = "frontPhoto";
    public static final String BACK_PHOTO = "backPhoto";
    public static final String LEFT_SIDE_PHOTO = "leftSidePhoto";
    public static final String RIGHT_SIDE_PHOTO = "rightSidePhoto";

    @Bind(R.id.front)
    ImageView front;
    @Bind(R.id.back)
    ImageView back;
    @Bind(R.id.leftSide)
    ImageView leftSide;
    @Bind(R.id.rightSide)
    ImageView rightSide;

    @Bind(R.id.frontDamagesCount)
    TextView frontDamagesCount;
    @Bind(R.id.backDamagesCount)
    TextView backDamagesCount;
    @Bind(R.id.rightsideDamagesCount)
    TextView rightSideDamagesCount;
    @Bind(R.id.leftSideDamagesCount)
    TextView leftSideDamagesCount;

    int selectedVehicleId;
    Damages damages;

    Class openOnClick;

    public VehiclePhotosNeWidget(Context context) {
        super(context);
        init(context);
    }

    public VehiclePhotosNeWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VehiclePhotosNeWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_car_photos, this);
        ButterKnife.bind(this, view);
    }

    @Override
    public VehiclePhotosNePresenter createPresenter() {
        return new VehiclePhotosNePresenter();
    }

    public void setFieldsFromDb(Damages damages) {
        this.damages = damages;
        if (damages.getFront() != null) {
            setCapturedImages(damages);
        }
    }

    public void setSelectedVehicleId(int selectedVehicleId) {
        this.selectedVehicleId = selectedVehicleId;
    }

    public Class getOpenOnClick() {
        return openOnClick;
    }

    public void setOpenOnClick(Class openOnClick) {
        this.openOnClick = openOnClick;
    }

    private void setCapturedImages(Damages damages) {
        Picasso picasso = Picasso.with(getContext());
        String photoPath;
        if (damages.getFront() != null) {
            photoPath = damages.getFront().getPhotoPath();
            picasso.invalidate(photoPath);
            picasso.load(photoPath).fit().into(front);
            front.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int frontDamage = damages.getFront().getMarkedDamages().size();
            frontDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, frontDamage, frontDamage));
        }
        if (damages.getBack() != null) {
            photoPath = damages.getBack().getPhotoPath();
            picasso.invalidate(photoPath);
            picasso.load(photoPath).fit().into(back);
            back.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getBack().getMarkedDamages().size();
            backDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
        if (damages.getLeftSide() != null) {
            photoPath = damages.getLeftSide().getPhotoPath();
            picasso.invalidate(photoPath);
            picasso.load(photoPath).fit().into(leftSide);
            leftSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getLeftSide().getMarkedDamages().size();
            leftSideDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
        if (damages.getRightSide() != null) {
            photoPath = damages.getRightSide().getPhotoPath();
            picasso.invalidate(photoPath);
            picasso.load(photoPath).fit().into(rightSide);
            rightSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getRightSide().getMarkedDamages().size();
            rightSideDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
    }


    @OnClick({R.id.front, R.id.back, R.id.rightSide, R.id.leftSide})
    void imageClick(View view) {
        String phototype;

        switch (view.getId()){
            case R.id.front:
                if(damages == null || !hasPhoto(damages.getFront())) return;
                phototype = FRONT_PHOTO;
                break;
            case R.id.back:
                if(damages == null || !hasPhoto(damages.getBack())) return;
                phototype = BACK_PHOTO;
                break;
            case R.id.rightSide:
                if(damages == null || !hasPhoto(damages.getRightSide())) return;
                phototype = RIGHT_SIDE_PHOTO;
                break;
            case R.id.leftSide:
                if(damages == null || !hasPhoto(damages.getLeftSide())) return;
                phototype = LEFT_SIDE_PHOTO;
                break;
            default:
                phototype = "";
                break;
        }
        Intent intent = new Intent(getContext(), openOnClick);
        intent.putExtra(PHOTO_TYPE, phototype);

        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        getContext().startActivity(intent);
    }

    private boolean hasPhoto(VehiclePhoto photo) {
        return photo != null
                && photo.getPhotoPath() != null
                && !TextUtils.isEmpty(photo.getPhotoPath());
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
