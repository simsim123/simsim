package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;

public class SaveProfileRequestBuilder {
    private String billingAddress;
    private String billingCity;
    private String billingEmail;
    private String billingFax;
    private String billingFirstName;
    private String billingLastName;
    private String billingPhone;
    private String billingState;
    private String billingZip;
    private String companyAddress;
    private String companyCity;
    private String companyEma;
    private String companyFax;
    private String companyMc;
    private String companyName;
    private String companyPhone;
    private String companyState;
    private String companyUsdot;
    private String companyWebsite;
    private String companyZip;
    private String driverEmail;
    private String driverPhone;
    private String firstName;
    private String lastName;
    private String userType;

    public SaveProfileRequestBuilder setBillingAddress(EditText billingAddress) {
        if(Utils.hasText(billingAddress)){
            this.billingAddress = Utils.getTextFromEditText(billingAddress);
        }        return this;
    }

    public SaveProfileRequestBuilder setBillingCity(EditText billingCity) {
        if(Utils.hasText(billingCity)){
            this.billingCity = Utils.getTextFromEditText(billingCity);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingEmail(EditText billingEmail) {
        if(Utils.hasText(billingEmail)){
            this.billingEmail = Utils.getTextFromEditText(billingEmail);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingFax(EditText billingFax) {
        if(Utils.hasText(billingFax)){
            this.billingFax = Utils.getTextFromEditText(billingFax);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingFirstName(EditText billingFirstName) {
        if(Utils.hasText(billingFirstName)){
            this.billingFirstName = Utils.getTextFromEditText(billingFirstName);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingLastName(EditText billingLastName) {
        if(Utils.hasText(billingLastName)){
            this.billingLastName = Utils.getTextFromEditText(billingLastName);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingPhone(EditText billingPhone) {
        if(Utils.hasText(billingPhone)){
            this.billingPhone = Utils.getTextFromEditText(billingPhone);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingState(EditText billingState) {
        if(Utils.hasText(billingState)){
            this.billingState = Utils.getTextFromEditText(billingState);
        }
        return this;
    }

    public SaveProfileRequestBuilder setBillingZip(EditText billingZip) {
        if(Utils.hasText(billingZip)){
            this.billingZip = Utils.getTextFromEditText(billingZip);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyAddress(EditText companyAddress) {
        if(Utils.hasText(companyAddress)){
            this.companyAddress = Utils.getTextFromEditText(companyAddress);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyCity(EditText companyCity) {
        if(Utils.hasText(companyCity)){
            this.companyCity = Utils.getTextFromEditText(companyCity);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyEma(EditText companyEma) {
        if(Utils.hasText(companyEma)){
            this.companyEma = Utils.getTextFromEditText(companyEma);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyFax(EditText companyFax) {
        if(Utils.hasText(companyFax)){
            this.companyFax = Utils.getTextFromEditText(companyFax);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyMc(EditText companyMc) {
        if(Utils.hasText(companyMc)){
            this.companyMc = Utils.getTextFromEditText(companyMc);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyName(EditText companyName) {
        if(Utils.hasText(companyName)){
            this.companyName = Utils.getTextFromEditText(companyName);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyPhone(EditText companyPhone) {
        if(Utils.hasText(companyPhone)){
            this.companyPhone = Utils.getTextFromEditText(companyPhone);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyState(EditText companyState) {
        if(Utils.hasText(companyState)){
            this.companyState = Utils.getTextFromEditText(companyState);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyUsdot(EditText companyUsdot) {
        if(Utils.hasText(companyUsdot)){
            this.companyUsdot = Utils.getTextFromEditText(companyUsdot);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyWebsite(EditText companyWebsite) {
        if(Utils.hasText(companyWebsite)){
            this.companyWebsite = Utils.getTextFromEditText(companyWebsite);
        }
        return this;
    }

    public SaveProfileRequestBuilder setCompanyZip(EditText companyZip) {
        if(Utils.hasText(companyZip)){
            this.companyZip = Utils.getTextFromEditText(companyZip);
        }
        return this;
    }

    public SaveProfileRequestBuilder setDriverEmail(EditText driverEmail) {
        if(Utils.hasText(driverEmail)){
            this.driverEmail = Utils.getTextFromEditText(driverEmail);
        }
        return this;
    }

    public SaveProfileRequestBuilder setDriverPhone(EditText driverPhone) {
        if(Utils.hasText(driverPhone)){
            this.driverPhone = Utils.getTextFromEditText(driverPhone);
        }
        return this;
    }

    public SaveProfileRequestBuilder setFirstName(EditText firstName) {
        this.firstName = Utils.getTextFromEditText(firstName);
        return this;
    }

    public SaveProfileRequestBuilder setLastName(EditText lastName) {
        if(Utils.hasText(lastName)){
            this.lastName = Utils.getTextFromEditText(lastName);
        }
        return this;
    }

    public SaveProfileRequestBuilder setLastname(String lastname) {
        this.lastName = lastname;
        return this;
    }

    public SaveProfileRequestBuilder setUserType(EditText userType) {
        if(Utils.hasText(userType)){
            this.userType = Utils.getTextFromEditText(userType);
        }
        return this;
    }

    public SaveProfileRequest createSaveProfileRequest() {
        return new SaveProfileRequest(billingAddress, billingCity, billingEmail, billingFax, billingFirstName, billingLastName, billingPhone, billingState, billingZip, companyAddress, companyCity, companyEma, companyFax, companyMc, companyName, companyPhone, companyState, companyUsdot, companyWebsite, companyZip, driverEmail, driverPhone, firstName, lastName, userType);
    }

    public SaveProfileRequestBuilder setFirstname(String firstname) {
        this.firstName = firstname;
        return this;
    }

    public SaveProfileRequestBuilder setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
        return this;
    }

    public SaveProfileRequestBuilder setBillingCity(String billingCity) {
        this.billingCity = billingCity;
        return this;

    }

    public SaveProfileRequestBuilder setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
        return this;
    }

    public SaveProfileRequestBuilder setBillingFax(String billingFax) {
        this.billingFax = billingFax;
        return this;
    }

    public SaveProfileRequestBuilder setBillingFirstname(String billingFirstname) {
        this.billingFirstName = billingFirstname;
        return this;
    }

    public SaveProfileRequestBuilder setBillingLastname(String billingLastname) {
        this.billingLastName = billingLastname;
        return this;
    }

    public SaveProfileRequestBuilder setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
        return this;
    }

    public SaveProfileRequestBuilder setBillingState(String billingState) {
        this.billingState = billingState;
        return this;
    }

    public SaveProfileRequestBuilder setBillingZip(String billingZip) {
        this.billingZip = billingZip;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyMc(String companyMc) {
        this.companyMc = companyMc;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyUsdot(String companyUsdot) {
        this.companyUsdot = companyUsdot;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyCity(String companyAddress) {
        this.companyAddress = companyAddress;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyEma(String companyEma) {
        this.companyEma = companyEma;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyPhone(String companyAddress) {
        this.companyAddress = companyAddress;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyState(String companyState) {
        this.companyState = companyState;
        return this;
    }

    public SaveProfileRequestBuilder setCompanyZip(String companyZip) {
        this.companyZip = companyZip;
        return this;
    }
}