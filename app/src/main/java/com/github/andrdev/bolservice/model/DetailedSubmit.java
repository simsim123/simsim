package com.github.andrdev.bolservice.model;


public class DetailedSubmit {

    String price;
    String paymentType;
    String cash;
    String creditCard;
    String comcheck;
    String billing;
    String originCustomerName;
    String originBuyerNumber;
    String originLotNumber;
    String originGatePass;
    String originAddress;
    String originCity;
    String originState;
    String originZip;
    String originPhone;
    String destinationCustomerName;
    String destinationBuyerNumber;
    String destinationLotNumber;
    String destinationGatePass;
    String destinationAddress;
    String destinationCity;
    String destinationState;
    String destinationZip;
    String destinationPhone;
    String pickupDate;
    String deliveryDate;
    SubmitAuthor author;
    SubmitDriver driver;

    public DetailedSubmit() {
    }

    public SubmitAuthor getAuthor() {
        return author;
    }

    public void setAuthor(SubmitAuthor author) {
        this.author = author;
    }

    public String getBilling() {
        return billing;
    }

    public void setBilling(String billing) {
        this.billing = billing;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getComcheck() {
        return comcheck;
    }

    public void setComcheck(String comcheck) {
        this.comcheck = comcheck;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getDestinationBuyerNumber() {
        return destinationBuyerNumber;
    }

    public void setDestinationBuyerNumber(String destinationBuyerNumber) {
        this.destinationBuyerNumber = destinationBuyerNumber;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getDestinationCustomerName() {
        return destinationCustomerName;
    }

    public void setDestinationCustomerName(String destinationCustomerName) {
        this.destinationCustomerName = destinationCustomerName;
    }

    public String getDestinationGatePass() {
        return destinationGatePass;
    }

    public void setDestinationGatePass(String destinationGatePass) {
        this.destinationGatePass = destinationGatePass;
    }

    public String getDestinationLotNumber() {
        return destinationLotNumber;
    }

    public void setDestinationLotNumber(String destinationLotNumber) {
        this.destinationLotNumber = destinationLotNumber;
    }

    public String getDestinationPhone() {
        return destinationPhone;
    }

    public void setDestinationPhone(String destinationPhone) {
        this.destinationPhone = destinationPhone;
    }

    public String getDestinationState() {
        return destinationState;
    }

    public void setDestinationState(String destinationState) {
        this.destinationState = destinationState;
    }

    public String getDestinationZip() {
        return destinationZip;
    }

    public void setDestinationZip(String destinationZip) {
        this.destinationZip = destinationZip;
    }

    public SubmitDriver getDriver() {
        return driver;
    }

    public void setDriver(SubmitDriver driver) {
        this.driver = driver;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getOriginBuyerNumber() {
        return originBuyerNumber;
    }

    public void setOriginBuyerNumber(String originBuyerNumber) {
        this.originBuyerNumber = originBuyerNumber;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getOriginCustomerName() {
        return originCustomerName;
    }

    public void setOriginCustomerName(String originCustomerName) {
        this.originCustomerName = originCustomerName;
    }

    public String getOriginGatePass() {
        return originGatePass;
    }

    public void setOriginGatePass(String originGatePass) {
        this.originGatePass = originGatePass;
    }

    public String getOriginLotNumber() {
        return originLotNumber;
    }

    public void setOriginLotNumber(String originLotNumber) {
        this.originLotNumber = originLotNumber;
    }

    public String getOriginPhone() {
        return originPhone;
    }

    public void setOriginPhone(String originPhone) {
        this.originPhone = originPhone;
    }

    public String getOriginState() {
        return originState;
    }

    public void setOriginState(String originState) {
        this.originState = originState;
    }

    public String getOriginZip() {
        return originZip;
    }

    public void setOriginZip(String originZip) {
        this.originZip = originZip;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
