package com.github.andrdev.bolservice;

import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

public abstract class BaseRetroPresenter<V extends MvpView> extends MvpBasePresenter<V> {

    protected abstract void requestFailed(Throwable throwable);

    protected abstract void requestSuccess(SimpleResponse simpleResponse);
}
