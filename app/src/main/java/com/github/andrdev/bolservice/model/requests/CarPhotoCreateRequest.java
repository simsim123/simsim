package com.github.andrdev.bolservice.model.requests;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;

public class CarPhotoCreateRequest {

    String side;
    String cid;
    String file;

    public CarPhotoCreateRequest(VehiclePhoto photo, String cid) {
        this.cid = cid;
        side = photo.getPhotoType();
        file = photo.getPhotoPath();
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }
}
