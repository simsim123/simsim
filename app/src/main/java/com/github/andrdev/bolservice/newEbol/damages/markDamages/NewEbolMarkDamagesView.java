package com.github.andrdev.bolservice.newEbol.damages.markDamages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface NewEbolMarkDamagesView extends MvpView{

    void openVehicleInspection();

    Context getViewContext();

    void setVehicleInfo(VehicleInfo vehicleInfo);

    void initViews();

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);
}
