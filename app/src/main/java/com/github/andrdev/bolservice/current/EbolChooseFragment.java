package com.github.andrdev.bolservice.current;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class EbolChooseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ebols_chooser, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.pickupEbols)
    void pendingPickupClick(View v){
        ((CurrentEbolsActivity)getActivity()).showPendingPickupFragment();
    }

    @OnClick(R.id.deliveryEbols)
    void pendingDeliveryClick(View v){
        ((CurrentEbolsActivity)getActivity()).showtPendingDeliveryFragment();
    }
}
