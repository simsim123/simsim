package com.github.andrdev.bolservice.messages;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.dialogs.SelectColleagueDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MessagesActivity extends AppCompatActivity {

    public static final String TAG_MESSAGES = "messages";
    public static final String TAG_CHAT = "chat";
    public static final String TAG_DIALOG = "dialog";
    public static final String INTERLOCUTOR_NAME = "interlocutorName";
    public static final String CONVERSATION_ID = "conversationId";
    public static final String INTERLOCUTOR_ID = "interlocutorId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showMessagesFragment();
        }
    }

    private void showMessagesFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new MessagesFragment(), TAG_MESSAGES).commit();
    }

    public void showChatFragment(String interlocutorName, long conversationId, long interlocutorId) {
        ChatFragment fragment = new ChatFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTERLOCUTOR_NAME, interlocutorName);
        bundle.putLong(CONVERSATION_ID, conversationId);
        bundle.putLong(INTERLOCUTOR_ID, interlocutorId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, TAG_CHAT).addToBackStack(TAG_CHAT).commit();
    }


    public void showChatFragmentNew(String interlocutorName, long interlocutorId) {
        ChatFragment fragment = new ChatFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INTERLOCUTOR_NAME, interlocutorName);
        bundle.putLong(INTERLOCUTOR_ID, interlocutorId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, TAG_CHAT).addToBackStack(TAG_CHAT).commit();
    }

    @OnClick(R.id.newDialog)
    void newDialogClick(View v) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        SelectColleagueDialogFragment newFragment = SelectColleagueDialogFragment.newInstance();
        newFragment.setCallback(
                colleague -> showChatFragmentNew(colleague.getFullNameFormatted(), colleague.getId()));
        newFragment.show(ft, TAG_DIALOG);
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        onBackPressed();
    }
}
