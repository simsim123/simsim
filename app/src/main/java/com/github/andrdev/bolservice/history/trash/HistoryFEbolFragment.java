//package com.github.andrdev.bolservice.history;
//
//import android.content.Intent;
//import android.util.Log;
//import android.view.View;
//import android.widget.TextView;
//
//import com.github.andrdev.bolservice.R;
//import com.github.andrdev.bolservice.db.Repo;
//import com.github.andrdev.bolservice.model.NewEbol;
//import com.github.andrdev.bolservice.model.VehicleInfo;
//import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
//import com.github.andrdev.bolservice.newEbol.NewEbolFragment;
//import com.github.andrdev.bolservice.originEbol.OriginCarDamagesActivity;
//import com.github.andrdev.bolservice.ui.SignActivity;
//import com.github.andrdev.bolservice.ui.fragments.BaseEbolFragment;
//
//import butterknife.Bind;
//import butterknife.OnClick;
//import nucleus.factory.RequiresPresenter;
//
//
//@RequiresPresenter(HistoryEbolPresenter.class)
//public class HistoryFEbolFragment extends BaseEbolFragment<HistoryEbolPresenter, TextView> {
//
//    @Bind(R.id.customerSignCheck)
//    View customerSignCheck;
//
//    @Bind(R.id.driverSignCheck)
//    View driverSignCheck;
//
//    @Bind(R.id.unableToSignCheck)
//    View unableToSignCheck;
//
//    @Override
//    protected int getLayoutResource() {
//        return R.layout.fragment_ebol_origin;
//    }
//
//    @Override
//    protected void getData() {
//        getPresenter().getEbol(getArguments());
//    }
//
//    @Override
//    protected void detailsClick(int carId) {
//        Intent intent = new Intent(getActivity(), HistoryVehicleInformationActivity.class);
//        intent.putExtra(NewEbolFragment.SELECTED_CAR_ID, carId);
//        startActivity(intent);
//    }
//
//
//    @Override
//    protected void damagesClick(VehicleInfo vehInfo) {
//        if(vehInfo.getCid() == -1) {
//            openMarkedDamage(vehInfo);
//        } else {
//            getPresenter().getDamages(vehInfo);
//        }
//    }
//
//    public void openMarkedDamage(VehicleInfo vehInfo) {
//        Intent intent = new Intent(getActivity(), OriginCarDamagesActivity.class);
//        intent.putExtra(NewEbolFragment.SELECTED_CAR_ID, vehInfo.getId());
//        startActivity(intent);
//    }
//
//    protected void setSignImages(NewEbol savedEbol) {
//        Log.d("dree", "sign");
//        if (savedEbol.getDeliveryDriverSignPath() != null) {
//            driverSignCheck.setVisibility(View.VISIBLE);
//            Log.d("dree", "signgdds");
//        } else {
//            driverSignCheck.setVisibility(View.INVISIBLE);
//            Log.d("dree", "signgddsn");
//        }
//        if (savedEbol.getDeliveryCustomerSignPath() != null) {
//            customerSignCheck.setVisibility(View.VISIBLE);
//            Log.d("dree", "signgdcs");
//        } else {
//            customerSignCheck.setVisibility(View.INVISIBLE);
//            Log.d("dree", "signgdcsn");
//        }
//        if (savedEbol.isDeliveryUnableToSign()) {
//            unableToSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            unableToSignCheck.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    @OnClick(R.id.customerSignature)
//    void customerSignatureClick() {
//        Intent intent = new Intent(getActivity(), SignActivity.class);
//        intent.putExtra("signatureType", "deliveryCustomerSign");
//        intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.driverSignature)
//    void driverSignatureClick() {
//        Intent intent = new Intent(getActivity(), SignActivity.class);
//        intent.putExtra("signatureType", "deliveryDriverSign");
//        intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.unableToSign)
//    void unableToSignClick() {
//        Repo repo = new Repo(getContext());
//        currentEbol.setDeliveryUnableToSign(true);
////        if(currentEbol.getDeliveryDriverSignPath()!=null) {
////            new File(currentEbol.getDeliveryDriverSignPath()).delete();
////        }
////        if(currentEbol.getDeliveryCustomerSignPath()!=null) {
////            new File(currentEbol.getDeliveryCustomerSignPath()).delete();
////        }
////        currentEbol.setDeliveryDriverSignPath(null);
////        currentEbol.setDeliveryCustomerSignPath(null);
//        repo.repoNewEbol.updateTempNewEbol(currentEbol);
//        setSignImages(currentEbol);
//    }
//
//    @OnClick(R.id.pickup)
//    void pickupClick() {
//        Repo repo = new Repo(getContext());
//        currentEbol.setType("delivered");
//        repo.repoNewEbol.updateTempNewEbol(currentEbol);
//        onBackClick();
//    }
//
//    @OnClick(R.id.backIm)
//    void onBackClick() {
//        getActivity().onBackPressed();
//    }
//}
