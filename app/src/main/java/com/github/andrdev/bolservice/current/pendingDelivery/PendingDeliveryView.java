package com.github.andrdev.bolservice.current.pendingDelivery;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

public interface PendingDeliveryView extends BaseSearchView<NewEbol> {
    void openNewEbol();


}
