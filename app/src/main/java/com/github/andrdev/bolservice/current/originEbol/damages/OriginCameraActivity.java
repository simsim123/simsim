package com.github.andrdev.bolservice.current.originEbol.damages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;


public class OriginCameraActivity extends AppCompatActivity {

    String photoType;
    int selectedCarId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        photoType = getIntent().getStringExtra(OriginDamagesFragment.PHOTO_TYPE);
        selectedCarId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
        if (savedInstanceState == null) {
            showMarkDamagesFragment(photoType);
        }
    }

    public void showMarkDamagesFragment(String photoType) {
        OriginMarkDamagesFragment fragment =  new OriginMarkDamagesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(OriginDamagesFragment.PHOTO_TYPE, photoType);
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedCarId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
    }

//    public void showVehicleInspectionsFragment() {
//        NewEbolVehicleInspectionFragment fragment =  new NewEbolVehicleInspectionFragment();
//        Bundle bundle = new Bundle();
//        bundle.putInt(NewEbolFragment.SELECTED_CAR_ID, selectedCarId);
//        fragment.setArguments(bundle);
//        getSupportFragmentManager().beginTransaction()
//                .replace(android.R.id.content, fragment).commit();
//
//    }
}
