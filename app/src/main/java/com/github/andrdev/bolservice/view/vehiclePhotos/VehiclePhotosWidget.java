package com.github.andrdev.bolservice.view.vehiclePhotos;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.MvpPercentRelativeLayout;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.NewEbolCameraAcitvity;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehiclePhotosWidget extends MvpPercentRelativeLayout<VehiclePhotosView, VehiclePhotosPresenter>
        implements VehiclePhotosView {

    public static final String PHOTO_TYPE = "photoType";
    public static final String FRONT_PHOTO = "frontPhoto";
    public static final String BACK_PHOTO = "backPhoto";
    public static final String LEFT_SIDE_PHOTO = "leftSidePhoto";
    public static final String RIGHT_SIDE_PHOTO = "rightSidePhoto";

    @Bind(R.id.front)
    ImageView front;
    @Bind(R.id.back)
    ImageView back;
    @Bind(R.id.leftSide)
    ImageView leftSide;
    @Bind(R.id.rightSide)
    ImageView rightSide;

    @Bind(R.id.frontDamagesCount)
    TextView frontDamagesCount;
    @Bind(R.id.backDamagesCount)
    TextView backDamagesCount;
    @Bind(R.id.rightsideDamagesCount)
    TextView rightSideDamagesCount;
    @Bind(R.id.leftSideDamagesCount)
    TextView leftSideDamagesCount;

    int selectedVehicleId;
    Damages damages;

    public VehiclePhotosWidget(Context context) {
        super(context);
        init(context);
    }

    public VehiclePhotosWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VehiclePhotosWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_car_photos, this);
        ButterKnife.bind(this, view);
    }

    @Override
    public VehiclePhotosPresenter createPresenter() {
        return new VehiclePhotosPresenter();
    }

    public void setFieldsFromDb(Damages damages) {
        this.damages = damages;
        if (damages.getFront() != null) {
            setCapturedImages(damages);
        }
    }

    public void setSelectedVehicleId(int selectedVehicleId) {
        this.selectedVehicleId = selectedVehicleId;
    }

    private void setCapturedImages(Damages damages) {
        Picasso picasso = Picasso.with(getContext());
        File file;
        if (damages.getFront() != null) {
            file = new File(damages.getFront().getPhotoPath());
            picasso.invalidate(file);
            picasso.load(file).fit().into(front);
            front.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int frontDamage = damages.getFront().getMarkedDamages().size();
            frontDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, frontDamage, frontDamage));
        }
        if (damages.getBack() != null) {
            file = new File(damages.getBack().getPhotoPath());
            picasso.invalidate(file);
            picasso.load(file).fit().into(back);
            back.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getBack().getMarkedDamages().size();
            backDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
        if (damages.getLeftSide() != null) {
            file = new File(damages.getLeftSide().getPhotoPath());
            picasso.invalidate(file);
            picasso.load(file).fit().into(leftSide);
            leftSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getLeftSide().getMarkedDamages().size();
            leftSideDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
        if (damages.getRightSide() != null) {
            file = new File(damages.getRightSide().getPhotoPath());
            picasso.invalidate(file);
            picasso.load(file).fit().into(rightSide);
            rightSide.setBackgroundDrawable(getResources().getDrawable(R.drawable.white_border_b));
            int damagesCounted = damages.getRightSide().getMarkedDamages().size();
            rightSideDamagesCount.setText(getResources().getQuantityString(
                    R.plurals.damagesMarked, damagesCounted, damagesCounted));
        }
    }


    @OnClick({R.id.front, R.id.back, R.id.rightSide, R.id.leftSide})
    void imageClick(View view) {
        String phototype;
        String action;

        switch (view.getId()){
            case R.id.front:
                phototype = FRONT_PHOTO;
                if(damages == null || damages.getFront() == null){
                    action = "create";
                } else {
                    action = "edit";
                }
                break;
            case R.id.back:
                if(damages == null || !hasPhoto(damages.getFront())) return;
                phototype = BACK_PHOTO;
                if(damages.getBack() == null){
                    action = "create";
                } else {
                    action = "edit";
                }
                break;
            case R.id.rightSide:
                if(damages == null || !hasPhoto(damages.getBack())) return;
                phototype = RIGHT_SIDE_PHOTO;
                if(damages.getRightSide() == null){
                    action = "create";
                } else {
                    action = "edit";
                }
                break;
            case R.id.leftSide:
                if(damages == null || !hasPhoto(damages.getRightSide())) return;
                phototype = LEFT_SIDE_PHOTO;
                if(damages.getLeftSide() == null){
                    action = "create";
                } else {
                    action = "edit";
                }
                break;
            default:
                phototype = "";
                action = "";
                break;
        }
        Intent intent = new Intent(getContext(), NewEbolCameraAcitvity.class);
        intent.putExtra(PHOTO_TYPE, phototype);

        intent.putExtra("action", action);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        getContext().startActivity(intent);
    }

    private boolean hasPhoto(VehiclePhoto photo) {
        return photo != null
                && photo.getPhotoPath() != null
                && !TextUtils.isEmpty(photo.getPhotoPath());
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
