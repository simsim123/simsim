package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.util.List;

public class EbolsResponse {

    List<NewEbol> ebols;

    public EbolsResponse() {
    }

    public List<NewEbol> getEbols() {
        return ebols;
    }

    public void setEbols(List<NewEbol> ebols) {
        this.ebols = ebols;
    }
}
