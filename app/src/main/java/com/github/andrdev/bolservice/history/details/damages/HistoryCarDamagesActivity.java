package com.github.andrdev.bolservice.history.details.damages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryCarDamagesActivity extends AppCompatActivity {

    @Bind(R.id.titleTool)
    TextView titleTool;
    @Bind(R.id.subTitle)
    TextView subTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_damages);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showDamagesFragment();
        }
    }

    private void showDamagesFragment() {
        HistoryDamagesFragment fragment = new HistoryDamagesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID,
                getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, fragment).commit();
    }

    public void setTitle(VehicleInfo info) {
        titleTool.setText(String.format("%s %s %s", info.getYearOfMake(), info.getMake(), info.getModel()));
        subTitle.setText(info.getVinNumber());
    }

    @OnClick(R.id.backIm)
    void onBackClicked(View view) {
        onBackPressed();
    }
}
