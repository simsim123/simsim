package com.github.andrdev.bolservice.newEbol.damages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface NewEbolDamagesView extends MvpView {

    Context getViewContext();

    void setCurrentVehicle(VehicleInfo vehicle);

    void setActivityTitle(String title, String vinNumber);

    void setDamages(Damages damages);

    void backToEbol();
}
