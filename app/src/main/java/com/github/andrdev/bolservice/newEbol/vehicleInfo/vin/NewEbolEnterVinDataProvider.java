package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class NewEbolEnterVinDataProvider implements DataProvider {

    private DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(vehicleId, callback::returnData);
    }

    public void saveTempVehicleOrUpdate(VehicleInfo tempInfo, DataCb<Boolean> callback) {
        dbHelper.saveTempVehicleOrUpdate(tempInfo, callback::returnData);
    }
    public void decodeVin(String vinNumber, DataCb<VinDecodeResponse> successCb,
                          DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().decodeVin(vinNumber,
                successCb::returnData,
                failedCb::returnData);
    }
}
