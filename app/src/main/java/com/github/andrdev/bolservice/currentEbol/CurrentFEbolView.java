package com.github.andrdev.bolservice.currentEbol;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface CurrentFEbolView extends MvpView {

    NewEbol getEbolToSave();

    Context getViewContext();

    void openNewVehicleInfoActivity();

    NewEbol getEbolToSaveDb();

    void setData(NewEbol ebol);

    void showVehicleDetails(int vehicleId);

    void showDamages(int vehicleId);

    void openCustomerSignature();

    void openDriverSignature();

    void showUnableToSign();

    void finishEbol();
}
