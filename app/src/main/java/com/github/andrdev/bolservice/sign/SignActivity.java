package com.github.andrdev.bolservice.sign;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        ButterKnife.bind(this);
        SignFragment fragment = new SignFragment();
        fragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, fragment).commit();
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        onBackPressed();
    }
}
