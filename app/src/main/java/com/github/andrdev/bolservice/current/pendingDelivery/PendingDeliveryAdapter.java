package com.github.andrdev.bolservice.current.pendingDelivery;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;


public class PendingDeliveryAdapter extends BaseAdapter<NewEbol, PendingDeliveryAdapter.PendingDeliveryHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_pending_delivery;
    }

    @Override
    protected PendingDeliveryHolder getHolder(View view) {
        return new PendingDeliveryHolder(view);
    }

    protected static class PendingDeliveryHolder extends BaseAdapter.BaseViewHolder<NewEbol> {

        @Bind(R.id.vehicleId)
        TextView vehicleId;
        @Bind(R.id.vehiclesCount)
        TextView vehiclesCount;
        @Bind(R.id.customerFirst)
        TextView customerFirst;
        @Bind(R.id.customerSecond)
        TextView customerSecond;
        @Bind(R.id.date)
        TextView date;

        public PendingDeliveryHolder(View itemView) {
            super(itemView);
        }

        protected void bind(NewEbol newEbol) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                SimpleDateFormat output = new SimpleDateFormat("dd MMMM, yyyy");
                Date d = sdf.parse(newEbol.getDate());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Customer origin = newEbol.getOrigin();
            Customer destination = newEbol.getDestination();
            vehicleId.setText(newEbol.getOrderId());
            String count = vehiclesCount.getContext()
                    .getResources().getQuantityString(R.plurals.vehicles,
                            newEbol.getVehiclesCount(), newEbol.getVehiclesCount());
            vehiclesCount.setText(count);
            customerFirst.setText(String.format("%s, %s, %s, %s, %s",  origin.getCustomerName(),
                    origin.getCity(),
                    origin.getState(),
                    origin.getZip(),
                    origin.getPhone()));
            customerSecond.setText(String.format("%s, %s, %s, %s, %s", destination.getCustomerName(),
                    destination.getCity(),
                    destination.getState(),
                    destination.getZip(),
                    destination.getPhone()));
        }
    }
}
