package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Profile;

import java.util.List;

import io.realm.RealmObject;


public class ProfileResponse  extends RealmObject {
    public static final String STATUS = "status";
    public static final String PROFILE = "profile";

    private String status;
    private Profile profile;

    public ProfileResponse(){ }

    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
    public void setProfile(Profile profile){
        this.profile = profile;
    }
    public Profile getProfile(){
        return this.profile;
    }
}
