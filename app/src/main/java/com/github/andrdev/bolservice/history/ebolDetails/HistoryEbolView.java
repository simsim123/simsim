package com.github.andrdev.bolservice.history.ebolDetails;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface HistoryEbolView extends MvpView {
    void setData(NewEbol ebol);

    void showVehicleDetails(int vehicleId);

    void showDamages(int vehicleId);

    Context getViewContext();
}
