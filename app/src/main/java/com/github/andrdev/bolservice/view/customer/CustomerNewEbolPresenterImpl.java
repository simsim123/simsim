package com.github.andrdev.bolservice.view.customer;

import android.location.Address;
import android.location.Location;
import android.util.Log;

import com.github.andrdev.bolservice.GpsAddressProvider;

import java.util.List;

public class CustomerNewEbolPresenterImpl extends CustomerNewEbolPresenter
        implements GpsAddressProvider.GpsHandlerCallback {

    CustomerDataProvider dataProvider;

    @Override
    public void attachView(CustomerNewEbolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new CustomerDataProvider();
        dataProvider.setMapsGpsHandler(new GpsAddressProvider(getView().getViewContext()));
    }

    @Override
    public void getAddress() {
        if(!isViewAttached()){
            return;
        }
        dataProvider.checkForGpsPermission(this::permissionsResult);
    }

    @Override
    public void permissionsResult(Boolean granted) {
        if (granted == null) {
            return;
        }

        if (granted) {
            isGpsTurnedOn();
        } else {
            noGpsPermission();
        }
    }

    @Override
    public void isGpsTurnedOn() {
        dataProvider.checkGpsIsTurnedOn(this::isGpsTurnedOn);
    }

    @Override
    public void isGpsTurnedOn(Boolean isGpsTurnedOn) {
        if (isGpsTurnedOn == null) {
            return;
        }

        if (isGpsTurnedOn) {
            askPlacesOrGps();
        } else {
            gpsTurnedOff();
        }
    }

    private void askPlacesOrGps() {
        if(!isViewAttached()){
            return;
        }
        getView().showPlacesOrGpsDialog();
    }

    @Override
    public void getAddressFromGps() {
        dataProvider.getGpsCoordinates(this::getAddressFromLocation);
    }

    @Override
    public void getAddressFromLocation(Location location) {
        Log.d("dree", "getAddressFromLocation: "+location.toString());
        dataProvider.getAddressFromCoordinates(location, this::gpsAddressSuccessOb);
    }

    @Override
    public void gpsAddressSuccessOb(List<Address> addresses) {
        addressReceived(addresses.get(0));
    }

    @Override
    public void noGpsPermission() {
        if(!isViewAttached()) {
            return;
        }
        getView().selectAddressFromGooglePlaces();
    }

    @Override
    public void addressReceived(Address address) {
        if(!isViewAttached()) {
            return;
        }
        getView().setAddressFromPrediction(address);
    }

    @Override
    public void gpsTurnedOff() {
        if(!isViewAttached()) {
            return;
        }
        getView().buildAlertMessageNoGps();
    }
}
