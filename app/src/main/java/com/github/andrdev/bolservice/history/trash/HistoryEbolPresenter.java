//package com.github.andrdev.bolservice.history.details;
//
//import android.content.Context;
//import android.graphics.Point;
//import android.os.Bundle;
//import android.view.Display;
//import android.view.WindowManager;
//
//import com.github.andrdev.bolservice.history.HistoryEbolView;
//import com.github.andrdev.bolservice.model.responses.DamagesG;
//import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
//import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
//
//import java.util.List;
//import retrofit.Callback;
//
//
//public class HistoryEbolPresenter extends MvpBasePresenter<HistoryEbolView> {
//
//    public void getEbol(Bundle arguments) {
//        if(getView() == null) return;
//
//        Repo repo = new Repo(getView().getContext());
//        NewEbol ebol;
//        if(arguments != null && arguments.containsKey(HistoryEbolAcitivty.EBOL_ID)) {
//            int id = arguments.getInt(HistoryEbolAcitivty.EBOL_ID);
//            ebol = repo.repoNewEbol.getNewEbolById(id);
//        } else {
//            ebol = repo.repoNewEbol.getTempNewEbol();
//        }
//
//        if(ebol != null) {
//            getView().setAndShowData(ebol);
//        }
//    }
//
//
//    public void getDamages(final VehicleInfo info) {
//        if(getView() == null) return;
//
//        final Repo repo = new Repo(getView().getContext());
//
//        Callback<DamagesGetResponse> ebolCb = new Callback<DamagesGetResponse>() {
//            @Override
//            public void success(DamagesGetResponse cars, Response response) {
//                if(getView() != null) {
//                    if (cars.getStatus().equals("true") && cars.getDamages() != null) {
//                        damagesToMarkDamage(info, cars.getDamages());
////                        ebol.addVehicleInfos(carsToVehicleInfos(cars.getCars()));
////                        repo.repoNewEbol.updateTempNewEbol(ebol);
////                        getDamages(cars);
//                        getView().openMarkedDamage(info);
//                    }
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        };
//        EbolNetworkWorker.getInstance().getDamages(String.valueOf(info.getCid()), ebolCb);
//    }
//
//    private void damagesToMarkDamage(VehicleInfo info, List<DamagesG> damages) {
//        MarkedDamage[] markedDamages = new MarkedDamage[4];
//        final Repo repo = new Repo(getView().getContext());
//
//        WindowManager wm = (WindowManager) getView().getContext().getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
////        repo.repoDamages.updateDamages(info.getDamages());
////        repo.repoVehicleInfo.refreshVehicleInfo(info);
////        VehicleInfo nVi = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//        info.getDamages().setFront(null);
//        info.getDamages().setBack(null);
//        info.getDamages().setRightSide(null);
//        info.getDamages().setLeftSide(null);
//        repo.repoDamages.updateDamages(info.getDamages());
//        repo.repoVehicleInfo.refreshVehicleInfo(info);
//        for (DamagesG dg : damages) {
//            MarkedDamage damage = new MarkedDamage();
//            repo.repoMarkedDamage.createTempMarkedDamage(damage);
//            repo.repoMarkedDamage.refresh(damage);
//            damage.setxPosition((int) (height * Double.parseDouble(dg.getPositionLeft()) / 100));
//            damage.setyPosition((int) (width * Double.parseDouble(dg.getPositionTop()) / 100));
//            repo.repoMarkedDamage.updateMarkedDamage(damage);
////            damage.getDamageTypes().add(dt);
////            damage.getDamageTypes().add();
//
//            switch (dg.getSide()) {
//                case "front":
//                    if (info.getDamages().getFront() == null) {
//                        CarPhoto damageF = new CarPhoto();
//                        damageF.setPhotoType("frontPhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageF);
//                        info.getDamages().setFront(damageF);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getFront().setPhotoPath(dg.getImage());
//                    info.getDamages().getFront().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getFront());
//                    break;
//                case "back":
//                    if (info.getDamages().getBack() == null) {
//                        CarPhoto damageB = new CarPhoto();
//                        damageB.setPhotoType("backPhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageB);
//                        info.getDamages().setBack(damageB);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getBack().setPhotoPath(dg.getImage());
//                    info.getDamages().getBack().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getBack());
//                    break;
//                case "rightside":
//                    if (info.getDamages().getRightSide() == null) {
//                        CarPhoto damageR = new CarPhoto();
//                        damageR.setPhotoType("rightSidePhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageR);
//                        info.getDamages().setRightSide(damageR);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getRightSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getRightSide().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getRightSide());
//                    break;
//                case "leftside":
//                    if (info.getDamages().getLeftSide() == null) {
//                        CarPhoto damageL = new CarPhoto();
//                        damageL.setPhotoType("leftSidePhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageL);
//                        info.getDamages().setLeftSide(damageL);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getLeftSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getLeftSide().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getLeftSide());
//                    break;
//                default:
//            }
//            DamageType dt = new DamageType();
//            dt.setState(true);
//            dt.setDamageShortName(dg.getCode());
//            dt.setMarkedDamage(damage);
//            repo.repoDamageType.createTempMarkedDamage(dt);
//        }
//    }
//}
