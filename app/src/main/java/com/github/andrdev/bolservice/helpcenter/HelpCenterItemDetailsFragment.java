package com.github.andrdev.bolservice.helpcenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.HelpCenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HelpCenterItemDetailsFragment extends Fragment {

    @Bind(R.id.itemText)
    TextView itemText;

    @Bind(R.id.titleTool)
    TextView titleTool;

    HelpCenter item;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_center_viewer, container, false);
        ButterKnife.bind(this, view);
        itemText.setText(item.getBody());
        titleTool.setText(item.getTitle());
        return view;
    }

    public void setHelpCenter(HelpCenter item) {
        this.item = item;
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }
}
