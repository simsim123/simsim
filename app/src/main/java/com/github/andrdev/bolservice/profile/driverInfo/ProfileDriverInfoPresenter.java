package com.github.andrdev.bolservice.profile.driverInfo;

import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ProfileDriverInfoPresenter extends MvpBasePresenter<ProfileDriverInfoView> {
    public abstract void getData();

    public abstract void saveData(SaveProfileRequest profileRequest);

    public  abstract void requestFailed(Throwable throwable);

    public  abstract void requestSuccess(SaveProfileResponse saveProfileResponse);
}
