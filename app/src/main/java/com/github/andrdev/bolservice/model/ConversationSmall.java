package com.github.andrdev.bolservice.model;


public class ConversationSmall {

    long conversationId;
    Message lastMessage;
    int interlocutorId;
    String interlocutorFullname;

    public ConversationSmall() {
    }

    public long getConversationId() {
        return conversationId;
    }

    public void setConversationId(long conversationId) {
        this.conversationId = conversationId;
    }

    public String getInterlocutorFullname() {
        return interlocutorFullname;
    }

    public void setInterlocutorFullname(String interlocutorFullname) {
        this.interlocutorFullname = interlocutorFullname;
    }

    public int getInterlocutorId() {
        return interlocutorId;
    }

    public void setInterlocutorId(int interlocutorId) {
        this.interlocutorId = interlocutorId;
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }
}
