package com.github.andrdev.bolservice.model;


public class OrderInfo {

    String vehicleId;
    String vehiclesCount;
    String firstCustomerName;
    String secondCustomerName;
    String firstAddress;
    String secondAddress;
    String firstAddressInfo;
    String secondAddressInfo;
    String firstPhone;
    String secondPhone;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFirstAddress() {
        return firstAddress;
    }

    public void setFirstAddress(String firstAddress) {
        this.firstAddress = firstAddress;
    }

    public String getFirstAddressInfo() {
        return firstAddressInfo;
    }

    public void setFirstAddressInfo(String firstAddressInfo) {
        this.firstAddressInfo = firstAddressInfo;
    }

    public String getFirstCustomerName() {
        return firstCustomerName;
    }

    public void setFirstCustomerName(String firstCustomerName) {
        this.firstCustomerName = firstCustomerName;
    }

    public String getFirstPhone() {
        return firstPhone;
    }

    public void setFirstPhone(String firstPhone) {
        this.firstPhone = firstPhone;
    }

    public String getSecondAddress() {
        return secondAddress;
    }

    public void setSecondAddress(String secondAddress) {
        this.secondAddress = secondAddress;
    }

    public String getSecondAddressInfo() {
        return secondAddressInfo;
    }

    public void setSecondAddressInfo(String secondAddressInfo) {
        this.secondAddressInfo = secondAddressInfo;
    }

    public String getSecondCustomerName() {
        return secondCustomerName;
    }

    public void setSecondCustomerName(String secondCustomerName) {
        this.secondCustomerName = secondCustomerName;
    }

    public String getSecondPhone() {
        return secondPhone;
    }

    public void setSecondPhone(String secondPhone) {
        this.secondPhone = secondPhone;
    }

    public String getVehiclesCount() {
        return vehiclesCount;
    }

    public void setVehiclesCount(String vehiclesCount) {
        this.vehiclesCount = vehiclesCount;
    }
}
