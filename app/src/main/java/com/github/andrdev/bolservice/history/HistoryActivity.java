package com.github.andrdev.bolservice.history;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.database.RealmDbData;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showHistoryFragment();
        }
    }

    private void showHistoryFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new HistoryFragment()).commit();
    }
    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        onBackPressed();
    }
}
