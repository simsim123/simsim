package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.HelpCenter;

import java.util.List;


public class HelpCenterResponse {

    String status;
    List<HelpCenter> helpCenter;

    public HelpCenterResponse() {
    }

    public List<HelpCenter> getHelpCenter() {
        return helpCenter;
    }

    public void setHelpCenter(List<HelpCenter> helpCenter) {
        this.helpCenter = helpCenter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
