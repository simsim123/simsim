package com.github.andrdev.bolservice.current.originEbol;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.current.originEbol.damages.OriginCarDamagesActivity;
import com.github.andrdev.bolservice.current.originEbol.vehicleInfo.OriginVehicleInformationActivity;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.sign.SignActivity;
import com.github.andrdev.bolservice.view.customerNotEditable.CustomerNeBWidget;
import com.github.andrdev.bolservice.view.customerNotEditable.CustomerNeWidget;
import com.github.andrdev.bolservice.view.vehicleInfos.VehicleInfosWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OriginFEbolFragment extends MvpFragment<OriginFEbolView, OriginEbolPresenter>
        implements OriginFEbolView {

    @Bind(R.id.originOrderId)
    protected TextView originOrderId;

    @Bind(R.id.originDate)
    protected TextView originDate;

    @Bind(R.id.vehiclesHost)
    VehicleInfosWidget vehicleInfos;

    @Bind(R.id.customerA)
    CustomerNeWidget customerA;

    @Bind(R.id.customerB)
    CustomerNeBWidget customerB;

    @Bind(R.id.customerSignCheck)
    View customerSignCheck;

    @Bind(R.id.driverSignCheck)
    View driverSignCheck;

    @Bind(R.id.unableToSignCheck)
    View unableToSignCheck;

    protected NewEbol currentEbol;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fogigin, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public OriginEbolPresenter createPresenter() {
        return new OriginEbolPresenterImpl();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void setData(NewEbol ebol) {
        currentEbol = ebol;
        showData();
    }

    protected void showData() {
        if (currentEbol == null) {
            return;
        }
        showVehicleInfos();
        setCommonFields(currentEbol);
        showCustomersInfos();
        setSignImages(currentEbol);
    }

    private void showVehicleInfos() {
        vehicleInfos.setVehiclesInfos(currentEbol.getVehicleInfos());
        vehicleInfos.setCallback(new VehicleInfosWidget.VehicleInfosCallback() {
            @Override
            public void detailsClickCb(VehicleInfo vehicleInfo) {
                detailsClick(vehicleInfo);
            }

            @Override
            public void damagesClickCb(VehicleInfo vehicleInfo) {
                damagesClick(vehicleInfo);
            }
        });
        vehicleInfos.showData();
    }

    private void showCustomersInfos() {
        if (currentEbol.getDestination() != null) {
            setDestinationCustomerFields(currentEbol.getDestination());
        }
        if (currentEbol.getOrigin() != null) {
            setOriginCustomerFields(currentEbol.getOrigin());
        }
    }

    private void setCommonFields(NewEbol ebol) {
        originOrderId.setText(ebol.getOrderId());
        originDate.setText(ebol.getDate());
    }

    private void setOriginCustomerFields(Customer origin) {
        customerA.setCustomerFields(origin);
    }

    private void setDestinationCustomerFields(Customer destination) {
        customerB.setCustomerFields(destination);
    }

    protected void detailsClick(VehicleInfo vehInfo) {
        getPresenter().detailsClick(vehInfo.getId());
    }

    @Override
    public void showVehicleDetails(int vehicleId) {
        Intent intent = new Intent(getActivity(), OriginVehicleInformationActivity.class);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    protected void damagesClick(VehicleInfo vehInfo) {
        getPresenter().damagesClick(vehInfo.getId());
    }

    @Override
    public void showDamages(int vehicleId) {
        Intent intent = new Intent(getActivity(), OriginCarDamagesActivity.class);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    private void setSignImages(NewEbol savedEbol) {
        setSignImage(savedEbol.getDeliveryDriverSignPath() != null, driverSignCheck);
        setSignImage(savedEbol.getDeliveryCustomerSignPath() != null, customerSignCheck);
        setSignImage(savedEbol.isDeliveryUnableToSign(), unableToSignCheck);
    }

    private void setSignImage(boolean hasSign, View signImage) {
        if (hasSign) {
            signImage.setVisibility(View.VISIBLE);
        } else {
            signImage.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.customerSignature)
    void customerSignatureClick() {
        getPresenter().customerSignatureClick();
    }

    @Override
    public void openCustomerSignature() {
        Intent intent = new Intent(getActivity(), SignActivity.class);
        intent.putExtra("signatureType", "deliveryCustomerSign");
        intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
        startActivity(intent);
    }

    @OnClick(R.id.driverSignature)
    void driverSignatureClick() {
        getPresenter().driverSignatureClick();
    }

    @Override
    public void openDriverSignature() {
        Intent intent = new Intent(getActivity(), SignActivity.class);
        intent.putExtra("signatureType", "deliveryDriverSign");
        intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
        startActivity(intent);
    }

    @OnClick(R.id.unableToSign)
    void unableToSignClick() {
        if(currentEbol.getDeliveryCustomerSignPath() == null) {
            currentEbol.setDeliveryUnableToSign(true);
            getPresenter().unableToSignClick();
        }
    }

    @Override
    public void showUnableToSign() {
        if (currentEbol.getDeliveryCustomerSignPath() == null) {
            unableToSignCheck.setVisibility(View.VISIBLE);
            setSignImages(currentEbol);
        }
    }

    @Override
    public NewEbol getEbolToSave() {
        return currentEbol;
    }

    @OnClick(R.id.pickup)
    void pickupClick() {
        onBackClick();
    }

    @OnClick(R.id.backIm)
    void onBackClick() {
        getActivity().onBackPressed();
    }
}
