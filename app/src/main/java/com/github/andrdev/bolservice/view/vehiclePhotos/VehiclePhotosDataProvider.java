package com.github.andrdev.bolservice.view.vehiclePhotos;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;

public class VehiclePhotosDataProvider implements DataProvider {

    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }
}
