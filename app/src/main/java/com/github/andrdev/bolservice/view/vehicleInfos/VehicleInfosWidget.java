package com.github.andrdev.bolservice.view.vehicleInfos;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.view.vehicleInfos.VehicleInfosPresenter;
import com.github.andrdev.bolservice.view.vehicleInfos.VehicleInfosView;
import com.hannesdorfmann.mosby.mvp.layout.MvpLinearLayout;

import java.util.List;

import butterknife.ButterKnife;

public class VehicleInfosWidget extends LinearLayout {

    private static final String TAG = "viw";
    private List<VehicleInfo> vehiclesInfos;
    private VehicleInfosCallback callback;

    public VehicleInfosWidget(Context context) {
        super(context);
        init(context);
    }

    public VehicleInfosWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VehicleInfosWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
//        View view = View.inflate(context, R.layout.view_vehicle_infos, this);
//        ButterKnife.bind(this, view);
    }

    private void drawVehicles() {
        removeAllViews();
        for (final VehicleInfo vehInfo : vehiclesInfos) {
            View damagedVehicle = View.inflate(getContext(), R.layout.list_item_damaged_car, null);
            Log.d(TAG, "drawVehicles: "+ vehInfo.getType());
            setVehicleClickers(vehInfo, damagedVehicle);
            setVehicleText(vehInfo, damagedVehicle);

            addView(damagedVehicle);

            LinearLayout.LayoutParams params =(LinearLayout.LayoutParams) damagedVehicle.getLayoutParams();
            params.bottomMargin = 8;
        }
    }

    private void setVehicleText(VehicleInfo vehInfo, View damagedVehicle) {
        TextView vehicleInfo = (TextView) damagedVehicle.findViewById(R.id.carInfo);
        TextView vin = (TextView) damagedVehicle.findViewById(R.id.vin);
        TextView damagesCount = (TextView) damagedVehicle.findViewById(R.id.damagesCount);

        vehicleInfo.setText(String.format(
                "%s %s %s", vehInfo.getYearOfMake(), vehInfo.getMake(), vehInfo.getModel()));
        vin.setText(vehInfo.getVinNumber());
        Damages vehDamages =  vehInfo.getDamages();
        int markedDamagesCount = 0;
        if(vehDamages != null) {
            markedDamagesCount = getMarkedDamagesCount(vehDamages, markedDamagesCount);
        }

        damagesCount.setText(getResources().getQuantityString(
                R.plurals.damagesMarked, markedDamagesCount, markedDamagesCount));
    }

    private void setVehicleClickers(final VehicleInfo vehInfo, View damagedVehicle) {
        damagedVehicle.findViewById(R.id.damages)
                .setOnClickListener(v -> callback.damagesClickCb(vehInfo));
        damagedVehicle.findViewById(R.id.details)
                .setOnClickListener(v -> callback.detailsClickCb(vehInfo));
    }

    private int getMarkedDamagesCount(Damages vehDamages, int markedDamagesCount) {
        if(vehDamages.getFront()!=null) {
            markedDamagesCount += vehDamages.getFront().getMarkedDamages().size();
        }
        if(vehDamages.getBack()!=null) {
            markedDamagesCount += vehDamages.getBack().getMarkedDamages().size();
        }
        if(vehDamages.getRightSide()!=null) {
            markedDamagesCount += vehDamages.getRightSide().getMarkedDamages().size();
        }
        if(vehDamages.getLeftSide()!=null) {
            markedDamagesCount += vehDamages.getLeftSide().getMarkedDamages().size();
        }
        if(vehDamages.getAdditionalFirst()!=null) {
            markedDamagesCount += vehDamages.getAdditionalFirst().getMarkedDamages().size();
        }
        if(vehDamages.getAdditionalSecond()!=null) {
            markedDamagesCount += vehDamages.getAdditionalSecond().getMarkedDamages().size();
        }
        return markedDamagesCount;
    }

//    @Override
//    public VehicleInfosPresenter createPresenter() {
//        return new VehicleInfosPresenter();
//    }

    public void setVehiclesInfos(List<VehicleInfo> vehiclesInfos) {
        this.vehiclesInfos = vehiclesInfos;
    }

    public void showData() {
        drawVehicles();
    }

    public void setCallback(VehicleInfosCallback callback) {
        this.callback = callback;
    }

    public interface VehicleInfosCallback{

        void detailsClickCb(VehicleInfo vehicleInfo);

        void damagesClickCb(VehicleInfo vehicleInfo);
    }
}
