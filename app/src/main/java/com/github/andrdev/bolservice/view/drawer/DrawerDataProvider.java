package com.github.andrdev.bolservice.view.drawer;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.Profile;

public class DrawerDataProvider implements DataProvider{

    DbHelper dbHelper;

    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getProfile(DataCb<Profile> callback) {
        dbHelper.getSavedProfile(callback::returnData);
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }
}
