package com.github.andrdev.bolservice.history.details.chooseDamage;


import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import rx.Observable;


public class HistoryCdtPresenter extends MvpBasePresenter<HistoryCdtView>{
    HistoryCdtDataProvider dataProvider;

    @Override
    public void attachView(HistoryCdtView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HistoryCdtDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }


    public void getData(String photoType, double yPosition, double xPosition, int selectedVehicleId) {
        dataProvider.getVehicleById(selectedVehicleId,
                (vehicleInfo) -> getVehiclePhoto(vehicleInfo, photoType, yPosition, xPosition));
    }

    private void getVehiclePhoto(VehicleInfo vehicleInfo, String photoType, double yPosition, double xPosition) {
        VehiclePhoto vehiclePhoto;
        switch (photoType) {
            case VehiclePhotosWidget.FRONT_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getFront();
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getBack();
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getLeftSide();
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getRightSide();
                break;
            default:
                vehiclePhoto = null;
                break;
        }
        getView().setVehiclePhoto(vehiclePhoto);
        Observable.from(vehiclePhoto.getMarkedDamages()).filter(damage
                -> damage.getxPosition() == xPosition && damage.getyPosition() == yPosition)
                .firstOrDefault(new MarkedDamage(xPosition, yPosition)).subscribe(this::setMarkedDamage);
    }

    private void setMarkedDamage(MarkedDamage markedDamage) {
        if(!isViewAttached()){
            return;
        }
        getView().setMarkedDamage(markedDamage);
        getView().initViews(); //
    }
}
