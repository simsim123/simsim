package com.github.andrdev.bolservice.model.realms;

import android.os.Parcel;
import android.os.Parcelable;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class CustomerRealm extends RealmObject{

    public static final String ID = "id";
    public static final String CUSTOMER_NAME = "customerName";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String PHONE = "phone";
    public static final String CELL = "cell";

    @PrimaryKey
    private int id;
    private String customerName;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private String cell;

    public CustomerRealm() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
