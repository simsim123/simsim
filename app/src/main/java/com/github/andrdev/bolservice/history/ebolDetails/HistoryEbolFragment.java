package com.github.andrdev.bolservice.history.ebolDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.history.details.damages.HistoryCarDamagesActivity;
import com.github.andrdev.bolservice.history.details.vehicleInfo.HistoryViActivity;
import com.github.andrdev.bolservice.invoices.InvoicesActivity;
import com.github.andrdev.bolservice.invoices.create.InvoiceNewFragment;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.share.ActivityShare;
import com.github.andrdev.bolservice.view.customerNotEditable.CustomerNeBWidget;
import com.github.andrdev.bolservice.view.customerNotEditable.CustomerNeWidget;
import com.github.andrdev.bolservice.view.vehicleInfos.VehicleInfosWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryEbolFragment extends MvpFragment<HistoryEbolView, HistoryEbolPresenter>
        implements HistoryEbolView {

    @Bind(R.id.originOrderId)
    protected TextView originOrderId;

    @Bind(R.id.originDate)
    protected TextView originDate;

    @Bind(R.id.vehiclesHost)
    VehicleInfosWidget vehicleInfos;

    @Bind(R.id.customerA)
    CustomerNeWidget customerA;

    @Bind(R.id.customerB)
    CustomerNeBWidget customerB;

    @Bind(R.id.pickupCustomerSign)
    ImageView pickupCustomerSign;

    @Bind(R.id.pickupDriverSign)
    ImageView pickupDriverSign;

    @Bind(R.id.deliveryCustomerSign)
    ImageView deliveryCustomerSign;

    @Bind(R.id.deliveryDriverSign)
    ImageView deliveryDriverSign;

    protected NewEbol currentEbol;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_ebol, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void setData(NewEbol ebol) {
        currentEbol = ebol;
        showData();
    }

    protected void showData() {
        if (currentEbol == null) {
            return;
        }
        showVehicleInfos();
        setCommonFields(currentEbol);
        showCustomersInfos();
        setSignImages(currentEbol);
    }

    private void showVehicleInfos() {
        vehicleInfos.setVehiclesInfos(currentEbol.getVehicleInfos());
        vehicleInfos.setCallback(new VehicleInfosWidget.VehicleInfosCallback() {
            @Override
            public void detailsClickCb(VehicleInfo vehicleInfo) {
                detailsClick(vehicleInfo);
            }

            @Override
            public void damagesClickCb(VehicleInfo vehicleInfo) {
                damagesClick(vehicleInfo);
            }
        });
        vehicleInfos.showData();
    }

    private void showCustomersInfos() {
        if(currentEbol.getDestination() != null) {
            setDestinationCustomerFields(currentEbol.getDestination());
        }
        if(currentEbol.getOrigin() != null) {
            setOriginCustomerFields(currentEbol.getOrigin());
        }
    }

    private void setCommonFields(NewEbol ebol) {
        originOrderId.setText(ebol.getOrderId());
        originDate.setText(ebol.getDate());
    }

    private void setOriginCustomerFields(Customer origin) {
        customerA.setCustomerFields(origin);
    }

    private void setDestinationCustomerFields(Customer destination) {
        customerB.setCustomerFields(destination);
    }

    protected void detailsClick(VehicleInfo vehInfo) {
        getPresenter().detailsClick(vehInfo.getId());
    }

    @Override
    public void showVehicleDetails(int vehicleId) {
        Intent intent = new Intent(getActivity(), HistoryViActivity.class);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    protected void damagesClick(VehicleInfo vehInfo) {
        getPresenter().damagesClick(vehInfo.getId());
    }

    @Override
    public void showDamages(int vehicleId) {
        Intent intent = new Intent(getActivity(), HistoryCarDamagesActivity.class);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public HistoryEbolPresenter createPresenter() {
        return new HistoryEbolPresenter();
    }

    protected void setSignImages(NewEbol ebol) {
        Picasso picasso = Picasso.with(getContext());
        if(ebol.getCustomerSignPath() != null && !TextUtils.isEmpty(ebol.getCustomerSignPath())){
            setSignPic(picasso, ebol.getCustomerSignPath(), pickupCustomerSign);
        }
        if(ebol.getDriverSignPath() != null && !TextUtils.isEmpty(ebol.getDriverSignPath())){
            setSignPic(picasso, ebol.getDriverSignPath(), pickupDriverSign);
        }
        if(ebol.getDeliveryCustomerSignPath() != null && !TextUtils.isEmpty(ebol.getDeliveryCustomerSignPath())){
            setSignPic(picasso, ebol.getDeliveryCustomerSignPath(), deliveryCustomerSign);
        }
        if(ebol.getDeliveryDriverSignPath() != null && !TextUtils.isEmpty(ebol.getDeliveryDriverSignPath())){
            setSignPic(picasso, ebol.getDeliveryDriverSignPath(), deliveryDriverSign);
        }

    }

    private void setSignPic(Picasso picasso, String photoPath, ImageView photo) {
        File file = new File(photoPath);
        picasso.invalidate(file);
        picasso.load(file).fit().centerCrop().into(photo);;
    }

    @OnClick(R.id.share)
    void onShareClick() {
        Intent intent = new Intent(getContext(), ActivityShare.class);
        startActivity(intent);
    }

    @OnClick(R.id.invoice)
    void onInvoiceClick() {
        Intent intent = new Intent(getContext(), InvoicesActivity.class);
        intent.putExtra(InvoicesActivity.OPEN_NEW, true);
        startActivity(intent);
    }

    @OnClick(R.id.backIm)
    void onBackClick() {
        getActivity().onBackPressed();
    }
}
