package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Notification;

import java.util.List;


public class NotificationsResponse {

    String status;
    List<Notification> notifications;

    public NotificationsResponse() {
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
