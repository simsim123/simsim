package com.github.andrdev.bolservice.colleagues;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Colleague;

import butterknife.Bind;


public class ColleaguesAdapter extends BaseAdapter<Colleague, ColleaguesAdapter.ColleaguesHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_drivers;
    }

    @Override
    protected ColleaguesHolder getHolder(View view) {
        return new ColleaguesHolder(view);
    }

    protected class ColleaguesHolder extends BaseAdapter.BaseViewHolder<Colleague> {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.phone)
        TextView phone;

        public ColleaguesHolder(View itemView) {
            super(itemView);
        }

        protected void bind(Colleague colleague) {
            name.setText(colleague.getFullNameFormatted());
            phone.setText(colleague.getDriverPhone());
        }
    }
}
