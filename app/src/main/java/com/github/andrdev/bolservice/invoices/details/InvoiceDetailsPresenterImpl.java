package com.github.andrdev.bolservice.invoices.details;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Invoice;

public class InvoiceDetailsPresenterImpl extends InvoiceDetailsPresenter {

    InvoiceDetailsDataProvider dataProvider;

    @Override
    public void attachView(InvoiceDetailsView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new InvoiceDetailsDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @SuppressWarnings("ConstantConditions")
    public void getData() {
        if(!isViewAttached()) {
            return;
        }
        dataProvider.getSavedInvoice(this::setData);
    }

    @SuppressWarnings("ConstantConditions")
    public void setData(Invoice invoice) {
        if(!isViewAttached()) {
            return;
        }
        getView().setData(invoice);
        getView().initFields();
    }
}
