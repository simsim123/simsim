package com.github.andrdev.bolservice.invoices.create;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


import com.github.andrdev.bolservice.dialogs.DatePickerFragment;
import com.github.andrdev.bolservice.dialogs.DialogFactory;
import com.github.andrdev.bolservice.dialogs.AddressAutocompleteDialogFragment;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.dialogs.SelectClientDialogFrament;
import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.requests.InvoiceRequestBuilder;

import com.github.andrdev.bolservice.share.ActivityShare;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class InvoiceNewFragment extends MvpFragment<InvoiceNewView, InvoiceNewPresenter>
        implements InvoiceNewView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @Bind(R.id.customerName)
    EditText customerName;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.state)
    EditText state;

    @Bind(R.id.city)
    EditText city;

    @Bind(R.id.zip)
    EditText zip;

    @Bind(R.id.phone)
    EditText phone;

    @Bind(R.id.cell)
    EditText cell;

    @Bind(R.id.invoiceId)
    EditText invoiceId;

    @Bind(R.id.date)
    Button date;

    @Bind(R.id.paymentTerms)
    Spinner paymentTerms;

    @Bind(R.id.cost)
    EditText cost;

    @Bind(R.id.notes)
    EditText notes;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice_new, container, false);
        ButterKnife.bind(this, view);
        initSpinner();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter();
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.paymentTerms, R.layout.spinner_item_payment_terms);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentTerms.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
        paymentTerms.setAdapter(adapter);
    }

    @OnClick(R.id.customerNameButton)
    void customerNameButtonClick() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
        newFragment.setCallback((client) -> {
            customerName.setText(client.getCustomerName());
            address.setText(client.getAddress());
            city.setText(client.getCity());
            state.setText(client.getState());
            zip.setText(client.getZIP());
            phone.setText(client.getPhone());
        });
        newFragment.show(ft, "dialog");
    }

    @OnClick({R.id.date})
    void dateSetClick(View view) {
//        DatePickerFragment newFragment = new DatePickerFragment();
//        newFragment.setCallback(new DatePickerFragment.OnDateSetCallback() {
//            @Override
//            public void dateSet(int year, int month, int day) {
//                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//                Calendar calendar = new GregorianCalendar();
//                calendar.set(year, month, day);
//                String formatedDate = sdf.format(calendar.getTime());
//                date.setText(formatedDate);
//            }
//        });
//        newFragment.show(getFragmentManager(), "datePicker");

        DatePickerFragment.OnDateSetCallback dateCallback = (year, month, day) -> setDate(year, month, day);

        DialogFactory.createDatePickerDialog(dateCallback).show(getFragmentManager(), "datePicker");
    }

    @OnClick(R.id.send)
    void sendInvoiceRequest() {
        if(!enteredInfoIsOk()) return;

        InvoiceRequest request = new InvoiceRequestBuilder()
                .setAddress(address)
                .setAmount(cost)
                .setCell(cell)
                .setCity(city)
                .setFullName(customerName)
                .setInvoiceId(invoiceId)
                .setNotes(notes)
                .setOrderId(invoiceId)
                .setPaymentTerms((String) paymentTerms.getSelectedItem())
                .setZip(zip)
                .setState(state)
                .setPhone(phone)
                .createInvoiceRequest();

        getPresenter().createInvoice(request);
    }

    @Override
    public void openShare() {
        Intent intent = new Intent(getContext(), ActivityShare.class);
        startActivity(intent);
    }

    private boolean enteredInfoIsOk() {
        return Utils.hasText(errorText, address, customerName, invoiceId);
    }

    @OnClick(R.id.locationButton)
    void updateLocation() {
        getAddress();
    }

    private void getAddress() {
        getPresenter().getAddress();
    }


    public void buildAlertMessageNoGps() {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Yes", (dialog, id) ->
//                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
//                .setNeutralButton("Use Google Places", (dialog, which) -> selectAddressFromGooglePlaces())
//                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
//        final AlertDialog alert = builder.create();

        DialogInterface.OnClickListener clickListener = (dialog, which) -> selectAddressFromGooglePlaces();

        DialogFactory.createAskToTurnOnGpsDialog(getContext(), clickListener).show();
    }

    @Override
    public void selectAddressFromGooglePlaces() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        PlacesAutocompleteDialogFragment newFragment = PlacesAutocompleteDialogFragment.newInstance();
//        newFragment.setCallback(new PlacesAutocompleteDialogFragment.SelectedCallback() {
//                                    @Override
//                                    public void itemSelected(Address gAddress) {
//                                        setAddress(gAddress);
//                                    }
//                                }
//        );
//        newFragment.show(ft, "dialog");


        AddressAutocompleteDialogFragment.SelectedCallback selectedCallback =
                (gAddress) -> setAddress(gAddress);
        DialogFactory.createSelectFromPlacesDialog(getContext(), selectedCallback)
                .show(getFragmentManager(), "datePicker");
    }

    private void setDate(int year, int month, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        String formatedDate = sdf.format(calendar.getTime());
        date.setText(formatedDate);
    }
    @Override
    public void setAddress(Address gAddress) {
        address.setText(gAddress.getThoroughfare());
        zip.setText(gAddress.getPostalCode());
        state.setText(gAddress.getAdminArea());
        city.setText(gAddress.getLocality());
    }

    @Override
    public void showFailedToast() {

    }

    @Override
    public void backToInvoices() {
        getActivity().onBackPressed();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @NonNull
    @Override
    public InvoiceNewPresenter createPresenter() {
        return new InvoiceNewPresenterImpl();
    }
}
