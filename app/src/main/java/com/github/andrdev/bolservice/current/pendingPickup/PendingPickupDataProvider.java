package com.github.andrdev.bolservice.current.pendingPickup;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PendingPickupDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void getNetEbols(DataCb<EbolsResponse> successCb,
                            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getSubmits("pendingPickup",
                successCb::returnData, failedCb::returnData);
//        NewEbol ebol = new NewEbol();
//        ebol.setType("temp");
//        ebol.setSubmitId(1010);
//        ebol.setDate("10/10/2017");
//        ebol.setVehiclesCount(10);
//        Customer customer = new Customer();
//        customer.setAddress("somedri");
//        customer.setCell("333323");
//        customer.setCity("rferre");
//        customer.setPhone("3454254");
//        customer.setCustomerName("dr3oro");
//        customer.setState("f4dfrsf");
//        customer.setZip("4324233");
//        Customer origin = new Customer();
//        origin.setAddress("somedri");
//        origin.setCell("33333");
//        origin.setCity("rfere");
//        origin.setPhone("345454");
//        origin.setCustomerName("droro");
//        origin.setState("fdfsf");
//        origin.setZip("432423");
//        ebol.setOrigin(origin);
//        ebol.setDestination(customer);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        ebol.setSubmitId(1010);
//        EbolsResponse eo = new EbolsResponse();
//        List<NewEbol> ebolList = new ArrayList<>();
//        ebolList.add(ebol);
//        eo.setEbols(ebolList);
//        successCb.returnData(eo);
    }

    public void getVehicles(String ebolId, DataCb<VehicleInfoResponse> successCb,
                            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getVehicles(ebolId,
                successCb::returnData, failedCb::returnData);
//        VehicleInfoResponse vir = new VehicleInfoResponse();
//        vir.setVehicleInfos(new ArrayList<>());
//        VehicleInfo vi = new VehicleInfo();
//        vi.setYearOfMake("432");
//        vi.setCid(2);
//        vi.setColor("gren");
//        vi.setMileage("33");
//        vi.setMake("susu");
//        vi.setPlateNumber("43243234");
//        vi.setVinNumber("923929");
//        vi.setModel("suzu");
//        Damages damages = new Damages();
//        damages.setCid(2);
//        damages.setFloorMats(2);
//        damages.setRemotes(1);
//        damages.setFloorMats(3);
//        damages.setHeadrest(5);
//        damages.setCargoCovers(true);
//        damages.setManuals(false);
//        damages.setRadio(false);
//        damages.setSpareTires(true);
//        vi.setDamages(damages);
//        vir.getVehicleInfos().add(vi);
//        successCb.returnData(vir);
    }

    public void deleteTempEbol(DataCb<Boolean> successCb) {
        dbHelper.deleteTempEbol(successCb::returnData);
    }

    public void saveEbol(NewEbol ebol, DataCb<Boolean> successCb) {
        dbHelper.updateSaveEbol(ebol, successCb::returnData);
    }

    public void getVehicleDamages(int cid, DataCb<DamagesResponse> successCb) {
//        DamagesResponse dr = new DamagesResponse();
//        dr.setVehiclePhotos(new HashMap<>());
//        VehiclePhoto photof = new VehiclePhoto();
//        photof.setPhotoPath("https://i.imgur.com/OmcLt1s.jpg");
//        photof.setNote("somenot");
//        photof.setPhotoType("frontPhoto");
//        photof.setMarkedDamages(new ArrayList<>());
//        photof.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto photob = new VehiclePhoto();
//        photob.setPhotoPath("https://i.imgur.com/OmcLt1s.jpg");
//        photob.setNote("somenotb");
//        photob.setPhotoType("backPhoto");
//        photob.setMarkedDamages(new ArrayList<>());
//        photob.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto photor = new VehiclePhoto();
//        photor.setPhotoPath("https://i.imgur.com/OmcLt1s.jpg");
//        photor.setNote("photor");
//        photor.setPhotoType("rightSidePhoto");
//        photor.setMarkedDamages(new ArrayList<>());
//        photor.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto photol = new VehiclePhoto();
//        photol.setPhotoPath("https://i.imgur.com/OmcLt1s.jpg");
//        photol.setNote("photol");
//        photol.setPhotoType("leftSidePhoto");
//        photol.setMarkedDamages(new ArrayList<>());
//        photol.setMarkedDamagesDelivery(new ArrayList<>());
//        MarkedDamage dm = new MarkedDamage();
//        dm.setDamageTypes(new ArrayList<>());
//        dm.setxPosition(44.44);
//        dm.setyPosition(14.55);
//        DamageType dt = new DamageType();
//        dt.setState(true);
//        dt.setDamageName("(C)-Cut");
//        dt.setDamageShortName("C");
//        dm.getDamageTypes().add(dt);
//        photof.getMarkedDamages().add(dm);
//        dr.getVehiclePhotos().put("front", photof);
//        dr.getVehiclePhotos().put("back", photob);
//        dr.getVehiclePhotos().put("rightside", photor);
//        dr.getVehiclePhotos().put("leftside", photol);
//        successCb.returnData(dr);
        EbolNetworkWorker2.getInstance().getDamages(String.valueOf(cid),
                successCb::returnData);
    }

    public void getDbEbols(DataCb<List<NewEbol>> successCb,
                           DataCb<Throwable> failedCb) {
        dbHelper.getDbEbols(successCb::returnData);
    }

    public void deleteNetSaveLocalEbol() {
        dbHelper.deleteNetSaveLocalEbol();
    }
}
