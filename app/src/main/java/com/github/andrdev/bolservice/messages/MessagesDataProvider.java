package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class MessagesDataProvider implements DataProvider {
    @Override
    public void deInit() {

    }

    public void getConversationsList(DataCb<ConversationsResponse> successCb,
                                     DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getConversationsList(
                successCb::returnData, failedCb::returnData);
    }

    public void markAsReadMessages(String conversationId, String lastMessageId) {
        EbolNetworkWorker2.getInstance().markAsReadMessages(conversationId, lastMessageId);
    }
}
