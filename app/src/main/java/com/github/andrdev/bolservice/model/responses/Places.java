package com.github.andrdev.bolservice.model.responses;

public final class Places {
    public final Result results[];
    public final String status;

    public Places(Result[] results, String status){
        this.results = results;
        this.status = status;
    }

    public Result[] getResults() {
        return results;
    }

    public String getStatus() {
        return status;
    }

    public static final class Result {
        public final AddressComponent address_components[];
        public final String formatted_address;
        public final Geometry geometry;
        public final String place_id;
        public final String[] types;

        public Result(AddressComponent[] address_components, String formatted_address, Geometry geometry, String place_id, String[] types){
            this.address_components = address_components;
            this.formatted_address = formatted_address;
            this.geometry = geometry;
            this.place_id = place_id;
            this.types = types;
        }

        public AddressComponent[] getAddress_components() {
            return address_components;
        }

        public String getFormatted_address() {
            return formatted_address;
        }

        public Geometry getGeometry() {
            return geometry;
        }

        public String getPlace_id() {
            return place_id;
        }

        public String[] getTypes() {
            return types;
        }

        public static final class AddressComponent {
            public final String long_name;
            public final String short_name;
            public final String[] types;

            public AddressComponent(String long_name, String short_name, String[] types){
                this.long_name = long_name;
                this.short_name = short_name;
                this.types = types;
            }

            public String getLong_name() {
                return long_name;
            }

            public String getShort_name() {
                return short_name;
            }

            public String[] getTypes() {
                return types;
            }
        }

        public static final class Geometry {
            public final Location location;
            public final String location_type;
            public final Viewport viewport;

            public Geometry(Location location, String location_type, Viewport viewport){
                this.location = location;
                this.location_type = location_type;
                this.viewport = viewport;
            }

            public static final class Location {
                public final double lat;
                public final double lng;

                public Location(double lat, double lng){
                    this.lat = lat;
                    this.lng = lng;
                }
            }

            public static final class Viewport {
                public final Northeast northeast;
                public final Southwest southwest;

                public Viewport(Northeast northeast, Southwest southwest){
                    this.northeast = northeast;
                    this.southwest = southwest;
                }

                public static final class Northeast {
                    public final double lat;
                    public final double lng;

                    public Northeast(double lat, double lng){
                        this.lat = lat;
                        this.lng = lng;
                    }
                }

                public static final class Southwest {
                    public final double lat;
                    public final double lng;

                    public Southwest(double lat, double lng){
                        this.lat = lat;
                        this.lng = lng;
                    }
                }
            }
        }
    }
}