package com.github.andrdev.bolservice.clients.edit;

import android.content.Context;

import com.github.andrdev.bolservice.model.Client;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ClientEditView extends MvpView {
    void initFields();

    void backToClientDetails();

    void setData(Client data);

    void showFailedToast();

    Context getViewContext();
}
