package com.github.andrdev.bolservice.view.customerNotEditable;

import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.dialogs.AddressAutocompleteDialogFragment;
import com.github.andrdev.bolservice.dialogs.DialogFactory;
import com.github.andrdev.bolservice.dialogs.SelectClientDialogFrament;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.hannesdorfmann.mosby.mvp.layout.MvpRelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerNeBWidget
        extends MvpRelativeLayout<CustomerNeView, CustomerNePresenter>
        implements CustomerNeView {

    @Bind(R.id.customerName)
    protected TextView customerName;

    @Bind(R.id.address)
    protected TextView address;

    @Bind(R.id.city)
    protected TextView city;

    @Bind(R.id.zip)
    protected TextView zip;

    @Bind(R.id.state)
    protected TextView state;

    @Bind(R.id.phone)
    protected TextView phone;

    @Bind(R.id.cell)
    protected TextView cell;

    FragmentManager fragmentManager;

    public CustomerNeBWidget(Context context) {
        super(context);
        init(context);
    }

    public CustomerNeBWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomerNeBWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public CustomerNeBWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_customer_ne_b, this);
        ButterKnife.bind(this, view);
    }

    @Override
    public CustomerNePresenter createPresenter() {
        return new CustomerNePresenterImpl();
    }

    @Override
    public void setCustomerFields(Customer customer) {
        customerName.setText(customer.getCustomerName());
        address.setText(customer.getAddress());
        city.setText(customer.getCity());
        state.setText(customer.getState());
        zip.setText(customer.getZip());
        phone.setText(customer.getPhone());
        cell.setText(customer.getCell());
    }
}