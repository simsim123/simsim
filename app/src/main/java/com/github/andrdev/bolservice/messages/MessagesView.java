package com.github.andrdev.bolservice.messages;

import com.hannesdorfmann.mosby.mvp.MvpView;


public interface MessagesView extends MvpView {

    void showNoDataText();

    void showRequestFailedToast();
}
