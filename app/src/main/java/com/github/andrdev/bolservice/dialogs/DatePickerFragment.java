package com.github.andrdev.bolservice.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;


public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    OnDateSetCallback callback;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void setCallback(OnDateSetCallback callback) {
        this.callback = callback;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if(callback != null){
            callback.dateSet(year, month, day);
        }
    }

    public interface OnDateSetCallback{
        void dateSet(int year, int month, int day);
    }
}
