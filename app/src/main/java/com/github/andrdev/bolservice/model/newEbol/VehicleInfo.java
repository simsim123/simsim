package com.github.andrdev.bolservice.model.newEbol;


import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;

public class VehicleInfo {

    private int id;
    private int cid = -1;
    private String yearOfMake;
    private String make;
    private String model;
    private String vinNumber;
    private String mileage;
    private String color;
    private String plateNumber;
    private String type;
    private Damages damages;

    public VehicleInfo() {
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Damages getDamages() {
        return damages;
    }

    public void setDamages(Damages damages) {
        this.damages = damages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(String yearOfMake) {
        this.yearOfMake = yearOfMake;
    }

    public boolean isLocal() {
        return cid == -1;
    }

    public boolean hasPhoto(String type) {
        if(damages == null)
            return false;
        switch (type) {
            case VehiclePhotosWidget.FRONT_PHOTO:
                return damages.getFront() != null
                        && damages.getFront().getPhotoPath() != null
                        && !damages.getFront().getPhotoPath().isEmpty();
            case VehiclePhotosWidget.BACK_PHOTO:
                return damages.getBack() != null
                        && damages.getBack().getPhotoPath() != null
                        && !damages.getBack().getPhotoPath().isEmpty();
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                return damages.getLeftSide() != null
                        && damages.getLeftSide().getPhotoPath() != null
                        && !damages.getLeftSide().getPhotoPath().isEmpty();
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                return damages.getRightSide() != null
                        && damages.getRightSide().getPhotoPath() != null
                        && !damages.getRightSide().getPhotoPath().isEmpty();
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "VehicleInfo{" +
                "cid=" + cid +
                ", id=" + id +
                ", yearOfMake='" + yearOfMake + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", vinNumber='" + vinNumber + '\'' +
                ", mileage='" + mileage + '\'' +
                ", color='" + color + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                ", type='" + type + '\'' +
                ", damages=" + damages +
                '}';
    }
}
