package com.github.andrdev.bolservice.current.pendingPickup;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.util.List;

public interface PendingPickupView extends BaseSearchView<NewEbol> {
    void openNewEbol();
    void addEbols(List<NewEbol> data);
}
