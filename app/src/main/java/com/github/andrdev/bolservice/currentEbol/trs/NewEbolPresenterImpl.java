package com.github.andrdev.bolservice.currentEbol.trs;

import android.location.Address;
import android.location.Location;


import com.github.andrdev.bolservice.GpsAddressProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.util.List;


public class NewEbolPresenterImpl extends com.github.andrdev.bolservice.currentEbol.trs.NewEbolPresenter
        implements GpsAddressProvider.GpsHandlerCallback {

    com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider dataProvider;

    @Override
    public void attachView(CurrentFEbolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider();
        dataProvider.setMapsGpsHandler(new GpsAddressProvider(getView().getViewContext()));
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getEbol(this::setEbol);
    }

    private void setEbol(NewEbol newEbol) {
        if (!isViewAttached()) {
            return;
        }

        if (newEbol != null) {
            getView().setData(newEbol);
        }
    }

    @Override
    public void saveEbol(com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSave();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }

    @Override
    public void finishEbolCreation() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openSendActivity);
    }

    private void openSendActivity(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().finishEbol();
    }

    @Override
    public void addNewVehicle() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::addNewVehicleEbolSaved);
    }

    private void addNewVehicleEbolSaved(boolean saved) {
        dataProvider.removeTempVehicle(this::openVehicleCreation);
    }

    private void openVehicleCreation(boolean cleared) {
        if (!isViewAttached()) {
            return;
        }
        getView().openNewVehicleInfoActivity();
    }

    @Override
    public void detailsClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showVehicleDetails(vehicleId));
    }

    public void showVehicleDetails(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showVehicleDetails(vehicleId);
    }

    @Override
    public void damagesClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showDamages(vehicleId));
    }

    public void showDamages(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showDamages(vehicleId);
    }

    @Override
    public void customerSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openCustomerSignature);
    }

    private void openCustomerSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openCustomerSignature();
    }

    @Override
    public void driverSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openDriverSignature);
    }

    private void openDriverSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openCustomerSignature();
    }

    @Override
    public void unableToSignClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::showUnableToSign);
    }

    public void showUnableToSign(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().showUnableToSign();
    }

    @Override
    public void getAddress() {
        if(!isViewAttached()){
            return;
        }
        dataProvider.checkForGpsPermission(this::permissionsResult);
    }

    @Override
    public void permissionsResult(Boolean granted) {
        if (granted == null) {
            return;
        }

        if (granted) {
            isGpsTurnedOn();
        } else {
            noGpsPermission();
        }
    }

    @Override
    public void isGpsTurnedOn() {
        dataProvider.checkGpsIsTurnedOn(this::isGpsTurnedOn);
    }

    @Override
    public void isGpsTurnedOn(Boolean isGpsTurnedOn) {
        if (isGpsTurnedOn == null) {
            return;
        }

        if (isGpsTurnedOn) {
            getLocation();
        } else {
            gpsTurnedOff();
        }
    }

    @Override
    public void getLocation() {
        dataProvider.getGpsCoordinates(this::getAddressFromLocation);
    }

    @Override
    public void getAddressFromLocation(Location location) {
        dataProvider.getAddressFromCoordinates(location, this::gpsAddressSuccessOb);
    }

    @Override
    public void gpsAddressSuccessOb(List<Address> addresses) {
        addressReceived(addresses.get(0));
    }

    @Override
    public void noGpsPermission() {
        if(!isViewAttached()) {
            return;
        }
        getView().selectAddressFromGooglePlacesOrigin();
    }

    @Override
    public void addressReceived(Address address) {
        if(!isViewAttached()) {
            return;
        }
        getView().setAddressFromPredictionOrigin(address);
    }

    @Override
    public void gpsTurnedOff() {
        if(!isViewAttached()) {
            return;
        }
        getView().buildAlertMessageNoGps();
    }
}
