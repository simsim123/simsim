package com.github.andrdev.bolservice.notifications;

import android.util.Log;

import com.github.andrdev.bolservice.model.Notification;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class NotificationsPresenterImpl extends NotificationsPresenter {

    @Override
    public void getData() {
        if(!isViewAttached()){
            return;
        }

        EbolNetworkWorker2.getInstance().getNotifications(
                this::getDataRequestSuccess,
                this::getDataRequestFailed);
    }

    @Override
    public void getDataRequestSuccess(NotificationsResponse notificationsResponse) {
        if(!isViewAttached()) {
            return;
        }

        if(notificationsResponse.getStatus().equals("true")) {
            Observable.from(notificationsResponse.getNotifications())
                    .filter(notification -> notification.getStatus().equals("new"))
                    .toList().subscribe(this::setNotifications);
        } else {
            getView().showFailedToast();
        }
    }

    public void setNotifications(List<Notification> notificationsFiltered) {
        if(!isViewAttached()) {
            return;
        }
        getView().setData(notificationsFiltered);
    }

    @Override
    public void getDataRequestFailed(Throwable throwable) {
        if(!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }
}
