package com.github.andrdev.bolservice.colleagues.create;

import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ColleagueNewPresenter extends MvpBasePresenter<ColleagueNewView> {

    abstract void createDriver(CreateDriverRequest request);

    abstract void requestFailed(Throwable throwable);

    abstract void requestSuccess(CreateDriverResponse simpleResponse);
}
