package com.github.andrdev.bolservice.model.realms;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class DamageTypeRealm extends RealmObject{

    public static final String ID = "id";
    public static final String DAMAGE_SHORT_NAME = "damageShortName";
    public static final String DAMAGE_NAME = "damageName";
    public static final String STATE = "state";


    @PrimaryKey
    private int id;
    private String damageShortName;
    private String damageName;
    private boolean state;

    public DamageTypeRealm() {
    }

    public DamageTypeRealm(String damageName, String damageShortName, boolean state) {
        this.damageName = damageName;
        this.damageShortName = damageShortName;
        this.state = state;
    }

    public String getDamageName() {
        return damageName;
    }

    public void setDamageName(String damageName) {
        this.damageName = damageName;
    }

    public String getDamageShortName() {
        return damageShortName;
    }

    public void setDamageShortName(String damageShortName) {
        this.damageShortName = damageShortName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean isState() {
        return state;
    }
}
