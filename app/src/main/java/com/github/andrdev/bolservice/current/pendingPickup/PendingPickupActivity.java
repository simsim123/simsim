package com.github.andrdev.bolservice.current.pendingPickup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.database.RealmDbData;

import butterknife.ButterKnife;


public class PendingPickupActivity  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_pickup);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showPendingPickupFragment();
        }
    }

    private void showPendingPickupFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new PendingPickupFragment()).commit();
    }
}