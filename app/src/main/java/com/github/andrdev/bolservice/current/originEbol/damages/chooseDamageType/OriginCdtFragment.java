package com.github.andrdev.bolservice.current.originEbol.damages.chooseDamageType;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.damages.OriginDamagesFragment;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.DamagesAdapter;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OriginCdtFragment extends MvpFragment<OriginCdtView, OriginCdtPresenter>
        implements OriginCdtView {


    private double yPosition;
    private double xPosition;
    List<DamageType> damageTypes = new ArrayList<>();

    {
        damageTypes.add(new DamageType("(S)-Scratched", "S", false));
        damageTypes.add(new DamageType("(D)-Dented", "D", false));
        damageTypes.add(new DamageType("(PC)-Paint Chip", "PC", false));
        damageTypes.add(new DamageType("(CR)-Cracked", "CR", false));
        damageTypes.add(new DamageType("(C)-Cut", "C", false));
        damageTypes.add(new DamageType("(BR)-Broken", "BR", false));
        damageTypes.add(new DamageType("(MD)-Major Damage", "MD", false));
        damageTypes.add(new DamageType("(B)-Bent", "B", false));
        damageTypes.add(new DamageType("(L)-Loose", "L", false));
        damageTypes.add(new DamageType("(M)-Missing", "M", false));
        damageTypes.add(new DamageType("(ST)-Stained", "ST", false));
        damageTypes.add(new DamageType("(F)-Faded", "F", false));
        damageTypes.add(new DamageType("(T)-Torn", "T", false));
        damageTypes.add(new DamageType("(FF)-Foreign Fluid", "FF", false));
        damageTypes.add(new DamageType("(G)-Gouged", "G", false));
        damageTypes.add(new DamageType("(P)-Pitted", "P", false));
        damageTypes.add(new DamageType("(SL)-Soiled", "SL", false));
    }

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    DamagesAdapter damagesAdapter;
    MarkedDamage markedDamage;
    VehiclePhoto tempVehiclePhoto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_damage, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        return view;
    }

    @Override
    public OriginCdtPresenter createPresenter() {
        return new OriginCdtPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        this.yPosition = getArguments().getDouble("yPosition");
        this.xPosition = getArguments().getDouble("xPosition");
        int selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        String photoType = getArguments().getString(OriginDamagesFragment.PHOTO_TYPE);
        getPresenter().getData(photoType, yPosition, xPosition, selectedVehicleId);
    }

    public void setMarkedDamage(MarkedDamage markedDamage) {
        this.markedDamage = markedDamage;
    }

    public void initViews() {
        if (markedDamage.getDamageTypes() != null) {
//            Observable.from(markedDamage.getDamageTypes()).forEach(savedDamage ->
//                    Observable.from(damageTypes).filter(damageType ->
//                            savedDamage.getDamageName().equals(damageType.getDamageName()))
//                            .forEach(damageType -> damageType.setState(savedDamage.getState())));
            for (DamageType type : markedDamage.getDamageTypes()) {
                for (DamageType tempDamage : damageTypes) {
                    if (tempDamage.getDamageName().equals(type.getDamageName())) {
                        tempDamage.setState(true);
                    }
                }
            }
            damagesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public VehiclePhoto getVehiclePhoto() {
        return tempVehiclePhoto;
    }

    @Override
    public void setVehiclePhoto(VehiclePhoto vehiclePhoto) {
        tempVehiclePhoto = vehiclePhoto;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }


    private void initRecycler() {
        damagesAdapter = new DamagesAdapter();
        damagesAdapter.setData(damageTypes);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(damagesAdapter);
    }

    @OnClick(R.id.ok)
    void saveClick(View view) {
        getActivity().finish();
    }
}
