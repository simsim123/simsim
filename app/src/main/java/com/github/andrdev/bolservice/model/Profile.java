package com.github.andrdev.bolservice.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Profile extends RealmObject {

    public static final String ID = "id";
    public static final String IS_BOSS = "isBoss";
    public static final String USERNAME = "username";
    public static final String FULL_NAME_FORMATTED = "fullNameFormatted";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String BILLING_FIRSTNAME = "billingFirstname";
    public static final String BILLING_LASTNAME = "billingLastname";
    public static final String BILLING_ADDRESS = "billingAddress";
    public static final String BILLING_CITY = "billingCity";
    public static final String BILLING_STATE = "billingState";
    public static final String BILLING_ZIP = "billingZip";
    public static final String BILLING_PHONE = "billingPhone";
    public static final String BILLING_FAX = "billingFax";
    public static final String BILLING_EMAIL = "billingEmail";
    public static final String COMPANY_NAME = "companyName";
    public static final String COMPANY_ADDRESS = "companyAddress";
    public static final String COMPANY_CITY = "companyCity";
    public static final String COMPANY_STATE = "companyState";
    public static final String COMPANY_ZIP = "companyZip";
    public static final String COMPANY_PHONE = "companyPhone";
    public static final String COMPANY_FAX = "companyFax";
    public static final String COMPANY_EMAIL = "companyEmail";
    public static final String COMPANY_WEBSITE = "companyWebsite";
    public static final String COMPANY_USDOT = "companyUsdot";
    public static final String COMPANY_MC = "companyMc";
    public static final String DRIVER_EMAIL = "driverEmail";
    public static final String DRIVER_PHONE = "driverPhone";
    public static final String USERTYPE = "usertype";

    @PrimaryKey
    private int id;
    private boolean isBoss;
    private String username;
    private String fullNameFormatted;
    private String firstname;
    private String lastname;
    private String billingFirstname;
    private String billingLastname;
    private String billingAddress;
    private String billingCity;
    private String billingState;
    private String billingZip;
    private String billingPhone;
    private String billingFax;
    private String billingEmail;
    private String companyName;
    private String companyAddress;
    private String companyCity;
    private String companyState;
    private String companyZip;
    private String companyPhone;
    private String companyFax;
    private String companyEmail;
    private String companyWebsite;
    private String companyUsdot;
    private String companyMc;
    private String driverEmail;
    private String driverPhone;
    private String usertype;

    public Profile(){ }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setIsBoss(boolean isBoss){
        this.isBoss = isBoss;
    }
    public boolean getIsBoss(){
        return this.isBoss;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setFullNameFormatted(String fullNameFormatted){
        this.fullNameFormatted = fullNameFormatted;
    }
    public String getFullNameFormatted(){
        return this.fullNameFormatted;
    }
    public void setFirstname(String firstname){
        this.firstname = firstname;
    }
    public String getFirstname(){
        return this.firstname;
    }
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
    public String getLastname(){
        return this.lastname;
    }
    public void setBillingFirstname(String billingFirstname){
        this.billingFirstname = billingFirstname;
    }
    public String getBillingFirstname(){
        return this.billingFirstname;
    }
    public void setBillingLastname(String billingLastname){
        this.billingLastname = billingLastname;
    }
    public String getBillingLastname(){
        return this.billingLastname;
    }
    public void setBillingAddress(String billingAddress){
        this.billingAddress = billingAddress;
    }
    public String getBillingAddress(){
        return this.billingAddress;
    }
    public void setBillingCity(String billingCity){
        this.billingCity = billingCity;
    }
    public String getBillingCity(){
        return this.billingCity;
    }
    public void setBillingState(String billingState){
        this.billingState = billingState;
    }
    public String getBillingState(){
        return this.billingState;
    }
    public void setBillingZip(String billingZip){
        this.billingZip = billingZip;
    }
    public String getBillingZip(){
        return this.billingZip;
    }
    public void setBillingPhone(String billingPhone){
        this.billingPhone = billingPhone;
    }
    public String getBillingPhone(){
        return this.billingPhone;
    }
    public void setBillingFax(String billingFax){
        this.billingFax = billingFax;
    }
    public String getBillingFax(){
        return this.billingFax;
    }
    public void setBillingEmail(String billingEmail){
        this.billingEmail = billingEmail;
    }
    public String getBillingEmail(){
        return this.billingEmail;
    }
    public void setCompanyName(String companyName){
        this.companyName = companyName;
    }
    public String getCompanyName(){
        return this.companyName;
    }
    public void setCompanyAddress(String companyAddress){
        this.companyAddress = companyAddress;
    }
    public String getCompanyAddress(){
        return this.companyAddress;
    }
    public void setCompanyCity(String companyCity){
        this.companyCity = companyCity;
    }
    public String getCompanyCity(){
        return this.companyCity;
    }
    public void setCompanyState(String companyState){
        this.companyState = companyState;
    }
    public String getCompanyState(){
        return this.companyState;
    }
    public void setCompanyZip(String companyZip){
        this.companyZip = companyZip;
    }
    public String getCompanyZip(){
        return this.companyZip;
    }
    public void setCompanyPhone(String companyPhone){
        this.companyPhone = companyPhone;
    }
    public String getCompanyPhone(){
        return this.companyPhone;
    }
    public void setCompanyFax(String companyFax){
        this.companyFax = companyFax;
    }
    public String getCompanyFax(){
        return this.companyFax;
    }
    public void setCompanyEmail(String companyEmail){
        this.companyEmail = companyEmail;
    }
    public String getCompanyEmail(){
        return this.companyEmail;
    }
    public void setCompanyWebsite(String companyWebsite){
        this.companyWebsite = companyWebsite;
    }
    public String getCompanyWebsite(){
        return this.companyWebsite;
    }
    public void setCompanyUsdot(String companyUsdot){
        this.companyUsdot = companyUsdot;
    }
    public String getCompanyUsdot(){
        return this.companyUsdot;
    }
    public void setCompanyMc(String companyMc){
        this.companyMc = companyMc;
    }
    public String getCompanyMc(){
        return this.companyMc;
    }
    public void setDriverEmail(String driverEmail){
        this.driverEmail = driverEmail;
    }
    public String getDriverEmail(){
        return this.driverEmail;
    }
    public void setDriverPhone(String driverPhone){
        this.driverPhone = driverPhone;
    }
    public String getDriverPhone(){
        return this.driverPhone;
    }
    public void setUsertype(String usertype){
        this.usertype = usertype;
    }
    public String getUsertype(){
        return this.usertype;
    }
}

