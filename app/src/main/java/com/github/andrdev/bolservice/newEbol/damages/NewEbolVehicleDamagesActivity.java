package com.github.andrdev.bolservice.newEbol.damages;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolVehicleDamagesActivity extends AppCompatActivity {

    @Bind(R.id.titleTool)
    TextView titleTool;
    @Bind(R.id.subTitle)
    TextView subTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_damages);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showDamagesFragment();
        }
    }

    private void showDamagesFragment() {
        NewFebolDamagesFragment fragment = new NewFebolDamagesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID,
                getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment).commit();
    }

    public void setTitle(String title, String vinNumber) {
        titleTool.setText(title);
        subTitle.setText(vinNumber);
    }

    @OnClick(R.id.backIm)
    void onBackClicked(View view) {
        onBackPressed();
    }
}
