package com.github.andrdev.bolservice.view.vehicleTools;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface VehicleToolsView extends MvpView {
    Context getViewContext();
}
