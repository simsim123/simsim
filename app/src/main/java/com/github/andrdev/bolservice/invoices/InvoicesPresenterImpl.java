package com.github.andrdev.bolservice.invoices;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class InvoicesPresenterImpl extends InvoicesPresenter {

    InvoicesDataProvider dataProvider;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new InvoicesDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData(String status) {
        if (!isViewAttached()) {
            return;
        }

        dataProvider.getInvoices(status,
                this::getDataRequestSuccess,
                this::getDataRequestFailed);
    }

    @Override
    public void getDataRequestSuccess(InvoicesResponse invoicesResponse) {
        if (!isViewAttached()) {
            return;
        }

        getView().stopRefresh();

        if (invoicesResponse.getStatus().equals("true")) {
            getView().setData(invoicesResponse.getInvoices());
        } else {
            getView().showRequestFailedToast();
        }
    }

    @Override
    public void getDataRequestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().stopRefresh();
        getView().showRequestFailedToast();
    }

    @Override
    public void listItemClicked(Invoice invoice) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.saveInvoice(invoice, this::openListItem);
    }

    public void showFailedToast(Exception e) {
        if (!isViewAttached()) {
            return;
        }
        getView().showRequestFailedToast();
    }

    public void openListItem(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openListItem();
    }
}
