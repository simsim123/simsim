package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Vehicle;

import java.util.List;

/**
 * Created by taiyokaze on 9/17/15.
 */
public class VehiclesResponse {
    String status;
    List<Vehicle> cars;

    public List<Vehicle> getCars() {
        return cars;
    }

    public void setCars(List<Vehicle> cars) {
        this.cars = cars;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
