package com.github.andrdev.bolservice.newEbol.damages.camera;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class CameraPresenter extends MvpBasePresenter<CameraView>{


    CameraDataProvider dataProvider;

    @Override
    public void attachView(CameraView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new CameraDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }


    public void getData(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getVehicleById(vehicleId, this::getVehicleSuccess);
    }

    public void getVehicleSuccess(VehicleInfo vehicleInfo) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(vehicleInfo);

    }

    public void saveTempVehiclePhoto(VehiclePhoto tempPhoto, VehicleInfo vehicleInfo) {
        if (!isViewAttached()) {
            return;
        }
        VehiclePhoto photoToDelete = getPhotoToRemove(tempPhoto.getPhotoType(), vehicleInfo.getDamages());
        dataProvider.deleteVehiclePhotoAndTempPhoto(photoToDelete, (deleted) ->
                saveVehiclePhoto(tempPhoto, vehicleInfo.getId()));
    }

    private void saveVehiclePhoto(VehiclePhoto tempPhoto, int vehicleId) {
        dataProvider.savePhoto(tempPhoto, vehicleId, this::saveVehiclePhotoSuccess);
    }

    private VehiclePhoto getPhotoToRemove(String photoType, Damages damages) {
        VehiclePhoto vehiclePhoto = null;
        switch (photoType){
            case VehiclePhotosWidget.FRONT_PHOTO:
                vehiclePhoto = damages.getFront();
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                vehiclePhoto = damages.getBack();
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                vehiclePhoto = damages.getRightSide();
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                vehiclePhoto = damages.getLeftSide();
                break;
            default:
                break;
        }
        return vehiclePhoto;
    }

    public void saveVehiclePhotoSuccess(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().showMarkDamagesFragment();
    }

    @Nullable
    private File writeData(File file, byte[] data) {
        if (file == null) {
            return null;
        }
        if(file.exists()){
            file.delete();
        }
        try {
            //write the file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
            //todo write to db and change fragment and lauch preview
        } catch (FileNotFoundException e) {
            Log.d("dree", e.getMessage());

        } catch (IOException e) {
            Log.d("dree", e.getMessage());

        }
        return file;
    }

    public void takePhoto(VehicleInfo vehicleInfo, File file, String photoType, byte[] data) {
        Observable.just(writeData(file, data))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(picture->takePhotoComplete(picture, vehicleInfo, photoType));
    }

    private void takePhotoComplete(File picture, VehicleInfo vehicleInfo, String photoType) {
        if (picture == null) {
            return;
        }
        VehiclePhoto tempPhoto = getVehiclePhoto(picture, photoType);
        saveTempVehiclePhoto(tempPhoto, vehicleInfo);
    }

    @NonNull
    private VehiclePhoto getVehiclePhoto(File pictureFile, String photoType) {
        VehiclePhoto tempPhoto = new VehiclePhoto();
        tempPhoto.setPhotoType(photoType);
        tempPhoto.setIsTemp(true);
        tempPhoto.setPhotoPath(pictureFile.toString());
        return tempPhoto;
    }
}
