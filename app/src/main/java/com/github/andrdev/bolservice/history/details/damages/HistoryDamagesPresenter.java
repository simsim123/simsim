package com.github.andrdev.bolservice.history.details.damages;

import com.github.andrdev.bolservice.current.originEbol.damages.OriginDamagesDataProvider;
import com.github.andrdev.bolservice.current.originEbol.damages.OriginDamagesView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.history.HistoryDataProvider;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;



public class HistoryDamagesPresenter extends MvpBasePresenter<HistoryDamagesView> {

    HistoryDamagesDataProvider dataProvider;

    @Override
    public void attachView(HistoryDamagesView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HistoryDamagesDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getVehicle(int selectedVehicleId) {
        dataProvider.getVehicleInfoById(selectedVehicleId, this::setCurrentVehicle);
    }

    private void setCurrentVehicle(VehicleInfo vehicleInfo) {
        if(!isViewAttached()){
            return;
        }
        getView().setVehicleInfo(vehicleInfo);
    }
}
