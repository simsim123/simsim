package com.github.andrdev.bolservice.history.details;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class HistoryMarkDamagesPresenter extends MvpBasePresenter<HistoryMarkDamagesView> {

    HistoryMarkDamagesDataProvider dataProvider;

    @Override
    public void attachView(HistoryMarkDamagesView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HistoryMarkDamagesDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData(int selectedVehicleId, String photoType) {
        dataProvider.getVehicleById(selectedVehicleId,
                vehicleInfo -> getVehiclePhotoWithType(vehicleInfo, photoType));
    }

    public void getVehiclePhotoWithType(VehicleInfo vehicleInfo, String photoType) {
        VehiclePhoto vehiclePhoto = null;
        switch (photoType) {
            case VehiclePhotosWidget.FRONT_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getFront();
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getBack();
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getLeftSide();
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getRightSide();
                break;
            default:
                break;
        }
        if(!isViewAttached()){
            return;
        }
        getView().setVehicleInfo(vehicleInfo);
        getView().setVehiclePhoto(vehiclePhoto);
        getView().initViews();
    }
}
