package com.github.andrdev.bolservice.current.pendingDelivery;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.OriginEbolAcitivty;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;

import butterknife.OnClick;


public class PendingDeliveryFragment
        extends BaseSearchFragment<NewEbol, PendingDeliveryAdapter,
        PendingDeliveryView, PendingDeliveryPresenter>
        implements PendingDeliveryView {


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_pending_delivery;
    }

    @Override
    protected void filter(NewEbol item, String temp) {
        if (item.getOrderId().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        RealmDbData rdd = new RealmDbData(getContext());
        rdd.deleteNetTempEbols();
        rdd.saveTempDbEbols();
        getPresenter().getEbols();
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        NewEbol ebolTemp =  mData.get(position);
        getPresenter().netItemClicked(ebolTemp);
        Log.d("eboolsre", "clicktop");
    }

    @Override
    protected PendingDeliveryAdapter getAdapter() {
        return new PendingDeliveryAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {

    }

    @Override
    public void openListItem() {
        Log.d("eboolsre", "clickbot");
        Intent intent = new Intent(getContext(), OriginEbolAcitivty.class);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }


    @Override
    public PendingDeliveryPresenter createPresenter() {
        return new PendingDeliveryPresenter();
    }


    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.newEbol)
    void onNewClick(View v) {
        getPresenter().newClick();
    }

    @Override
    public void openNewEbol() {
        Intent intent = new Intent(getContext(), NewEbolActivity.class);
        startActivity(intent);
    }
}
