package com.github.andrdev.bolservice.clients.create;

import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.github.andrdev.bolservice.newEbol.damages.camera.CameraView;


public class ClientNewPresenterImpl extends ClientNewPresenter {

    ClientNewDataProvider dataProvider;

    @Override
    public void attachView(ClientNewView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ClientNewDataProvider();
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    void createClient(ClientCreateRequest request) {
        dataProvider.createClient(request, this::requestSuccess, this::requestFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestSuccess(SimpleResponse simpleResponse) {
        if (!isViewAttached()) {
            return;
        }

        if (simpleResponse.getStatus().equals("true")) {
            getView().backToClients();
        } else {
            getView().showFailedToast();
        }
    }
}
