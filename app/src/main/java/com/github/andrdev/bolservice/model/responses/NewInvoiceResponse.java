package com.github.andrdev.bolservice.model.responses;


public class NewInvoiceResponse {

    String status;
    long invoiceId;

    public NewInvoiceResponse() {
    }

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
