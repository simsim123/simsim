package com.github.andrdev.bolservice.messages;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Message;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChatFragment extends MvpFragment<ChatView, ChatPresenter> implements ChatView {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.enterMessage)
    EditText enterMessage;

    @Bind(R.id.titleTool)
    TextView titleTool;

    ChatAdapter dialogAdapter;
    List<Message> messages = new ArrayList<>();

    int ownerId;
    long userId;

    String otherUserFullName;
    String otherUserNick;

    private Handler mHandler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog, container, false);
        ButterKnife.bind(this, view);
        otherUserFullName = getArguments().getString(MessagesActivity.INTERLOCUTOR_NAME);
        otherUserNick = otherUserFullName.substring(0, otherUserFullName.indexOf(" "));
        titleTool.setText(otherUserFullName);
        return view;
    }

    @Override
    public ChatPresenter createPresenter() {
        return new ChatPresenterImpl();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getArguments() != null && getArguments().containsKey(MessagesActivity.CONVERSATION_ID)) {
            long conversationId = getArguments().getLong(MessagesActivity.CONVERSATION_ID);
            sendRequest(conversationId);
        }
        userId = getArguments().getLong(MessagesActivity.INTERLOCUTOR_ID);
        getProfileData();
    }

    private void getProfileData() {
        getPresenter().getProfileData();
    }

    @Override
    public void sendRequest(final long conversationId) {
        getPresenter().getMessages(conversationId);
        mHandler.postDelayed(()->getPresenter().getMessages(conversationId), 5000);
    }

    @Override
    public void setData(List<Message> data) {
        dialogAdapter.setData(data);
        dialogAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(data.size()-1);
    }

    @OnClick(R.id.okButton)
    void sendMessage() {
        String messageText = enterMessage.getText().toString();
        getPresenter().sendMessage(messageText, String.valueOf(userId));
        enterMessage.setText("");
    }

    private void initRecycler() {
        dialogAdapter = new ChatAdapter();
        dialogAdapter.setOwnerId(ownerId);
        dialogAdapter.setData(messages);
        dialogAdapter.setOtherUserNick(otherUserNick);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(dialogAdapter);
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }

    @Override
    public void showChatList(int id) {
        ownerId = id;
        initRecycler();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}