package com.github.andrdev.bolservice.history;

import android.content.Context;
import android.content.Intent;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.history.ebolDetails.HistoryEbolActivity;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;


public class HistoryFragment
        extends BaseSearchFragment<NewEbol, HistoryAdapter, BaseSearchView, HistoryPresenter> {


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_history;
    }

    @Override
    protected void filter(NewEbol item, String temp) {
        if (item.getOrderId().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        RealmDbData rdd = new RealmDbData(getContext());
        rdd.deleteNetTempEbols();
        rdd.saveTempDbEbols();
        getPresenter().getEbols();
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        NewEbol ebolTemp =  mData.get(position);
        getPresenter().netItemClicked(ebolTemp);
    }

    @Override
    protected HistoryAdapter getAdapter() {
        return new HistoryAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {

    }

    @Override
    public void openListItem() {
        Intent intent = new Intent(getContext(), HistoryEbolActivity.class);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public HistoryPresenter createPresenter() {
        return new HistoryPresenter();
    }
}
