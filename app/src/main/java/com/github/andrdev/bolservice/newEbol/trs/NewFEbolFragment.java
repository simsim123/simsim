//package com.github.andrdev.bolservice.newEbol;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.location.Address;
//import android.os.Build;
//import android.support.v4.app.FragmentTransaction;
//import android.text.TextUtils;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//
//import com.github.andrdev.bolservice.AddressAutocompleteDialogFragment;
//import com.github.andrdev.bolservice.BaseEbolFragment;
//import com.github.andrdev.bolservice.DatePickerFragment;
//import com.github.andrdev.bolservice.DialogFactory;
//import com.github.andrdev.bolservice.R;
//import com.github.andrdev.bolservice.Utils;
//import com.github.andrdev.bolservice.model.Client;
//import com.github.andrdev.bolservice.model.newEbol.Customer;
//import com.github.andrdev.bolservice.model.newEbol.NewEbol;
//import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
//import com.github.andrdev.bolservice.newEbol.damages.NewEbolCarDamagesActivity;
//import com.github.andrdev.bolservice.newEbol.vehicleInfo.NewEbolVehicleInformationActivity;
//
//import butterknife.Bind;
//import butterknife.OnClick;
//
//
//public class NewFEbolFragment extends BaseEbolFragment<NewFEbolView, NewEbolPresenter, EditText>
//        implements NewFEbolView {
//
//    @Bind(R.id.customerSignCheck)
//    View customerSignCheck;
//
//    @Bind(R.id.driverSignCheck)
//    View driverSignCheck;
//
//    @Bind(R.id.unableToSignCheck)
//    View unableToSignCheck;
//
//    @Override
//    protected int getLayoutResource() {
//        return R.layout.fragment_ebol_new;
//    }
//
//    @Override
//    protected void getData() {
//        getPresenter().getData();
//    }
//
//    @Override
//    public void setData(NewEbol ebol) {
//        currentEbol = ebol;
//        showData();
//    }
//
//    @Override
//    protected void setSignImages(NewEbol savedEbol) {
//        setSignImage(savedEbol.getDriverSignPath() != null, driverSignCheck);
//        setSignImage(savedEbol.getCustomerSignPath() != null, customerSignCheck);
//        setSignImage(savedEbol.isUnableToSign(), unableToSignCheck);
//    }
//
//    private void setSignImage(boolean hasSign, View signImage) {
//        if (hasSign) {
//            signImage.setVisibility(View.VISIBLE);
//        } else {
//            signImage.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    @Override
//    protected void detailsClick(int carId) {
//        getPresenter().detailsClick(carId);
//    }
//
//    @Override
//    public void showVehicleDetails(int carId) {
//        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
//        intent.putExtra(SELECTED_VEHICLE_ID, carId);
//        startActivity(intent);
//    }
//
//    @Override
//    protected void damagesClick(VehicleInfo vehInfo) {
//        getPresenter().damagesClick(vehInfo.getId());
//    }
//
//    @Override
//    public void showDamages(int carId) {
//        Intent intent = new Intent(getActivity(), NewEbolCarDamagesActivity.class);
//        intent.putExtra(SELECTED_VEHICLE_ID, carId);
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.addCars)
//    void plusClick() {
//        getPresenter().addNewVehicle();
//    }
//
//    @OnClick(R.id.customerSignature)
//    void customerSignatureClick() {
//        getPresenter().customerSignatureClick();
//    }
//
//    @Override
//    public void openCustomerSignature() {
//        if (Utils.hasText(originOrderId) && canSign()) {
//            Intent intent = new Intent(getActivity(), SignActivity.class);
//            intent.putExtra("signatureType", "customerSign");
//            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
//            startActivity(intent);
//        }
//    }
//
//    private boolean canSign() {
//        if(!currentEbol.getVehicleInfos().isEmpty()
//                && !TextUtils.isEmpty(currentEbol.getOrigin().getCustomerName())
//                || !TextUtils.isEmpty(currentEbol.getOrigin().getAddress())){
//            return true;
//        }
//        return false;
//    }
//
//    @OnClick(R.id.driverSignature)
//    void driverSignatureClick() {
//        getPresenter().driverSignatureClick();
//    }
//
//    @Override
//    public void openDriverSignature() {
//        if (Utils.hasText(originOrderId) && canSign()) {
//            Intent intent = new Intent(getActivity(), SignActivity.class);
//            intent.putExtra("signatureType", "driverSign");
//            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
//            startActivity(intent);
//        }
//    }
//
//    @OnClick(R.id.unableToSign)
//    void unableToSignClick() {
//        currentEbol.setUnableToSign(true);
//        getPresenter().unableToSignClick();
//    }
//
//    @Override
//    public void showUnableToSign(){
//        if (Utils.hasText(originOrderId) && canSign()) {
//            unableToSignCheck.setVisibility(View.VISIBLE);
//            setSignImages(currentEbol);
//        }
//    }
//
//    @Override
//    public void finishEbol() {
//
//    }
//
//    @OnClick(R.id.next)
//    void nextClick() {
//        currentEbol.setType("pendingPickup");
//        getPresenter().finishEbolCreation();
//        Intent intent;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
////            intent = new Intent(getContext(), PdfActActivity.class);
//        } else {
////            intent = new Intent(getContext(), PdfActPrekk.class);
//        }
//
////        startActivity(intent);
//        getActivity().finish();
//    }
//
//    @OnClick(R.id.originLocationButton)
//    void originLocationButtonClick() {
//        getPresenter().getAddress();
//    }
//
//    @OnClick(R.id.destinationLocationButton)
//    void destinationLocationButtonClick() {
//        selectAddressFromGooglePlacesDestination();
//    }
//
//    @OnClick(R.id.originCustomerNameButton)
//    void originCustomerNameButtonClick() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
//        newFragment.setCallback(new SelectClientDialogFrament.SelectedCallback() {
//            @Override
//            public void clientSelected(Client client) {
//                originCustomerName.setText(client.getCustomerName());
//                originAddress.setText(client.getAddress());
//                originCity.setText(client.getCity());
//                originState.setText(client.getState());
//                originZip.setText(client.getZIP());
//                originPhone.setText(client.getPhone());
//            }
//        });
//        newFragment.show(ft, "dialog");
//    }
//
//
//    @OnClick(R.id.destinationCustomerNameButton)
//    void destinationCustomerNameButtonClick() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
//        newFragment.setCallback(new SelectClientDialogFrament.SelectedCallback() {
//            @Override
//            public void clientSelected(Client client) {
//                destinationCustomerName.setText(client.getCustomerName());
//                destinationAddress.setText(client.getAddress());
//                destinationCity.setText(client.getCity());
//                destinationState.setText(client.getState());
//                destinationZip.setText(client.getZIP());
//                destinationPhone.setText(client.getPhone());
//            }
//        });
//        newFragment.show(ft, "dialog");
//    }
//
//    @OnClick(R.id.originDate)
//    void dateSetClick(View view) {
//        hideSoftKeyboard(getActivity());
//        DatePickerFragment newFragment = new DatePickerFragment();
//        newFragment.setCallback((year, month, day) -> {
//            String date = String.format("%s/%s/%s", month + 1, day, year);
//            originDate.setText(date);
//        });
//        newFragment.show(getFragmentManager(), "datePicker");
//    }
//
//    private Customer getOriginCustomer(NewEbol savedEbol) {
//        Customer origin = savedEbol.getOrigin();
//        if (origin == null) {
//            origin = new Customer();
//        }
//        origin.setCustomerName(Utils.getTextFromEditText(originCustomerName));
//        origin.setAddress(Utils.getTextFromEditText(originAddress));
//        origin.setCity(Utils.getTextFromEditText(originCity));
//        origin.setState(Utils.getTextFromEditText(originState));
//        origin.setZip(Utils.getTextFromEditText(originZip));
//        origin.setPhone(Utils.getTextFromEditText(originPhone));
//        origin.setCell(Utils.getTextFromEditText(originCell));
//        return origin;
//    }
//
//    private Customer getDestinationCustomer(NewEbol savedEbol) {
//        Customer destination = savedEbol.getDestination();
//        if(destination == null) {
//            destination = new Customer();
//        }
//        destination.setCustomerName(Utils.getTextFromEditText(destinationCustomerName));
//        destination.setAddress(Utils.getTextFromEditText(destinationAddress));
//        destination.setCity(Utils.getTextFromEditText(destinationCity));
//        destination.setState(Utils.getTextFromEditText(destinationState));
//        destination.setZip(Utils.getTextFromEditText(destinationZip));
//        destination.setPhone(Utils.getTextFromEditText(destinationPhone));
//        destination.setCell(Utils.getTextFromEditText(destinationCell));
//        return destination;
//    }
//
//    @Override
//    public NewEbol getEbolToSave() {
//        if (currentEbol == null) {
//            currentEbol = new NewEbol();
//        }
//        String orderId = originOrderId.getText().toString();
//
//        Customer origin = getOriginCustomer(currentEbol);
//        Customer destination = getDestinationCustomer(currentEbol);
//
//        currentEbol.setOrderId(orderId);
//        currentEbol.setDate(originDate.getText().toString());
//        currentEbol.setOrigin(origin);
//        currentEbol.setDestination(destination);
//
//        return currentEbol;
//    }
//
//    @Override
//    public void buildAlertMessageNoGps() {
//        DialogInterface.OnClickListener clickListener = (dialog, which)
//                -> selectAddressFromGooglePlacesOrigin();
//        DialogFactory.createAskToTurnOnGpsDialog(getContext(), clickListener).show();
//    }
//
//    @Override
//    public void selectAddressFromGooglePlacesOrigin() {
//        AddressAutocompleteDialogFragment.SelectedCallback selectedCallback =
//                this::setAddressFromPredictionOrigin;
//        DialogFactory.createSelectFromPlacesDialog(getContext(), selectedCallback)
//                .show(getFragmentManager(), "datePicker");
//    }
//
//    private void selectAddressFromGooglePlacesDestination() {
//        AddressAutocompleteDialogFragment.SelectedCallback selectedCallback =
//                this::setAddressFromPrediction;
//        DialogFactory.createSelectFromPlacesDialog(getContext(), selectedCallback)
//                .show(getFragmentManager(), "datePicker");
//    }
//
//    public void setAddressFromPrediction(Address address) {
//        destinationAddress.setText(address.getThoroughfare());
//        destinationCity.setText(address.getAdminArea());
//        destinationState.setText(address.getLocality());
//        destinationState.setText(address.getPostalCode());
//    }
//
//    @Override
//    public void setAddressFromPredictionOrigin(Address address) {
//        originAddress.setText(address.getThoroughfare());
//        originCity.setText(address.getAdminArea());
//        originState.setText(address.getLocality());
//        originZip.setText(address.getPostalCode());
//    }
//
//    @Override
//    public Context getViewContext() {
//        return getContext();
//    }
//
//    @Override
//    public void openNewVehicleInfoActivity() {
//        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
//        startActivity(intent);
//    }
//
//    @Override
//    public NewEbolPresenter createPresenter() {
//        return new NewEbolPresenterImpl();
//    }
//
//    public static void hideSoftKeyboard(Activity activity) {
//        InputMethodManager inputMethodManager = (InputMethodManager)
//                activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
//    }
//}
