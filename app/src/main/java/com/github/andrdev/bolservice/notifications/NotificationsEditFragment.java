package com.github.andrdev.bolservice.notifications;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.model.Notification;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NotificationsEditFragment
        extends MvpFragment<NotificationsEditView, NotificationsEditPresenter>
        implements NotificationsEditView {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.done)
    TextView done;

    @Bind(R.id.delete)
    TextView delete;

    NotificationsEditAdapter notificationsEditAdapter;

    List<Notification> notifications = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications_edit, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void getData() {
        getPresenter().getData();
    }

    @OnClick(R.id.delete)
    void deleteClick() {
        List<Long> notificationIds = notificationsEditAdapter.getSelected();
        getPresenter().deleteNotifications(notificationIds);
    }

    @OnClick(R.id.done)
    void doneClick() {
        getActivity().onBackPressed();
    }

    private void initRecycler() {
        notificationsEditAdapter = new NotificationsEditAdapter();
        notificationsEditAdapter.setData(notifications);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(notificationsEditAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                (view, position) -> notificationsEditAdapter.flipSelector(position)));
    }

    @Override
    public void setData(List<Notification> data) {
        this.notifications = data;
        notificationsEditAdapter.setData(notifications);
        notificationsEditAdapter.notifyDataSetChanged();
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @NonNull
    @Override
    public NotificationsEditPresenter createPresenter() {
        return new NotificationsEditPresenterImpl();
    }
}
