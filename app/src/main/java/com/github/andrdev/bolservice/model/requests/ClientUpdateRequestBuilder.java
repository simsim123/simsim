package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;

public class ClientUpdateRequestBuilder {
    private String address;
    private String cid;
    private String city;
    private String customerName;
    private String email;
    private String fax;
    private String state;
    private String tel;
    private String zip;

    public ClientUpdateRequestBuilder setAddress(EditText address) {
        if(Utils.hasText(address)){
            this.address = Utils.getTextFromEditText(address);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setCid(String cid) {
        this.cid = cid;
        return this;
    }

    public ClientUpdateRequestBuilder setCity(EditText city) {
        if(Utils.hasText(city)){
            this.city = Utils.getTextFromEditText(city);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setCustomerName(EditText customerName) {
        if(Utils.hasText(customerName)){
            this.customerName = Utils.getTextFromEditText(customerName);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setEmail(EditText email) {
        if(Utils.hasText(email)){
            this.email = Utils.getTextFromEditText(email);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setFax(EditText fax) {
        if(Utils.hasText(fax)){
            this.fax = Utils.getTextFromEditText(fax);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setState(EditText state) {
        if(Utils.hasText(state)){
            this.state = Utils.getTextFromEditText(state);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setTel(EditText tel) {
        if(Utils.hasText(tel)){
            this.tel = Utils.getTextFromEditText(tel);
        }
        return this;
    }

    public ClientUpdateRequestBuilder setZip(EditText zip) {
        if(Utils.hasText(zip)){
            this.zip = Utils.getTextFromEditText(zip);
        }
        return this;
    }

    public ClientUpdateRequest createClientUpdateRequest() {
        return new ClientUpdateRequest(address, cid, city, customerName, email, fax, state, tel, zip);
    }
}