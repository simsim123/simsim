package com.github.andrdev.bolservice.model.responses;

import java.util.List;


public class NotificationsMarkResponse {

    String status;
    String msg;
    List<Long> nids;

    public NotificationsMarkResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Long> getNids() {
        return nids;
    }

    public void setNids(List<Long> nids) {
        this.nids = nids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
