package com.github.andrdev.bolservice.dashboard;

import android.os.Bundle;

import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.R;


public class DashboardActivity extends BaseDrawerActivity {

    public static final String TAG_DASHBOARD_FRAGMENT = "dashboardFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, new DashBoardFragment(), TAG_DASHBOARD_FRAGMENT)
                    .commit();
        }
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 0;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_dashboard;
    }
}
