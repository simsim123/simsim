package com.github.andrdev.bolservice.login;

import android.content.Context;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class LoginDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void setAuthenticatedRestWorker(LoginRequest loginRequest) {
        EbolNetworkWorker2.setAuthenticatedRestWorker(loginRequest);
    }

    public void getProfile(
            DataCb<ProfileResponse> successCb,
            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getProfile(
                successCb::returnData,
                failedCb::returnData);
    }

    public void saveProfile(Profile profile, DataCb<Boolean> successCb) {
        dbHelper.saveProfile(profile, successCb::returnData);
    }
}
