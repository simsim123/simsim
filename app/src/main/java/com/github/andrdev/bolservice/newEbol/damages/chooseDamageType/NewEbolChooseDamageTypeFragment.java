package com.github.andrdev.bolservice.newEbol.damages.chooseDamageType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.DamagesAdapter;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;


public class NewEbolChooseDamageTypeFragment
        extends MvpFragment<NewEbolChooseDamageTypeView, NewEbolChooseDamageTypePresenter>
        implements NewEbolChooseDamageTypeView {
    private static final String TAG = "dreeMd";

    List<DamageType> damageTypes = new ArrayList<>();
    private double yPosition;
    private double xPosition;

    {
        damageTypes.add(new DamageType("(S)-Scratched", "S", false));
        damageTypes.add(new DamageType("(D)-Dented", "D", false));
        damageTypes.add(new DamageType("(PC)-Paint Chip", "PC", false));
        damageTypes.add(new DamageType("(CR)-Cracked", "CR", false));
        damageTypes.add(new DamageType("(C)-Cut", "C", false));
        damageTypes.add(new DamageType("(BR)-Broken", "BR", false));
        damageTypes.add(new DamageType("(MD)-Major Damage", "MD", false));
        damageTypes.add(new DamageType("(B)-Bent", "B", false));
        damageTypes.add(new DamageType("(L)-Loose", "L", false));
        damageTypes.add(new DamageType("(M)-Missing", "M", false));
        damageTypes.add(new DamageType("(ST)-Stained", "ST", false));
        damageTypes.add(new DamageType("(F)-Faded", "F", false));
        damageTypes.add(new DamageType("(T)-Torn", "T", false));
        damageTypes.add(new DamageType("(FF)-Foreign Fluid", "FF", false));
        damageTypes.add(new DamageType("(G)-Gouged", "G", false));
        damageTypes.add(new DamageType("(P)-Pitted", "P", false));
        damageTypes.add(new DamageType("(SL)-Soiled", "SL", false));
    }

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    DamagesAdapter damagesAdapater;
    MarkedDamage markedDamage;
    VehiclePhoto tempVehiclePhoto;
//    boolean fromEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_damage, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        this.yPosition = getArguments().getDouble("yPosition");
        this.xPosition = getArguments().getDouble("xPosition");
        Log.d(TAG, "getData: " + xPosition + " " + yPosition);
        int selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
//        if (getArguments().containsKey("phototype")) {
//        fromEdit = true;
        String photoType = getArguments().getString("phototype");
//        }
        getPresenter().getData(photoType, yPosition, xPosition, selectedVehicleId);
    }

    @Override
    public void setMarkedDamage(MarkedDamage markedDamage) {
        this.markedDamage = markedDamage;
//        List<MarkedDamage> markedDamages = new ArrayList<>(
//                tempCarPhoto.getMarkedDamages());
//        for (MarkedDamage damage : markedDamages) {
//            if (damage.getxPosition() == xPosition && damage.getyPosition() == yPosition) {
//                markedDamage = damage;
//                break;
//            }
//        }
//        if (markedDamage == null) {
//            markedDamage = new MarkedDamage();
//            markedDamage.setyPosition(yPosition);
//            markedDamage.setxPosition(xPosition);
//            markedDamage.setIsTemp(true);
//            tempCarPhoto.getMarkedDamages().add(markedDamage);
//            repo.repoCarPhoto.updateVehiclePhoto(tempCarPhoto);
//        }
    }

    @Override
    public void initViews() {
        if (markedDamage.getDamageTypes() != null) {
            Log.d(TAG, "initViews: " + markedDamage.getxPosition() + " "+markedDamage.getyPosition());
//            Observable.from(markedDamage.getDamageTypes()).forEach(savedDamage ->
//                    Observable.from(damageTypes).filter(damageType ->
//                            savedDamage.getDamageName().equals(damageType.getDamageName()))
//                            .forEach(damageType -> damageType.setState(savedDamage.getState())));
            for (DamageType type : markedDamage.getDamageTypes()) {
                Log.d(TAG, "initViews: " + markedDamage.getDamageTypes().size());
                for (DamageType tempDamage : damageTypes) {
                    if (tempDamage.getDamageName().equals(type.getDamageName())) {
                        tempDamage.setState(true);
                        Log.d(TAG, "initViews: " + tempDamage.getDamageName());
                    }
                }
            }
            damagesAdapater.notifyDataSetChanged();
        }
    }

    @Override
    public VehiclePhoto getVehiclePhoto() {
        return tempVehiclePhoto;
    }

    @Override
    public void setVehiclePhoto(VehiclePhoto vehiclePhoto) {
        tempVehiclePhoto = vehiclePhoto;
    }

    private void initRecycler() {
        damagesAdapater = new DamagesAdapter();
        damagesAdapater.setData(damageTypes);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), timedClicker()));
        recyclerView.setAdapter(damagesAdapater);
    }


    @NonNull
    private RecyclerItemClickListener.TimedOnItemClickListener timedClicker() {
        RecyclerItemClickListener.TimedOnItemClickListener clicke = new RecyclerItemClickListener.TimedOnItemClickListener(){
            @Override
            public void onTimedClick(View view, int position) {
                damagesAdapater.itemClicked(position);
            }
        };
        clicke.delay = 500;
        return clicke;
    }

    @OnClick(R.id.ok)
    void saveClick(View view) {
        if (markedDamage == null) {
            markedDamage = new MarkedDamage(xPosition, yPosition);
            markedDamage.setDamageTypes(new ArrayList<>());
            tempVehiclePhoto.getMarkedDamages().add(markedDamage);
        } else if(markedDamage.getDamageTypes() == null) {
            markedDamage.setDamageTypes(new ArrayList<>());
            tempVehiclePhoto.getMarkedDamages().add(markedDamage);
        } else {
            markedDamage.getDamageTypes().clear();
        }

        boolean hasDamage = false;
        for (DamageType damageType : damageTypes) {
            if (damageType.getState()) {
                Log.d(TAG, "saveClick: " + damageType.getDamageName());
                hasDamage = true;
                markedDamage.getDamageTypes().add(damageType);
            }
        }

        for (MarkedDamage damage : tempVehiclePhoto.getMarkedDamages()) {
            if (damage.getxPosition() == xPosition) {
                for (DamageType damageType : damage.getDamageTypes()) {
                        Log.d(TAG, "saveClick: " + damageType.getDamageName());
                }
            }

        }
        if (!hasDamage) {
            deleteClick();
        } else {
            getPresenter().saveDamage();
        }
    }

    @Override
    public void setSaveResultAndFinish() {
        Intent intent = new Intent();
        intent.putExtra("action", "create");
//            intent.putExtra("fromEdit", fromEdit);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @OnClick(R.id.delete)
    void deleteClick() {
        tempVehiclePhoto.getMarkedDamages().remove(markedDamage);
//        repo.repoCarPhoto.updateVehiclePhoto(tempCarPhoto);
//        repo.repoMarkedDamage.deleteMarkedDamage(markedDamage);
        getPresenter().updateVehiclePhoto(tempVehiclePhoto);
    }

    @Override
    public void setDeleteResultAndFinish() {
        Intent intent = new Intent();
        intent.putExtra("action", "delete");
        intent.putExtra("damageCode", 44);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public NewEbolChooseDamageTypePresenter createPresenter() {
        return new NewEbolChooseDamageTypePresenter();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
