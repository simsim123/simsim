package com.github.andrdev.bolservice.history.details.vehicleInfo;

import android.os.Bundle;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public class HistoryViPresenter extends MvpBasePresenter<HistoryViView> {

    HistoryViDataProvider dataProvider;

    @Override
    public void attachView(HistoryViView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HistoryViDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getVehicleInfo(int vehicleId) {
        dataProvider.getVehicleById(vehicleId, this::setVehicleFields);
    }

    private void setVehicleFields(VehicleInfo vehicleInfo) {
        getView().setVehicleInfo(vehicleInfo);
    }
}
