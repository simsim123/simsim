package com.github.andrdev.bolservice.clients;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ClientsDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getClients(String s, DataCb<ClientsResponse> successCb, DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getClients(s, successCb::returnData, failedCb::returnData);
    }

    public void saveClient(Client client, DataCb<Boolean> successCb) {
        dbHelper.saveClient(client, successCb::returnData);
    }
}
