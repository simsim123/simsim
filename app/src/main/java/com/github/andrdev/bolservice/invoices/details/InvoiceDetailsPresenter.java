package com.github.andrdev.bolservice.invoices.details;

import com.github.andrdev.bolservice.model.Invoice;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class InvoiceDetailsPresenter extends MvpBasePresenter<InvoiceDetailsView> {

    @SuppressWarnings("ConstantConditions")
    public abstract void getData();

    @SuppressWarnings("ConstantConditions")
    public abstract void setData(Invoice invoice);
//
//    public default void failed(Exception exception);
}
