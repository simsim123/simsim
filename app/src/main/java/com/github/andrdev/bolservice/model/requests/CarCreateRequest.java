package com.github.andrdev.bolservice.model.requests;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class CarCreateRequest {

    int sid;
    String make;
    String yearOfMake;
    String model;
    String color;
    String mileage;
    String vinNumber;
    String plateNumber;
    int radioPresent;
    int manuals;
    int spareTire;
    int cargoCover;
    int windscreen;
    int keysCount;
    int remotesCount;
    int headrestsCount;
    int floorMats;

    public CarCreateRequest(VehicleInfo info, int sid) {
        this.sid = sid;
        make = info.getMake();
        yearOfMake = info.getYearOfMake();
        model = info.getModel();
        color = info.getColor();
        mileage = info.getMileage();
        vinNumber = info.getVinNumber();
        plateNumber = info.getPlateNumber();
        manuals = info.getDamages().isRadio() ? 1 : 0;
        cargoCover = info.getDamages().isCargoCovers() ? 1 : 0;
        windscreen = info.getDamages().isRadio() ? 1 : 0;
        keysCount = info.getDamages().getKeys();
        remotesCount = info.getDamages().getRemotes();
        headrestsCount = info.getDamages().getHeadrest();
        floorMats = info.getDamages().getFloorMats();
    }

    public String isCargoCover() {
        return String.valueOf(cargoCover);
    }

    public void setCargoCover(boolean cargoCover) {
        this.cargoCover = cargoCover?1:0;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFloorMats() {
        return String.valueOf(floorMats);
    }

    public void setFloorMats(int floorMats) {
        this.floorMats = floorMats;
    }

    public String getHeadrestsCount() {
        return String.valueOf(headrestsCount);
    }

    public void setHeadrestsCount(int headrestsCount) {
        this.headrestsCount = headrestsCount;
    }

    public String getKeysCount() {
        return String.valueOf(keysCount);
    }

    public void setKeysCount(int keysCount) {
        this.keysCount = keysCount;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String isManuals() {
        return String.valueOf(manuals);
    }

    public void setManuals(boolean manuals) {
        this.manuals = manuals?1:0;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String isRadioPresent() {
        return String.valueOf(radioPresent);
    }

    public void setRadioPresent(boolean radioPresent) {
        this.radioPresent = radioPresent?1:0;
    }

    public String getRemotesCount() {
        return String.valueOf(remotesCount);
    }

    public void setRemotesCount(int remotesCount) {
        this.remotesCount = remotesCount;
    }

    public String getSid() {
        return String.valueOf(sid);
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String isSpareTire() {
        return String.valueOf(spareTire);
    }

    public void setSpareTire(boolean spareTire) {
        this.spareTire = spareTire?1:0;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String isWindscreen() {
        return String.valueOf(windscreen);
    }

    public void setWindscreen(boolean windscreen) {
        this.windscreen = windscreen?1:0;
    }

    public String getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(String yearOfMake) {
        this.yearOfMake = yearOfMake;
    }
}
