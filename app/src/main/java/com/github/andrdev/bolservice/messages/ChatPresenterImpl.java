package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.MessageRequest;
import com.github.andrdev.bolservice.model.responses.ConversationResponse;
import com.github.andrdev.bolservice.model.responses.PostMessageResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class ChatPresenterImpl extends ChatPresenter {

    ChatDataProvider dataProvider;

    @Override
    public void attachView(ChatView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ChatDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getMessages(long collId) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getConversation(collId, this::requestSuccess, this::requestFailed);
    }

    public void requestSuccess(ConversationResponse conversationResponse) {
        if (!isViewAttached()) {
            return;
        }
        if(conversationResponse.getStatus().equals("true")) {
            getView().setData(conversationResponse.getConversation());
        }
    }

    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
    }
    @Override
    public void sendMessage(String text, String userId) {
        if (!isViewAttached()) {
            return;
        }
        MessageRequest messageRequest = new MessageRequest(text, userId);
        dataProvider.postMessage(messageRequest,
                this::sendMessageSuccess,
                this::requestFailed);
    }

    public void sendMessageSuccess(PostMessageResponse postMessageResponse) {
        if(postMessageResponse.getStatus().equals("true")) {
            getView().sendRequest(postMessageResponse.getConversationId());
        }
    }

    @Override
    public void getProfileData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getProfile(this::getOwnerId);
    }

    @Override
    public void getOwnerId(Profile profile){
        if (!isViewAttached()) {
            return;
        }
        int ownerId = profile.getId();
        getView().showChatList(ownerId);
    }
}
