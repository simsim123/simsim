package com.github.andrdev.bolservice.newEbol.damages.chooseDamageType;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface NewEbolChooseDamageTypeView extends MvpView {
    void setSaveResultAndFinish();

    void setDeleteResultAndFinish();

    Context getViewContext();

    void setMarkedDamage(MarkedDamage markedDamage);

    void initViews();

    VehiclePhoto getVehiclePhoto();

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);
}
