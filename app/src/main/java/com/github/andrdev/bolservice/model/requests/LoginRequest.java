package com.github.andrdev.bolservice.model.requests;


public class LoginRequest {

    String login;
    String password;

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
