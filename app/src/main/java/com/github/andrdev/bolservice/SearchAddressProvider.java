package com.github.andrdev.bolservice;

import android.content.Context;
import android.location.Address;
import android.util.Log;

import com.github.andrdev.bolservice.model.responses.PlaceInfo;
import com.github.andrdev.bolservice.model.responses.Predictions;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SearchAddressProvider {

    Context context;

    private static final String TAG = "dreeGpsAut";

    PlacesHandlerCallback placesCallback;

    public SearchAddressProvider(Context context, PlacesHandlerCallback placesCallback) {
        this.context = context;
        this.placesCallback = placesCallback;
    }

    public void getAddressesFromQuery(String userQuery) {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
        Log.d(TAG, "getAddressesFromQuery: " + userQuery);
        locationProvider.getGeocodeObservable(userQuery, 10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::placesResult, this::onerror);
    }

    public void getAddressesFromQueryGjs(String userQuery) {
        EbolNetworkWorker2.getInstance().requestAddressAutocomplete(userQuery, this::convert);
    }

    private void convert(Predictions predictions) {
        List<Address> addresses = new ArrayList<>();
        for (Predictions.Prediction prediction : predictions.getPredictions()) {
            Address adr = new Address(new Locale("en"));
            adr.setThoroughfare(prediction.getTerms()[0].value);
            adr.setCountryName(prediction.getDescription());
            adr.setUrl(prediction.getPlace_id());
            addresses.add(adr);
        }
        placesResult(addresses);
    }

    private void onerror(Throwable throwable) {
        Log.d(TAG, "onerror: " + throwable.getMessage());
    }

    private void placesResult(List<Address> addresses) {
        placesCallback.addressesFromQuery(addresses);
    }

    public void getInfo(String placeId) {
        EbolNetworkWorker2.getInstance().requestPlaceInfoByPlaceId(placeId, this::returnToCallback);
    }

    private void returnToCallback(PlaceInfo o) {
        placesCallback.returnResult(o);
    }

    public interface PlacesHandlerCallback {
        void addressesFromQuery(List<Address> addresses);
        void returnResult(PlaceInfo o);
    }
}
