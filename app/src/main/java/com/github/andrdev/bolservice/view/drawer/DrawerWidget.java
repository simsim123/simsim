package com.github.andrdev.bolservice.view.drawer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.clients.ClientsActivity;
import com.github.andrdev.bolservice.colleagues.ColleaguesActivity;
import com.github.andrdev.bolservice.dashboard.DashboardActivity;
import com.github.andrdev.bolservice.helpcenter.HelpCenterActivity;
import com.github.andrdev.bolservice.login.LoginActivity;
import com.github.andrdev.bolservice.model.DrawerLine;
import com.github.andrdev.bolservice.notifications.NotificationsActivity;
import com.github.andrdev.bolservice.profile.ProfileActivity;
import com.hannesdorfmann.mosby.mvp.layout.MvpRelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DrawerWidget extends MvpRelativeLayout<DrawerView, DrawerPresenter> implements DrawerView {

    static List<DrawerLine> drawerLines = new ArrayList<>();
    static {
        drawerLines.add(new DrawerLine(R.drawable.car, "Dashboard"));
        drawerLines.add(new DrawerLine(R.drawable.notebook, "Clients"));
        drawerLines.add(new DrawerLine(R.drawable.bell, "Notifications"));
        drawerLines.add(new DrawerLine(R.drawable.friends, "Colleague"));
        drawerLines.add(new DrawerLine(R.drawable.help, "Help Center"));
        drawerLines.add(new DrawerLine(R.drawable.logout, "Sign Out"));
    }

    private int currentItemPosition;
    private DrawerCallback drawerCallback;

    @Bind(R.id.name)
    TextView mUserName;

    @Bind(R.id.menuList)
    RecyclerView mMenuList;

    public DrawerWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public DrawerWidget(Context context) {
        super(context);
        init(context);
    }

    public DrawerWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawerWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_drawer, this);
        ButterKnife.bind(this, view);
        initRecycler();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getPresenter().getData();
    }

    private void initRecycler() {
        DrawerAdapter adapter = new DrawerAdapter();
        adapter.setData(drawerLines);
        mMenuList.setLayoutManager(new LinearLayoutManager(getContext()));
        mMenuList.setAdapter(adapter);
        mMenuList.getAdapter().notifyDataSetChanged();
        mMenuList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                (view, position) -> onDrawerRowClick(position)));
    }

    private void onDrawerRowClick(int position) {
        if(currentItemPosition == position){
            drawerCallback.removeDrawerFromScreen();
            return;
        }
        Intent intent = new Intent();
        switch (position){
            case 0:
                intent = new Intent(getContext(), DashboardActivity.class);
                break;
            case 1:
                intent = new Intent(getContext(), ClientsActivity.class);
                break;
            case 2:
                intent = new Intent(getContext(), NotificationsActivity.class);
                break;
            case 3:
                intent = new Intent(getContext(), ColleaguesActivity.class);
                break;
            case 4:
                intent = new Intent(getContext(), HelpCenterActivity.class);
                break;
            case 5:
                intent = new Intent(getContext(), LoginActivity.class);
                break;
            default:
                break;
        }
        drawerCallback.removeDrawerFromScreen();
        getContext().startActivity(intent);
        drawerCallback.removeCurrentItem();
    }

    @OnClick({R.id.avatar, R.id.myAccount, R.id.name})
    void userClick() {
        if(currentItemPosition == 9) {
            drawerCallback.removeDrawerFromScreen();
        }
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        getContext().startActivity(intent);
        drawerCallback.removeCurrentItem();
    }

    public int getCurrentItemPosition() {
        return currentItemPosition;
    }

    public void setCurrentItemPosition(int currentItemPosition) {
        this.currentItemPosition = currentItemPosition;
    }

    public DrawerCallback getDrawerCallback() {
        return drawerCallback;
    }

    public void setDrawerCallback(DrawerCallback drawerCallback) {
        this.drawerCallback = drawerCallback;
    }

    @Override
    public DrawerPresenter createPresenter() {
        return new DrawerPresenter();
    }

    @Override
    public void setUserName(String userName) {
        mUserName.setText(userName);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    public interface DrawerCallback{
        void removeDrawerFromScreen();
        void removeCurrentItem();
    }
}
