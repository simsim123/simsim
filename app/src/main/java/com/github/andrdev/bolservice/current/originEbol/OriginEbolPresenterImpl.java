package com.github.andrdev.bolservice.current.originEbol;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.newEbol.trs.NewEbolDataProvider;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public class OriginEbolPresenterImpl extends OriginEbolPresenter {

    OriginEbolDataProvider dataProvider;

    @Override
    public void attachView(OriginFEbolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new OriginEbolDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getTempEbol(this::setEbol);
    }

    private void setEbol(NewEbol newEbol) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(newEbol);
    }

//    @Override
//    public void finishEbolCreation() {
//        if (!isViewAttached()) {
//            return;
//        }
//        saveEbol(this::openSendActivity);
//    }
//
//    private void openSendActivity(boolean saved) {
//        if (!isViewAttached()) {
//            return;
//        }
//        getView().finishEbol();
//    }

    @Override
    public void detailsClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showVehicleDetails(vehicleId);
    }

    @Override
    public void damagesClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showDamages(vehicleId);
    }

    @Override
    public void customerSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        getView().openCustomerSignature();
    }

    @Override
    public void driverSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        getView().openDriverSignature();
    }

    @Override
    public void unableToSignClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::showUnableToSign);
    }

    private void showUnableToSign(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().showUnableToSign();
    }
//
    public void saveEbol(NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSave();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }
//    public void getDamages(final VehicleInfo info) {
//        if(getView() == null) return;
//
//        final Repo repo = new Repo(getView().getContext());
//
//        final NewEbol ebol = repo.repoNewEbol.getTempNewEbol();
//
//        Callback<DamagesGetResponse> ebolCb = new Callback<DamagesGetResponse>() {
//            @Override
//            public void success(DamagesGetResponse cars, Response response) {
//                if(getView() != null) {
//                    if (cars.getStatus().equals("true") && cars.getDamages() != null) {
//                        List<DamagesG> damages1 = new ArrayList<>();
//                        DamagesG dm = new DamagesG();
//                        dm.setCode("CR");
//                        dm.setPositionLeft("40.2");
//                        dm.setPositionTop("40.2");
//                        dm.setSide("back");
//                        dm.setImage("http://www.yourinspirationweb.com/wp-content/uploads/2012/09/portfolio-3-colonne.png");
//                        DamagesG dm1 = new DamagesG();
//                        dm1.setCode("CR");
//                        dm1.setPositionLeft("60.2");
//                        dm1.setPositionTop("60.2");
//                        dm1.setSide("back");
//                        dm1.setImage("http://www.yourinspirationweb.com/wp-content/uploads/2012/09/portfolio-3-colonne.png");
//
//                        DamagesG dm2 = new DamagesG();
//                        dm2.setCode("CR");
//                        dm2.setPositionLeft("60.2");
//                        dm2.setPositionTop("60.2");
//                        dm2.setSide("front");
//                        dm2.setImage("http://www.yourinspirationweb.com/wp-content/uploads/2012/09/portfolio-3-colonne.png");
//                        damages1.add(dm);
//                        damages1.add(dm1);
//                        damages1.add(dm2);
//                        damagesToMarkDamage(info, damages1);
////                        ebol.addVehicleInfos(carsToVehicleInfos(cars.getCars()));
////                        repo.repoNewEbol.updateTempNewEbol(ebol);
////                        getDamages(cars);
//                        getView().openMarkedDamage(info);
//                    }
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        };
//        EbolNetworkWorker.getInstance().getDamages(String.valueOf(info.getCid()), ebolCb);
//    }
//
//    private void damagesToMarkDamage(VehicleInfo info, List<DamagesG> damages) {
//        MarkedDamage[] markedDamages = new MarkedDamage[4];
//        final Repo repo = new Repo(getView().getContext());
//
//
//        WindowManager wm = (WindowManager) getView().getContext().getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
////        repo.repoDamages.updateDamages(info.getDamages());
////        repo.repoVehicleInfo.refreshVehicleInfo(info);
////        VehicleInfo nVi = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//        info.getDamages().setFront(null);
//        info.getDamages().setBack(null);
//        info.getDamages().setRightSide(null);
//        info.getDamages().setLeftSide(null);
//        repo.repoDamages.updateDamages(info.getDamages());
//        repo.repoVehicleInfo.refreshVehicleInfo(info);
//        for (DamagesG dg : damages) {
//
//            MarkedDamage damage = new MarkedDamage();
//            repo.repoMarkedDamage.createTempMarkedDamage(damage);
//            repo.repoMarkedDamage.refresh(damage);
//            damage.setxPosition((int) (height * Double.parseDouble(dg.getPositionLeft()) / 100));
//            damage.setyPosition((int) (width * Double.parseDouble(dg.getPositionTop()) / 100));
//            repo.repoMarkedDamage.updateMarkedDamage(damage);
////            damage.getDamageTypes().add(dt);
////            damage.getDamageTypes().add();
//            switch (dg.getSide()) {
//                case "front":
//                    if (info.getDamages().getFront() == null) {
//                        CarPhoto damageF = new CarPhoto();
//                        damageF.setPhotoType("frontPhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageF);
//                        info.getDamages().setFront(damageF);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getFront().setPhotoPath(dg.getImage());
//                    info.getDamages().getFront().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getFront());
//                    break;
//                case "back":
//                    if (info.getDamages().getBack() == null) {
//                        CarPhoto damageB = new CarPhoto();
//                        damageB.setPhotoType("backPhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageB);
//                        info.getDamages().setBack(damageB);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getBack().setPhotoPath(dg.getImage());
//                    info.getDamages().getBack().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getBack());
//                    break;
//                case "rightside":
//                    if (info.getDamages().getRightSide() == null) {
//                        CarPhoto damageR = new CarPhoto();
//                        damageR.setPhotoType("rightSidePhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageR);
//                        info.getDamages().setRightSide(damageR);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getRightSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getRightSide().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getRightSide());
//                    break;
//                case "leftside":
//                    if (info.getDamages().getLeftSide() == null) {
//                        CarPhoto damageL = new CarPhoto();
//                        damageL.setPhotoType("leftSidePhoto");
//                        repo.repoCarPhoto.createCarPhoto(damageL);
//                        info.getDamages().setLeftSide(damageL);
//                        repo.repoDamages.updateDamages(info.getDamages());
//                        info = repo.repoVehicleInfo.getVehicleInfoById(info.getId());
//                    }
//                    info.getDamages().getLeftSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getLeftSide().getMarkedDamages().add(damage);
//                    repo.repoCarPhoto.updateCarPhoto(info.getDamages().getLeftSide());
//                    break;
//                default:
//            }
//            DamageType dt = new DamageType();
//            dt.setState(true);
//            dt.setDamageShortName(dg.getCode());
//            dt.setMarkedDamage(damage);
//            repo.repoDamageType.createTempMarkedDamage(dt);
//        }
//    }
}
