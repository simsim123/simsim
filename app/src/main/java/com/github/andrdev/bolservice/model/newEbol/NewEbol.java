package com.github.andrdev.bolservice.model.newEbol;

import java.util.List;


public class NewEbol {

    private int id;
    private String orderId;
    private String date;
    private Customer origin;
    private Customer destination;
    private List<VehicleInfo> vehicleInfos;
    private String type;
    private String driverSignPath;
    private String customerSignPath;
    private boolean unableToSign;
    private String deliveryDriverSignPath;
    private String deliveryCustomerSignPath;
    private boolean deliveryUnableToSign;
    private int vehiclesCount;
    private long submitId;
    private boolean savedToDb;

    public NewEbol() {
    }


    public NewEbol(String orderId, String date, Customer origin, Customer destination) {
        this();
        this.orderId = orderId;
        this.date = date;
        this.origin = origin;
        this.destination = destination;
    }

    public String getCustomerSignPath() {
        return customerSignPath;
    }

    public void setCustomerSignPath(String customerSignPath) {
        this.customerSignPath = customerSignPath;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryCustomerSignPath() {
        return deliveryCustomerSignPath;
    }

    public void setDeliveryCustomerSignPath(String deliveryCustomerSignPath) {
        this.deliveryCustomerSignPath = deliveryCustomerSignPath;
    }

    public String getDeliveryDriverSignPath() {
        return deliveryDriverSignPath;
    }

    public void setDeliveryDriverSignPath(String deliveryDriverSignPath) {
        this.deliveryDriverSignPath = deliveryDriverSignPath;
    }

    public boolean isDeliveryUnableToSign() {
        return deliveryUnableToSign;
    }

    public void setDeliveryUnableToSign(boolean deliveryUnableToSign) {
        this.deliveryUnableToSign = deliveryUnableToSign;
    }

    public Customer getDestination() {
        return destination;
    }

    public void setDestination(Customer destination) {
        this.destination = destination;
    }

    public String getDriverSignPath() {
        return driverSignPath;
    }

    public void setDriverSignPath(String driverSignPath) {
        this.driverSignPath = driverSignPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Customer getOrigin() {
        return origin;
    }

    public void setOrigin(Customer origin) {
        this.origin = origin;
    }

    public long getSubmitId() {
        return submitId;
    }

    public void setSubmitId(long submitId) {
        this.submitId = submitId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUnableToSign() {
        return unableToSign;
    }

    public void setUnableToSign(boolean unableToSign) {
        this.unableToSign = unableToSign;
    }

    public List<VehicleInfo> getVehicleInfos() {
        return vehicleInfos;
    }

    public void setVehicleInfos(List<VehicleInfo> vehicleInfos) {
        this.vehicleInfos = vehicleInfos;
    }

    public int getVehiclesCount() {
        return vehiclesCount;
    }

    public void setVehiclesCount(int vehiclesCount) {
        this.vehiclesCount = vehiclesCount;
    }


    public boolean isLocal() {
        return submitId == 0;
    }

    @Override
    public String toString() {
        return "NewEbol{" +
                "customerSignPath='" + customerSignPath + '\'' +
                ", id=" + id +
                ", orderId='" + orderId + '\'' +
                ", date='" + date + '\'' +
                ", origin=" + origin +
                ", destination=" + destination +
                ", vehicleInfos=" + vehicleInfos +
                ", type='" + type + '\'' +
                ", driverSignPath='" + driverSignPath + '\'' +
                ", unableToSign=" + unableToSign +
                ", deliveryDriverSignPath='" + deliveryDriverSignPath + '\'' +
                ", deliveryCustomerSignPath='" + deliveryCustomerSignPath + '\'' +
                ", deliveryUnableToSign=" + deliveryUnableToSign +
                ", vehiclesCount=" + vehiclesCount +
                ", submitId=" + submitId +
                '}';
    }

    public void setIsSavedToDb(boolean savedToDb) {
        this.savedToDb = savedToDb;
    }

    public boolean isSavedToDb() {
        return savedToDb;
    }
}
