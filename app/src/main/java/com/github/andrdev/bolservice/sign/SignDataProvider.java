package com.github.andrdev.bolservice.sign;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.io.FileOutputStream;
import java.io.IOException;

public class SignDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @NonNull
    public void saveSignatureToFile(Bitmap b, String filePath, DataCb<String> callback) {
        String signPath = filePath;
        try {
            FileOutputStream mFileOutStream = new FileOutputStream(signPath);
            b.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
            mFileOutStream.flush();
            mFileOutStream.close();
        } catch (IOException e) {
            Log.v("log_tag", e.toString());
        }
        callback.returnData(signPath);
    }

    public void getEbol(DataCb<NewEbol> callback) {
        dbHelper.getTempEbol(callback::returnData);
    }

    public void saveOrUpdateEbol(NewEbol newEbol, DataCb<Boolean> callback) {
        dbHelper.updateOrSaveEbol(newEbol, callback::returnData);
    }
}
