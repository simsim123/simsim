package com.github.andrdev.bolservice.currentEbol;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public abstract class CurrentFEbolPresenter extends MvpBasePresenter<CurrentFEbolView> {

    public abstract void getData();

    abstract void saveEbol(com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider.DataCb<Boolean> callback);

    public abstract void finishEbolCreation();

    public abstract void addNewVehicle();

    public abstract void detailsClick(int vehicleId);

    abstract void justSaveEbol();

    abstract void justSaveEbolDb();

    public abstract void damagesClick(int vehicleId);

    public abstract void customerSignatureClick();

    public abstract void driverSignatureClick();

    public abstract void unableToSignClick();
}
