package com.github.andrdev.bolservice.newEbol.damages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface NEVehicleInspectionView extends MvpView {
    void setVehicleInfo(VehicleInfo vehicleInfo);

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);

    void initViews();

    Context getViewContext();

    VehiclePhoto getVehiclePhoto();

    void showCameraFragment(String nextPhototype);

    void hideNote();

    void backToDamages();
}
