package com.github.andrdev.bolservice.model.responses;


public class NewLocationResponse {

    String status;
    long locationId;

    public NewLocationResponse() {
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
