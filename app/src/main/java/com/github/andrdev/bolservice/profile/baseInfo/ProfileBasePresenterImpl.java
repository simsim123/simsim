package com.github.andrdev.bolservice.profile.baseInfo;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Profile;


public class ProfileBasePresenterImpl extends ProfileBasePresenter {

    ProfileBaseInfoDataProvider dataProvider;

    @Override
    public void attachView(ProfileBaseView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ProfileBaseInfoDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getSavedProfile(this::setData);
    }

    public void setData(Profile profile) {
        if (!isViewAttached()) {
            return;
        }
        getView().setProfile(profile);
        getView().initFields();
    }
}
