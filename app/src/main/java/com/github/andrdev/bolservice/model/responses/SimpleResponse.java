package com.github.andrdev.bolservice.model.responses;


public class SimpleResponse {

    String status;
    String msg;

    public SimpleResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
