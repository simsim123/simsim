package com.github.andrdev.bolservice.newEbol.damages.markDamages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.NewEbolCameraAcitvity;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.github.andrdev.bolservice.view.markDamages.MarkDamagesWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolMarkDamagesFragment
        extends MvpFragment<NewEbolMarkDamagesView, NewEbolMarkDamagesPresenter>
        implements NewEbolMarkDamagesView {

    @Bind(R.id.photoResult)
    ImageView photoResult;
    @Bind(R.id.carModel)
    TextView vehicleModel;
    @Bind(R.id.photoType)
    TextView type;
    @Bind(R.id.photoCounter)
    TextView photoCounter;
    @Bind(R.id.markDamages)
    MarkDamagesWidget markDamages;

    String photoType;
    int selectedVehicleId;

    VehicleInfo vehicleInfo;
    VehiclePhoto vehiclePhoto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mark_damages, container, false);
        ButterKnife.bind(this, view);

        markDamages.setCallback(this::startDamageChooseActivity);

        if(savedInstanceState != null){
            selectedVehicleId = savedInstanceState.getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        } else if (getArguments() != null) {
            selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        photoType = getArguments().getString("phototype");
        getPresenter().getData(selectedVehicleId, photoType);
    }

    @Override
    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    @Override
    public void initViews() {
        photoType = vehiclePhoto.getPhotoType();
        loadPhoto(vehiclePhoto.getPhotoPath());
        showViews(photoType);
    }

    public void loadPhoto(String photoPath) {
        if(vehiclePhoto.isLocal()) {
            loadPhotoFromDisk(photoPath);
        } else {
            Picasso.with(getContext()).load(photoPath).into(photoResult);
        }
    }

    private void loadPhotoFromDisk(String photoPath) {
        File file = new File(photoPath);
        Picasso picasso = Picasso.with(getContext());
        picasso.invalidate(file);
        picasso.load(file).fit().into(photoResult);
    }

    private void showViews(String photoType) {
        setPhotoDescription(photoType);
        markDamages.setPhoto(vehiclePhoto);
        markDamages.paintDamages();
    }

    @Override
    public void setVehiclePhoto(VehiclePhoto vehiclePhoto) {
        this.vehiclePhoto = vehiclePhoto;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        super.onSaveInstanceState(bundle);
    }

    public void startDamageChooseActivity(Intent intent) {
        intent.putExtra("phototype", photoType);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        startActivityForResult(intent, 32908);
    }

    private void setPhotoDescription(String photoType) {
        switch (photoType){
            case VehiclePhotosWidget.FRONT_PHOTO:
                type.setText("Driver Front");
                photoCounter.setText("1 of 4");
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                type.setText("Driver Back");
                photoCounter.setText("2 of 4");
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                type.setText("Driver Right");
                photoCounter.setText("3 of 4");
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                type.setText("Driver Left");
                photoCounter.setText("4 of 4");
                break;
            default:
                type.setVisibility(View.INVISIBLE);
                photoCounter.setVisibility(View.INVISIBLE);
                break;
        }
        vehicleModel.setText(String.format("%s %s %s", vehicleInfo.getYearOfMake(), vehicleInfo.getMake(),
                vehicleInfo.getModel()));
    }

    @OnClick(R.id.cancel)
    void cancelClick(View v) {
        ((NewEbolCameraAcitvity)getActivity()).showCameraFragment(photoType);
    }

    @OnClick(R.id.save)
    void saveClick(View v) {
        List<MarkedDamage> markedDamages = new ArrayList<>(vehiclePhoto.getMarkedDamages());
        for (int i = 0; i < markedDamages.size(); i++) {
            markedDamages.get(i).setIsTemp(false);
        }
        getPresenter().updateVehiclePhoto(vehiclePhoto);
    }

    @Override
    public void openVehicleInspection() {
        ((NewEbolCameraAcitvity)getActivity()).showVehicleInspectionsFragment(photoType);
    }

    public void onActivityResult(int requestCode, int resultCode,
                                 Intent intent) {

        if (requestCode == 32908 && resultCode == Activity.RESULT_OK) {
            String action = intent.getStringExtra("action");

            if(action.equals("delete")) {
                markDamages.deleteDamage();
            } else {
                getData();
                //todo mb look how it works
//                showViews(photoType, intent.getBooleanExtra("fromEdit", false));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

//    private void paintDamages() {
//        for (final MarkedDamage markedDamage : tempPhoto.getMarkedDamages()) {
//            int yPos = markedDamage.getyPosition();
//            int xPos = markedDamage.getxPosition();
//            RelativeLayout.LayoutParams params = getLayoutParams(yPos, xPos);
//            TextView textView = getDamageView(markedDamage, params);
//            parent.addView(textView);
//        }
//    }

//    @NonNull
//    private RelativeLayout.LayoutParams getLayoutParams(int yPos, int xPos) {
//        RelativeLayout.LayoutParams params =
//                new RelativeLayout.LayoutParams(getPixelSize(35), getPixelSize(35));
//        params.leftMargin = xPos;
//        params.topMargin = yPos;
//        return params;
//    }
//
//    int getPixelSize(int i) {
//        Resources r = getResources();
//        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, r.getDisplayMetrics());
//    }

//    @NonNull
//    private TextView getDamageView(final MarkedDamage markedDamage, RelativeLayout.LayoutParams params) {
//        String damageCode;
//        if(markedDamage.getDamageTypes().size() == 1) {
//            damageCode = new ArrayList<>(markedDamage.getDamageTypes()).get(0).getDamageShortName();
//        } else {
//            damageCode = String.valueOf(new ArrayList<>(markedDamage.getDamageTypes()).size());
//        }
//        int yPos = markedDamage.getyPosition();
//        int xPos = markedDamage.getxPosition();
//        String key = String.format("%d%d", yPos, xPos);
//        if (drawedDamages.containsKey(key)) {
//            View view = drawedDamages.remove(key);
//            parent.removeView(view);
//        }
//        TextView textView = new TextView(getContext());
//        textView.setLayoutParams(params);
//        textView.setText(damageCode);
//        textView.setTextColor(Color.WHITE);
//        textView.setGravity(Gravity.CENTER);
//        textView.setTextSize(16);
//        textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.krasny));
//        textView.setOnClickListener(v -> openDamageActivity(markedDamage, photoType));
//        drawedDamages.put(key, textView);
//        return textView;
//    }
//
//    private void openDamageActivity(MarkedDamage markedDamage, String photoType) {
//        NewEbolMarkDamagesFragment.this.xPosition = markedDamage.getxPosition();
//        NewEbolMarkDamagesFragment.this.yPosition = markedDamage.getyPosition();
//        Intent intent = new Intent(getContext(), NewEbolChooseDamageTypeActivity.class);
//        intent.putExtra("xPosition", markedDamage.getxPosition());
//        intent.putExtra("yPosition", markedDamage.getyPosition());
//        if (getArguments() != null && getArguments().containsKey("phototype")) {
//            intent.putExtra("phototype", photoType);
//            intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
//        }
//        startActivityForResult(intent, 32908);
//    }
//
//    private void showViews(String photoType) {
//
//        setPhotoDescription(photoType);
//        markDamages.setMarkedDamages(carPhoto.getMarkedDamages());
//        markDamages.paintDamages();
//
////        if (fromEdit) {
////            getEditedCarPhoto(photoType);
////        } else {
////            tempPhoto = getPresenter();
////        }
////        if(tempPhoto == null){
////            tempPhoto = repo.repoCarPhoto.getTempCarPhoto();
////        }
////        setPhotoDescription(photoType);
////        paintDamages();
////        parent.invalidate();
//    }

//    private void getEditedCarPhoto(String photoType) {
//        vehicleInfo = repo.repoVehicleInfo.getVehicleInfoById(selectedVehicleId);
//        switch (photoType){
//            case NewEbolDamagesFragment.FRONT_PHOTO:
//                tempPhoto = vehicleInfo.getDamages().getFront();
//                break;
//            case NewEbolDamagesFragment.BACK_PHOTO:
//                tempPhoto = vehicleInfo.getDamages().getBack();
//                break;
//            case NewEbolDamagesFragment.LEFT_SIDE_PHOTO:
//                tempPhoto = vehicleInfo.getDamages().getLeftSide();
//                break;
//            case NewEbolDamagesFragment.RIGHT_SIDE_PHOTO:
//                tempPhoto = vehicleInfo.getDamages().getRightSide();
//                break;
//            case NewEbolDamagesFragment.ADDITIONAL_PHOTO_FIRST:
//                tempPhoto = vehicleInfo.getDamages().getAdditionalFirst();
//                break;
//            case NewEbolDamagesFragment.ADDITIONAL_PHOTO_SECOND:
//                tempPhoto = vehicleInfo.getDamages().getAdditionalSecond();
//                break;
//            default:
//        }
//    }
//
//    public boolean onTouch(View v, MotionEvent event) {
//        int action = event.getAction();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                break;
//            case MotionEvent.ACTION_MOVE:
//                break;
//            case MotionEvent.ACTION_UP:
//                openChooseDamaTypeActivity(event);
//                break;
//            case MotionEvent.ACTION_CANCEL:
//                break;
//            default:
//                break;
//        }
//        return true;
//    }
//
//    public void openChooseDamaTypeActivity(MotionEvent event) {
//        xPosition = (int)event.getX();
//        yPosition = (int)event.getY();
//        Intent intent = new Intent(getContext(), NewEbolChooseDamageTypeActivity.class);
//        if (getArguments() != null && getArguments().containsKey("phototype")) {
//            intent.putExtra("phototype", getArguments().getString("phototype"));
//            intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
//        }
//        intent.putExtra("xPosition", xPosition);
//        intent.putExtra("yPosition", yPosition);
//        startActivityForResult(intent, 32908);
//    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public NewEbolMarkDamagesPresenter createPresenter() {
        return new NewEbolMarkDamagesPresenter();
    }
}
