package com.github.andrdev.bolservice.history;

import android.util.Log;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Submit;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.SubmitsResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit.Callback;


public class HistoryPresenter extends MvpBasePresenter<BaseSearchView> {

    HistoryDataProvider dataProvider;
    Set<Integer> vehicleIds;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        vehicleIds = new HashSet<>();
        dataProvider = new HistoryDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getEbols() {
        dataProvider.getEbols(this::setData, this::stopRefresh);
    }

    private void stopRefresh(Throwable t) {
        if(!isViewAttached()){
            return;
        }
        getView().stopRefresh();
        getView().showRequestFailedToast();
    }

    private void setData(EbolsResponse ebolsResponse) {
        if(!isViewAttached()){
            return;
        }
        getView().addData(ebolsResponse.getEbols());
    }

    public void netItemClicked(NewEbol ebol) {
        dataProvider.deleteTempEbol(deleted -> getVehicles(ebol));
    }
    private void getVehicles(NewEbol ebol) {
        dataProvider.getVehicles(String.valueOf(ebol.getSubmitId()),
                response -> getVehicleInfosSuccess(response, ebol));
    }

    private void getVehicleInfosSuccess(VehicleInfoResponse vehicleInfoResponse, NewEbol ebol) {
        ebol.setType("temp");
        ebol.setVehicleInfos(vehicleInfoResponse.getVehicleInfos());
        for (VehicleInfo info : ebol.getVehicleInfos()) {
            vehicleIds.add(info.getCid());
            dataProvider.getVehicleDamages(info.getCid(), damagesResponse ->
                    setDamagesToVehicle(ebol, info, damagesResponse));
        }
    }

    private void setDamagesToVehicle(NewEbol ebol, VehicleInfo info, DamagesResponse damagesResponse) {
        vehicleIds.remove(info.getCid());
        Map<String, VehiclePhoto> photos = damagesResponse.getVehiclePhotos();
        if(photos.containsKey("front")){
            info.getDamages().setFront(photos.get("front"));
        }
        if(photos.containsKey("back")){
            info.getDamages().setBack(photos.get("back"));
        }
        if(photos.containsKey("rightside")){
            info.getDamages().setRightSide(photos.get("rightside"));
        }
        if(photos.containsKey("leftside")){
            info.getDamages().setLeftSide(photos.get("leftside"));
        }
        if(vehicleIds.size() == 0){
            dataProvider.saveEbol(ebol, this::openEbol);
        }
    }

    private void openEbol(boolean saved) {
        if(!isViewAttached()){
            return;
        }
        getView().openListItem();
    }

//    private void saveEbol(NewEbol ebolTemp) {
//        ebolTemp.setType("temp");
//        dataProvider.saveEbol(ebolTemp, saved -> getVehicles(ebolTemp.getSubmitId()));
//    }


//    List<NewEbol> submitToEbol(List<Submit> submits) {
//        List<NewEbol> ebols = new ArrayList<>();
//        for (Submit submit : submits) {
//            NewEbol ebol = new NewEbol();
//            ebol.setSubmitId(submit.getId());
//            ebol.setType(submit.getStatus());
////        ebol.setDate(submit.getCreatedAt());
//            try {
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
//                Date d = sdf.parse(submit.getCreatedAt());
//                String formattedTime = output.format(d);
//                Log.d("dreedatese", formattedTime);
//                ebol.setDate(formattedTime);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
////            SimpleDateFormat month_date = new SimpleDateFormat();
////            ebol.setDate(1505050);
//            ebol.setOrderId(submit.getOrderId());
//            ebol.setVehiclesCount(submit.getVehiclesCount());
//            Customer origin = new Customer();
//            origin.setCustomerName(submit.getDetailed().getOriginCustomerName());
//            origin.setAddress(submit.getDetailed().getOriginAddress());
//            origin.setCity(submit.getDetailed().getOriginCity());
//            origin.setState(submit.getDetailed().getOriginState());
//            origin.setZip(submit.getDetailed().getOriginZip());
//            origin.setPhone(submit.getDetailed().getOriginPhone());
//            origin.setCell(submit.getDetailed().getOriginBuyerNumber());
//            Customer destination = new Customer();
//            destination.setCustomerName(submit.getDetailed().getDestinationCustomerName());
//            destination.setAddress(submit.getDetailed().getDestinationAddress());
//            destination.setCity(submit.getDetailed().getDestinationCity());
//            destination.setState(submit.getDetailed().getDestinationState());
//            destination.setZip(submit.getDetailed().getDestinationZip());
//            destination.setPhone(submit.getDetailed().getDestinationPhone());
//            destination.setCell(submit.getDetailed().getDestinationBuyerNumber());
//            ebol.setOrigin(origin);
//            ebol.setDestination(destination);
//            ebols.add(ebol);
//        }
//        return ebols;
////    }
//
//    private List<VehicleInfo> carsToVehicleInfos(List<Car> cars) {
//        List<VehicleInfo> vehicles = new ArrayList<>();
//        for (Car car : cars) {
//            VehicleInfo info = new VehicleInfo();
//            info.setCid(car.getId());
//            info.setPlate(car.getPlateNumber());
//            info.setVin(car.getVinNumber());
//            info.setColor(car.getColor());
//            info.setMake(car.getMake());
//            info.setModel(car.getModel());
//            info.setMileage(car.getMileage());
//            info.setYear(car.getYearOfMake());
//            Damages damages = new Damages();
//            damages.setFloorMats(Integer.valueOf(car.getFloorMats()));
//            damages.setHeadrest(Integer.valueOf(car.getHeadrestsCount()));
//            damages.setKeys(Integer.valueOf(car.getKeysCount()));
//            damages.setRemotes(Integer.valueOf(car.getRemotesCount()));
//            damages.setCargoCovers(car.isCargoCover());
//            damages.setSpareTires(car.isSpareTire());
//            damages.setManuals(car.isManuals());
//            damages.setManuals(car.isRadioPresent());
//            info.setDamages(damages);
//            vehicles.add(info);
//        }
//        return vehicles;
//    }
}
