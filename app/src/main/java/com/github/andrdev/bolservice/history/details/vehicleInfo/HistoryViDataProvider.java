package com.github.andrdev.bolservice.history.details.vehicleInfo;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class HistoryViDataProvider implements DataProvider {

    private DbHelper dbHelper;

    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> successCb) {
        dbHelper.getVehicleById(vehicleId, successCb::returnData);
    }
}
