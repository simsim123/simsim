package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;


public class CompanyInfoRequestBuilder {
    private String address;
    private String city;
    private String companyName;
    private String dotNumber;
    private String email;
    private String fax;
    private String mcNumber;
    private String phone;
    private String state;
    private String website;
    private String zip;

    public CompanyInfoRequestBuilder setAddress(EditText address) {
        if(Utils.hasText(address)){
            this.address = Utils.getTextFromEditText(address);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setCity(EditText city) {
        if(Utils.hasText(city)){
            this.city = Utils.getTextFromEditText(city);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setEmail(EditText email) {
        if(Utils.hasText(email)){
            this.email = Utils.getTextFromEditText(email);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setFax(EditText fax) {
        if(Utils.hasText(fax)){
            this.fax = Utils.getTextFromEditText(fax);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setPhone(EditText phone) {
        if(Utils.hasText(phone)){
            this.phone = Utils.getTextFromEditText(phone);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setState(EditText state) {
        if(Utils.hasText(state)){
            this.state = Utils.getTextFromEditText(state);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setZip(EditText zip) {
        if(Utils.hasText(zip)){
            this.zip = Utils.getTextFromEditText(zip);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setCompanyName(EditText companyName) {
        if(Utils.hasText(companyName)){
            this.companyName = Utils.getTextFromEditText(companyName);
        }
        return this;
    }

    public CompanyInfoRequestBuilder setDotNumber(EditText dotNumber) {
        if(Utils.hasText(dotNumber)){
            this.dotNumber = Utils.getTextFromEditText(dotNumber);
        }
        return this;
    }


    public CompanyInfoRequestBuilder setMcNumber(EditText mcNumber) {
        if(Utils.hasText(mcNumber)){
            this.mcNumber = Utils.getTextFromEditText(mcNumber);
        }
        return this;
    }


    public CompanyInfoRequestBuilder setWebsite(EditText website) {
        if(Utils.hasText(website)){
            this.website = Utils.getTextFromEditText(website);
        }
        return this;
    }


    public CompanyInfoRequest createCompanyInfo() {
        return new CompanyInfoRequest(address, city, companyName, dotNumber, email, fax, mcNumber, phone,
                state, website, zip);
    }
}