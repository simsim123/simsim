package com.github.andrdev.bolservice.view.markDamagesNe;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface MarkDamagesNeView extends MvpView{
    Context getViewContext();
}
