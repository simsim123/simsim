package com.github.andrdev.bolservice.profile.baseInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ProfileBaseView extends MvpView {

    Context getViewContext();

    void setProfile(Profile profile);

    void initFields();

    void showFailedToast();

}
