package com.github.andrdev.bolservice.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.clients.ClientsAdapter;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectClientDialogFrament extends DialogFragment {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.searchAct)
    EditText searchAct;

    ClientsAdapter clientsAdapter;
    List<Client> clientsMain = new ArrayList<>();
    List<Client> clients = new ArrayList<>();
    SelectedCallback callback;
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_choose_client, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        sendRequest();
        searchAct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String temp = searchAct.getText().toString().toLowerCase();
                if (temp.length() > 0) {
                    clients = new ArrayList<>();
                    for (Client client : clientsMain) {
                        if (client.getCustomerName().toLowerCase().contains(temp)
                                || client.getPhone().toLowerCase().contains(temp)) {
                            clients.add(client);
                        }
                    }
                } else {
                    clients = clientsMain;
                }
                clientsAdapter.setData(clients);
                clientsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private void sendRequest() {
        EbolNetworkWorker2.getInstance().getClients("", this::setClients, this::showFailedToast);
    }

    private void showFailedToast(Throwable throwable) {
        Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG);
    }

    public void setClients(ClientsResponse clientsResponse) {
        clientsMain = clientsResponse.getClients();
        clients = clientsResponse.getClients();
        clientsAdapter.setData(clientsResponse.getClients());
        clientsAdapter.notifyDataSetChanged();
    }

    public SelectedCallback getCallback() {
        return callback;
    }

    public void setCallback(SelectedCallback callback) {
        this.callback = callback;
    }

    private void initRecycler() {
        clientsAdapter = new ClientsAdapter();
        clientsAdapter.setData(clients);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(clientsAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                (view, position)->{
                        Client client = clients.get(position);
                        callback.clientSelected(client);
                        dismiss();
                    }));
    }

    @OnClick(R.id.cancel)
    void cancelClick() {
        dismiss();
    }

    public static SelectClientDialogFrament newInstance() {
        SelectClientDialogFrament dialogFrament = new SelectClientDialogFrament();
        return dialogFrament;
    }

    public interface SelectedCallback{
        void clientSelected(Client client);
    }
}