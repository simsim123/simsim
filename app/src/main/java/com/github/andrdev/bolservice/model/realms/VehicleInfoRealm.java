package com.github.andrdev.bolservice.model.realms;

import com.github.andrdev.bolservice.model.realms.DamagesRealm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class VehicleInfoRealm extends RealmObject {

    public static final String ID = "id";
    public static final String CID = "cid";
    public static final String YEAR = "yearOfMake";
    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String COLOR = "color";
    public static final String MILEAGE = "mileage";
    public static final String VIN_NUMBER = "vinNumber";
    public static final String PLATE_NUMBER = "plateNumber";
    public static final String TYPE = "type";
    public static final String DAMAGES = "damages";

    @PrimaryKey
    private int id;
    private int cid = -1;
    private String yearOfMake;
    private String make;
    private String model;
    private String vinNumber;
    private String mileage;
    private String color;
    private String plateNumber;
    private String type;
    private DamagesRealm damages;

    public VehicleInfoRealm() {
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public DamagesRealm getDamages() {
        return damages;
    }

    public void setDamages(DamagesRealm damages) {
        this.damages = damages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(String yearOfMake) {
        this.yearOfMake = yearOfMake;
    }
}
