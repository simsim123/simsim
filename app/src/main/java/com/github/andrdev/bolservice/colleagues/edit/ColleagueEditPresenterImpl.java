package com.github.andrdev.bolservice.colleagues.edit;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class ColleagueEditPresenterImpl extends ColleagueEditPresenter {

    ColleagueEditDataProvider dataProvider;

    @Override
    public void attachView(ColleagueEditView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ColleagueEditDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getSavedColleague(this::colleagueRequestSuccess);
    }

    @Override
    public void deleteItem(Colleague colleague) {
        if (!isViewAttached()) {
            return;
        }
        EbolNetworkWorker2.getInstance().deleteDriver(String.valueOf(colleague.getId()), this);
    }


    @SuppressWarnings("ConstantConditions")
    @Override
    public void deleteRequestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void deleteRequestSuccess(SimpleResponse simpleResponse) {
        if (!isViewAttached()) {
            return;
        }
        getView().backToColleagueDetails();
    }

    @Override
    public void updateColleague(final DriverUpdateRequest request) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.updateDriver(request,
                (response) -> onUpdateSuccess(request),
                this::onUpdateFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onUpdateSuccess(DriverUpdateRequest request) {
        if (!isViewAttached()) {
            return;
        }

        Colleague colleague = driverRequestToColleague(request);
        dataProvider.saveColleague(colleague, this::colleagueSaved);
    }

    private Colleague driverRequestToColleague(DriverUpdateRequest driverRequest) {
        Colleague colleague = new Colleague();
        colleague.setFirstname(driverRequest.getFirstName());
        colleague.setLastname(driverRequest.getLastName());
        colleague.setDriverEmail(driverRequest.getDriverEmail());
        colleague.setDriverPhone(driverRequest.getDriverPhone());
        return colleague;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onUpdateFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    public void colleagueRequestSuccess(Colleague colleague) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(colleague);
        getView().initFields();
    }

    @SuppressWarnings("ConstantConditions")
    public void colleagueSaved(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().backToColleagueDetails();
    }

    @SuppressWarnings("ConstantConditions")
    public void saveColleagueFailed() {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }
}
