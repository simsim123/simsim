package com.github.andrdev.bolservice.colleagues.edit;

import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ColleagueEditPresenter extends MvpBasePresenter<ColleagueEditView> {

    public abstract void getData();

    public abstract void deleteItem(Colleague colleague);

    public abstract void deleteRequestFailed(Throwable throwable);

    public abstract void deleteRequestSuccess(SimpleResponse simpleResponse);

    public abstract void updateColleague(DriverUpdateRequest request);

    public abstract void onUpdateSuccess(DriverUpdateRequest request);

    public abstract void onUpdateFailed(Throwable throwable);
}
