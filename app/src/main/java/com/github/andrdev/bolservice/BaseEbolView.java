package com.github.andrdev.bolservice;

import com.github.andrdev.bolservice.model.realms.NewEbolRealm;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface BaseEbolView extends MvpView {
    void setAndShowData(NewEbolRealm ebol);
}
