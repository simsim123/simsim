package com.github.andrdev.bolservice.model.deserializers;

import android.util.Log;

import com.github.andrdev.bolservice.model.Vehicle;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.model.responses.VehiclesResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VehicleInfosResponseTypedFactory extends CustomizedTypeAdapterFactory<VehicleInfoResponse> {

    public VehicleInfosResponseTypedFactory(Class<VehicleInfoResponse> customizedClass) {
        super(customizedClass);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected VehicleInfoResponse deserialize(JsonElement deserialized, Gson gson) {
        VehiclesResponse vehiclesResponse = null;
        Log.d("ss", deserialized.toString());
        try {
            vehiclesResponse = gson.getDelegateAdapter(this, TypeToken.get(VehiclesResponse.class))
                    .fromJson(String.valueOf(deserialized));
        } catch (IOException e) {
            e.printStackTrace();
        }
        VehicleInfoResponse vehicleInfoResponse = new VehicleInfoResponse();
        vehicleInfoResponse.setVehicleInfos(carsToVehicleInfos(vehiclesResponse.getCars()));
        return vehicleInfoResponse;
    }

    private List<VehicleInfo> carsToVehicleInfos(List<Vehicle> vehicles) {
        List<VehicleInfo> vehicleInfos = new ArrayList<>();
        for (Vehicle car : vehicles) {
            VehicleInfo info = new VehicleInfo();
            info.setCid(car.getId());
            info.setPlateNumber(car.getPlateNumber());
            info.setVinNumber(car.getVinNumber());
            info.setColor(car.getColor());
            info.setMake(car.getMake());
            info.setModel(car.getModel());
            info.setMileage(car.getMileage());
            info.setYearOfMake(car.getYearOfMake());
            Damages damages = new Damages();
            damages.setFloorMats(Integer.valueOf(car.getFloorMats()));
            damages.setHeadrest(Integer.valueOf(car.getHeadrestsCount()));
            damages.setKeys(Integer.valueOf(car.getKeysCount()));
            damages.setRemotes(Integer.valueOf(car.getRemotesCount()));
            damages.setCargoCovers(car.isCargoCover());
            damages.setSpareTires(car.isSpareTire());
            damages.setManuals(car.isManuals());
            damages.setManuals(car.isRadioPresent());
            info.setDamages(damages);
            vehicleInfos.add(info);
        }
        return vehicleInfos;
    }
}
