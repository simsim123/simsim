package com.github.andrdev.bolservice.model.requests;


public class ClientCreateRequest {

    String customerName;
    String address;
    String city;
    String tel;
    String fax;
    String zip;
    String email;
    String state;

    public ClientCreateRequest(String address, String city, String customerName, String email, String fax, String state, String tel, String zip) {
        this.address = address;
        this.city = city;
        this.customerName = customerName;
        this.email = email;
        this.fax = fax;
        this.state = state;
        this.tel = tel;
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
