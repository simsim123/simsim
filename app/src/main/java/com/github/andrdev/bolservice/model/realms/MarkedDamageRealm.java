package com.github.andrdev.bolservice.model.realms;

import com.github.andrdev.bolservice.model.realms.DamageTypeRealm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class MarkedDamageRealm extends RealmObject{

    public static final String ID = "id";
    public static final String Y_POSITION = "yPosition";
    public static final String X_POSITION = "xPosition";
    public static final String DAMAGE_TYPES = "damageTypes";
    public static final String IS_TEMP = "isTemp";


    @PrimaryKey
    private int id;
    private double yPosition;
    private double xPosition;
    private RealmList<DamageTypeRealm> damageTypes;
    private boolean isTemp;

    public MarkedDamageRealm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTemp() {
        return isTemp;
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp;
    }

    public double getxPosition() {
        return xPosition;
    }

    public void setxPosition(double xPosition) {
        this.xPosition = xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public void setyPosition(double yPosition) {
        this.yPosition = yPosition;
    }

    public RealmList<DamageTypeRealm> getDamageTypes() {
        return damageTypes;
    }

    public void setDamageTypes(RealmList<DamageTypeRealm> damageTypes) {
        this.damageTypes = damageTypes;
    }
}
