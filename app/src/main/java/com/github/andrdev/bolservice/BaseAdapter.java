package com.github.andrdev.bolservice;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.ButterKnife;


public abstract class BaseAdapter<T, VH extends BaseAdapter.BaseViewHolder<T>>
        extends RecyclerView.Adapter<VH>  {

    protected List<T> data;

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).inflate(getLayoutResource(viewType), parent, false);
        return getHolder(view);
    }

    protected abstract int getLayoutResource(int viewType);

    protected abstract VH getHolder(View view);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public abstract static class BaseViewHolder<IT> extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected abstract void bind(IT item);
    }
}
