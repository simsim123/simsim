package com.github.andrdev.bolservice.clients;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.model.Client;

import butterknife.Bind;


public class ClientsAdapter extends BaseAdapter<Client, ClientsAdapter.ClientsHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_drivers;
    }

    @Override
    protected ClientsHolder getHolder(View view) {
        return new ClientsHolder(view);
    }

    protected static class ClientsHolder extends BaseAdapter.BaseViewHolder<Client> {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.phone)
        TextView phone;

        public ClientsHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(Client item) {
            name.setText(item.getCustomerName());
            phone.setText(item.getPhone());
        }
    }
}
