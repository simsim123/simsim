package com.github.andrdev.bolservice.model;

import io.realm.RealmObject;


public class Invoice extends RealmObject {
    public static final String ID = "id";
    public static final String INVOICE_ID = "invoiceId";
    public static final String AMOUNT = "amount";
    public static final String PAYMENT_TERMS = "paymentTerms";
    public static final String CREATED_AT = "createdAt";
    public static final String USER = "user";
    public static final String STATUS = "status";
    public static final String FULLNAME = "fullname";
    public static final String ORDER_ID = "orderId";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String PHONE = "phone";
    public static final String CELL = "cell";
    public static final String NOTES = "notes";
    public static final String VEHICLES_COUNT = "vehiclesCount";

    private int id;
    private String invoiceId;
    private float amount;
    private String paymentTerms;
    private String createdAt;
    private int user;
    private String status;
    private String fullname;
    private String orderId;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phone;
    private String cell;
    private String notes;
    private int vehiclesCount;

    public Invoice(){ }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setInvoiceId(String invoiceId){
        this.invoiceId = invoiceId;
    }
    public String getInvoiceId(){
        return this.invoiceId;
    }
    public void setAmount(float amount){
        this.amount = amount;
    }
    public float getAmount(){
        return this.amount;
    }
    public void setPaymentTerms(String paymentTerms){
        this.paymentTerms = paymentTerms;
    }
    public String getPaymentTerms(){
        return this.paymentTerms;
    }
    public void setCreatedAt(String createdAt){
        this.createdAt = createdAt;
    }
    public String getCreatedAt(){
        return this.createdAt;
    }
    public void setUser(int user){
        this.user = user;
    }
    public int getUser(){
        return this.user;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
    public void setFullname(String fullname){
        this.fullname = fullname;
    }
    public String getFullname(){
        return this.fullname;
    }
    public void setOrderId(String orderId){
        this.orderId = orderId;
    }
    public String getOrderId(){
        return this.orderId;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress(){
        return this.address;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return state;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getZip() {
        return zip;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setCell(String cell){
        this.cell = cell;
    }
    public String getCell(){
        return this.cell;
    }
    public void setNotes(String notes){
        this.notes = notes;
    }
    public String getNotes(){
        return this.notes;
    }
    public void setVehiclesCount(int vehiclesCount) {
        this.vehiclesCount = vehiclesCount;
    }
    public int getVehiclesCount() {
        return vehiclesCount;
    }
}

