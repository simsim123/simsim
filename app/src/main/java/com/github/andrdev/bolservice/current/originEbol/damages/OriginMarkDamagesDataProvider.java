package com.github.andrdev.bolservice.current.originEbol.damages;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.requests.NewDamageRequest;
import com.github.andrdev.bolservice.model.responses.NewDamageResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;

public class OriginMarkDamagesDataProvider implements DataProvider {

    private DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getTempVehicle(DataCb<VehicleInfo> callbackSuccess) {
        dbHelper.getTempVehicle(callbackSuccess::returnData);
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(vehicleId, callback::returnData);
    }

//    public void getTempCarPhoto(DataCb<CarPhoto> callback) {
//        dbHelper.getTempCarPhoto(callback::returnData);
//    }

    public void updateVehiclePhoto(VehiclePhoto vehiclePhoto, DataCb<Boolean> callback) {
        dbHelper.updateVehiclePhoto(vehiclePhoto, callback::returnData);
    }

    public void updateVehicleInfo(VehicleInfo vehicleInfo, DataCb<Boolean> callback) {
        dbHelper.updateVehicle(vehicleInfo, callback::returnData);
    }

    public void sendNewDamagesRequest(final VehicleInfo info, DataCb<VehicleInfo> callback) {
        final Map<String, NewDamageRequest> map = new HashMap<>();
//
//        WindowManager wm = (WindowManager) getView().getContext().getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//        ndr.setPositionTop(String.valueOf(round(dm.getyPosition() / (height / 100d), 2)));
//        ndr.setPositionLeft(String.valueOf(round(dm.getxPosition() / (width / 100d), 2)));
//
//        if(info.getDamages().getFront()!=null) {
//            for (MarkedDamage dm : info.getDamages().getFront().getMarkedDamagesDelivery()) {
//                if(!dm.isTemp()) continue;
//                NewDamageRequest ndr = new NewDamageRequest();
//                ndr.setCid(String.valueOf(info.getCid()));
//                ndr.setSide("front");
//                ndr.setPositionTop(String.valueOf(round(dm.getyPosition() / (height / 100d), 2)));
//                ndr.setPositionLeft(String.valueOf(round(dm.getxPosition() / (width / 100d), 2)));
//                ndr.setCode(damageTypesString(dm.getDamageTypes()));
//                map.put(ndr.getPositionLeft() + ndr.getPositionTop(), ndr);
//                EbolNetworkWorker2.getInstance().postDamages(ndr, ebolCb);
//                dm.setIsTemp(false);
//                repo.repoMarkedDamage.updateMarkedDamage(dm);
//            }
//        }
//        if(info.getDamages().getBack()!=null) {
//            for (MarkedDamage dm : info.getDamages().getBack().getMarkedDamagesDelivery()) {
//                if(!dm.isTemp()) continue;
//                NewDamageRequest ndr = new NewDamageRequest();
//                ndr.setCid(String.valueOf(info.getCid()));
//                ndr.setSide("back");
//                ndr.setPositionTop(String.valueOf(round(dm.getyPosition() / (height / 100d), 2)));
//                ndr.setPositionLeft(String.valueOf(round(dm.getxPosition() / (width / 100d), 2)));
//                ndr.setCode(damageTypesString(dm.getDamageTypes()));
//                map.put(ndr.getPositionLeft() + ndr.getPositionTop(), ndr);
//                EbolNetworkWorker2.getInstance().postDamages(ndr, ebolCb);
//                dm.setIsTemp(false);
//                repo.repoMarkedDamage.updateMarkedDamage(dm);
//            }
//        }
//        if(info.getDamages().getRightSide()!=null) {
//            for (MarkedDamage dm : info.getDamages().getRightSide().getMarkedDamagesDelivery()) {
//                if(!dm.isTemp()) continue;
//                NewDamageRequest ndr = new NewDamageRequest();
//                ndr.setCid(String.valueOf(info.getCid()));
//                ndr.setSide("passenger");
//                ndr.setPositionTop(String.valueOf(round(dm.getyPosition() / (height / 100d), 2)));
//                ndr.setPositionLeft(String.valueOf(round(dm.getxPosition() / (width / 100d), 2)));
//                ndr.setCode(damageTypesString(dm.getDamageTypes()));
//                map.put(ndr.getPositionLeft() + ndr.getPositionTop(), ndr);
//                EbolNetworkWorker2.getInstance().postDamages(ndr, ebolCb);
//                dm.setIsTemp(false);
//                repo.repoMarkedDamage.updateMarkedDamage(dm);
//            }
//        }
//        if(info.getDamages().getLeftSide()!=null) {
//            for (MarkedDamage dm : info.getDamages().getLeftSide().getMarkedDamagesDelivery()) {
//                if(!dm.isTemp()) continue;
//                NewDamageRequest ndr = new NewDamageRequest();
//                ndr.setCid(String.valueOf(info.getCid()));
//                ndr.setSide("driver");
//
//                ndr.setPositionTop(String.valueOf(round(dm.getyPosition() / (height / 100d), 2)));
//                ndr.setPositionLeft(String.valueOf(round(dm.getxPosition() / (width / 100d), 2)));
//                ndr.setCode(damageTypesString(dm.getDamageTypes()));
//                map.put(ndr.getPositionLeft() + ndr.getPositionTop(), ndr);
//                EbolNetworkWorker2.getInstance().postDamages(ndr, ebolCb);
//                dm.setIsTemp(false);
//                repo.repoMarkedDamage.updateMarkedDamage(dm);
//            }
//        }
        callback.returnData(info);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private String damageTypesString(List<DamageType> damageTypes) {
        StringBuilder builder = new StringBuilder();
        for (DamageType type : damageTypes) {
            builder.append(type.getDamageShortName()).append(",");
        }
        return builder.toString();
    }
}
