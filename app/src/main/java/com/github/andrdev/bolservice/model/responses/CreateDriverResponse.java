package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Driver;


public class CreateDriverResponse {

    String status;
    String msg;
    Driver driver;

    public CreateDriverResponse() {
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
