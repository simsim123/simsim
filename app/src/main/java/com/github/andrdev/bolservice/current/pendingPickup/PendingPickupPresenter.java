package com.github.andrdev.bolservice.current.pendingPickup;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rx.Observable;


public class PendingPickupPresenter  extends MvpBasePresenter<PendingPickupView> {

    PendingPickupDataProvider dataProvider;
    Set<Integer> vehicleIds;

    @Override
    public void attachView(PendingPickupView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        vehicleIds = new HashSet<>();
        dataProvider = new PendingPickupDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    private void stopRefresh(Throwable t) {
        if(!isViewAttached()){
            return;
        }
        getView().stopRefresh();
        getView().showRequestFailedToast();
    }

    private void setData(EbolsResponse ebolsResponse) {
        if(isViewAttached())
            getView().stopRefresh();
            getView().addEbols(ebolsResponse.getEbols());
    }

    private void setData(List<NewEbol> ebols) {
        if(!isViewAttached()){
            return;
        }
        getView().addEbols(ebols);
    }

    public void netItemClicked(NewEbol ebol) {
        dataProvider.deleteNetSaveLocalEbol();
        Log.d("dree", "netItemClicked: "+ebol.getSubmitId());
        if (ebol.getSubmitId() != 0) {
            Log.d("dree", "netItemClicked: "+ebol.getSubmitId());
            getVehicles(ebol);
        } else {
            ebol.setType("temp");
            dataProvider.saveEbol(ebol, this::openEbol);
        }
    }

    private void getVehicles(NewEbol ebol) {
        dataProvider.getVehicles(String.valueOf(ebol.getSubmitId()),
                response -> getVehicleInfosSuccess(response, ebol), (t)-> Log.d("Dreeerror", "getVehicles: "+t.getMessage()));
    }
    private void getVehicleInfosSuccess(VehicleInfoResponse vehicleInfoResponse, NewEbol ebol) {
        ebol.setType("temp");
        Log.d("dree", "netItemClicked: " + ebol.getType());
        ebol.setVehicleInfos(vehicleInfoResponse.getVehicleInfos());
        for (VehicleInfo info : ebol.getVehicleInfos()) {
            vehicleIds.add(info.getCid());
            dataProvider.getVehicleDamages(info.getCid(), damagesResponse ->
                    setDamagesToVehicle(ebol, info, damagesResponse));
        }
        if(vehicleInfoResponse.getVehicleInfos().isEmpty()) {
            dataProvider.saveEbol(ebol, this::openEbol);
        }
    }

    private void setDamagesToVehicle(NewEbol ebol, VehicleInfo info, DamagesResponse damagesResponse) {
        vehicleIds.remove(info.getCid());
        Map<String, VehiclePhoto> photos = damagesResponse.getVehiclePhotos();
        if(photos.containsKey("front")){
            info.getDamages().setFront(photos.get("front"));
        }
        if(photos.containsKey("back")){
            info.getDamages().setBack(photos.get("back"));
        }
        if(photos.containsKey("rightside")){
            info.getDamages().setRightSide(photos.get("rightside"));
        }
        if(photos.containsKey("leftside")){
            info.getDamages().setLeftSide(photos.get("leftside"));
        }
        Log.d("dree", "netItemClicked1: " + ebol.getType());
        if(vehicleIds.isEmpty()){
            Log.d("dree", "netItemClicked2: " + ebol.getType());
            dataProvider.saveEbol(ebol, this::openEbol);
        }
    }

    private void openEbol(boolean saved) {
        if(!isViewAttached()){
            return;
        }
        getView().openListItem();
    }

    public void getEbols() {
        dataProvider.getNetEbols(this::setData, this::stopRefresh);
        dataProvider.getDbEbols(this::setData, this::stopRefresh);
    }

    public void newClick() {
        dataProvider.deleteTempEbol(this::openNewEbol);
    }

    private void openNewEbol(boolean deleted) {
        if(!isViewAttached()){
            return;
        }
        getView().openNewEbol();
    }
}
