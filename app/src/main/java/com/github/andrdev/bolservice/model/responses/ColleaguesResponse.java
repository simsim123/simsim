package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Colleague;

import java.util.List;


public class ColleaguesResponse {
    String status;
    List<Colleague> colleagues;

    public List<Colleague> getColleagues() {
        return colleagues;
    }

    public void setColleagues(List<Colleague> colleagues) {
        this.colleagues = colleagues;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
