package com.github.andrdev.bolservice.current.originEbol.damages.editDamageType;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;

public class OriginEdtDataProvider implements DataProvider {
    private DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(vehicleId, callback::returnData);
    }

    public void updateVehiclePhoto(VehiclePhoto vehiclePhoto, DataCb<Boolean> callback) {
        dbHelper.updateVehiclePhoto(vehiclePhoto, callback::returnData);
    }
}
