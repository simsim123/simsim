package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.ConversationSmall;

import java.util.List;


public class ConversationsResponse {

    String status;
    List<ConversationSmall> conversations;

    public ConversationsResponse() {
    }

    public List<ConversationSmall> getConversations() {
        return conversations;
    }

    public void setConversations(List<ConversationSmall> conversations) {
        this.conversations = conversations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
