package com.github.andrdev.bolservice.clients;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import io.realm.Realm;


public abstract class ClientsPresenter extends MvpBasePresenter<BaseSearchView> {

    public abstract void getData();

    public abstract void requestFailed(Throwable throwable);

    public abstract void requestSuccess(ClientsResponse clientsResponse);

    public abstract void listItemClicked(Client client);

    public abstract void clientSaved(boolean saved);
}
