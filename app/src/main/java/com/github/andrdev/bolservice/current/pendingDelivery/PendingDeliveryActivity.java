package com.github.andrdev.bolservice.current.pendingDelivery;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.database.RealmDbData;

import butterknife.ButterKnife;


public class PendingDeliveryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_delivery);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showPendingDeliveryFragment();
        }
    }

    private void showPendingDeliveryFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new PendingDeliveryFragment()).commit();
    }
}
