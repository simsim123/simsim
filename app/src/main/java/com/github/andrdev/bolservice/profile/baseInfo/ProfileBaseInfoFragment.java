package com.github.andrdev.bolservice.profile.baseInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileBaseInfoFragment extends MvpFragment<ProfileBaseView, ProfileBasePresenter>
        implements ProfileBaseView {

    @BindString(R.string.passwordsNotIdentical)
    String passwordsNotIdentical;

    @Bind(R.id.username)
    TextView username;

    @Bind(R.id.password)
    EditText password;

    @Bind(R.id.repeatPassword)
    EditText repeatPassword;

    @Bind(R.id.baseSelected)
    View baseSelected;

    @Bind(R.id.proSelected)
    View proSelected;

    Profile profile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_base_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @OnClick(R.id.proNotSelected)
    void proNotSelectedClick() {
        proSelected.setVisibility(View.VISIBLE);
        baseSelected.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.baseNotSelected)
    void baseNotSelectedClick() {
        baseSelected.setVisibility(View.VISIBLE);
        proSelected.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        Utils.isCorrectRepeatPassword(passwordsNotIdentical, password, repeatPassword);
    }

    @Override
    public ProfileBasePresenter createPresenter() {
        return new ProfileBasePresenterImpl();
    }

    @Override
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public void initFields() {
        username.setText(profile.getUsername());
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
