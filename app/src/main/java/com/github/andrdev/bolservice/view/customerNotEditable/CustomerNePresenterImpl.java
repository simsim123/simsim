package com.github.andrdev.bolservice.view.customerNotEditable;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.GpsAddressProvider;

import java.util.List;

public class CustomerNePresenterImpl extends CustomerNePresenter {

    CustomerNeDataProvider dataProvider;

    @Override
    public void attachView(CustomerNeView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new CustomerNeDataProvider();
    }
}
