package com.github.andrdev.bolservice.history.details.damages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface HistoryDamagesView extends MvpView {
    void setVehicleInfo(VehicleInfo currentCar);

    Context getViewContext();
}
