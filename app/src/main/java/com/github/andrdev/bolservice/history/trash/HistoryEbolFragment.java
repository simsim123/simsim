//package com.github.andrdev.bolservice.history;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.github.andrdev.bolservice.NucleusFragment;
//import com.github.andrdev.bolservice.R;
//import com.github.andrdev.bolservice.db.Repo;
//import com.github.andrdev.bolservice.model.Customer;
//import com.github.andrdev.bolservice.model.Damages;
//import com.github.andrdev.bolservice.model.NewEbol;
//import com.github.andrdev.bolservice.model.VehicleInfo;
//import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
//import com.github.andrdev.bolservice.newEbol.NewEbolFragment;
//import com.github.andrdev.bolservice.ui.SignActivity;
//
//import java.io.File;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import nucleus.factory.RequiresPresenter;
//
///**
// * Created by taiyokaze on 9/27/15.
// */
//@RequiresPresenter(HistoryEbolPresenter.class)
//public class HistoryEbolFragment extends NucleusFragment<HistoryEbolPresenter> {
//
//    @Bind(R.id.originOrderId)
//    TextView originOrderId;
//
////    @Bind(R.id.originDay)
//    TextView originDay;
//
////    @Bind(R.id.originMonth)
//    TextView originMonth;
//
////    @Bind(R.id.originYear)
//    TextView originYear;
//
//    @Bind(R.id.originCustomerName)
//    TextView originCustomerName;
//
//    @Bind(R.id.originAddress)
//    TextView originAddress;
//
//    @Bind(R.id.originCity)
//    TextView originCity;
//
//    @Bind(R.id.originZip)
//    TextView originZip;
//
//    @Bind(R.id.originState)
//    TextView originState;
//
//    @Bind(R.id.originPhone)
//    TextView originPhone;
//
//    @Bind(R.id.originCell)
//    TextView originCell;
//
//    @Bind(R.id.destinationCustomerName)
//    TextView destinationCustomerName;
//
//    @Bind(R.id.destinationAddress)
//    TextView destinationAddress;
//
//    @Bind(R.id.destinationCity)
//    TextView destinationCity;
//
//    @Bind(R.id.destinationZip)
//    TextView destinationZip;
//
//    @Bind(R.id.destinationState)
//    TextView destinationState;
//
//    @Bind(R.id.destinationPhone)
//    TextView destinationPhone;
//
//    @Bind(R.id.destinationCell)
//    TextView destinationCell;
//
//    @Bind(R.id.customerSignCheck)
//    View customerSignCheck;
//
//    @Bind(R.id.driverSignCheck)
//    View driverSignCheck;
//
//    @Bind(R.id.unableToSignCheck)
//    View unableToSignCheck;
//
//    @Bind(R.id.vehiclesHost)
//    LinearLayout vehicleHost;
//
//    NewEbol currentEbol;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_ebol_origin, container, false);
//        ButterKnife.bind(this, view);
//        return view;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    void setAndShowData(NewEbol ebol){
//        currentEbol = ebol;
//        vehicleHost.removeAllViews();
//        setCommonFields(ebol);
//        setDestinationCustomerFields(ebol.getDestination());
//        setOriginCustomerFields(ebol.getOrigin());
//        drawVehicles(ebol);
//    }
//
//    private void drawVehicles(NewEbol ebol) {
//        for (final VehicleInfo vehInfo : ebol.getVehicleInfos()) {
//            View damagedCar = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_damaged_car, null);
//
//            setVehicleClickers(vehInfo, damagedCar);
//            setVehicleText(vehInfo, damagedCar);
//
//            vehicleHost.addView(damagedCar);
//
//            LinearLayout.LayoutParams params =(LinearLayout.LayoutParams) damagedCar.getLayoutParams();
//            params.bottomMargin = 8;
//        }
//    }
//
//    private void setVehicleText(VehicleInfo vehInfo, View damagedCar) {
//        TextView carInfo = (TextView) damagedCar.findViewById(R.id.carInfo);
//        TextView vin = (TextView) damagedCar.findViewById(R.id.vin);
//        TextView damagesCount = (TextView) damagedCar.findViewById(R.id.damagesCount);
//
//        carInfo.setText(String.format(
//                "%s %s %s", vehInfo.getYear(), vehInfo.getMake(), vehInfo.getModel()));
//        vin.setText(vehInfo.getVin());
//        Damages vehDamages =  vehInfo.getDamages();
//        int markedDamagesCount = 0;
//        if(vehDamages != null) {
//            markedDamagesCount = getMarkedDamagesCount(vehDamages, markedDamagesCount);
//        }
//
//        damagesCount.setText(getResources().getQuantityString(
//                R.plurals.damagesMarked, markedDamagesCount, markedDamagesCount));
//    }
//
//    private void setVehicleClickers(final VehicleInfo vehInfo, View damagedCar) {
//        damagedCar.findViewById(R.id.damages)
//                .setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        damagesClick(vehInfo.getId());
//                    }
//                });
//        damagedCar.findViewById(R.id.details)
//                .setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        detailsClick(vehInfo.getId());
//                    }
//                });
//    }
//
//    private int getMarkedDamagesCount(Damages vehDamages, int markedDamagesCount) {
//        if(vehDamages.getFront()!=null) {
//            markedDamagesCount += vehDamages.getFront().getMarkedDamages().size();
//        }
//        if(vehDamages.getBack()!=null) {
//            markedDamagesCount += vehDamages.getBack().getMarkedDamages().size();
//        }
//        if(vehDamages.getRightSide()!=null) {
//            markedDamagesCount += vehDamages.getRightSide().getMarkedDamages().size();
//        }
//        if(vehDamages.getLeftSide()!=null) {
//            markedDamagesCount += vehDamages.getLeftSide().getMarkedDamages().size();
//        }
//        if(vehDamages.getAdditionalFirst()!=null) {
//            markedDamagesCount += vehDamages.getAdditionalFirst().getMarkedDamages().size();
//        }
//        if(vehDamages.getAdditionalSecond()!=null) {
//            markedDamagesCount += vehDamages.getAdditionalSecond().getMarkedDamages().size();
//        }
//        return markedDamagesCount;
//    }
//
//    private void setCommonFields(NewEbol ebol) {
//        originOrderId.setText(ebol.getOrderId());
//        Calendar cal=Calendar.getInstance();
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//            Date d = sdf.parse(ebol.getDate());
//            cal.setTime(d);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        originDay.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
//        originMonth.setText(String.valueOf(cal.get(Calendar.MONTH) + 1));
//        originYear.setText(String.valueOf(cal.get(Calendar.YEAR)));
//    }
//
//    void detailsClick(int id){
//        Intent intent = new Intent(getActivity(), HistoryVehicleInformationActivity.class);
//        intent.putExtra(NewEbolFragment.SELECTED_CAR_ID, id);
//        startActivity(intent);
//    }
//
//    void damagesClick(int id){
//        Intent intent = new Intent(getActivity(), HistoryCarDamagesActivity.class);
//        intent.putExtra(NewEbolFragment.SELECTED_CAR_ID, id);
//        startActivity(intent);
//    }
//
//    private void setSignImages(NewEbol savedEbol) {
//        if (savedEbol.getDriverSignPath() != null) {
//            driverSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            driverSignCheck.setVisibility(View.INVISIBLE);
//        }
//        if (savedEbol.getCustomerSignPath() != null) {
//            customerSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            customerSignCheck.setVisibility(View.INVISIBLE);
//        }
//        if (savedEbol.isUnableToSign()) {
//            unableToSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            unableToSignCheck.setVisibility(View.INVISIBLE);
//        }
//    }
//
//
//    @OnClick(R.id.customerSignature)
//    void customerSignatureClick() {
//        Intent intent = new Intent(getActivity(), SignActivity.class);
//        intent.putExtra("signatureType", "deliveryCustomerSign");
//        intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.driverSignature)
//    void driverSignatureClick() {
//            Intent intent = new Intent(getActivity(), SignActivity.class);
//            intent.putExtra("signatureType", "deliveryDriverSign");
//            intent.putExtra(NewEbolActivity.EBOL_ID, currentEbol.getOrderId());
//            startActivity(intent);
//    }
//
//    @OnClick(R.id.unableToSign)
//    void unableToSignClick() {
//        Repo repo = new Repo(getContext());
//        currentEbol.setDeliveryUnableToSign(true);
//        new File(currentEbol.getDeliveryCustomerSignPath()).delete();
//        new File(currentEbol.getDeliveryDriverSignPath()).delete();
//        currentEbol.setDeliveryDriverSignPath(null);
//        currentEbol.setDeliveryCustomerSignPath(null);
//        repo.repoNewEbol.updateTempNewEbol(currentEbol);
//        setSignImages(currentEbol);
//    }
//
//    @OnClick(R.id.pickup)
//    void pickupClick() {
//        Repo repo = new Repo(getContext());
//        currentEbol.setType("delivered");
//        repo.repoNewEbol.updateTempNewEbol(currentEbol);
//        onBackClick();
//    }
//
//    private void setOriginCustomerFields(Customer origin) {
//        originCustomerName.setText(origin.getCustomerName());
//        originAddress.setText(origin.getAddress());
//        originCity.setText(origin.getCity());
//        originState.setText(origin.getState());
//        originZip.setText(origin.getZip());
//        originPhone.setText(origin.getPhone());
//        originCell.setText(origin.getCell());
//    }
//
//    private void setDestinationCustomerFields(Customer destination) {
//        destinationCustomerName.setText(destination.getCustomerName());
//        destinationAddress.setText(destination.getAddress());
//        destinationCity.setText(destination.getCity());
//        destinationState.setText(destination.getState());
//        destinationZip.setText(destination.getZip());
//        destinationPhone.setText(destination.getPhone());
//        destinationCell.setText(destination.getCell());
//    }
//
//    @OnClick(R.id.backIm)
//    void onBackClick() {
//        getActivity().onBackPressed();
//    }
//}
//
