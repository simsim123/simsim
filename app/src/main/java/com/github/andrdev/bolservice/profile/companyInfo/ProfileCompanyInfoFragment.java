package com.github.andrdev.bolservice.profile.companyInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileCompanyInfoFragment
        extends MvpFragment<ProfileCompanyInfoView, ProfileCompanyInfoPresenter>
        implements ProfileCompanyInfoView {

    @Bind(R.id.companyName)
    EditText companyName;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.city)
    EditText city;

    @Bind(R.id.state)
    EditText state;

    @Bind(R.id.zip)
    EditText zip;

    @Bind(R.id.phone)
    EditText phone;

    @Bind(R.id.usdot)
    EditText usdot;

    @Bind(R.id.mcSharp)
    EditText mcSharp;

    @Bind(R.id.save)
    Button save;

    Profile profile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_company_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public void initFields() {
        companyName.setText(profile.getCompanyName());
        address.setText(profile.getCompanyAddress());
        city.setText(profile.getCompanyCity());
        state.setText(profile.getCompanyState());
        zip.setText(profile.getCompanyZip());
        phone.setText(profile.getCompanyPhone());
        usdot.setText(profile.getCompanyUsdot());
        mcSharp.setText(profile.getCompanyMc());
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.save)
    void saveClick(View view) {

        Log.d("dree", "degestd");
        SaveProfileRequest profileRequest = new SaveProfileRequestBuilder()
                .setFirstname(profile.getFirstname())
                .setLastname(profile.getLastname())
                .setBillingAddress(profile.getBillingAddress())
                .setBillingCity(profile.getBillingCity())
                .setBillingEmail(profile.getBillingEmail())
                .setBillingFax(profile.getBillingFax())
                .setBillingFirstname(profile.getBillingFirstname())
                .setBillingLastname(profile.getBillingLastname())
                .setBillingPhone(profile.getBillingPhone())
                .setBillingState(profile.getBillingState())
                .setBillingZip(profile.getBillingZip())
                .setCompanyEma(profile.getCompanyEmail())
                .setCompanyAddress(profile.getCompanyAddress())
                .setCompanyFax(profile.getCompanyFax())
                .setCompanyAddress(address)
                .setCompanyCity(city)
                .setCompanyMc(mcSharp)
                .setCompanyUsdot(usdot)
                .setCompanyName(companyName)
                .setCompanyPhone(phone)
                .setCompanyState(state)
                .setCompanyZip(zip).createSaveProfileRequest();
        getPresenter().saveData(profileRequest);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public ProfileCompanyInfoPresenter createPresenter() {
        return new ProfileCompanyInfoPresenterImpl();
    }
}
