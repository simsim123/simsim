package com.github.andrdev.bolservice.profile;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.view.SlidingTabLayout;

import butterknife.OnClick;


public class ProfileActivity extends BaseDrawerActivity {

    ViewPager pager;
    ProfileViewPagerAdapter adapter;
    SlidingTabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPager();
    }

    private void initPager() {
        adapter =  new ProfileViewPagerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer((position) ->
                getResources().getColor(R.color.orange));

        tabs.setViewPager(pager);
    }

    @OnClick({R.id.avatar, R.id.myAccount, R.id.name})
    void userClick() {
        if (mSlidingPanel.isOpen()) {
            mSlidingPanel.closePane();
        }
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 9;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_profile;
    }
}
