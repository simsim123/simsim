package com.github.andrdev.bolservice.model.realms;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class NewEbolRealm extends RealmObject {

    public static final String ID = "id";
    public static final String ORDER_ID = "orderId";
    public static final String DATE = "date";
    public static final String ORIGIN = "origin";
    public static final String DESTINATION = "destination";
    public static final String VEHICLE_INFOS = "vehicleInfos";
    public static final String TYPE = "type";
    public static final String DRIVER_SIGN_PATH = "driverSignPath";
    public static final String CUSTOMER_SIGN_PATH = "customerSignPath";
    public static final String UNABLE_TO_SIGN = "unableToSign";
    public static final String DELIVERY_DRIVER_SIGN_PATH = "deliveryDriverSignPath";
    public static final String DELIVERY_CUSTOMER_SIGN_PATH = "deliveryCustomerSignPath";
    public static final String DELIVERY_UNABLE_TO_SIGN = "deliveryUnableToSign";
    public static final String VEHICLES_COUNT = "vehiclesCount";
    public static final String SUBMIT_ID = "submitId";
    public static final String SAVED_TO_DB = "isSavedToDb";


    @PrimaryKey
    private int id;
    private String orderId;
    private String date;
    private CustomerRealm origin;
    private CustomerRealm destination;
    private RealmList<VehicleInfoRealm> vehicleInfos;
    private String type;
    private String driverSignPath;
    private String customerSignPath;
    private boolean unableToSign;
    private String deliveryDriverSignPath;
    private String deliveryCustomerSignPath;
    private boolean deliveryUnableToSign;
    private int vehiclesCount;
    private long submitId;
    private boolean isSavedToDb;

    public NewEbolRealm() {
    }


    public NewEbolRealm(String orderId, String date, CustomerRealm origin, CustomerRealm destination) {
        this();
        this.orderId = orderId;
        this.date = date;
        this.origin = origin;
        this.destination = destination;
    }

    public String getCustomerSignPath() {
        return customerSignPath;
    }

    public void setCustomerSignPath(String customerSignPath) {
        this.customerSignPath = customerSignPath;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryCustomerSignPath() {
        return deliveryCustomerSignPath;
    }

    public void setDeliveryCustomerSignPath(String deliveryCustomerSignPath) {
        this.deliveryCustomerSignPath = deliveryCustomerSignPath;
    }

    public String getDeliveryDriverSignPath() {
        return deliveryDriverSignPath;
    }

    public void setDeliveryDriverSignPath(String deliveryDriverSignPath) {
        this.deliveryDriverSignPath = deliveryDriverSignPath;
    }

    public boolean isDeliveryUnableToSign() {
        return deliveryUnableToSign;
    }

    public void setDeliveryUnableToSign(boolean deliveryUnableToSign) {
        this.deliveryUnableToSign = deliveryUnableToSign;
    }

    public CustomerRealm getDestination() {
        return destination;
    }

    public void setDestination(CustomerRealm destination) {
        this.destination = destination;
    }

    public String getDriverSignPath() {
        return driverSignPath;
    }

    public void setDriverSignPath(String driverSignPath) {
        this.driverSignPath = driverSignPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CustomerRealm getOrigin() {
        return origin;
    }

    public void setOrigin(CustomerRealm origin) {
        this.origin = origin;
    }

    public long getSubmitId() {
        return submitId;
    }

    public void setSubmitId(long submitId) {
        this.submitId = submitId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUnableToSign() {
        return unableToSign;
    }

    public void setUnableToSign(boolean unableToSign) {
        this.unableToSign = unableToSign;
    }

    public RealmList<VehicleInfoRealm> getVehicleInfos() {
        return vehicleInfos;
    }

    public void setVehicleInfos(RealmList<VehicleInfoRealm> vehicleInfos) {
        this.vehicleInfos = vehicleInfos;
    }

    public int getVehiclesCount() {
        return vehiclesCount;
    }

    public void setVehiclesCount(int vehiclesCount) {
        this.vehiclesCount = vehiclesCount;
    }

    public void setIsSavedToDb(boolean savedToDb) {
        this.isSavedToDb = savedToDb;
    }

    public boolean isSavedToDb() {
        return isSavedToDb;
    }
}
