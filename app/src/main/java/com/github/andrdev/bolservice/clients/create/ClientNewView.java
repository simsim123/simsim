package com.github.andrdev.bolservice.clients.create;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ClientNewView extends MvpView {

    void showFailedToast();

    void backToClients();
}
