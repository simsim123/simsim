package com.github.andrdev.bolservice.database;

import android.content.Context;

import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class DbHelperRxImpl implements DbHelper {

    RealmDbData realmDbData;

    public DbHelperRxImpl(Context context) {
        realmDbData = new RealmDbData(context);
    }

    @Override
    public RealmDbData getRealmDbData() {
        return realmDbData;
    }

    @Override
    public void setRealmDbData(RealmDbData realmDbData) {
        this.realmDbData = realmDbData;
    }

    @Override
    public void saveProfile(Profile profile, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveProfile(profile))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getSavedProfile(DataCallback<Profile> callback) {
        Observable.just(realmDbData.getSavedProfile())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getIsUserBoss(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.getIsUserBoss())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void saveClient(Client client, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveClient(client))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getSavedClient(DataCallback<Client> callback) {
        Observable.just(realmDbData.getSavedClient())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void saveColleague(Colleague colleague, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveColleague(colleague))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getSavedColleague(DataCallback<Colleague> callback) {
        Observable.just(realmDbData.getSavedColleague())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void saveInvoice(Invoice invoice, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveInvoice(invoice))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getSavedInvoice(DataCallback<Invoice> callback) {
        Observable.just(realmDbData.getSavedInvoice())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void updateOrSaveEbol(NewEbol ebol, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.updateOrSaveEbol(ebol))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void updateSaveEbol(NewEbol ebol, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveEbol(ebol))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getEbol(DataCallback<NewEbol> callback) {
        Observable.just(realmDbData.getOrCreateEbol())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getOrCreateEbol(DataCallback<NewEbol> callback) {
        Observable.just(realmDbData.getOrCreateEbol())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getTempEbol(DataCallback<NewEbol> callback) {
        Observable.just(realmDbData.getTempEbol())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void clearVehicleInfoTable(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.clearVehicleInfoTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void clearTempVehicleInfo(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.clearTempVehicleInfo())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void saveTempVehicleOrUpdate(VehicleInfo info, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveTempVehicleOrUpdate(info))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void updateVehicle(VehicleInfo info, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.updateVehicle(info))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getTempVehicle(DataCallback<VehicleInfo> callback) {
        Observable.just(realmDbData.getTempVehicle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getVehicleById(int vehicleId, DataCallback<VehicleInfo> callback) {
        Observable.just(realmDbData.getVehicleById(vehicleId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void updateDamages(Damages damages, int vehicleId, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.updateDamages(damages, vehicleId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void saveVehiclePhoto(VehiclePhoto tempPhoto, int vehicleId, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.saveVehiclePhoto(tempPhoto, vehicleId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void deleteTempEbol(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.deleteTempEbol())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void deleteNetTempEbols(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.deleteNetTempEbols())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void getDbEbols(DataCallback<List<NewEbol>> returnData) {
        Observable.just(realmDbData.getEbols())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(returnData::requestSuccess);
    }

    @Override
    public void deleteNetSaveLocalEbol() {
        realmDbData.deleteNetTempEbols();
        realmDbData.saveTempDbEbols();
    }


    @Override
    public void updateVehiclePhoto(VehiclePhoto vehiclePhoto, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.updateVehiclePhoto(vehiclePhoto))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void deleteTempVehiclePhoto(DataCallback<Boolean> callback) {
        Observable.just(realmDbData.deleteTempVehiclePhoto())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void deleteVehiclePhotoById(int photoId, DataCallback<Boolean> callback) {
        Observable.just(realmDbData.deleteVehiclePhotoById(photoId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::requestSuccess);
    }

    @Override
    public void deInit() {
        realmDbData.deInit();
    }

    @Override
    public void deleteVehiclePhotoTempAndSave(VehiclePhoto photoToDelete, VehiclePhoto tempPhoto, DataCallback<Boolean> callback) {

    }


}
