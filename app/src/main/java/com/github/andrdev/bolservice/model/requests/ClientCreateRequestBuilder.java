package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;


public class ClientCreateRequestBuilder {

    private String address;
    private String cid;
    private String city;
    private String customerName;
    private String email;
    private String fax;
    private String state;
    private String tel;
    private String zip;

    public ClientCreateRequestBuilder setAddress(EditText address) {
        if (Utils.hasText(address)) {
            this.address = Utils.getTextFromEditText(address);
        }
        return this;
    }

    public ClientCreateRequestBuilder setCid(String cid) {
        this.cid = cid;
        return this;
    }

    public ClientCreateRequestBuilder setCity(EditText city) {
        if (Utils.hasText(city)) {
            this.city = Utils.getTextFromEditText(city);
        }
        return this;
    }

    public ClientCreateRequestBuilder setCustomerName(EditText customerName) {
        if (Utils.hasText(customerName)) {
            this.customerName = Utils.getTextFromEditText(customerName);
        }
        return this;
    }

    public ClientCreateRequestBuilder setEmail(EditText email) {
        if (Utils.hasText(email)) {
            this.email = Utils.getTextFromEditText(email);
        }
        return this;
    }

    public ClientCreateRequestBuilder setFax(EditText fax) {
        if (Utils.hasText(fax)) {
            this.fax = Utils.getTextFromEditText(fax);
        }
        return this;
    }

    public ClientCreateRequestBuilder setState(EditText state) {
        if (Utils.hasText(state)) {
            this.state = Utils.getTextFromEditText(state);
        }
        return this;
    }

    public ClientCreateRequestBuilder setTel(EditText tel) {
        if (Utils.hasText(tel)) {
            this.tel = Utils.getTextFromEditText(tel);
        }
        return this;
    }

    public ClientCreateRequestBuilder setZip(EditText zip) {
        if (Utils.hasText(zip)) {
            this.zip = Utils.getTextFromEditText(zip);
        }
        return this;
    }

    public ClientCreateRequest createClientCreateRequest() {
        return new ClientCreateRequest(address, city, customerName, email, fax, state, tel, zip);
    }
}