package com.github.andrdev.bolservice.login;

import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


abstract public class LoginPresenter extends MvpBasePresenter<LoginView>{

    @SuppressWarnings("ConstantConditions")
    public abstract void sendLoginRequest(final LoginRequest loginRequest);

    @SuppressWarnings("ConstantConditions")
    public abstract void loginSuccess(ProfileResponse response);

    @SuppressWarnings("ConstantConditions")
    public abstract void loginFailed(Throwable throwable);

    @SuppressWarnings("ConstantConditions")
    public abstract void profileSaved(boolean saved);
}
