package com.github.andrdev.bolservice.invoices.create;

import android.content.Context;
import android.location.Address;

import com.hannesdorfmann.mosby.mvp.MvpView;

interface InvoiceNewView extends MvpView{
    void showFailedToast();

    void backToInvoices();

    void buildAlertMessageNoGps();

    void selectAddressFromGooglePlaces();

    void setAddress(Address gAddress);

    Context getViewContext();

    void openShare();
}
