package com.github.andrdev.bolservice.login;

import android.content.Context;

import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface LoginView extends MvpView {

    void showProgress();

    void hideProgress();

    void openDashBoardActivity();

    void showLoginErrorToast();

    Context getViewContext();

    void saveLoginInfo(LoginRequest loginInfo);
}
