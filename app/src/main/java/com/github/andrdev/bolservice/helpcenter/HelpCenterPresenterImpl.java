package com.github.andrdev.bolservice.helpcenter;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class HelpCenterPresenterImpl extends HelpCenterPresenter {

    HelpCenterDataProvider dataProvider;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HelpCenterDataProvider();
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getHelpCenter(this::requestSuccess, this::requestFailed);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showRequestFailedToast();
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void requestSuccess(HelpCenterResponse helpCenterResponse) {
        if (!isViewAttached()) {
            return;
        }
        if (helpCenterResponse.getStatus().equals("true")
                && helpCenterResponse.getHelpCenter() != null && !helpCenterResponse.getHelpCenter().isEmpty()) {
            getView().setData(helpCenterResponse.getHelpCenter());
        } else {
            getView().showNoDataText();
        }
    }
}
