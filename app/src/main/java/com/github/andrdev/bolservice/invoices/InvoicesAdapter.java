package com.github.andrdev.bolservice.invoices;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Invoice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;


public class InvoicesAdapter extends BaseAdapter<Invoice, InvoicesAdapter.InvoicesHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_invoice;
    }

    @Override
    protected InvoicesHolder getHolder(View view) {
        return new InvoicesHolder(view);
    }

    protected class InvoicesHolder extends BaseAdapter.BaseViewHolder<Invoice> {

        @Bind(R.id.orderId)
        TextView orderId;
        @Bind(R.id.customerName)
        TextView customerName;
        @Bind(R.id.ownerInfo)
        TextView ownerInfo;
        @Bind(R.id.vehiclesCount)
        TextView vehiclesCount;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.cost)
        TextView cost;
        @Bind(R.id.timeLine)
        TextView timeLine;
        @Bind(R.id.phoneNumber)
        TextView phoneNumber;

        public InvoicesHolder(View itemView) {
            super(itemView);
        }

        protected void bind(Invoice invoice) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yy");
                Date d = sdf.parse(invoice.getCreatedAt());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            orderId.setText(invoice.getOrderId());
            customerName.setText(invoice.getFullname());
            ownerInfo.setText(invoice.getAddress());
            timeLine.setText(invoice.getPaymentTerms());
            cost.setText("$"+String.valueOf(invoice.getAmount()));
            phoneNumber.setText(invoice.getCell());
            String count = vehiclesCount.getContext()
                    .getResources().getQuantityString(R.plurals.vehicles,
                            invoice.getVehiclesCount(), invoice.getVehiclesCount());
            vehiclesCount.setText(count.toLowerCase());
        }
    }
}
