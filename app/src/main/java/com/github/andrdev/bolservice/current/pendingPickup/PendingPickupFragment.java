package com.github.andrdev.bolservice.current.pendingPickup;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.OriginEbolAcitivty;
import com.github.andrdev.bolservice.currentEbol.CurrentEbolActivity;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;


public class PendingPickupFragment
        extends BaseSearchFragment<NewEbol, PendingPickupAdapter, PendingPickupView, PendingPickupPresenter>
        implements PendingPickupView {

    @Bind(R.id.progress)
    View progress;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_pending_pickup;
    }

    @Override
    protected void filter(NewEbol item, String temp) {
        if (item.getOrderId().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        RealmDbData rdd = new RealmDbData(getContext());
        rdd.deleteNetTempEbols();
        rdd.saveTempDbEbols();
        hideProgress();
        mMainData.clear();
        mData.clear();
        getPresenter().getEbols();
    }

    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        showProgress();
        NewEbol ebolTemp = mData.get(position);
        getPresenter().netItemClicked(ebolTemp);
    }

    @Override
    protected PendingPickupAdapter getAdapter() {
        return new PendingPickupAdapter();
    }

    @Override
    public void showNoDataText() {
    }

    @Override
    public void showRequestFailedToast() {
    }

    @Override
    public void openListItem() {
        hideProgress();
        Intent intent = new Intent(getContext(), CurrentEbolActivity.class);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @OnClick(R.id.progress)
    void onProgressClick(View v) {

    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.newEbol)
    void onNewClick(View v) {
        getPresenter().newClick();
    }

    @Override
    public PendingPickupPresenter createPresenter() {
        return new PendingPickupPresenter();
    }

    @Override
    public void openNewEbol() {
        Intent intent = new Intent(getContext(), NewEbolActivity.class);
        startActivity(intent);
    }

    @Override
    public void addEbols(List<NewEbol> data) {
        mMainData.addAll(data);
        mData.addAll(data);
        updateDataOnAdapter();
    }
}
