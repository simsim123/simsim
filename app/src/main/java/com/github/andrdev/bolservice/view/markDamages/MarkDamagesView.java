package com.github.andrdev.bolservice.view.markDamages;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface MarkDamagesView extends MvpView{
    Context getViewContext();
}
