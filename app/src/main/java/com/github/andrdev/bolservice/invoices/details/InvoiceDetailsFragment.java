package com.github.andrdev.bolservice.invoices.details;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Invoice;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class InvoiceDetailsFragment extends MvpFragment<InvoiceDetailsView, InvoiceDetailsPresenter>
        implements InvoiceDetailsView {


    @Bind(R.id.customerName)
    TextView customerName;

    @Bind(R.id.address)
    TextView address;

    @Bind(R.id.state)
    TextView state;

    @Bind(R.id.city)
    TextView city;

    @Bind(R.id.zip)
    TextView zip;

    @Bind(R.id.phone)
    TextView phone;

    @Bind(R.id.cell)
    TextView cell;

    @Bind(R.id.invoiceId)
    TextView invoiceId;

    @Bind(R.id.date)
    TextView date;

    @Bind(R.id.paymentTerms)
    TextView paymentTerms;

    @Bind(R.id.cost)
    TextView cost;

    @Bind(R.id.notes)
    TextView notes;

    Invoice invoice;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }


    //todo uncomment
    @OnClick(R.id.sendPrint)
    void nextClick() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            intent = new Intent(getContext(), PdfActActivity.class);
        } else {
//            intent = new Intent(getContext(), PdfActPrekk.class);
        }

//        startActivity(intent);
//        getActivity().finish();
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @NonNull
    @Override
    public InvoiceDetailsPresenter createPresenter() {
        return new InvoiceDetailsPresenterImpl();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void setData(Invoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public void initFields() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat output = new SimpleDateFormat("MM/dd/yy");
            Date d = sdf.parse(invoice.getCreatedAt());
            String formattedTime = output.format(d);
            date.setText(formattedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String [] addressFull = invoice.getAddress().split(",");
        Log.d("dreeadd", Arrays.toString(addressFull));
        customerName.setText(invoice.getFullname());
        address.setText(invoice.getAddress());
        city.setText(invoice.getCity());
        state.setText(invoice.getState());
        zip.setText(invoice.getZip());
        phone.setText(invoice.getPhone());
        cell.setText(invoice.getCell());
        invoiceId.setText(invoice.getInvoiceId());
        notes.setText(invoice.getNotes());
        state.setText(invoice.getState());
        zip.setText(invoice.getZip());
        cost.setText(String.valueOf(invoice.getAmount()));
        paymentTerms.setText(invoice.getPaymentTerms());
    }
}
