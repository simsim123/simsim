package com.github.andrdev.bolservice.sign;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
import com.github.andrdev.bolservice.view.DrawingView;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignFragment extends MvpFragment<SignView, SignPresenter> implements SignView {

    @Bind(R.id.drawingView)
    DrawingView dv ;

    private String ebolId;
    private String signatureType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign, container, false);
        ButterKnife.bind(this, view);
        ebolId = getArguments().getString(NewEbolActivity.EBOL_ID);
        signatureType = getArguments().getString("signatureType");
        dv.setPaint(getPaint());
        return view;
    }

    private Paint getPaint() {
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(6);
        return mPaint;
    }

    @OnClick(R.id.save)
    void saveClick(View v) {
        getPresenter().signSaveClick();
    }

    @Override
    public Bitmap getSignOnBitmap() {
        Bitmap b = Bitmap.createBitmap(dv.getWidth(), dv.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        dv.draw(canvas);
        return b;
    }

    @Override
    public void backToEbol() {
        getActivity().onBackPressed();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public String getSignatureType() {
        return signatureType;
    }

    @Override
    public String getEbolId() {
        return ebolId;
    }

    @OnClick(R.id.clear)
    void clearClick(View v) {
        dv.clear();
    }

    @Override
    public SignPresenter createPresenter() {
        return new SignPresenter();
    }
}
