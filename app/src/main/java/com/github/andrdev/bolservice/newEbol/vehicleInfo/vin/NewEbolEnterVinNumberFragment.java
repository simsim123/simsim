package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolEnterVinNumberFragment
        extends MvpFragment<NewEbolEnterVinNumberView, NewEbolEnterVinNumberPresenter>
        implements NewEbolEnterVinNumberView {

    @Bind(R.id.vinNumber)
    EditText vinNumber;

    VehicleInfo vehicleInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_vin, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public NewEbolEnterVinNumberPresenter createPresenter() {
        return new NewEbolEnterVinNumberPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getArguments() != null) {
            getData();
        }
    }

    private void getData() {
        if(getArguments().containsKey(NewEbolScanVinFragment.VIN)){
            vinNumber.setText(getArguments().getString(NewEbolScanVinFragment.VIN));
        }
        if (getArguments().containsKey(NewFFebolFragment.SELECTED_VEHICLE_ID)) {
            int selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
            getPresenter().getData(selectedVehicleId);
        }
    }

    @Override
    public void setData(VehicleInfo info) {
        vehicleInfo = info;
        if(vinNumber.getText().toString().isEmpty()) {
            vinNumber.setText(vehicleInfo.getVinNumber());
        } else {
            decodeVin(Utils.getTextFromEditText(vinNumber));
        }
    }

    @OnClick(R.id.search)
    void searchClick(View view) {
        decodeVin(Utils.getTextFromEditText(vinNumber));
    }

    private void decodeVin(String vinNumber) {
        getPresenter().decodeVin(vinNumber);
    }

    @Override
    public void backToVehicleInfo() {
        getActivity().onBackPressed();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void showFailedToast() {

    }

    @Override
    public VehicleInfo getVehicleInfo() {
        return vehicleInfo;
    }
}
