package com.github.andrdev.bolservice.invoices.details;

import android.content.Context;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Invoice;

public class InvoiceDetailsDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getSavedInvoice(DataCb<Invoice> successCb) {
        dbHelper.getSavedInvoice(successCb::returnData);
    }
}
