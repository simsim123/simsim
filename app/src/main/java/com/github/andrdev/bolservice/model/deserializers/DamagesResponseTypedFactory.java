package com.github.andrdev.bolservice.model.deserializers;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesG;
import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.SubmitsResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DamagesResponseTypedFactory extends CustomizedTypeAdapterFactory<DamagesResponse> {

    public DamagesResponseTypedFactory(Class<DamagesResponse> customizedClass) {
        super(customizedClass);
    }

    //todo fix
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected DamagesResponse deserialize(JsonElement deserialized, Gson gson) {
        DamagesGetResponse damagesGResponse = null;
        Log.d("eboolsre", deserialized.toString());
        try {
            damagesGResponse = gson.getDelegateAdapter(this, TypeToken.get(DamagesGetResponse.class))
                    .fromJson(String.valueOf(deserialized));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        EbolsResponse ebolsResponse = new EbolsResponse();
//        ebolsResponse.setEbols(submitToEbol(submitsResponse.getSubmits()));
        return damagesToMarkDamage(damagesGResponse.getDamages());
    }

    private DamagesResponse damagesToMarkDamage(List<DamagesG> damages) {
        DamagesResponse damagesResponse = new DamagesResponse();
        Map<String, VehiclePhoto> photos = new HashMap<>();
        damagesResponse.setVehiclePhotos(photos);
        for (DamagesG dg : damages) {
            MarkedDamage damage = new MarkedDamage();
            damage.setDamageTypes(new ArrayList<>());
            damage.setxPosition(Double.parseDouble(dg.getPositionLeft()));
            damage.setyPosition(Double.parseDouble(dg.getPositionTop()));

            DamageType dt = new DamageType();
            dt.setState(true);
            dt.setDamageShortName(dg.getCode());
            damage.getDamageTypes().add(dt);

            VehiclePhoto photo;

            switch (dg.getSide()) {
                case "front":
                    if (!photos.containsKey("front")) {
                        photo = new VehiclePhoto();
                        photo.setPhotoType("frontPhoto");
                        photo.setPhotoPath(dg.getImage());
                        photo.setMarkedDamages(new ArrayList<>());
                        photos.put("front", photo);
                    }
                    photo = photos.get("front");
                    photo.getMarkedDamages().add(damage);
                    break;
                case "back":
                    if (!photos.containsKey("back")) {
                        photo = new VehiclePhoto();
                        photo.setPhotoType("backPhoto");
                        photo.setPhotoPath(dg.getImage());
                        photo.setMarkedDamages(new ArrayList<>());
                        photos.put("back", photo);
                    }
                    photo = photos.get("back");
                    photo.getMarkedDamages().add(damage);
                    break;
                case "rightside":
                    if (!photos.containsKey("rightside")) {
                        photo = new VehiclePhoto();
                        photo.setPhotoType("rightSidePhoto");
                        photo.setPhotoPath(dg.getImage());
                        photo.setMarkedDamages(new ArrayList<>());
                        photos.put("rightside", photo);
                    }
                    photo = photos.get("rightside");
                    photo.getMarkedDamages().add(damage);
                    break;
                case "leftside":
                    if (!photos.containsKey("leftside")) {
                        photo = new VehiclePhoto();
                        photo.setPhotoType("leftSidePhoto");
                        photo.setPhotoPath(dg.getImage());
                        photo.setMarkedDamages(new ArrayList<>());
                        photos.put("leftside", photo);
                    }
                    photo = photos.get("leftside");
                    photo.getMarkedDamages().add(damage);
                    break;
                default:
            }
        }
        return damagesResponse;
    }
}