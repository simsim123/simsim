package com.github.andrdev.bolservice.model.requests;


public class InvoiceRequest {

    String amount;
    String invoiceId;
    String paymentTerms;
    String notes;
    String fullName;
    String orderId;
    String address;
    String city;
    String state;
    String zip;
    String phone;
    String cell;
    String vCount;

    public InvoiceRequest() {
    }

    public InvoiceRequest(String address, String amount, String cell, String city, String fullName,
                          String invoiceId, String notes, String orderId, String paymentTerms,
                          String phone, String state, String vCount, String zip) {
        this.address = address;
        this.amount = amount;
        this.cell = cell;
        this.city = city;
        this.fullName = fullName;
        this.invoiceId = invoiceId;
        this.notes = notes;
        this.orderId = orderId;
        this.paymentTerms = paymentTerms;
        this.phone = phone;
        this.state = state;
        this.vCount = vCount;
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getvCount() {
        return vCount;
    }

    public void setvCount(String vCount) {
        this.vCount = vCount;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
