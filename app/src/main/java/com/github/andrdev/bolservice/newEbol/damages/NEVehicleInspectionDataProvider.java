package com.github.andrdev.bolservice.newEbol.damages;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

import java.io.File;

public class NEVehicleInspectionDataProvider implements DataProvider {

    private DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getTempVehicle(DataCb<VehicleInfo> callbackSuccess) {
        dbHelper.getTempVehicle(callbackSuccess::returnData);
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(vehicleId, callback::returnData);
    }

    public void updateVehiclePhoto(VehiclePhoto vehiclePhoto, DataCb<Boolean> callback) {
        dbHelper.updateVehiclePhoto(vehiclePhoto, callback::returnData);
    }

    public void deleteVehiclePhoto(VehiclePhoto vehiclePhoto, DataCb<Boolean> callback) {
        dbHelper.deleteVehiclePhotoById(vehiclePhoto.getId(), callback::returnData);
    }
}
