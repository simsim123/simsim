package com.github.andrdev.bolservice;

import android.text.TextUtils;
import android.widget.EditText;


public class Utils {

    public static boolean hasText(EditText editText) {
        return !TextUtils.isEmpty(editText.getText().toString());
    }

    public static boolean hasText(String errorString, EditText... editText) {
        boolean hasEmpty = false;
        for (EditText eT : editText) {
            if (!hasText(eT)) {
                eT.setError(errorString);
                hasEmpty = true;
            }
        }
        return !hasEmpty;
    }

    public static String getTextFromEditText(EditText editText) {
        return editText.getText().toString();
    }

    public static boolean isValidEmail(String wrongEmail, EditText email) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(getTextFromEditText(email)).matches()) {
            return true;
        } else {
            email.setError(wrongEmail);
        }
//        if(getTextFromEditText(email).valid) {
//            return true;
//        }
//        email.setError();
        return false;
    }

    public static boolean isValidPhone(EditText phone) {
//        if(getTextFromEditText(phone).valid) {
//            return true;
//        }
//        phone.setError();
        return false;
    }

    public static boolean isValidPassword(EditText password) {
        if (getTextFromEditText(password).length() < 6) {
//            password.setError();
            return false;
        }
        return true;
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isCorrectRepeatPassword(String passwordsNotIdentical, EditText password, EditText repeat) {
        if (getTextFromEditText(password).equals(getTextFromEditText(repeat))) {
            return true;
        }
        repeat.setError(passwordsNotIdentical);
        return false;
    }
}