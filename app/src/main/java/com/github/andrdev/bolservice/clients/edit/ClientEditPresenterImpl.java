package com.github.andrdev.bolservice.clients.edit;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;


public class ClientEditPresenterImpl extends ClientEditPresenter {

    ClientEditDataProvider dataProvider;

    @Override
    public void attachView(ClientEditView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ClientEditDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }
    @SuppressWarnings("ConstantConditions")
    @Override
    public void getData() {
        if(!isViewAttached()) {
            return;
        }
        dataProvider.getSavedClient(this::clientRequestSuccess);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void deleteItem(Client client) {
        if(!isViewAttached()) {
            return;
        }
        EbolNetworkWorker2.getInstance().deleteClient(String.valueOf(client.getId()), this);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void deleteRequestFailed(Throwable throwable) {
        if(!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void deleteRequestSuccess(SimpleResponse simpleResponse) {
        if(!isViewAttached()) {
            return;
        }
        getView().backToClientDetails();
    }

    @Override
    public void updateItem(final ClientUpdateRequest request) {
        if(!isViewAttached()) {
            return;
        }
        dataProvider.updateClient(request, (response)->onUpdateSuccess(request), this::onUpdateFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onUpdateSuccess(ClientUpdateRequest request) {
        if(!isViewAttached()) {
            return;
        }
        Client client = clientRequestToClient(request);
        dataProvider.saveClient(client, this::clientSaved);
    }

    private Client clientRequestToClient(ClientUpdateRequest clientRequest) {
        Client client = new Client();
        client.setAddress(clientRequest.getAddress());
        client.setCity(clientRequest.getCity());
        client.setCustomerName(clientRequest.getCustomerName());
        client.setPhone(clientRequest.getTel());
        client.setState(clientRequest.getState());
        client.setZIP(clientRequest.getZip());
        return client;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onUpdateFailed(Throwable throwable) {
        if(!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    public void clientRequestSuccess(Client client) {
        if(!isViewAttached()) {
            return;
        }
        getView().setData(client);
        getView().initFields();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void clientSaved(boolean saved) {
        if(!isViewAttached()) {
            return;
        }
        getView().backToClientDetails();
    }
}
