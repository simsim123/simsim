package com.github.andrdev.bolservice.clients.create;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.requests.ClientCreateRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ClientNewFragment extends MvpFragment<ClientNewView, ClientNewPresenter> implements ClientNewView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @BindString(R.string.wrongEmail)
    String wrongEmail;

    @Bind(R.id.name)
    EditText name;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.city)
    EditText city;

    @Bind(R.id.state)
    EditText state;

    @Bind(R.id.zip)
    EditText zip;

    @Bind(R.id.phone)
    EditText phone;

    @Bind(R.id.email)
    EditText email;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_new, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        if(!enteredInfoIsOk()) return;
        ClientCreateRequest request = getClientInfoFromViews();

        sendRequest(request);
    }

    private ClientCreateRequest getClientInfoFromViews() {
        return new ClientCreateRequestBuilder()
                .setCustomerName(name)
                .setAddress(address)
                .setCity(city)
                .setEmail(email)
                .setState(state)
                .setTel(phone)
                .setZip(zip)
                .createClientCreateRequest();
    }


    private boolean enteredInfoIsOk() {
        return Utils.hasText(errorText, name, address, city, email, state, phone, zip)
                && Utils.isValidEmail(wrongEmail, email);
    }

    private void sendRequest(ClientCreateRequest request) {
        getPresenter().createClient(request);
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToClients() {
        getActivity().onBackPressed();
    }

    @NonNull
    @Override
    public ClientNewPresenter createPresenter() {
        return new ClientNewPresenterImpl();
    }
}
