package com.github.andrdev.bolservice.current.pendingDelivery;

import android.util.Log;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class PendingDeliveryDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void getEbols(DataCb<EbolsResponse> successCb,
                         DataCb<Throwable> failedCb) {
        Log.d("eboolsre", "pendingDelivery");
        EbolNetworkWorker2.getInstance().getSubmits("pendingDelivery",
                successCb::returnData, failedCb::returnData);
    }

    public void getVehicles(String ebolId, DataCb<VehicleInfoResponse> successCb) {
        EbolNetworkWorker2.getInstance().getVehicles(ebolId,
                successCb::returnData, (t)-> Log.d("get", "getVehicles: "+t.getMessage()));
    }

    public void deleteTempEbol(DataCb<Boolean> successCb) {
        dbHelper.deleteTempEbol(successCb::returnData);
    }

    public void saveEbol(NewEbol ebol, DataCb<Boolean> successCb) {
        dbHelper.updateOrSaveEbol(ebol, successCb::returnData);
    }

    public void getVehicleDamages(int cid, DataCb<DamagesResponse> successCb) {
        EbolNetworkWorker2.getInstance().getDamages(String.valueOf(cid),
                successCb::returnData);
    }
}