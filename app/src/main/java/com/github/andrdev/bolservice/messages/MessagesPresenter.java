package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.ConversationSmall;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;


public abstract class MessagesPresenter extends MvpBasePresenter<BaseSearchView> {

    abstract void getConversations();

    protected abstract void markMessagesAsRead(List<ConversationSmall> conversations);
}
