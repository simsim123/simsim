package com.github.andrdev.bolservice.clients.edit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ClientEditFragment extends MvpFragment<ClientEditView, ClientEditPresenter>
        implements ClientEditView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @BindString(R.string.wrongEmail)
    String wrongEmail;

    @Bind(R.id.name)
    EditText name;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.city)
    EditText city;

    @Bind(R.id.state)
    EditText state;

    @Bind(R.id.zip)
    EditText zip;

    @Bind(R.id.phone)
    EditText phone;

    @Bind(R.id.email)
    EditText email;

    @Bind(R.id.titleTool)
    TextView titleTool;

    Client client;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_edit, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @NonNull
    @Override
    public ClientEditPresenter createPresenter() {
        return new ClientEditPresenterImpl();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void initFields() {
        name.setText(client.getCustomerName());
        address.setText(client.getAddress());
        city.setText(client.getCity());
        state.setText(client.getState());
        zip.setText(client.getZIP());
        phone.setText(client.getPhone());
        titleTool.setText(client.getCustomerName());
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        if (!enteredInfoIsOk()) {
            return;
        }

        ClientUpdateRequest request = new ClientUpdateRequestBuilder()
                .setCid(String.valueOf(client.getId()))
                .setCustomerName(name)
                .setAddress(address)
                .setCity(city)
                .setEmail(email)
                .setState(state)
                .setTel(phone)
                .setZip(zip)
                .createClientUpdateRequest();

        updateData(request);
    }

    private boolean enteredInfoIsOk() {
        return Utils.hasText(errorText, name, address, city, state, phone, zip)
                && Utils.isValidEmail(wrongEmail, email);
    }

    @OnClick(R.id.delete)
    void deleteClick(View view) {
        getPresenter().deleteItem(client);
    }

    private void updateData(ClientUpdateRequest request) {
        getPresenter().updateItem(request);
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void backToClientDetails() {
        getActivity().onBackPressed();
    }

    @Override
    public void setData(Client data) {
        this.client = data;
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getViewContext(){
        return getContext();
    }
}
