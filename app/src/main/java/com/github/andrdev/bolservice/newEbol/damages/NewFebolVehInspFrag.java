package com.github.andrdev.bolservice.newEbol.damages;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.view.markDamages.MarkDamagesWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewFebolVehInspFrag
        extends MvpFragment<NEVehicleInspectionView, NEVehicleInspectionPresenter>
        implements NEVehicleInspectionView {

    private static final String TAG = "dreePhCont";
    @Bind(R.id.photoResult)
    ImageView photoResult;
    @Bind(R.id.carModel)
    TextView vehicleModel;
    @Bind(R.id.notesView)
    RelativeLayout notesView;
    @Bind(R.id.notesText)
    EditText notesText;
    @Bind(R.id.markDamages)
    MarkDamagesWidget markDamages;

    VehicleInfo vehicleInfo;
    String photoType;
    int selectedVehicleId;
    VehiclePhoto vehiclePhoto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_inspection, container, false);
        ButterKnife.bind(this, view);
        selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        photoType = getArguments().getString("phototype");
        getPresenter().getData(selectedVehicleId, photoType);
    }

    @Override
    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    @Override
    public void setVehiclePhoto(VehiclePhoto vehiclePhoto) {
        this.vehiclePhoto = vehiclePhoto;
    }


    @Override
    public void initViews() {
        photoType = vehiclePhoto.getPhotoType();
        loadPhoto(vehiclePhoto.getPhotoPath());
        showViews();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public VehiclePhoto getVehiclePhoto() {
        return vehiclePhoto;
    }

    public void loadPhoto(String photoPath) {
        if(vehiclePhoto.isLocal()) {
            loadPhotoFromDisk(photoPath);
        } else {
            Picasso.with(getContext()).load(photoPath).into(photoResult);
        }
    }

    private void loadPhotoFromDisk(String photoPath) {
        File file = new File(photoPath);
        Picasso picasso = Picasso.with(getContext());
        picasso.load(file).fit().into(photoResult);
    }

    private void showViews() {
        setPhotoDescription();
        markDamages.setPhoto(vehiclePhoto);
        markDamages.paintDamages();
    }

    private void setPhotoDescription() {
        vehicleModel.setText(String.format("%s %s %s", vehicleInfo.getYearOfMake(),
                vehicleInfo.getMake(), vehicleInfo.getModel()));
    }

    @OnClick(R.id.continueToNext)
    void continueToNextClick(View v) {
        Log.d(TAG, "continueToNextClick: ");
        getPresenter().continueToNext();
    }

    @Override
    public void showCameraFragment(String nextPhototype) {
        Log.d(TAG, "showCameraFragment: "+nextPhototype);
        ((NewEbolCameraAcitvity) getActivity()).showCameraFragment(nextPhototype);
    }

    @OnClick(R.id.addNotesP)
    void addNotesPClick(View v){
        notesView.setVisibility(View.VISIBLE);
        notesText.setText(vehiclePhoto.getNote());
    }

    @OnClick(R.id.saveNote)
    void saveNoteClick(View v){
        vehiclePhoto.setNote(notesText.getText().toString());
        getPresenter().saveNoteClicked();
    }

    @Override
    public void hideNote() {
        notesView.setVisibility(View.GONE);
    }

    @Override
    public void backToDamages() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.markDamageP)
    void markDamagePClick(View v){
        ((NewEbolCameraAcitvity)getActivity()).showMarkDamagesFragment(vehiclePhoto.getPhotoType());
    }

    @OnClick(R.id.retakePhotoP)
    void retakePhotoPClick(View v) {
        getPresenter().retakePhotoClicked();
    }

    @OnClick(R.id.cancel)
    void cancelClick(View view) {
        getActivity().finish();
    }

    @Override
    public NEVehicleInspectionPresenter createPresenter() {
        return new NEVehicleInspectionPresenter();
    }
}