package com.github.andrdev.bolservice.model;


public class Vehicle {
    int id;
    String yearOfMake;
    String make;
    String model;
    String color;
    String mileage;
    String vinNumber;
    String plateNumber;
    boolean radioPresent;
    boolean manuals;
    boolean spareTire;
    boolean cargoCover;
    boolean windscreen;
    String keysCount;
    String remotesCount;
    String headrestsCount;
    String floorMats;
//    String damages;

    public boolean isCargoCover() {
        return cargoCover;
    }

    public void setCargoCover(boolean cargoCover) {
        this.cargoCover = cargoCover;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFloorMats() {
        return floorMats;
    }

    public void setFloorMats(String floorMats) {
        this.floorMats = floorMats;
    }

    public String getHeadrestsCount() {
        return headrestsCount;
    }

    public void setHeadrestsCount(String headrestsCount) {
        this.headrestsCount = headrestsCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeysCount() {
        return keysCount;
    }

    public void setKeysCount(String keysCount) {
        this.keysCount = keysCount;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public boolean isManuals() {
        return manuals;
    }

    public void setManuals(boolean manuals) {
        this.manuals = manuals;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public boolean isRadioPresent() {
        return radioPresent;
    }

    public void setRadioPresent(boolean radioPresent) {
        this.radioPresent = radioPresent;
    }

    public String getRemotesCount() {
        return remotesCount;
    }

    public void setRemotesCount(String remotesCount) {
        this.remotesCount = remotesCount;
    }

    public boolean isSpareTire() {
        return spareTire;
    }

    public void setSpareTire(boolean spareTire) {
        this.spareTire = spareTire;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public boolean isWindscreen() {
        return windscreen;
    }

    public void setWindscreen(boolean windscreen) {
        this.windscreen = windscreen;
    }

    public String getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(String yearOfMake) {
        this.yearOfMake = yearOfMake;
    }
}
