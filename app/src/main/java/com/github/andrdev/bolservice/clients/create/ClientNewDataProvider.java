package com.github.andrdev.bolservice.clients.create;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ClientNewDataProvider implements DataProvider {

    public void createClient(ClientCreateRequest request,
                             DataCb<SimpleResponse> successCb,
                             DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().createClient(request, successCb::returnData, failedCb::returnData);
    }

    public void deInit() {
        
    }
}
