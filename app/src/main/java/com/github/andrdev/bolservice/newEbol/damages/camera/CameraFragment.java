package com.github.andrdev.bolservice.newEbol.damages.camera;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.NewEbolCameraAcitvity;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.google.zxing.client.android.camera.CameraConfigurationUtils;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CameraFragment extends MvpFragment<CameraView, CameraPresenter> implements CameraView {

    private Camera camera;
    private CameraPreview cameraPreview;
    private PictureCallback pi;

    @Bind(R.id.carModel)
    TextView vehicleModel;
    @Bind(R.id.photoType)
    TextView type;
    @Bind(R.id.photoCounter)
    TextView photoCounter;
    @Bind(R.id.cameraView)
    LinearLayout cameraView;

    File file;
    String photoType;
    VehicleInfo vehicleInfo;

    private int selectedVehicleId;
    boolean splashOn = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void getData() {
        photoType = getArguments().getString(VehiclePhotosWidget.PHOTO_TYPE);
        selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        getPresenter().getData(selectedVehicleId);
    }

    @Override
    public void setData(VehicleInfo info) {
        vehicleInfo = info;
        file = new File(getActivity().getFilesDir() + "/"
                +System.currentTimeMillis()+ selectedVehicleId + photoType + ".png");
        showViews();
        if (cameraView != null) {
            cameraView.removeAllViews();
        }
        if (cameraPreview != null) {
            cameraPreview.refreshCamera(camera);
        }
        initCamera();
    }

    @Override
    public void initCamera() {
        camera = Camera.open();
        cameraPreview = new CameraPreview(getActivity(), camera);
        cameraPreview.refreshCamera(camera);
        Camera.Parameters p = camera.getParameters();
        List<String> focusModes = p.getSupportedFocusModes();
        List<Camera.Size> sizes = p.getSupportedPictureSizes();
        Camera.Size size = getPictureSize(sizes);
        p.setPictureSize(size.width, size.height);
        if(focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            p.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        if(splashOn) {
//            CameraConfigurationUtils.setTorch(p, true);
            p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
        } else {
//            CameraConfigurationUtils.setTorch(p, false);

            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        }
        camera.setParameters(p);
        cameraView.addView(cameraPreview);
        pi = getPictureCallback();
    }

    private Camera.Size getPictureSize(List<Camera.Size> sizes) {
        Camera.Size temp = null;
        for (Camera.Size picS : sizes) {
            if (picS.width > 1000 && picS.width < 1500) {
                temp = picS;
            } else if (temp != null && picS.width < 1000 && temp.width < picS.width) {
                temp = picS;
            }
        }
        if (temp == null) {
            return sizes.get(0);
        } else {
            return temp;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(camera != null) {
            camera.release();
        }
    }

    @OnClick(R.id.capture)
    void captureClick(){
        camera.takePicture(null, null, pi);
    }

    @OnClick(R.id.splash)
    void splashClick() {
        if(getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            camera.release();
            splashOn = !splashOn;
            cameraView.removeAllViews();
            initCamera();
        }
    }

    @OnClick(R.id.cancel)
    void cancelClick(View view) {
        getActivity().finish();
    }

    private void showViews() {
        switch (photoType){
            case VehiclePhotosWidget.FRONT_PHOTO:
                type.setText("Driver Front");
                photoCounter.setText("1 of 4");
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                type.setText("Driver Back");
                photoCounter.setText("2 of 4");
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                type.setText("Driver Right");
                photoCounter.setText("3 of 4");
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                type.setText("Driver Left");
                photoCounter.setText("4 of 4");
                break;
            default:
                type.setVisibility(View.INVISIBLE);
                photoCounter.setVisibility(View.INVISIBLE);
                break;
        }
        vehicleModel.setText(String.format("%s %s %s", vehicleInfo.getYearOfMake(), vehicleInfo.getMake(),
                vehicleInfo.getModel()));
    }

    private PictureCallback getPictureCallback() {
        return (data, camera) -> {
            //make a new picture file
            getPresenter().takePhoto(vehicleInfo, file, photoType, data);
            stopCamera(camera);
        };
    }

    @Override
    public void showMarkDamagesFragment() {
        ((NewEbolCameraAcitvity)CameraFragment.this.getActivity()).showVehicleInspectionsFragment(photoType);
    }

    @Override
    public void stopCamera(Camera camera) {
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public CameraPresenter createPresenter() {
        return new CameraPresenter();
    }

}
