package com.github.andrdev.bolservice.helpcenter;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class HelpCenterPresenter extends MvpBasePresenter<BaseSearchView> {

    public abstract void getData();

    @SuppressWarnings("ConstantConditions")
    public abstract void requestFailed(Throwable throwable);

    @SuppressWarnings("ConstantConditions")
    public abstract void requestSuccess(HelpCenterResponse helpCenterResponse);
}
