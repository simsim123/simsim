package com.github.andrdev.bolservice.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.colleagues.ColleaguesAdapter;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;


public class SelectColleagueDialogFragment extends DialogFragment {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.searchAct)
    EditText searchAct;

    ColleaguesAdapter colleaguesAdapter;
    List<Colleague> colleaguesMain = new ArrayList<>();
    List<Colleague> colleagues = new ArrayList<>();
    SelectedCallback callback;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_choose_colleague, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        sendRequest();
        searchAct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String temp = searchAct.getText().toString().toLowerCase();
                if (temp.length() > 0) {
                    colleagues = new ArrayList<>();
                    Observable.from(colleaguesMain)
                            .filter(colleague -> colleagueHasText(colleague, temp))
                            .subscribe(colleague -> colleagues.add(colleague));
                } else {
                    colleagues = colleaguesMain;
                }
                colleaguesAdapter.setData(colleagues);
                colleaguesAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private Boolean colleagueHasText(Colleague client, String temp) {
        return client.getFullNameFormatted().toLowerCase().contains(temp)
                || client.getDriverPhone().toLowerCase().contains(temp);
    }

    private void sendRequest() {
        EbolNetworkWorker2.getInstance().getColleagues(this::setColleagues, this::requestFailed);
    }

    private void requestFailed(Throwable throwable) {

    }

    public void setColleagues(ColleaguesResponse colleaguesResponse) {
        colleaguesMain = colleaguesResponse.getColleagues();
        colleagues = colleaguesResponse.getColleagues();
        colleaguesAdapter.setData(colleaguesResponse.getColleagues());
        colleaguesAdapter.notifyDataSetChanged();
    }

    public SelectedCallback getCallback() {
        return callback;
    }

    public void setCallback(SelectedCallback callback) {
        this.callback = callback;
    }

    private void initRecycler() {
        colleaguesAdapter = new ColleaguesAdapter();
        colleaguesAdapter.setData(colleagues);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(colleaguesAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                (view, position) -> {
                    Colleague item = colleagues.get(position);
                    callback.itemSelected(item);
                    dismiss();
                }));
    }

    @OnClick(R.id.cancel)
    void cancelClick() {
        dismiss();
    }

    public static SelectColleagueDialogFragment newInstance() {
        SelectColleagueDialogFragment dialogFrament = new SelectColleagueDialogFragment();
        return dialogFrament;
    }

    public interface SelectedCallback{
        void itemSelected(Colleague item);
    }
}

