package com.github.andrdev.bolservice.newEbol.vehicleInfo;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class NewEbolVehicleInfoDataProvider implements DataProvider{

    private DbHelper dbHelper;

    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getTempVehicle(DataCb<VehicleInfo> successCb) {
        dbHelper.getTempVehicle(successCb::returnData);
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> successCb) {
        dbHelper.getVehicleById(vehicleId, successCb::returnData);
    }

    public void updateVehicle(VehicleInfo vehicleInfo, DataCb<Boolean> successCb) {
        dbHelper.updateVehicle(vehicleInfo, successCb::returnData);
    }
    public void saveTempVehicleOrUpdate(VehicleInfo vehicleInfo, DataCb<Boolean> successCb) {
        dbHelper.saveTempVehicleOrUpdate(vehicleInfo, successCb::returnData);
    }
}
