package com.github.andrdev.bolservice.clients.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.clients.ClientsActivity;
import com.github.andrdev.bolservice.model.Client;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ClientDetailsFragment extends MvpFragment<ClientDetailsView, ClientDetailsPresenter>
        implements ClientDetailsView {

    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.address)
    TextView address;
    @Bind(R.id.city)
    TextView city;
    @Bind(R.id.state)
    TextView state;
    @Bind(R.id.zip)
    TextView zip;
    @Bind(R.id.phone)
    TextView phone;
    @Bind(R.id.email)
    TextView email;
    @Bind(R.id.titleTool)
    TextView titleTool;
    @Bind(R.id.editClient)
    TextView editClient;

    Client client;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void initFields() {
        name.setText(client.getCustomerName());
        Log.d("dree", client.getCustomerName());
        address.setText(client.getAddress());
        city.setText(client.getCity());
        state.setText(client.getState());
        zip.setText(client.getZIP());
        phone.setText(client.getPhone());
        titleTool.setText(client.getCustomerName());
    }


    @OnClick(R.id.editClient)
    void editClick(View view) {
        ((ClientsActivity) getActivity()).showClientEditFragment();
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void backToClients() {
        getActivity().onBackPressed();
    }

    @Override
    public void setData(Client client) {
        this.client = client;
    }

    @Override
    public void showEditButton() {
        editClient.setVisibility(View.VISIBLE);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @NonNull
    @Override
    public ClientDetailsPresenter createPresenter() {
        return new ClientDetailsPresenterImpl();
    }
}