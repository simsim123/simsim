package com.github.andrdev.bolservice.newEbol.vehicleInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.vehicleInfo.vin.NewEbolScanVinActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolVehicleInfoFragment extends MvpFragment<NewEbolVehicleInfoView, NewEbolVehicleInfoPresenter>
        implements NewEbolVehicleInfoView {

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @Bind(R.id.year)
    EditText year;

    @Bind(R.id.make)
    EditText make;

    @Bind(R.id.model)
    EditText model;

    @Bind(R.id.vin)
    EditText vin;

    @Bind(R.id.mileage)
    EditText mileage;

    @Bind(R.id.color)
    EditText color;

    @Bind(R.id.plate)
    EditText plate;

    VehicleInfo vehicleInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @NonNull
    @Override
    public NewEbolVehicleInfoPresenter createPresenter() {
        return new NewEbolVehicleInfoPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(NewFFebolFragment.SELECTED_VEHICLE_ID)) {
            int selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
            getPresenter().getVehicleById(selectedVehicleId);
        } else {
            getPresenter().getTempVehicle();
        }
    }

    @Override
    public void setFields(VehicleInfo info) {
        if(info != null) {
            vehicleInfo = info;
            setDataToFields();
        } else {
            vehicleInfo = new VehicleInfo();
            vehicleInfo.setType("temp");
        }
    }

    private void setDataToFields() {
        color.setText(vehicleInfo.getColor());
        make.setText(vehicleInfo.getMake());
        mileage.setText(vehicleInfo.getMileage());
        model.setText(vehicleInfo.getModel());
        plate.setText(vehicleInfo.getPlateNumber());
        year.setText(vehicleInfo.getYearOfMake());
        vin.setText(vehicleInfo.getVinNumber());
    }

    @OnClick(R.id.scanb)
    void scanClick() {
        getPresenter().scanClick();
    }

    @Override
    public void openScanVin() {
        new IntentIntegrator(getActivity()).setCaptureActivity(NewEbolScanVinActivity.class).initiateScan();
    }

    @OnClick(R.id.enterb)
    void enterClick() {
        getPresenter().enterClick();
    }

    @Override
    public void openEnterVin() {
        ((NewEbolVehicleInformationActivity) getActivity()).showEnterVinNumberFragment();
    }

    @OnClick(R.id.next)
    void saveClick(View v) {
        if(!checkfields()){
            return;
        }

        getPresenter().nextClick();
    }

    @Override
    public void saveFields() {
        vehicleInfo.setColor(Utils.getTextFromEditText(color));
        vehicleInfo.setMake(Utils.getTextFromEditText(make));
        vehicleInfo.setMileage(Utils.getTextFromEditText(mileage));
        vehicleInfo.setModel(Utils.getTextFromEditText(model));
        vehicleInfo.setPlateNumber(Utils.getTextFromEditText(plate));
        vehicleInfo.setYearOfMake(Utils.getTextFromEditText(year));
        vehicleInfo.setVinNumber(Utils.getTextFromEditText(vin));
    }

    @Override
    public VehicleInfo getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void backToEbol() {
        getActivity().onBackPressed();
    }

    private boolean checkfields() {
        return Utils.hasText(errorText, make, model, year);
    }
}
