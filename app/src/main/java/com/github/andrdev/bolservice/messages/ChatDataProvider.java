package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.MessageRequest;
import com.github.andrdev.bolservice.model.responses.ConversationResponse;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.model.responses.PostMessageResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ChatDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getConversation(long collId,
                                DataCb<ConversationResponse> successCb,
                                DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getConversation(collId,
                successCb::returnData,
                failedCb::returnData);
    }

    public void postMessage(MessageRequest messageRequest,
                            DataCb<PostMessageResponse> successCb,
                            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().postMessage(messageRequest,
                successCb::returnData,
                failedCb::returnData);
    }

    public void getProfile(DataCb<Profile> successCb) {
        dbHelper.getSavedProfile(successCb::returnData);
    }
}
