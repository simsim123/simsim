package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Invoice;

import java.util.List;


public class InvoicesResponse {
    String status;
    List<Invoice> invoices;

    public InvoicesResponse() {
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
