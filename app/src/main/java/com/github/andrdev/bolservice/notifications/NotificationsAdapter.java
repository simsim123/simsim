package com.github.andrdev.bolservice.notifications;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.model.Notification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;


public class NotificationsAdapter extends BaseAdapter<Notification, NotificationsAdapter.NotificationsHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        if(viewType == 1) {
            return R.layout.list_item_notification_pickup;
        } else {
            return R.layout.list_item_notification_delivery;
        }
    }

    @Override
    protected NotificationsHolder getHolder(View view) {
        return new NotificationsHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getAddressType().equals("pendingPickup")?1:2;
    }

    protected static class NotificationsHolder extends BaseAdapter.BaseViewHolder<Notification> {

        @Bind(R.id.orderId)
        TextView orderId;
        @Bind(R.id.timeLine)
        TextView timeLine;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.newSign)
        TextView newSign;
        @Bind(R.id.customerName)
        TextView customerName;
        @Bind(R.id.address)
        TextView address;
        @Bind(R.id.vehiclesCount)
        TextView vehiclesCount;

        public NotificationsHolder(View itemView) {
            super(itemView);
        }

        protected void bind(Notification notification) {
            timeLine.setVisibility(View.INVISIBLE);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yy, HH:mm a");
                Date d = sdf.parse(notification.getItemDate());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            orderId.setText(String.valueOf(notification.getSubmitOrderId()));
            customerName.setText(notification.getItemName());
            address.setText(notification.getSubmitAddress());
            vehiclesCount.setText(notification.getItemString());
            if (notification.getStatus().equals("new")) {
                newSign.setVisibility(View.VISIBLE);
            } else {
                newSign.setVisibility(View.INVISIBLE);
            }
        }
    }
}
