package com.github.andrdev.bolservice.current.originEbol.vehicleInfo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class OriginVehicleInformationActivity extends AppCompatActivity {

    private static final String TAG_VEHICLE_INFO = "vehicleInfo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_info);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showVehickeInfoFragment();
        }
    }

    private void showVehickeInfoFragment() {
        OriginVehicleInformationFragment fragment = new OriginVehicleInformationFragment();
        if(getIntent().hasExtra(NewFFebolFragment.SELECTED_VEHICLE_ID)) {
            fragment.setArguments(getIntent().getExtras());
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment, TAG_VEHICLE_INFO).commit();
    }


    @OnClick(R.id.backIm)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

