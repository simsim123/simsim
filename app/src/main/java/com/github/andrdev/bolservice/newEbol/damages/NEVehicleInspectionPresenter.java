package com.github.andrdev.bolservice.newEbol.damages;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.File;

public class NEVehicleInspectionPresenter extends MvpBasePresenter<NEVehicleInspectionView>{

    private static final String TAG = "dreePho";
    NEVehicleInspectionDataProvider dataProvider;

    @Override
    public void attachView(NEVehicleInspectionView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new NEVehicleInspectionDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData(int selectedVehicleId, String photoType) {
        dataProvider.getVehicleById(selectedVehicleId,
                vehicleInfo -> getVehiclePhotoWithType(vehicleInfo, photoType));
    }

    public void getVehiclePhotoWithType(VehicleInfo vehicleInfo, String photoType) {
        VehiclePhoto vehiclePhoto = null;
        switch (photoType) {
            case VehiclePhotosWidget.FRONT_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getFront();
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getBack();
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getLeftSide();
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                vehiclePhoto = vehicleInfo.getDamages().getRightSide();
                break;
            default:
                break;
        }

        if(!isViewAttached()){
            return;
        }
        getView().setVehicleInfo(vehicleInfo);
        getView().setVehiclePhoto(vehiclePhoto);
        getView().initViews();
    }

    public void updateVehiclePhoto(VehiclePhoto vehiclePhoto) {
//        dataProvider.updateVehiclePhoto(carPhoto, this::openVehicleInspection);
    }

    public void continueToNext() {
        if(!isViewAttached()){
            return;
        }
        VehiclePhoto vehiclePhoto = getView().getVehiclePhoto();
        vehiclePhoto.setIsTemp(false);
        String nextType = getNextPhototype(vehiclePhoto.getPhotoType());
        dataProvider.updateVehiclePhoto(vehiclePhoto, (saved) -> photoSaved(nextType));
    }

    private void photoSaved(String nextPhotoType) {
        if(!isViewAttached()){
            return;
        }
        Log.d(TAG, "photoSaved: ");
        if(nextPhotoType.equalsIgnoreCase("done")) {
            getView().backToDamages();
        } else {
            getView().showCameraFragment(nextPhotoType);
        }
    }

    public String getNextPhototype(String phototype) {
        String nextPhototype = "";
        switch (phototype) {
            case VehiclePhotosWidget.FRONT_PHOTO:
                nextPhototype = VehiclePhotosWidget.BACK_PHOTO;
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                nextPhototype = VehiclePhotosWidget.RIGHT_SIDE_PHOTO;
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                nextPhototype = VehiclePhotosWidget.LEFT_SIDE_PHOTO;
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                nextPhototype = "done";
                break;
            default:
                break;
        }
        Log.d(TAG, "getNextPhototype: "+nextPhototype);
        return nextPhototype;
    }

    public void saveNoteClicked() {
        if(!isViewAttached()){
            return;
        }
        VehiclePhoto vehiclePhoto = getView().getVehiclePhoto();
        dataProvider.updateVehiclePhoto(vehiclePhoto, this::noteSaved);
    }

    public void noteSaved(boolean saved) {
        if(!isViewAttached()) {
            return;
        }
        getView().hideNote();
    }

    public void retakePhotoClicked() {
        if(!isViewAttached()){
            return;
        }
        VehiclePhoto vehiclePhoto = getView().getVehiclePhoto();
        dataProvider.deleteVehiclePhoto(vehiclePhoto, (deleted) -> deletePhotoFile(vehiclePhoto));
    }

    void deletePhotoFile(VehiclePhoto photo) {
        File file = new File(photo.getPhotoPath());
        if (file.exists()) {
            file.delete();
        }
        retakePhotoDeleted(photo.getPhotoType());
    }

    private void retakePhotoDeleted(String photoType) {
        if(!isViewAttached()){
            return;
        }
        getView().showCameraFragment(photoType);
    }
}
