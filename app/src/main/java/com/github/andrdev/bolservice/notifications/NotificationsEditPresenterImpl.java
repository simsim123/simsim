package com.github.andrdev.bolservice.notifications;

import com.github.andrdev.bolservice.model.Notification;
import com.github.andrdev.bolservice.model.responses.NotificationsMarkResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NotificationsEditPresenterImpl extends NotificationsEditPresenter {

    @Override
    public void getData() {
        if(!isViewAttached()){
            return;
        }

        EbolNetworkWorker2.getInstance().getNotifications(
                this::getDataRequestSuccess,
                this::getDataRequestFailed);
    }

    @Override
    public void getDataRequestSuccess(NotificationsResponse notificationsResponse) {
        if(!isViewAttached()) {
            return;
        }

        if(notificationsResponse.getStatus().equals("true")) {
            Observable.from(notificationsResponse.getNotifications())
                    .filter(notification -> notification.getStatus().equals("new"))
                    .toList().subscribe(this::setNotifications);
        } else {
            getView().showFailedToast();
        }
    }

    public void setNotifications(List<Notification> notificationsFiltered) {
        if(!isViewAttached()) {
            return;
        }
        getView().setData(notificationsFiltered);
    }

    @Override
    public void getDataRequestFailed(Throwable throwable) {
        if(!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @Override
    public void deleteNotifications(List<Long> notificationIds) {
        EbolNetworkWorker2.getInstance().markAsReadNotifications(notificationIds,
                this::markAsReadNotifSuccess,
                this::getDataRequestFailed);
    }

    @Override
    public void markAsReadNotifSuccess(NotificationsMarkResponse notificationsMarkResponse) {
        if(isViewAttached()) {
            return;
        }

        if(notificationsMarkResponse.getStatus().equals("true")) {
            getView().getData();
        } else {
            getView().showFailedToast();
        }
    }
}
