package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.github.andrdev.bolservice.R;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolScanVinFragment extends Fragment
        implements CompoundBarcodeView.TorchListener {

    public static final String VIN = "vin";
//    @Bind(R.id.zxing_barcode_scanner)
//    ZXingScannerView mScannerView;

    private CaptureManager capture;
    @Bind(R.id.zxing_barcode_scanner)
     CompoundBarcodeView barcodeScannerView;

    @Bind(R.id.cancel)
    Button cancel;

    boolean isLightsOn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vinscaner, container, false);
        ButterKnife.bind(this, view);
        capture = new CaptureManager(getActivity(), barcodeScannerView);
        barcodeScannerView.setTorchListener(this);

        capture.initializeFromIntent(getActivity().getIntent(), savedInstanceState);
        capture.decode();
        return view;
    }

//    @Override
//    public void onResume() {
//        super.onResume();

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...

//        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
//        mScannerView.startCamera();          // Start camera on resume
//
//        mScannerView.setAutoFocus(true);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
////        mScannerView.stopCamera();           // Stop camera on pause
//    }

    @Override
    public void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

//    @Override
//    public void handleResult(Result rawResult) {
//        // Do something with the result here
//        Log.v("dree", rawResult.getText()); // Prints scan results
//        Log.v("dree", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
//        Intent intent = new Intent();
//        intent.putExtra(VIN, rawResult.getText());
//        getActivity().setResult(Activity.RESULT_OK, intent);
//        getActivity().finish();
//    }

    @OnClick(R.id.enter)
    void enterClick(View view) {
        getActivity().setResult(319);
        getActivity().finish();
    }

    @OnClick(R.id.lights)
    void lightsClick(View view) {
//        Camera camera = CameraUtils.getCameraInstance();
//        Camera.Parameters params = camera.getParameters();
//        if(isLightsOn) {
//            mScannerView.setFlash(!isLightsOn);
        isLightsOn = !isLightsOn;
        if (isLightsOn) {
            barcodeScannerView.setTorchOn();
        } else {
            barcodeScannerView.setTorchOff();
        }
//            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
//        } else {
//            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
//        }

    }

    @OnClick(R.id.cancel)
    void cancelClick(View view) {
        getActivity().setResult(Activity.RESULT_CANCELED);
        getActivity().finish();
    }

    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }
}

