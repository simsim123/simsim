package com.github.andrdev.bolservice.newEbol.damages.camera;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class CameraDataProvider implements DataProvider {

    private DbHelper dbHelper;

    public void setDbHelper(DbHelper dataProvider) {
        this.dbHelper = dataProvider;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void getVehicleById(int vehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(vehicleId, callback::returnData);
    }

    public void deleteVehiclePhotoAndTempPhoto(VehiclePhoto photoToDelete, DataCb<Boolean> callback) {
        if(photoToDelete != null) {
            dbHelper.deleteVehiclePhotoById(photoToDelete.getId(), (deleted) -> deleteTempVehiclePhoto(callback));
        } else {
            deleteTempVehiclePhoto(callback);
        }
    }

    public void deleteTempVehiclePhoto(DataCb<Boolean> callback) {
        dbHelper.deleteTempVehiclePhoto(callback::returnData);
    }

    public void savePhoto(VehiclePhoto tempPhoto, int vehicleId, DataCb<Boolean> callback) {
        dbHelper.saveVehiclePhoto(tempPhoto, vehicleId, callback::returnData);
    }
}
