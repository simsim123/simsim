package com.github.andrdev.bolservice.colleagues.create;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ColleagueNewDataProvider implements DataProvider {

    public void createColleague(CreateDriverRequest request,
                                DataCb<CreateDriverResponse> successCb,
                                DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().createDriver(request, successCb::returnData, failedCb::returnData);
    }

    public void deInit() {

    }
}
