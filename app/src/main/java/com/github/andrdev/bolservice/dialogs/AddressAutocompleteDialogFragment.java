package com.github.andrdev.bolservice.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.RecyclerItemClickListener;
import com.github.andrdev.bolservice.SearchAddressProvider;
import com.github.andrdev.bolservice.model.responses.PlaceInfo;
import com.github.andrdev.bolservice.model.responses.Places;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddressAutocompleteDialogFragment extends DialogFragment
        implements SearchAddressProvider.PlacesHandlerCallback {

    private static final String TAG = "dreeGpsAut";
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.searchAct)
    EditText searchAct;

    AddressAdapter predictionAdapter;
    SelectedCallback callback;
    List<Address> predictions = new ArrayList<>();
    private SearchAddressProvider placesHandler;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_places_autocomplete, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        searchAct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String temp = searchAct.getText().toString().toLowerCase();
                Log.d(TAG, "onTextChanged: "+temp);
                sendRequest(temp);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private void sendRequest(String text) {
        placesHandler.getAddressesFromQueryGjs(text);
    }

    public SelectedCallback getCallback() {
        return callback;
    }

    public void setCallback(SelectedCallback callback) {
        this.callback = callback;
    }

    private void initRecycler() {
        predictionAdapter = new AddressAdapter();
        predictionAdapter.setData(predictions);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(predictionAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                (view, position)->{
                    placesHandler.getInfo(predictions.get(position).getUrl());
                }));
    }

    @OnClick(R.id.cancel)
    void cancelClick() {
        dismiss();
    }

    @Override
    public void addressesFromQuery(List<Address> addresses) {
        Log.d(TAG, "addressesFromQuery: "+addresses.size());
        predictions = addresses;
        predictionAdapter.setData(predictions);
        predictionAdapter.notifyDataSetChanged();
    }

    @Override
    public void returnResult(PlaceInfo prediction) {
        String tempStreet = null;
        String tempNumber = null;
        Address adr = new Address(new Locale("en"));
        for(PlaceInfo.Result.AddressComponent component:prediction.getResult().getAddress_components()) {
            switch (component.getTypes()[0]) {
                case "street_number":
                    tempNumber = component.getLong_name();
                    break;
                case "route":
                    tempStreet = component.getLong_name();
                    break;
                case "sublocality_level_1":
                case "locality":
                    adr.setAdminArea(component.getLong_name());
                case "sublocality":
                    adr.setSubLocality(component.getLong_name());
                    break;
                case "administrative_area_level_1":
                    adr.setLocality(component.getLong_name());
                    break;
                case "postal_code":
                    adr.setPostalCode(component.getLong_name());
                    break;
            }
        }
        if (tempNumber == null) {
            tempNumber = "";
        }
        if (tempStreet == null) {
            tempStreet = "";
        }
        adr.setThoroughfare(String.format("%s %s", tempNumber, tempStreet));
        callback.itemSelected(adr);

        dismiss();

    }

    public void setPlacesHandler(SearchAddressProvider placesHandler) {
        this.placesHandler = placesHandler;
    }

    public static AddressAutocompleteDialogFragment newInstance(Context context) {
        AddressAutocompleteDialogFragment dialogFragment = new AddressAutocompleteDialogFragment();
        SearchAddressProvider placesHandler = new SearchAddressProvider(context, dialogFragment);
        dialogFragment.setPlacesHandler(placesHandler);
        return dialogFragment;
    }

    public interface SelectedCallback{
        void itemSelected(Address address);
    }
}