package com.github.andrdev.bolservice.colleagues.edit;

import android.content.Context;

import com.github.andrdev.bolservice.model.Colleague;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ColleagueEditView extends MvpView {

    void initFields();

    void setData(Colleague colleague);

    void showFailedToast();

    void backToColleagueDetails();

    Context getViewContext();
}
