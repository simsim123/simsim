package com.github.andrdev.bolservice.current.originEbol.damages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginMarkDamagesView extends MvpView{
    void setVehicleInfo(VehicleInfo vehicleInfo);

    void initViews();

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);

    Context getViewContext();

    void backToDamages();
}
