package com.github.andrdev.bolservice.share;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.history.HistoryFragment;
import com.github.andrdev.bolservice.model.requests.ShareRequest;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityShare extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showHistoryFragment();
        }
    }

    private void showHistoryFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new HistoryFragment()).commit();
    }
    @OnClick(R.id.print)
    void onPrintClick(View v) {

    }
    @OnClick(R.id.sendMail)
    void onSendMailClick(View v) {
        ShareRequest sr = new ShareRequest();
        sr.setType("mil");
        sr.setEntityId("mil");
        sr.setToEmail("mil");
        EbolNetworkWorker2.getInstance().share(sr).subscribe(resp->onBackClick(v));
    }

    @OnClick(R.id.dismiss)
    void onDismissClick(View v) {
        onBackPressed();
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        onBackPressed();
    }
}
