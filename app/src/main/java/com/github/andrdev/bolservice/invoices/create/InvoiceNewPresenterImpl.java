package com.github.andrdev.bolservice.invoices.create;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.GpsAddressProvider;
import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.List;


public class InvoiceNewPresenterImpl extends InvoiceNewPresenter {

    InvoiceNewDataProvider dataProvider;

    @Override
    public void attachView(InvoiceNewView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new InvoiceNewDataProvider();
        dataProvider.setMapsGpsHandler(getMapsGpsHandler());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    protected GpsAddressProvider getMapsGpsHandler() {
        return new GpsAddressProvider(getView().getViewContext());
    }

    @Override
    public void getAddress() {
        if(!isViewAttached()){
            return;
        }
        dataProvider.hasGpsPermissions(this::permissionsResult);
//        RxPermissions.getInstance(getView().getViewContext())
//                .request(Manifest.permission.ACCESS_FINE_LOCATION,
//                        Manifest.permission.ACCESS_COARSE_LOCATION)
//                .subscribe(this::handlePermissionRequestResult);
    }

    @Override
    protected void permissionsResult(Boolean granted) {
        if (granted == null) {
            return;
        }

        if (granted) {
            isGpsTurnedOn();
        } else {
            noGpsPermission();
        }
    }

    @Override
    protected void isGpsTurnedOn() {
        dataProvider.checkGpsTurnedOn(this::gpsTurnedOn);
    }

    @Override
    protected void gpsTurnedOn(Boolean gpsTurnedOn) {
        if (gpsTurnedOn == null) {
            return;
        }

        if (gpsTurnedOn) {
            getLocation();
        } else {
            gpsTurnedOff();
        }
    }

    @Override
    protected void getLocation() {
        dataProvider.getCurrentGpsLocationOv(this::getAddressFromLocation);
    }

    @Override
    protected void getAddressFromLocation(Location location) {
        dataProvider.getAddressObsVO(location, this::gpsAddressSuccessOb);
    }

    @Override
    protected void gpsAddressSuccessOb(List<Address> addresses) {
        addressReceived(addresses.get(0));
    }
//
//    private void handlePermissionRequestResult(Boolean granted) {
//        if(!isViewAttached()){
//            return;
//        }
//
//        if (granted) {
//            getGpsAddress();
//        } else {
//            noGpsPermission();
//        }
//    }

//    private void getGpsAddress() {
//        if(!isViewAttached()){
//            return;
//        }
//
//        final LocationManager mLocationManager = (LocationManager) getView().getViewContext()
//                .getSystemService(Context.LOCATION_SERVICE);
//
//        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            gpsTurnedOff();
//        } else {
//            getCurrentGpsLocation();
//        }
//    }
//
//    private void getCurrentGpsLocation() {
//        if(!isViewAttached()){
//            return;
//        }
//
//        LocationRequest request = LocationRequest.create()
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setNumUpdates(1);
//
//        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getView().getViewContext());
//
//        locationProvider.getUpdatedLocation(request)
//                .subscribe((location) -> getAddressObs(locationProvider, location));
//    }
//
//    private void getAddressObs(ReactiveLocationProvider locationProvider, Location location) {
//        if(!isViewAttached()){
//            return;
//        }
//
//        locationProvider.getReverseGeocodeObservable(location.getLatitude(), location.getLongitude(), 1)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(this::gpsAddressSuccess);
//    }
//
//    private void gpsAddressSuccess(List<Address> addresses) {
//        if(!isViewAttached()){
//            return;
//        }
//
//
//    }
//
//    public void getPlaces(String userQuery) {
//        if(!isViewAttached()){
//            return;
//        }
//
//        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getView().getViewContext());
//        locationProvider.getGeocodeObservable(userQuery, 10)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(this::gpsAddressSuccess);
//    }

    @Override
    public void createInvoice(InvoiceRequest request) {
        dataProvider.newInvoice(request,
                this::requestSuccess,
                this::requestFailed);
    }

    @Override
    protected void requestFailed(Throwable throwable) {
        if(!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    @Override
    protected void requestSuccess(NewInvoiceResponse simpleResponse) {
        if(!isViewAttached()) {
            return;
        }

        if(simpleResponse.getStatus().equals("true")) {
            getView().openShare();
        } else {
            getView().showFailedToast();
        }
    }

    @Override
    public void noGpsPermission() {
        if(!isViewAttached()) {
            return;
        }
        getView().selectAddressFromGooglePlaces();
    }

    @Override
    public void addressReceived(Address address) {
        if(!isViewAttached()) {
            return;
        }
        getView().setAddress(address);
    }

    @Override
    public void gpsTurnedOff() {
        if(!isViewAttached()) {
            return;
        }
        getView().buildAlertMessageNoGps();
    }
}
