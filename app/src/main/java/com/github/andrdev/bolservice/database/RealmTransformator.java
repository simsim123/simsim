package com.github.andrdev.bolservice.database;


import android.util.Log;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.realms.VehiclePhotoRealm;
import com.github.andrdev.bolservice.model.realms.CustomerRealm;
import com.github.andrdev.bolservice.model.realms.DamageTypeRealm;
import com.github.andrdev.bolservice.model.realms.DamagesRealm;
import com.github.andrdev.bolservice.model.realms.MarkedDamageRealm;
import com.github.andrdev.bolservice.model.realms.NewEbolRealm;
import com.github.andrdev.bolservice.model.realms.VehicleInfoRealm;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;

public class RealmTransformator {

    private static final String TAG = "dreeMD";

    public static DamageTypeRealm damageTypeToRealm(DamageType damageType, Realm realm) {
        DamageTypeRealm damageTypeRealm = new DamageTypeRealm();

        if (damageType.getId() != 0) {
            damageTypeRealm.setId(damageType.getId());
        } else {
            damageTypeRealm.setId(getId(realm, DamageTypeRealm.class));
        }

        damageTypeRealm.setState(damageType.getState());
        damageTypeRealm.setDamageName(damageType.getDamageName());
        Log.d(TAG, "damageTypeToRealm: " + damageType.getDamageName());
        damageTypeRealm.setDamageShortName(damageType.getDamageShortName());

        damageTypeRealm = realm.copyToRealmOrUpdate(damageTypeRealm);

        return damageTypeRealm;
    }

    public static DamageType damageTypeRealmToPojo(DamageTypeRealm damageTypeRealm) {
        DamageType damageType = new DamageType();
        damageType.setId(damageTypeRealm.getId());
        damageType.setState(damageTypeRealm.getState());
        damageType.setDamageName(damageTypeRealm.getDamageName());
        Log.d(TAG, "damageTypeRealmToPojo: " +damageType.getDamageName());
        damageType.setDamageShortName(damageTypeRealm.getDamageShortName());
        return damageType;
    }

    public static MarkedDamageRealm markedDamageToRealm(MarkedDamage markedDamage, Realm realm) {
        MarkedDamageRealm markedDamageRealm = new MarkedDamageRealm();

        if (markedDamage.getId() != 0) {
            markedDamageRealm.setId(markedDamage.getId());
        } else {
            markedDamageRealm.setId(getId(realm, MarkedDamageRealm.class));
        }

        if (markedDamage.getDamageTypes() == null) {
            markedDamage.setDamageTypes(new ArrayList<>());
        }

        markedDamageRealm.setIsTemp(markedDamage.isTemp());
        markedDamageRealm.setxPosition(markedDamage.getxPosition());
        markedDamageRealm.setyPosition(markedDamage.getyPosition());

        markedDamageRealm = realm.copyToRealmOrUpdate(markedDamageRealm);


                for (DamageType damageType : markedDamage.getDamageTypes()) {
                    Log.d(TAG, "markedDamageToRealm: " + damageType.getDamageName());
                }
        for (DamageType damageType : markedDamage.getDamageTypes()) {
            markedDamageRealm.getDamageTypes().add(damageTypeToRealm(damageType, realm));
        }

        markedDamageRealm = realm.copyToRealmOrUpdate(markedDamageRealm);
        return markedDamageRealm;
    }

    public static MarkedDamage markedDamageRealmToPojo(MarkedDamageRealm markedDamageRealm) {
        MarkedDamage markedDamage = new MarkedDamage();
        markedDamage.setIsTemp(markedDamageRealm.isTemp());
        markedDamage.setxPosition(markedDamageRealm.getxPosition());
        markedDamage.setyPosition(markedDamageRealm.getyPosition());
        markedDamage.setDamageTypes(new ArrayList<>());

        for (DamageTypeRealm damageTypeRealm : markedDamageRealm.getDamageTypes()) {
            markedDamage.getDamageTypes().add(damageTypeRealmToPojo(damageTypeRealm));
        }
        return markedDamage;
    }

    public static VehiclePhotoRealm vehiclePhotoToRealm(VehiclePhoto vehiclePhoto, Realm realm) {
        VehiclePhotoRealm vehiclePhotoRealm = new VehiclePhotoRealm();

        if (vehiclePhoto.getId() != 0) {
            vehiclePhotoRealm.setId(vehiclePhoto.getId());
        } else {
            vehiclePhotoRealm.setId(getId(realm, VehiclePhotoRealm.class));
        }

        if (vehiclePhoto.getMarkedDamages() == null) {
            vehiclePhoto.setMarkedDamages(new ArrayList<>());
        }

        if (vehiclePhoto.getMarkedDamagesDelivery() == null) {
            vehiclePhoto.setMarkedDamagesDelivery(new ArrayList<>());
        }

        vehiclePhotoRealm.setIsTemp(vehiclePhoto.isTemp());
        vehiclePhotoRealm.setNote(vehiclePhoto.getNote());
        vehiclePhotoRealm.setPhotoType(vehiclePhoto.getPhotoType());
        vehiclePhotoRealm.setPhotoPath(vehiclePhoto.getPhotoPath());

        vehiclePhotoRealm = realm.copyToRealmOrUpdate(vehiclePhotoRealm);

        for (MarkedDamage markedDamage : vehiclePhoto.getMarkedDamages()) {
            vehiclePhotoRealm.getMarkedDamages().add(markedDamageToRealm(markedDamage, realm));
        }
        for (MarkedDamage markedDamage : vehiclePhoto.getMarkedDamagesDelivery()) {
            vehiclePhotoRealm.getMarkedDamagesDelivery().add(markedDamageToRealm(markedDamage, realm));
        }

        vehiclePhotoRealm = realm.copyToRealmOrUpdate(vehiclePhotoRealm);
        return vehiclePhotoRealm;
    }

    public static VehiclePhoto vehiclePhotoRealmToPojo(VehiclePhotoRealm vehiclePhotoRealm) {
        VehiclePhoto vehiclePhoto = new VehiclePhoto();

        vehiclePhoto.setIsTemp(vehiclePhotoRealm.isTemp());
        vehiclePhoto.setId(vehiclePhotoRealm.getId());
        vehiclePhoto.setNote(vehiclePhotoRealm.getNote());
        vehiclePhoto.setPhotoType(vehiclePhotoRealm.getPhotoType());
        vehiclePhoto.setPhotoPath(vehiclePhotoRealm.getPhotoPath());

        vehiclePhoto.setMarkedDamages(new ArrayList<>());
        vehiclePhoto.setMarkedDamagesDelivery(new ArrayList<>());

        for (MarkedDamageRealm markedDamageRealm : vehiclePhotoRealm.getMarkedDamages()) {
            vehiclePhoto.getMarkedDamages().add(markedDamageRealmToPojo(markedDamageRealm));
        }

        for (MarkedDamageRealm markedDamageRealm : vehiclePhotoRealm.getMarkedDamagesDelivery()) {
            vehiclePhoto.getMarkedDamagesDelivery().add(markedDamageRealmToPojo(markedDamageRealm));
        }
        return vehiclePhoto;
    }

    public static DamagesRealm damagesToRealm(Damages damages, Realm realm) {
        DamagesRealm damagesRealm = new DamagesRealm();

        if (damages.getId() != 0) {
            damagesRealm.setId(damages.getId());
        } else {
            damagesRealm.setId(getId(realm, DamagesRealm.class));
        }
        damagesRealm.setCid(damages.getId());
        damagesRealm.setFloorMats(damages.getFloorMats());
        damagesRealm.setHeadrest(damages.getHeadrest());
        damagesRealm.setKeys(damages.getKeys());
        damagesRealm.setManuals(damages.isManuals());
        damagesRealm.setRadio(damages.isRadio());
        damagesRealm.setRemotes(damages.getRemotes());
        damagesRealm.setSpareTires(damages.isSpareTires());
        damagesRealm.setCargoCovers(damages.isCargoCovers());
        if (damages.getFront() != null) {
            damagesRealm.setFront(vehiclePhotoToRealm(damages.getFront(), realm));
        }
        if (damages.getBack() != null) {
            damagesRealm.setBack(vehiclePhotoToRealm(damages.getBack(), realm));
        }
        if (damages.getRightSide() != null) {
            damagesRealm.setRightSide(vehiclePhotoToRealm(damages.getRightSide(), realm));
        }
        if (damages.getLeftSide() != null) {
            damagesRealm.setLeftSide(vehiclePhotoToRealm(damages.getLeftSide(), realm));
        }
        damagesRealm = realm.copyToRealmOrUpdate(damagesRealm);
        return damagesRealm;
    }

    public static Damages damagesRealmToPojo(DamagesRealm damagesRealm) {
        Damages damages = new Damages();
        if (damagesRealm == null) {
            return null;
        }

        damages.setId(damagesRealm.getId());
        damages.setCid(damagesRealm.getId());
        damages.setFloorMats(damagesRealm.getFloorMats());
        damages.setHeadrest(damagesRealm.getHeadrest());
        damages.setKeys(damagesRealm.getKeys());
        damages.setRemotes(damagesRealm.getRemotes());
        damages.setManuals(damagesRealm.isManuals());
        damages.setRadio(damagesRealm.isRadio());
        damages.setSpareTires(damagesRealm.isSpareTires());
        damages.setCargoCovers(damagesRealm.isCargoCovers());
        if (damagesRealm.getFront() != null) {
            damages.setFront(vehiclePhotoRealmToPojo(damagesRealm.getFront()));
        }
        if (damagesRealm.getBack() != null) {
            damages.setBack(vehiclePhotoRealmToPojo(damagesRealm.getBack()));
        }
        if (damagesRealm.getRightSide() != null) {
            damages.setRightSide(vehiclePhotoRealmToPojo(damagesRealm.getRightSide()));
        }
        if (damagesRealm.getLeftSide() != null) {
            damages.setLeftSide(vehiclePhotoRealmToPojo(damagesRealm.getLeftSide()));
        }

        return damages;
    }

    public static VehicleInfoRealm vehicleInfoToRealm(VehicleInfo vehicleInfo, Realm realm) {
        VehicleInfoRealm vehicleInfoRealm = new VehicleInfoRealm();

        if (vehicleInfo.getId() != 0) {
            vehicleInfoRealm.setId(vehicleInfo.getId());
        } else {
            vehicleInfoRealm.setId(getId(realm, VehicleInfoRealm.class));
        }
        vehicleInfoRealm.setCid(vehicleInfo.getCid());
        vehicleInfoRealm.setModel(vehicleInfo.getModel());
        vehicleInfoRealm.setYearOfMake(vehicleInfo.getYearOfMake());
        vehicleInfoRealm.setMake(vehicleInfo.getMake());
        vehicleInfoRealm.setVinNumber(vehicleInfo.getVinNumber());
        vehicleInfoRealm.setColor(vehicleInfo.getColor());
        vehicleInfoRealm.setMileage(vehicleInfo.getMileage());
        vehicleInfoRealm.setPlateNumber(vehicleInfo.getPlateNumber());
        vehicleInfoRealm.setType(vehicleInfo.getType());

        if (vehicleInfo.getDamages() == null) {
            vehicleInfo.setDamages(new Damages());
        }
        vehicleInfoRealm.setDamages(damagesToRealm(vehicleInfo.getDamages(), realm));
        vehicleInfoRealm = realm.copyToRealmOrUpdate(vehicleInfoRealm);
        return vehicleInfoRealm;
    }

    public static VehicleInfo vehicleInfoRealmToPojo(VehicleInfoRealm vehicleInfoRealm) {
        VehicleInfo vehicleInfo = new VehicleInfo();
        if (vehicleInfoRealm == null) {
//            vehicleInfo.setType("temp");
//            return vehicleInfo;
            return null;
        }
        vehicleInfo.setId(vehicleInfoRealm.getId());
        vehicleInfo.setCid(vehicleInfoRealm.getCid());
        vehicleInfo.setModel(vehicleInfoRealm.getModel());
        vehicleInfo.setYearOfMake(vehicleInfoRealm.getYearOfMake());
        vehicleInfo.setMake(vehicleInfoRealm.getMake());
        vehicleInfo.setVinNumber(vehicleInfoRealm.getVinNumber());
        vehicleInfo.setColor(vehicleInfoRealm.getColor());
        vehicleInfo.setMileage(vehicleInfoRealm.getMileage());
        vehicleInfo.setPlateNumber(vehicleInfoRealm.getPlateNumber());
        vehicleInfo.setType(vehicleInfoRealm.getType());
        if (vehicleInfoRealm.getDamages() != null) {
            vehicleInfo.setDamages(damagesRealmToPojo(vehicleInfoRealm.getDamages()));
        }
        return vehicleInfo;
    }

    public static CustomerRealm customerToRealm(Customer customer, Realm realm) {
        CustomerRealm customerRealm = new CustomerRealm();

        if (customer.getId() != 0) {
            customerRealm.setId(customer.getId());
        } else {
            customerRealm.setId(getId(realm, CustomerRealm.class));
        }
        customerRealm.setAddress(customer.getAddress());
        customerRealm.setCell(customer.getCell());
        customerRealm.setCity(customer.getCity());
        customerRealm.setCustomerName(customer.getCustomerName());
        customerRealm.setZip(customer.getZip());
        customerRealm.setState(customer.getState());
        customerRealm.setPhone(customer.getPhone());
        customerRealm = realm.copyToRealmOrUpdate(customerRealm);
        return customerRealm;
    }

    public static Customer customerRealmToPojo(CustomerRealm customerRealm) {
        Customer customer = new Customer();

        customer.setId(customerRealm.getId());
        customer.setAddress(customerRealm.getAddress());
        customer.setCell(customerRealm.getCell());
        customer.setCity(customerRealm.getCity());
        customer.setCustomerName(customerRealm.getCustomerName());
        customer.setZip(customerRealm.getZip());
        customer.setState(customerRealm.getState());
        customer.setPhone(customerRealm.getPhone());
        return customer;
    }

    public static NewEbolRealm newEbolToRealm(NewEbol newEbol, Realm realm) {
        NewEbolRealm newEbolRealm = new NewEbolRealm();
        try {
            if (newEbol.getId() != 0) {
                newEbolRealm.setId(newEbol.getId());
            } else {
                newEbolRealm.setId(getId(realm, NewEbolRealm.class));
            }

            if (newEbol.getVehicleInfos() == null) {
                newEbol.setVehicleInfos(new ArrayList<>());
            }

            Log.v("dreev1", newEbol.getOrderId()!=null?newEbol.getOrderId():"nyull");
            newEbolRealm.setOrderId(newEbol.getOrderId());
            newEbolRealm.setDate(newEbol.getDate());
            newEbolRealm.setType(newEbol.getType());
            newEbolRealm.setIsSavedToDb(newEbol.isSavedToDb());
            newEbolRealm.setDriverSignPath(newEbol.getDriverSignPath());
            newEbolRealm.setCustomerSignPath(newEbol.getCustomerSignPath());
            newEbolRealm.setUnableToSign(newEbol.isUnableToSign());
            newEbolRealm.setDeliveryCustomerSignPath(newEbol.getDeliveryCustomerSignPath());
            newEbolRealm.setDeliveryDriverSignPath(newEbol.getDeliveryDriverSignPath());
            newEbolRealm.setDeliveryUnableToSign(newEbol.isDeliveryUnableToSign());
            newEbolRealm.setVehiclesCount(newEbol.getVehiclesCount());
            newEbolRealm.setSubmitId(newEbol.getSubmitId());

            if (newEbol.getOrigin() != null) {
                newEbolRealm.setOrigin(customerToRealm(newEbol.getOrigin(), realm));
            }

            if (newEbol.getDestination() != null) {
                newEbolRealm.setDestination(customerToRealm(newEbol.getDestination(), realm));
            }

            newEbolRealm = realm.copyToRealmOrUpdate(newEbolRealm);

            Log.v("dreev1", newEbol.getOrderId()!=null?newEbol.getOrderId():"nyull");
            for (VehicleInfo vehicleInfo : newEbol.getVehicleInfos()) {
                newEbolRealm.getVehicleInfos().add(vehicleInfoToRealm(vehicleInfo, realm));
            }
        } catch (Exception e) {
            Log.d("dree", e.getLocalizedMessage());
        }
        return newEbolRealm;
    }

    public static NewEbol newEbolRealmToPojo(NewEbolRealm newEbolRealm) {
        NewEbol newEbol = new NewEbol();
        if (newEbolRealm == null) {
            return newEbol;
        }
        newEbol.setId(newEbolRealm.getId());
        Log.v("dreev3", newEbol.getOrderId() != null ? newEbol.getOrderId() : "nyull");
        newEbol.setOrderId(newEbolRealm.getOrderId());
        Log.v("dreev4", newEbol.getOrderId() != null ? newEbol.getOrderId() : "nyull");
        newEbol.setDate(newEbolRealm.getDate());
        newEbol.setType(newEbolRealm.getType());
        newEbol.setDriverSignPath(newEbolRealm.getDriverSignPath());
        newEbol.setCustomerSignPath(newEbolRealm.getCustomerSignPath());
        newEbol.setUnableToSign(newEbolRealm.isUnableToSign());
        newEbol.setDeliveryCustomerSignPath(newEbolRealm.getDeliveryCustomerSignPath());
        newEbol.setDeliveryDriverSignPath(newEbolRealm.getDeliveryDriverSignPath());
        newEbol.setDeliveryUnableToSign(newEbolRealm.isDeliveryUnableToSign());
        newEbol.setVehiclesCount(newEbolRealm.getVehiclesCount());
        newEbol.setSubmitId(newEbolRealm.getSubmitId());
        newEbol.setIsSavedToDb(newEbolRealm.isSavedToDb());
        if (newEbolRealm.getOrigin() != null) {
            newEbol.setOrigin(customerRealmToPojo(newEbolRealm.getOrigin()));
        }
        if (newEbolRealm.getDestination() != null) {
            newEbol.setDestination(customerRealmToPojo(newEbolRealm.getDestination()));
        }

        newEbol.setVehicleInfos(new ArrayList<>());

        for (VehicleInfoRealm vehicleInfo : newEbolRealm.getVehicleInfos()) {
            newEbol.getVehicleInfos().add(vehicleInfoRealmToPojo(vehicleInfo));
        }
        return newEbol;
    }

    private static <E extends RealmObject> int getId(Realm realm, Class<E> clazz) {
        Number id = realm.where(clazz).max("id");
        if (id != null) {
            return id.intValue() + 1;
        } else {
            return 1;
        }
    }
}
