package com.github.andrdev.bolservice;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public abstract class BaseSearchFragment<T, A extends BaseAdapter,
        V extends MvpView, P extends MvpBasePresenter<V>> extends MvpFragment<V, P>
        implements BaseSearchView<T> {

    @Nullable @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Bind(R.id.searchAct)
    EditText mSearchAct;

    A mAdapter;

    public List<T> mMainData = new ArrayList<>();
    public List<T> mData = new ArrayList<>();

    private long timeClicked;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(this::getData);
        }
        mSearchAct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAndFilter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void searchAndFilter() {
        String temp = mSearchAct.getText().toString().toLowerCase();
        if (temp.length() > 0) {
            mData = new ArrayList<>();
            for (T item : mMainData) {
                filter(item, temp);
            }
        } else {
            mData = mMainData;
        }
        updateDataOnAdapter();
    }

    private void initRecycler() {
        mAdapter = getAdapter();
        mAdapter.setData(mData);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                timedClicker()));
    }

    @NonNull
    private RecyclerItemClickListener.TimedOnItemClickListener timedClicker() {
        return new RecyclerItemClickListener.TimedOnItemClickListener(){
            @Override
            public void onTimedClick(View view, int position) {
                    BaseSearchFragment.this.recyclerViewItemClicked(position);
            }
        };
    }

    @Override
    public void addData(List<T> data) {
        mMainData.clear();
        mMainData.addAll(data);
        mData = mMainData;
        updateDataOnAdapter();
    }

    @Override
    public void setData(List<T> data) {
        mMainData = data;
        mData = mMainData;
        updateDataOnAdapter();
    }

    protected void updateDataOnAdapter() {
        mAdapter.setData(mData);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void stopRefresh() {
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.postDelayed(()->mSwipeRefreshLayout.setRefreshing(false), 1000);
        }
    }

    protected abstract int getLayoutResource();

     protected abstract void filter(T item, String temp);

    protected abstract void getData();

    protected abstract void recyclerViewItemClicked(int position);

    protected abstract A getAdapter();
}
