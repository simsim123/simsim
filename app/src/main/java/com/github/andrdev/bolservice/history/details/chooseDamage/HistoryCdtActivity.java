package com.github.andrdev.bolservice.history.details.chooseDamage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.history.details.damages.HistoryDamagesFragment;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;


public class HistoryCdtActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            showInvoicesFragment();
        }
    }

    private void showInvoicesFragment() {
        HistoryCdtFragment fragment = new HistoryCdtFragment();
        Bundle bundle = new Bundle();
        bundle.putAll(getIntent().getExtras());
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, getIntent().getIntExtra(
                NewFFebolFragment.SELECTED_VEHICLE_ID, 0));
        bundle.putString(HistoryDamagesFragment.PHOTO_TYPE, getIntent().getStringExtra(
                HistoryDamagesFragment.PHOTO_TYPE));
        bundle.putInt("yPosition", getIntent().getIntExtra(
                "yPosition", 0));
        bundle.putInt("xPosition", getIntent().getIntExtra(
                "xPosition", 0));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();
    }
}

