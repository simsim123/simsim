package com.github.andrdev.bolservice.colleagues.details;


import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.database.RealmDbData;
import com.github.andrdev.bolservice.model.Colleague;

public class ColleagueDetailsPresenterImpl extends ColleagueDetailsPresenter {

    ColleagueDetailsDataProvider dataProvider;

    @Override
    public void attachView(ColleagueDetailsView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ColleagueDetailsDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getSavedColleague(this::colleagueRequestSuccess);
        dataProvider.getIsUserBoss(this::userIsBoss);
    }

    @SuppressWarnings("ConstantConditions")
    public void colleagueRequestSuccess(Colleague colleague) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(colleague);
        getView().initFields();
    }

    @SuppressWarnings("ConstantConditions")
    public void userIsBoss(boolean isBoss) {
        if (!isViewAttached()) {
            return;
        }
        if (isBoss) {
            getView().showEditButton();
        }
    }
}
