package com.github.andrdev.bolservice.current.originEbol.damages;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginDamagesView extends MvpView {
    void setVehicleInfo(VehicleInfo currentCar);

    Context getViewContext();
}
