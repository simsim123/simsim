package com.github.andrdev.bolservice.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


public class DialogFactory {

    public static Dialog createAskToTurnOnGpsDialog(Context context,
                                                    DialogInterface.OnClickListener clickListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) ->
                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNeutralButton("Use Google Places", clickListener)
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        return builder.create();
    }

    public static Dialog createAskGpsLocationOrPlacesDialog(
            Context context, DialogInterface.OnClickListener gpsClick,
            DialogInterface.OnClickListener placesClick) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Use your current location or try to find address?")
                .setCancelable(false)
                .setPositiveButton("Current Location", gpsClick)
                .setNeutralButton("Find Address", placesClick);
        return builder.create();
    }

    public static DialogFragment createDatePickerDialog(DatePickerFragment.OnDateSetCallback dateCallback) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setCallback(dateCallback);
        return newFragment;
    }

    public static DialogFragment createSelectFromPlacesDialog(Context context,
            AddressAutocompleteDialogFragment.SelectedCallback selectedCallback) {
        AddressAutocompleteDialogFragment newFragment = AddressAutocompleteDialogFragment.newInstance(context);
        newFragment.setCallback(selectedCallback);
        return newFragment;
    }
}
