package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.realms.VehicleInfoRealm;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface NewEbolEnterVinNumberView extends MvpView {
    void setData(VehicleInfo info);

    void backToVehicleInfo();

    Context getViewContext();

    void showFailedToast();

    VehicleInfo getVehicleInfo();
}
