package com.github.andrdev.bolservice.clients.details;

import android.content.Context;

import com.github.andrdev.bolservice.model.Client;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ClientDetailsView extends MvpView{

    void initFields();

    void backToClients();

    void setData(Client client);

    void showEditButton();

    Context getViewContext();
}
