package com.github.andrdev.bolservice.invoices;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class InvoicesDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void saveInvoice(Invoice invoice, DataCb<Boolean> successCb) {
        dbHelper.saveInvoice(invoice, successCb::returnData);
    }

    public void getInvoices(String status,
                            DataCb<InvoicesResponse> successCb,
                            DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getInvoices(status,
                successCb::returnData,
                failedCb::returnData);
    }
}
