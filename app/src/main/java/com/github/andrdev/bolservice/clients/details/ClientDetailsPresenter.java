package com.github.andrdev.bolservice.clients.details;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ClientDetailsPresenter extends MvpBasePresenter<ClientDetailsView> {

    public abstract void getData();
}
