package com.github.andrdev.bolservice.newEbol.damages;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.github.andrdev.bolservice.R;

import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.damages.camera.CameraFragment;
import com.github.andrdev.bolservice.newEbol.damages.markDamages.NewEbolMarkDamagesFragment;


public class NewEbolCameraAcitvity extends AppCompatActivity {

    String action;
    int selectedVehicleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String photoType = getIntent().getStringExtra("photoType");

        action = getIntent().getStringExtra("action");
        selectedVehicleId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
        if (savedInstanceState == null) {
            if(action.equals("create")) {
                showCameraFragment(photoType);
            } else {
                showVehicleInspectionsFragment(photoType);
            }
        }
    }

    public void showCameraFragment(String type) {
        CameraFragment cameraFragment =  new CameraFragment();
        Bundle bundle = new Bundle();
        bundle.putString("photoType", type);
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        cameraFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, cameraFragment).commit();
    }

    public void showMarkDamagesFragment(String photoType) {
        NewEbolMarkDamagesFragment fragment =  new NewEbolMarkDamagesFragment();
        Bundle bundle = new Bundle();
        bundle.putString("action", action);
        bundle.putString("phototype", photoType);
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
    }

    public void showVehicleInspectionsFragment(String type) {
        NewFebolVehInspFrag fragment =  new NewFebolVehInspFrag();
        Bundle bundle = new Bundle();
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        bundle.putString("phototype", type);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (Build.VERSION.SDK_INT < 16) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
//            ActionBar actionBar = getActionBar();
//            actionBar.hide();
            }
    }
}
