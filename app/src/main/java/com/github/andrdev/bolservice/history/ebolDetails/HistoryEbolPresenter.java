package com.github.andrdev.bolservice.history.ebolDetails;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesG;
import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;



public class HistoryEbolPresenter extends MvpBasePresenter<HistoryEbolView> {

    HistoryEbolDataProvider dataProvider;

    @Override
    public void attachView(HistoryEbolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new HistoryEbolDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData() {
        dataProvider.getTempEbol(this::showEbol);
    }

    private void showEbol(NewEbol ebol) {
        getView().setData(ebol);
    }

    public void getDamages(final VehicleInfo info) {
        dataProvider.getDamages(String.valueOf(info.getCid()),
                damages -> damagesToMarkDamage(info, damages));
    }

    public void damagesClick(int vehicleId) {
        getView().showDamages(vehicleId);
    }

    public void detailsClick(int vehicleId) {
        getView().showVehicleDetails(vehicleId);
    }

    private void damagesToMarkDamage(VehicleInfo info, DamagesResponse damages) {
//        VehiclePhoto damageF = new VehiclePhoto();
//        damageF.setPhotoType("frontPhoto");
//        damageF.setMarkedDamages(new ArrayList<>());
//        damageF.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto damageB = new VehiclePhoto();
//        damageB.setPhotoType("backPhoto");
//        damageB.setMarkedDamages(new ArrayList<>());
//        damageB.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto damageR = new VehiclePhoto();
//        damageR.setPhotoType("rightSidePhoto");
//        damageR.setMarkedDamages(new ArrayList<>());
//        damageR.setMarkedDamagesDelivery(new ArrayList<>());
//        VehiclePhoto damageL = new VehiclePhoto();
//        damageL.setPhotoType("leftSidePhoto");
//        damageL.setMarkedDamages(new ArrayList<>());
//        damageL.setMarkedDamagesDelivery(new ArrayList<>());
//
//        WindowManager wm = (WindowManager) getView().getViewContext().getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
//        info.getDamages().setFront(damageF);
//        info.getDamages().setBack(damageB);
//        info.getDamages().setRightSide(damageR);
//        info.getDamages().setLeftSide(damageL);
//
//        for (DamagesG dg : damages.getDamages()) {
//            MarkedDamage damage = new MarkedDamage();
//            damage.setDamageTypes(new ArrayList<>());
//            DamageType dt = new DamageType();
//            dt.setState(true);
//            dt.setDamageShortName(dg.getCode());
//            damage.getDamageTypes().add(dt);
////            damage.getDamageTypes().add();
//            damage.setxPosition((int) (width * Double.parseDouble(dg.getPositionLeft())));
//            damage.setyPosition((int) (height * Double.parseDouble(dg.getPositionTop())));
//            switch (dg.getSide()) {
//                case "front":
//                    info.getDamages().getFront().setPhotoPath(dg.getImage());
//                    info.getDamages().getFront().getMarkedDamages().add(damage);
//                    break;
//                case "back":
//                    info.getDamages().getBack().setPhotoPath(dg.getImage());
//                    info.getDamages().getBack().getMarkedDamages().add(damage);
//                    break;
//                case "rightside":
//                    info.getDamages().getRightSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getRightSide().getMarkedDamages().add(damage);
//                    break;
//                case "leftside":
//                    info.getDamages().getLeftSide().setPhotoPath(dg.getImage());
//                    info.getDamages().getLeftSide().getMarkedDamages().add(damage);
//                    break;
//                default:
//            }
//        }
//        getView().showDamages(info.getId());
    }
}
