package com.github.andrdev.bolservice.clients.details;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Client;


public class ClientDetailsPresenterImpl extends ClientDetailsPresenter{

    ClientDetailsDataProvider dataProvider;

    @Override
    public void attachView(ClientDetailsView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ClientDetailsDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getSavedClient(this::clientRequestSuccess);
        dataProvider.getIsUserBoss(this::userIsBoss);
    }

    @SuppressWarnings("ConstantConditions")
    public void clientRequestSuccess(Client client) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(client);
        getView().initFields();
    }

    @SuppressWarnings("ConstantConditions")
    public void userIsBoss(boolean isBoss) {
        if (!isViewAttached()) {
            return;
        }
        if (isBoss) {
            getView().showEditButton();
        }
    }
}
