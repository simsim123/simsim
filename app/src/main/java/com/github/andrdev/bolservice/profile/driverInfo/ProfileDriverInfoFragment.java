package com.github.andrdev.bolservice.profile.driverInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequestBuilder;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileDriverInfoFragment extends MvpFragment<ProfileDriverInfoView, ProfileDriverInfoPresenter>
        implements ProfileDriverInfoView {

    @Bind(R.id.firstName)
    EditText firstName;

    @Bind(R.id.lastName)
    EditText lastName;

    @Bind(R.id.address)
    EditText address;

    @Bind(R.id.city)
    EditText city;

    @Bind(R.id.state)
    EditText state;

    @Bind(R.id.zip)
    EditText zip;

    @Bind(R.id.phone)
    EditText phone;

    @Bind(R.id.email)
    EditText email;

    @Bind(R.id.save)
    Button save;

    Profile profile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_driver_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public ProfileDriverInfoPresenter createPresenter() {
        return new ProfileDriverInfoPresenterImpl();
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().getData();
    }

    @Override
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public void initFields() {
        firstName.setText(profile.getFirstname());
        lastName.setText(profile.getLastname());
        address.setText(profile.getBillingAddress());
        city.setText(profile.getBillingCity());
        state.setText(profile.getBillingState());
        zip.setText(profile.getBillingZip());
        phone.setText(profile.getDriverPhone());
        email.setText(profile.getDriverEmail());
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @OnClick(R.id.save)
    void saveClick(View view) {

        Log.d("dree", "degest");
        SaveProfileRequest profileRequest = new SaveProfileRequestBuilder()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setBillingAddress(address)
                .setBillingCity(city)
                .setBillingEmail(email)
                .setBillingFax(profile.getBillingFax())
                .setBillingFirstname(profile.getBillingFirstname())
                .setBillingLastname(profile.getBillingLastname())
                .setBillingPhone(phone)
                .setBillingState(state)
                .setBillingZip(zip)
                .setCompanyAddress(profile.getCompanyAddress())
                .setCompanyCity(profile.getCompanyCity())
                .setCompanyEma(profile.getCompanyEmail())
                .setCompanyFax(profile.getCompanyFax())
                .setCompanyName(profile.getCompanyName())
                .setCompanyPhone(profile.getCompanyPhone())
                .setCompanyState(profile.getCompanyState())
                .setCompanyZip(profile.getCompanyZip())
                .setCompanyEma(profile.getCompanyEmail())
                .setCompanyAddress(profile.getCompanyAddress())
                .setCompanyMc(profile.getCompanyMc())
                .setCompanyUsdot(profile.getCompanyUsdot())
                .createSaveProfileRequest();
        getPresenter().saveData(profileRequest);
    }
}
