package com.github.andrdev.bolservice.model;


public class RegistrationInfoRequest {
    String username;
    String password;

    public RegistrationInfoRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
