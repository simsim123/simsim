package com.github.andrdev.bolservice.profile.baseInfo;

import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


abstract public class ProfileBasePresenter extends MvpBasePresenter<ProfileBaseView> {

    abstract public void getData();

    abstract public void setData(Profile profile);

}
