package com.github.andrdev.bolservice.helpcenter;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class HelpCenterDataProvider implements DataProvider{


    public void deInit() {

    }


    public void getHelpCenter(DataCb<HelpCenterResponse> successCb, DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().getHelpCenter(successCb::returnData, failedCb::returnData);
    }
}
