package com.github.andrdev.bolservice.newEbol.damages;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class NewEbolDamagesPresenter extends MvpBasePresenter<NewEbolDamagesView>{


    NewEbolDamagesDataProvider dataProvider;

    @Override
    public void attachView(NewEbolDamagesView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new NewEbolDamagesDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getData() {

    }

    public void getVehicle(int selectedVehicleId) {
        dataProvider.getVehicleInfoById(selectedVehicleId, this::setCurrentVehicle);
    }

    private void setCurrentVehicle(VehicleInfo vehicleInfo) {
        if(!isViewAttached()){
            return;
        }
        getView().setCurrentVehicle(vehicleInfo);
        String activityTitle = String.format("%s %s %s", vehicleInfo.getYearOfMake(),
                vehicleInfo.getMake(), vehicleInfo.getModel());
        getView().setActivityTitle(activityTitle, vehicleInfo.getVinNumber());
        getView().setDamages(vehicleInfo.getDamages());
    }

    public void updateDamages(Damages damages, int vehicleId) {
        if(!isViewAttached()){
            return;
        }
        dataProvider.updateDamages(damages, vehicleId, this::backToEbol);
    }

    private void backToEbol(boolean saved) {
        if(!isViewAttached()) {
            return;
        }
        getView().backToEbol();
    }

    public void updateVehicle(VehicleInfo vehicle) {
        if(!isViewAttached()){
            return;
        }
        dataProvider.updateVehicle(vehicle, this::backToEbol);
    }
}
