package com.github.andrdev.bolservice.view.vehicleTools;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import com.github.andrdev.bolservice.MvpPercentRelativeLayout;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.view.ToolsSwitch;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VehicleToolsWidget extends MvpPercentRelativeLayout<VehicleToolsView, VehicleToolsPresenter>
        implements VehicleToolsView {

    @Bind(R.id.keysCount)
    EditText keysCount;
    @Bind(R.id.headRestCount)
    EditText headRestCount;
    @Bind(R.id.remotesCount)
    EditText remotesCount;
    @Bind(R.id.floorMatsCount)
    EditText floorMatsCount;

    @Bind(R.id.radioSw)
    ToolsSwitch radio;
    @Bind(R.id.spareTires)
    ToolsSwitch spareTires;
    @Bind(R.id.manuals)
    ToolsSwitch manuals;
    @Bind(R.id.cargoCovers)
    ToolsSwitch cargoCovers;

    public VehicleToolsWidget(Context context) {
        super(context);
        init(context);
    }

    public VehicleToolsWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VehicleToolsWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_car_tools, this);
        ButterKnife.bind(this, view);
        radio.requestFocus();
    }


    @Override
    public VehicleToolsPresenter createPresenter() {
        return new VehicleToolsPresenter();
    }


    public void setFieldsFromDb(Damages damages) {
        remotesCount.setText(String.valueOf(damages.getRemotes()));
        floorMatsCount.setText(String.valueOf(damages.getFloorMats()));
        headRestCount.setText(String.valueOf(damages.getHeadrest()));
        keysCount.setText(String.valueOf(damages.getKeys()));

        radio.setState(damages.isRadio());
        cargoCovers.setState(damages.isCargoCovers());
        spareTires.setState(damages.isSpareTires());
        manuals.setState(damages.isManuals());
    }

    public void setToolsToDamages(Damages damages) {
        damages.setCargoCovers(cargoCovers.getState());
        damages.setSpareTires(spareTires.getState());
        damages.setManuals(manuals.getState());
        damages.setRadio(radio.getState());

        int tempRemotesCount = 0;
        if (!TextUtils.isEmpty(Utils.getTextFromEditText(remotesCount))) {
            tempRemotesCount = Integer.valueOf(Utils.getTextFromEditText(remotesCount));
        }
        damages.setRemotes(tempRemotesCount);

        int tempKeysCount = 0;
        if (!TextUtils.isEmpty(Utils.getTextFromEditText(keysCount))) {
            tempKeysCount = Integer.valueOf(Utils.getTextFromEditText(keysCount));
        }
        damages.setKeys(tempKeysCount);

        int tempHeadRestCount = 0;
        if (!TextUtils.isEmpty(Utils.getTextFromEditText(headRestCount))) {
            tempHeadRestCount = Integer.valueOf(Utils.getTextFromEditText(headRestCount));
        }
        damages.setHeadrest(tempHeadRestCount);

        int tempFloorMatsCount = 0;
        if (!TextUtils.isEmpty(Utils.getTextFromEditText(floorMatsCount))) {
            tempFloorMatsCount = Integer.valueOf(Utils.getTextFromEditText(floorMatsCount));
        }
        damages.setFloorMats(tempFloorMatsCount);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable state = super.onSaveInstanceState();
        SavedState ss = new SavedState(state);
        ss.keysCount = Integer.valueOf(Utils.getTextFromEditText(keysCount));
        ss.headRestCount = Integer.valueOf(Utils.getTextFromEditText(headRestCount));
        ss.remotesCount = Integer.valueOf(Utils.getTextFromEditText(remotesCount));
        ss.floorMatsCount = Integer.valueOf(Utils.getTextFromEditText(floorMatsCount));
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(state);
        if(!(ss.keysCount==0)) {
            keysCount.setText(ss.keysCount);
        }
        if(!(ss.headRestCount==0)) {
            headRestCount.setText(ss.headRestCount);
        }
        if(!(ss.remotesCount==0)) {
            remotesCount.setText(ss.remotesCount);
        }
        if(!(ss.floorMatsCount==0)) {
            floorMatsCount.setText(ss.floorMatsCount);
        }
    }

    public static class SavedState extends BaseSavedState {
        int keysCount;
        int headRestCount;
        int remotesCount;
        int floorMatsCount;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            keysCount = in.readInt();
            headRestCount = in.readInt();
            remotesCount = in.readInt();
            floorMatsCount = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(keysCount);
            out.writeInt(headRestCount);
            out.writeInt(remotesCount);
            out.writeInt(floorMatsCount);
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
