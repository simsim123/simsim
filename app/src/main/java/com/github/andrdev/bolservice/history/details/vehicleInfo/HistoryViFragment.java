package com.github.andrdev.bolservice.history.details.vehicleInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryViFragment extends MvpFragment<HistoryViView, HistoryViPresenter>
        implements HistoryViView {

    @Bind(R.id.year)
    TextView year;

    @Bind(R.id.make)
    TextView make;

    @Bind(R.id.model)
    TextView model;

    @Bind(R.id.vin)
    TextView vin;

    @Bind(R.id.mileage)
    TextView mileage;

    @Bind(R.id.color)
    TextView color;

    @Bind(R.id.plate)
    TextView plate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_origin_vehicle_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public HistoryViPresenter createPresenter() {
        return new HistoryViPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        int vehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        getPresenter().getVehicleInfo(vehicleId);
    }

    public void setVehicleInfo(VehicleInfo info) {
            year.setText(info.getYearOfMake());
            make.setText(info.getMake());
            mileage.setText(info.getMileage());
            model.setText(info.getModel());
            plate.setText(info.getPlateNumber());
            vin.setText(info.getVinNumber());
            color.setText(info.getColor());
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @OnClick(R.id.next)
    void saveClick(View v) {
        saveFields();
    }

    private void saveFields() {
        getActivity().onBackPressed();
    }
}
