package com.github.andrdev.bolservice.clients;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.BaseSearchFragment;


public class ClientsFragment extends BaseSearchFragment<Client, ClientsAdapter, BaseSearchView, ClientsPresenter> {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_clients;
    }

    @Override
    protected void filter(Client item, String temp) {
        if (item.getCustomerName().toLowerCase().contains(temp)
                || item.getPhone().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        getPresenter().getData();
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        getPresenter().listItemClicked(mData.get(position));
    }

    @Override
    protected ClientsAdapter getAdapter() {
        return new ClientsAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void openListItem() {
        ((ClientsActivity) getActivity()).showClientDetailsFragment();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public ClientsPresenter createPresenter() {
        return new ClientsPresenterImpl();
    }
}