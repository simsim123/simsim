package com.github.andrdev.bolservice.invoices.details;

import android.content.Context;

import com.github.andrdev.bolservice.model.Invoice;
import com.hannesdorfmann.mosby.mvp.MvpView;

interface InvoiceDetailsView extends MvpView{

    Context getViewContext();

    void setData(Invoice invoice);

    void initFields();
}
