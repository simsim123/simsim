package com.github.andrdev.bolservice.newEbol.damages.camera;

import android.content.Context;
import android.hardware.Camera;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface CameraView extends MvpView {
    void getData();

    void setData(VehicleInfo info);

    void initCamera();

    void showMarkDamagesFragment();

    void stopCamera(Camera camera);

    Context getViewContext();
}
