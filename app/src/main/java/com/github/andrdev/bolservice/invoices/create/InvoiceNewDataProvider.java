package com.github.andrdev.bolservice.invoices.create;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.GpsAddressProvider;
import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.List;

public class InvoiceNewDataProvider implements DataProvider {

    GpsAddressProvider mapsGpsHandler;

    public GpsAddressProvider getMapsGpsHandler() {
        return mapsGpsHandler;
    }

    public void setMapsGpsHandler(GpsAddressProvider mapsGpsHandler) {
        this.mapsGpsHandler = mapsGpsHandler;
    }

    @Override
    public void deInit() {

    }

    public void hasGpsPermissions(DataCb<Boolean> callback) {
        mapsGpsHandler.hasGpsPermissions(callback::returnData);
    }

    public void checkGpsTurnedOn(DataCb<Boolean> callback) {
        mapsGpsHandler.checkGpsTurnedOn(callback::returnData);
    }

    public void getCurrentGpsLocationOv(DataCb<Location> callback) {
        mapsGpsHandler.getCurrentGpsLocationOv(callback::returnData);
    }

    public void getAddressObsVO(Location location, DataCb<List<Address>> callback) {
        mapsGpsHandler.getAddressGJs(location, callback::returnData);
    }

    public void newInvoice(InvoiceRequest request,
                           DataCb<NewInvoiceResponse> successCb,
                           DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().newInvoice(request,
                successCb::returnData,
                failedCb::returnData);
    }
}
