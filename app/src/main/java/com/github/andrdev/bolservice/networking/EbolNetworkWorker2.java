package com.github.andrdev.bolservice.networking;


import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.github.andrdev.bolservice.clients.edit.ClientEditPresenter;
import com.github.andrdev.bolservice.colleagues.edit.ColleagueEditPresenter;
import com.github.andrdev.bolservice.model.deserializers.DamagesResponseTypedFactory;
import com.github.andrdev.bolservice.model.deserializers.EbolResponseTypedFactory;
import com.github.andrdev.bolservice.model.deserializers.ProfileResponseTypedFactory;
import com.github.andrdev.bolservice.model.deserializers.VehicleInfosResponseTypedFactory;
import com.github.andrdev.bolservice.model.requests.CarCreateRequest;
import com.github.andrdev.bolservice.model.requests.CarPhotoCreateRequest;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.requests.MessageRequest;
import com.github.andrdev.bolservice.model.requests.NewDamageRequest;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.requests.ShareRequest;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.github.andrdev.bolservice.model.responses.ConversationResponse;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;
import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.DriversResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.model.responses.NewDamageResponse;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsMarkResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.github.andrdev.bolservice.model.responses.PlaceInfo;
import com.github.andrdev.bolservice.model.responses.Places;
import com.github.andrdev.bolservice.model.responses.PostMessageResponse;
import com.github.andrdev.bolservice.model.responses.Predictions;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


//todo siingleton
public class EbolNetworkWorker2 {

    private final static String TEST_URL = "http://dbl.perevertajlo.com/api";

    private final static String LIVE_URL = "http://boldirect.com/";
    private final static String GOOGLE_URL = "https://maps.googleapis.com/maps/api/";
    private static EbolNetworkWorker2 ebolNetworkWorker;

    private static volatile EbolRetroService2 ebolRetroService;
    private static volatile GoogleApiRetroService googleApiRetroService;
    private static LoginRequest loginInfo;
    private EbolNetworkWorker2() {
        createRestWorker();
    }

    public static EbolNetworkWorker2 getInstance() {
        if (ebolNetworkWorker == null) {
            synchronized (EbolNetworkWorker2.class) {
                if (ebolNetworkWorker == null) {
                    ebolNetworkWorker = new EbolNetworkWorker2();
                }
            }
        }
        return ebolNetworkWorker;
    }

    public static boolean isLoggedIn() {
        return loginInfo != null;
    }

    private void createRestWorker() {
//        RestAdapter retrofit = new RestAdapter.Builder()
//                .setEndpoint(LIVE_URL)
//                .setClient(new OkClient(new OkHttpClient()))
//                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(new OkHttpClient()))
//                .setLog(new AndroidLog("DREE"))
//                .build();
//
//        ebolRetroService = retrofit.create(EbolRetroService.class);
    }

    public static void setAuthenticatedRestWorker(LoginRequest loginRequest) {
        loginInfo = loginRequest;
        final String credentials = loginInfo.getLogin() + ":" + loginInfo.getPassword();

        String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient();
        client.interceptors().clear();
        client.interceptors().add(logging);
        client.interceptors().add(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Authorization", basic)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")//yyyy-MM-dd'T'HH:mm:ss
                .registerTypeAdapterFactory(new DamagesResponseTypedFactory(DamagesResponse.class))
                .registerTypeAdapterFactory(new ProfileResponseTypedFactory(ProfileResponse.class))
                .registerTypeAdapterFactory(new EbolResponseTypedFactory(EbolsResponse.class))
                .registerTypeAdapterFactory(new VehicleInfosResponseTypedFactory(VehicleInfoResponse.class))
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                                return false;
                            }
                })
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LIVE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        ebolRetroService = retrofit.create(EbolRetroService2.class);

        OkHttpClient client2 = new OkHttpClient();
        client2.interceptors().add(logging);

        Retrofit retrofitg = new Retrofit.Builder()
                .baseUrl(GOOGLE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client2)
                .build();
        googleApiRetroService = retrofitg.create(GoogleApiRetroService.class);
    }

    public void locationNew(String longitude, String latitude) {
        Map<String, String> map = new HashMap<>();
        map.put("longitude", longitude);
        map.put("latitude", latitude);
        ebolRetroService.locationNew(map);
    }

    public void decodeVin(String vinNumber,
                          SuccessCallback<VinDecodeResponse> successCb,
                          FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getVinNumberParams(vinNumber);
        ebolRetroService.decodeVin(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void newInvoice(InvoiceRequest invoice,
                           SuccessCallback<NewInvoiceResponse> successCb,
                           FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getNewInvoiceParams(invoice);
        ebolRetroService.newInvoice(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getInvoices(String status,
                            SuccessCallback<InvoicesResponse> successCb,
                            FailedCallback failedCb) {
        ebolRetroService.getInvoices(status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }


    public void getNotifications(SuccessCallback<NotificationsResponse> successCb,
                                 FailedCallback failedCb) {
        ebolRetroService.getNotifications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void markAsReadNotifications(List<Long> notificationIds,
                                        SuccessCallback<NotificationsMarkResponse> successCb,
                                        FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getMarkAsReadNotificationsParams(notificationIds);
        ebolRetroService.markAsReadNotifications(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getProfile(SuccessCallback<ProfileResponse> successCb,
                           FailedCallback failedCb) {
        ebolRetroService.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        successCb::getDataRequestSuccess,
                        failedCb::getDataRequestFailed);
    }

    public void saveProfile(SaveProfileRequest profile,
                            SuccessCallback<SaveProfileResponse> successCb,
                            FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getSaveProfileParams(profile);
        ebolRetroService.saveProfile(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void markAsReadMessages(String cid, String mid) {
        Map<String, String> map = UtilsNetRequest.getMarkAsReadMessagesParams(cid, mid);
        ebolRetroService.markAsReadMessages(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public void getClients(String search,
                           SuccessCallback<ClientsResponse> successCb,
                           FailedCallback failedCb) {
        ebolRetroService.getClients(search)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void createClient(ClientCreateRequest client,
                             SuccessCallback<SimpleResponse> successCb,
                             FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getCreateClientParams(client);
        ebolRetroService.createClient(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void updateClient(ClientUpdateRequest client,
                             SuccessCallback<SimpleResponse> successCb,
                             FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getUpdateClientParams(client);
        ebolRetroService.updateClient(map)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed).unsubscribe();
    }

    public void deleteClient(String cid, ClientEditPresenter clientEditPresenter) {
        Map<String, String> map = UtilsNetRequest.getDeleteClientParams(cid);
        ebolRetroService.deleteClient(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(clientEditPresenter::deleteRequestSuccess,
                        clientEditPresenter::deleteRequestFailed);
    }

    public Observable<DriversResponse> getDrivers() {
        return ebolRetroService.getDrivers();
    }

    public void createDriver(CreateDriverRequest driver, SuccessCallback<CreateDriverResponse> successCb,
                             FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getCreateDriverParams(driver);
        ebolRetroService.createDriver(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void updateDriver(DriverUpdateRequest driver, SuccessCallback<SimpleResponse> successCb,
                             FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getUpdateDriverParams(driver);
        ebolRetroService.updateDriver(map)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void deleteDriver(String did, ColleagueEditPresenter callback) {
        Map<String, String> map = UtilsNetRequest.getDeleteDriverParams(did);
        ebolRetroService.deleteDriver(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::deleteRequestSuccess, callback::deleteRequestFailed);
    }

    public void getConversationsList(SuccessCallback<ConversationsResponse> successCb,
                                     FailedCallback failedCb) {
        ebolRetroService.getConversationsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getConversation(long cid,
                                SuccessCallback<ConversationResponse> successCb,
                                FailedCallback failedCb) {
        ebolRetroService.getConversation(cid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    //    public void postMessage(MessageRequest messageRequesteResponse> cb) {
    public void postMessage(MessageRequest request,
                            SuccessCallback<PostMessageResponse> successCb,
                            FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getPostMessageParams(request);
        ebolRetroService.postMessage(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getColleagues(SuccessCallback<ColleaguesResponse> successCb,
                              FailedCallback failedCb) {
        ebolRetroService.getColleagues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getHelpCenter(SuccessCallback<HelpCenterResponse> successCb,
                                   FailedCallback failedCb) {
        ebolRetroService.getHelpCenter()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getSubmits(String status, SuccessCallback<EbolsResponse> successCb,
                           FailedCallback failedCb) {
        Log.d("eboolsre", "pendingDelivery2");
        ebolRetroService.getSubmits(status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getVehicles(String sid, SuccessCallback<VehicleInfoResponse> successCb,
                            FailedCallback failedCb) {
        ebolRetroService.getVehicles(sid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public Observable<SimpleResponse> createVehicle(CarCreateRequest request)
//            , SuccessCallback<SimpleResponse> successCb, FailedCallback failedCb) {
    {
        Map<String, String> map = UtilsNetRequest.getCreateCarParams(request);
        return ebolRetroService.newVehicle(map);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public Observable<SimpleResponse> share(ShareRequest request){
//            , SuccessCallback<SimpleResponse> successCb,
//                              FailedCallback failedCb) {
        Map<String, String> map = UtilsNetRequest.getShareParams(request);
        return ebolRetroService.share(map);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public Observable<SimpleResponse> uploadPhoto(CarPhotoCreateRequest request){
//            , SuccessCallback<SimpleResponse> successCb,
//                              FailedCallback failedCb) {
        RequestBody zippedPackage = RequestBody.create(MediaType.parse("multipart/form-data"),
                new File(request.getFile()));
        return ebolRetroService.photoUpload(request.getCid(), request.getSide(), zippedPackage);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(successCb::getDataRequestSuccess, failedCb::getDataRequestFailed);
    }

    public void getDamages(String cid, SuccessCallback<DamagesResponse> successCb) {
        ebolRetroService.getDamages(cid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess);
    }

    public Observable<NewDamageResponse>  postDamages(NewDamageRequest request) {
        Map<String, String> map = UtilsNetRequest.getPostDamagesParams(request);
        return ebolRetroService.postDamage(map);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(successCb::getDataRequestSuccess);
    }


    public void login(LoginRequest loginInfo) {
        Map<String, String> map = new HashMap<>();
        map.put("username", loginInfo.getLogin());
        map.put("firstname", loginInfo.getPassword());
        ebolRetroService.login(map, new Object());
    }

    public void requestAddressByCordinates(String longLat, SuccessCallback<Places> successCb) {
        Map<String, String> map = new HashMap<>();
        map.put("latlng", longLat);
        map.put("location_type", "ROOFTOP");
        map.put("result_type", "street_address");
        map.put("key", "AIzaSyAozoN8hLVF5hzLHD6_XtZ_Gjful1N9oas");
        googleApiRetroService.requestAddressByCordinates(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, this::errorGapi);
    }

    public void requestAddressAutocomplete(String text, SuccessCallback<Predictions> successCb) {
        Map<String, String> map = new HashMap<>();
        map.put("input", text);
        map.put("types", "address");
        map.put("components", "country:us");
//        map.put("result_type", "street_address");
        map.put("key", "AIzaSyAozoN8hLVF5hzLHD6_XtZ_Gjful1N9oas");
        googleApiRetroService.requestAddressAutocomplete(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, this::errorGapi);
    }

    public void requestPlaceInfoByPlaceId(String text, SuccessCallback<PlaceInfo> successCb) {
        Map<String, String> map = new HashMap<>();
        map.put("placeid", text);
        map.put("key", "AIzaSyAozoN8hLVF5hzLHD6_XtZ_Gjful1N9oas");
        googleApiRetroService.requestPlaceInfoByPlaceId(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successCb::getDataRequestSuccess, this::errorGapi);
    }

    private void errorGapi(Throwable throwable) {
        Log.d("dreeeggg: ", "errorGapi: "+throwable.toString());
    }

    public static LoginRequest getLoginInfo() {
        return loginInfo;
    }

    public static void setLoginInfo(LoginRequest loginInfo) {
        EbolNetworkWorker2.loginInfo = loginInfo;
    }



    public interface SuccessCallback<T> {
        void getDataRequestSuccess(T data);
    }

    public interface FailedCallback {
        void getDataRequestFailed(Throwable throwable);
    }
}

