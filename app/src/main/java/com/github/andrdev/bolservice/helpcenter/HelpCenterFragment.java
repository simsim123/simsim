package com.github.andrdev.bolservice.helpcenter;

import android.content.Context;
import android.widget.Toast;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.HelpCenter;
import com.github.andrdev.bolservice.BaseSearchFragment;


public class HelpCenterFragment extends BaseSearchFragment<HelpCenter, HelpCenterAdapter,
        BaseSearchView, HelpCenterPresenter> {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_help_center;
    }

    @Override
    protected void filter(HelpCenter item, String temp) {
        if (item.getTitle().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        getPresenter().getData();
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        ((HelpCenterActivity) getActivity()).showHelpCenterItemDetailsFragment(mData.get(position));
    }

    @Override
    protected HelpCenterAdapter getAdapter() {
        return  new HelpCenterAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void openListItem() {
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public HelpCenterPresenter createPresenter() {
        return new HelpCenterPresenterImpl();
    }
}
