package com.github.andrdev.bolservice.view.customerNotEditable;

import android.content.Context;
import android.location.Address;

import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface CustomerNeView extends MvpView {

    void setCustomerFields(Customer customer);
}
