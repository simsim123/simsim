//package com.github.andrdev.bolservice.newEbol;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.app.AlertDialog;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.github.andrdev.bolservice.NucleusFragment;
//import com.github.andrdev.bolservice.R;
//import com.github.andrdev.bolservice.Utils;
//import com.github.andrdev.bolservice.db.Repo;
//import com.github.andrdev.bolservice.model.Client;
//import com.github.andrdev.bolservice.model.Customer;
//import com.github.andrdev.bolservice.model.Damages;
//import com.github.andrdev.bolservice.model.NewEbol;
//import com.github.andrdev.bolservice.model.VehicleInfo;
//import com.github.andrdev.bolservice.model.responses.Places;
//import com.github.andrdev.bolservice.model.responses.Predictions;
//import com.github.andrdev.bolservice.newEbol.damages.NewEbolCarDamagesActivity;
//import com.github.andrdev.bolservice.newEbol.vehicleInfo.NewEbolVehicleInformationActivity;
//import com.github.andrdev.bolservice.presenters.NewEbolPresenter;
//import com.github.andrdev.bolservice.ui.SignActivity;
//import com.github.andrdev.bolservice.ui.fragments.DatePickerFragment;
//import com.github.andrdev.bolservice.ui.fragments.PlacesAutocompleteDialogFragment;
//import com.github.andrdev.bolservice.ui.fragments.SelectClientDialogFrament;
//
//import java.io.File;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import nucleus.factory.RequiresPresenter;
//
//
//@RequiresPresenter(NewEbolPresenterImpl.class)
//public class NewEbolFragment extends NucleusFragment<NewEbolPresenterImpl> {
//
//    public static final String SELECTED_VEHICLE_ID = "selectedCarId";
//    public static final String TEMP = "temp";
//    public static final String TYPE = "type";
//
//    @Bind(R.id.originOrderId)
//    EditText originOrderId;
//
////    @Bind(R.id.originDay)
//    TextView originDay;
//
////    @Bind(R.id.originMonth)
//    TextView originMonth;
//
////    @Bind(R.id.originYear)
//    TextView originYear;
//
//    @Bind(R.id.originCustomerName)
//    EditText originCustomerName;
//
//    @Bind(R.id.originAddress)
//    EditText originAddress;
//
//    @Bind(R.id.originCity)
//    EditText originCity;
//
//    @Bind(R.id.originZip)
//    EditText originZip;
//
//    @Bind(R.id.originState)
//    EditText originState;
//
//    @Bind(R.id.originPhone)
//    EditText originPhone;
//
//    @Bind(R.id.originCell)
//    EditText originCell;
//
//    @Bind(R.id.destinationCustomerName)
//    EditText destinationCustomerName;
//
//    @Bind(R.id.destinationAddress)
//    EditText destinationAddress;
//
//    @Bind(R.id.destinationCity)
//    EditText destinationCity;
//
//    @Bind(R.id.destinationZip)
//    EditText destinationZip;
//
//    @Bind(R.id.destinationState)
//    EditText destinationState;
//
//    @Bind(R.id.destinationPhone)
//    EditText destinationPhone;
//
//    @Bind(R.id.destinationCell)
//    EditText destinationCell;
//
//    @Bind(R.id.next)
//    Button next;
//
//    @Bind(R.id.vehiclesHost)
//    LinearLayout vehicleHost;
//
//    @Bind(R.id.customerSignCheck)
//    View customerSignCheck;
//
//    @Bind(R.id.driverSignCheck)
//    View driverSignCheck;
//
//    @Bind(R.id.unableToSignCheck)
//    View unableToSignCheck;
//
//    Repo repo;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_ebol_new, container, false);
//        ButterKnife.bind(this, view);
//
//        repo = new Repo(getContext());
//        return view;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        getPresenter().getData();
//    }
//
//    public void setData(NewEbol ebol){
//        vehicleHost.removeAllViews();
//        setSignImages(ebol);
//        drawVehicles(ebol);
//    }
//
//    private void drawVehicles(NewEbol ebol) {
//        for (final VehicleInfo vehInfo : ebol.getVehicleInfos()) {
//            View damagedCar = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_damaged_car, null);
//
//            setVehicleClickers(vehInfo, damagedCar);
//            setVehicleText(vehInfo, damagedCar);
//
//            vehicleHost.addView(damagedCar);
//
//            LinearLayout.LayoutParams params =(LinearLayout.LayoutParams) damagedCar.getLayoutParams();
//            params.bottomMargin = 8;
//        }
//    }
//
//    private void setVehicleText(VehicleInfo vehInfo, View damagedCar) {
//        TextView carInfo = (TextView) damagedCar.findViewById(R.id.carInfo);
//        TextView vin = (TextView) damagedCar.findViewById(R.id.vin);
//        TextView damagesCount = (TextView) damagedCar.findViewById(R.id.damagesCount);
//
//        carInfo.setText(String.format(
//                "%s %s %s", vehInfo.getYear(), vehInfo.getMake(), vehInfo.getModel()));
//
//        vin.setText(vehInfo.getVin());
//        Damages vehDamages =  vehInfo.getDamages();
//        int markedDamagesCount = 0;
//        if(vehDamages != null) {
//            markedDamagesCount = getMarkedDamagesCount(vehDamages, markedDamagesCount);
//        }
//
//        damagesCount.setText(getResources().getQuantityString(
//                R.plurals.damagesMarked, markedDamagesCount, markedDamagesCount));
//    }
//
//    private void setVehicleClickers(final VehicleInfo vehInfo, View damagedCar) {
//        damagedCar.findViewById(R.id.damages)
//                .setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        damagesClick(vehInfo.getId());
//                    }
//                });
//        damagedCar.findViewById(R.id.details)
//                .setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        detailsClick(vehInfo.getId());
//                    }
//                });
//    }
//
//    private int getMarkedDamagesCount(Damages vehDamages, int markedDamagesCount) {
//        if(vehDamages.getFront()!=null) {
//            markedDamagesCount += vehDamages.getFront().getMarkedDamages().size();
//        }
//        if(vehDamages.getBack()!=null) {
//            markedDamagesCount += vehDamages.getBack().getMarkedDamages().size();
//        }
//        if(vehDamages.getRightSide()!=null) {
//            markedDamagesCount += vehDamages.getRightSide().getMarkedDamages().size();
//        }
//        if(vehDamages.getLeftSide()!=null) {
//            markedDamagesCount += vehDamages.getLeftSide().getMarkedDamages().size();
//        }
//        if(vehDamages.getAdditionalFirst()!=null) {
//            markedDamagesCount += vehDamages.getAdditionalFirst().getMarkedDamages().size();
//        }
//        if(vehDamages.getAdditionalSecond()!=null) {
//            markedDamagesCount += vehDamages.getAdditionalSecond().getMarkedDamages().size();
//        }
//        return markedDamagesCount;
//    }
//
//    private void setSignImages(NewEbol savedEbol) {
//        if (savedEbol.getDriverSignPath() != null) {
//            driverSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            driverSignCheck.setVisibility(View.INVISIBLE);
//        }
//        if (savedEbol.getCustomerSignPath() != null) {
//            customerSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            customerSignCheck.setVisibility(View.INVISIBLE);
//        }
//        if (savedEbol.isUnableToSign()) {
//            unableToSignCheck.setVisibility(View.VISIBLE);
//        } else {
//            unableToSignCheck.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    private void saveEnteredEbol() {
//        String date = String.format("%s/%s/%s", originMonth.getText().toString(),
//                originDay.getText().toString(), originYear.getText().toString());
//        String orderId = originOrderId.getText().toString();
//
//        NewEbol savedEbol = getPresenter().getEbol();
//
//        Customer origin = getOriginCustomer(savedEbol);
//        Customer destination = getDestinationCustomer(savedEbol);
//
//        savedEbol.setOrderId(orderId);
//        savedEbol.setDate(date);
//        savedEbol.setOrigin(origin);
//        savedEbol.setDestination(destination);
//
//        getPresenter().updateData(savedEbol);
//    }
//
//    private void restoreEnteredEbol(NewEbol ebol){
//        setOriginCustomerFields(ebol);
//        setDestinationCustomerFields(ebol);
//    }
//
//    private void getCordinates() {
//        getPresenter().getCordinates();
//    }
//
//    private void selectAddressFromGooglePlacesDestination() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        PlacesAutocompleteDialogFragment newFragment = PlacesAutocompleteDialogFragment.newInstance();
//        newFragment.setCallback(
//                new PlacesAutocompleteDialogFragment.SelectedCallback() {
//                    @Override
//                    public void itemSelected(Predictions.Prediction prediction) {
//                        setAddressFromPrediction(prediction);
//                    }
//                }
//        );
//        newFragment.show(ft, "dialog");
//    }
//
//    private void setAddressFromPrediction(Predictions.Prediction prediction) {
//        switch (prediction.getTerms().length) {
//            case 4:
//                destinationAddress.setText(prediction.getTerms()[0].value);
//            case 3:
//                destinationCity.setText(prediction.getTerms()[1].value);
//            case 2:
//                destinationState.setText(prediction.getTerms()[2].value);
//        }
//    }
//
//    private void setAddressFromPredictionOrigin(Predictions.Prediction prediction) {
//        switch (prediction.getTerms().length) {
//            case 4:
//                originAddress.setText(prediction.getTerms()[0].value);
//            case 3:
//                originCity.setText(prediction.getTerms()[1].value);
//            case 2:
//                originState.setText(prediction.getTerms()[2].value);
//        }
//    }
//
//    public void setLocationData(Places places) {
//        String tempStreet = null;
//        String tempNumber = null;
//        for(Places.Result.AddressComponent component:places.getResults()[0].getAddress_components()) {
//            switch (component.getTypes()[0]) {
//                case "street_number":
//                    if (tempStreet == null) {
//                        tempNumber = component.getLong_name();
//                    } else {
//                        originAddress.setText(String.format("%s %s", tempStreet, component.getLong_name()));
//                    }
//                    break;
//                case "route":
//                    if (tempNumber == null) {
//                        tempStreet = component.getLong_name();
//                    } else {
//                        originAddress.setText(String.format("%s %s", component.getLong_name(), tempNumber));
//                    }
//                    break;
//                case "sublocality_level_1":
//                case "locality":
//                case "sublocality":
//                    originCity.setText(component.getLong_name());
//                    break;
//                case "administrative_area_level_1":
//                    originState.setText(component.getLong_name());
//                    break;
//                case "postal_code":
//                    originZip.setText(component.getLong_name());
//                    break;
//            }
//        }
//    }
//
//    public void buildAlertMessageNoGps() {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                    }
//                })
//                .setNeutralButton("Use Places", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        selectAddressFromGooglePlacesOrigin();
//                    }})
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        dialog.cancel();
//                    }
//                });
//        final AlertDialog alert = builder.create();
//        alert.show();
//    }
//
//    private void selectAddressFromGooglePlacesOrigin() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        PlacesAutocompleteDialogFragment newFragment = PlacesAutocompleteDialogFragment.newInstance();
//        newFragment.setCallback(
//                new PlacesAutocompleteDialogFragment.SelectedCallback() {
//                    @Override
//                    public void itemSelected(Predictions.Prediction prediction) {
//                        setAddressFromPredictionOrigin(prediction);
//                    }
//                }
//        );
//        newFragment.show(ft, "dialog");
//    }
//
//    void detailsClick(int carId){
//        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
//        intent.putExtra(SELECTED_VEHICLE_ID, carId);
//        saveEnteredEbol();
//        startActivity(intent);
//    }
//
//    void damagesClick(int carId){
//        Intent intent = new Intent(getActivity(), NewEbolCarDamagesActivity.class);
//        intent.putExtra(SELECTED_VEHICLE_ID, carId);
//        saveEnteredEbol();
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.addCars)
//    void plusClick() {
//        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
//        saveEnteredEbol();
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.customerSignature)
//    void customerSignatureClick() {
//        if (Utils.hasText(originOrderId)) {
//            saveEnteredEbol();
//            Intent intent = new Intent(getActivity(), SignActivity.class);
//            intent.putExtra("signatureType", "customerSign");
//            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
//            startActivity(intent);
//        }
//    }
//
//    @OnClick(R.id.driverSignature)
//    void driverSignatureClick() {
//        if (Utils.hasText(originOrderId)) {
//            saveEnteredEbol();
//            Intent intent = new Intent(getActivity(), SignActivity.class);
//            intent.putExtra("signatureType", "driverSign");
//            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
//            startActivity(intent);
//        }
//    }
//
//    @OnClick(R.id.unableToSign)
//    void unableToSignClick() {
//        saveEnteredEbol();
//        Repo repo = new Repo(getContext());
//        NewEbol ebol = repo.repoNewEbol.getTempNewEbol();
//        ebol.setUnableToSign(true);
//        new File(ebol.getDriverSignPath()).delete();
//        new File(ebol.getCustomerSignPath()).delete();
//        ebol.setDriverSignPath(null);
//        ebol.setCustomerSignPath(null);
//        repo.repoNewEbol.updateTempNewEbol(ebol);
//        unableToSignCheck.setVisibility(View.VISIBLE);
//        setSignImages(ebol);
//    }
//
//    @OnClick(R.id.next)
//    void nextClick() {
//        saveEnteredEbol();
//        getPresenter().finishEbolCreation();
//        getActivity().onBackPressed();
//    }
//
//    @OnClick(R.id.originLocationButton)
//    void originLocationButtonClick() {
//        getCordinates();
//    }
//
//    @OnClick(R.id.destinationLocationButton)
//    void destinationLocationButtonClick() {
//        selectAddressFromGooglePlacesDestination();
//    }
//
//    @OnClick(R.id.originCustomerNameButton)
//    void originCustomerNameButtonClick() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
//        newFragment.setCallback(new SelectClientDialogFrament.SelectedCallback() {
//            @Override
//            public void clientSelected(Client client) {
//                originCustomerName.setText(client.getCustomerName());
//                originAddress.setText(client.getAddress());
//                originCity.setText(client.getCity());
//                originState.setText(client.getState());
//                originZip.setText(client.getZIP());
//                originPhone.setText(client.getPhone());
//            }
//        });
//        newFragment.show(ft, "dialog");
//    }
//
//
//    @OnClick(R.id.destinationCustomerNameButton)
//    void destinationCustomerNameButtonClick() {
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectClientDialogFrament newFragment = SelectClientDialogFrament.newInstance();
//        newFragment.setCallback(new SelectClientDialogFrament.SelectedCallback() {
//            @Override
//            public void clientSelected(Client client) {
//                destinationCustomerName.setText(client.getCustomerName());
//                destinationAddress.setText(client.getAddress());
//                destinationCity.setText(client.getCity());
//                destinationState.setText(client.getState());
//                destinationZip.setText(client.getZIP());
//                destinationPhone.setText(client.getPhone());
//            }
//        });
//        newFragment.show(ft, "dialog");
//    }
//
////    @OnClick({R.id.originDay, R.id.originMonth, R.id.originYear})
//    void dateSetClick(View view) {
//        DatePickerFragment newFragment = new DatePickerFragment();
//        newFragment.setCallback(new DatePickerFragment.OnDateSetCallback() {
//            @Override
//            public void dateSet(int year, int month, int day) {
//                originYear.setText(String.valueOf(year));
//                originMonth.setText(String.valueOf(month+1));
//                originDay.setText(String.valueOf(day));
//            }
//        });
//        newFragment.show(getFragmentManager(), "datePicker");
//    }
//
//    private boolean isValidFields() {
//        return Utils.hasText("", originOrderId, originCustomerName, originAddress,
//                originCity, originState, originZip, originPhone, originCell, destinationCustomerName,
//                destinationAddress, destinationCity, destinationState, destinationZip, destinationPhone,
//                destinationCell)
//                && Utils.isValidPhone(originPhone)
//                && Utils.isValidPhone(originCell)
//                && Utils.isValidPhone(destinationPhone)
//                && Utils.isValidPhone(destinationCell);
//    }
//
//    private Customer getOriginCustomer(NewEbol savedEbol) {
//        Customer origin = savedEbol.getOrigin();
//        if(origin == null) {
//            origin = new Customer();
//            repo.repoCustomer.createCustomer(origin);
//        }
//        origin.setCustomerName(Utils.getTextFromEditText(originCustomerName));
//        origin.setAddress(Utils.getTextFromEditText(originAddress));
//        origin.setCity(Utils.getTextFromEditText(originCity));
//        origin.setState(Utils.getTextFromEditText(originState));
//        origin.setZip(Utils.getTextFromEditText(originZip));
//        origin.setPhone(Utils.getTextFromEditText(originPhone));
//        origin.setCell(Utils.getTextFromEditText(originCell));
//        repo.repoCustomer.updateCustomer(origin);
//        return origin;
//    }
//
//    private void setOriginCustomerFields(NewEbol ebol) {
//        Customer origin = ebol.getOrigin();
//        originCustomerName.setText(origin.getCustomerName());
//        originAddress.setText(origin.getAddress());
//        originCity.setText(origin.getCity());
//        originState.setText(origin.getState());
//        originZip.setText(origin.getZip());
//        originPhone.setText(origin.getPhone());
//        originCell.setText(origin.getCell());
//    }
//
//    public Customer getDestinationCustomer(NewEbol savedEbol) {
//        Customer destination = savedEbol.getDestination();
//        if(destination == null) {
//            destination = new Customer();
//            repo.repoCustomer.createCustomer(destination);
//        }
//        destination.setCustomerName(Utils.getTextFromEditText(destinationCustomerName));
//        destination.setAddress(Utils.getTextFromEditText(destinationAddress));
//        destination.setCity(Utils.getTextFromEditText(destinationCity));
//        destination.setState(Utils.getTextFromEditText(destinationState));
//        destination.setZip(Utils.getTextFromEditText(destinationZip));
//        destination.setPhone(Utils.getTextFromEditText(destinationPhone));
//        destination.setCell(Utils.getTextFromEditText(destinationCell));
//        repo.repoCustomer.updateCustomer(destination);
//        return destination;
//    }
//
//    private void setDestinationCustomerFields(NewEbol ebol) {
//        Customer destination = ebol.getDestination();
//        originCustomerName.setText(destination.getCustomerName());
//        originAddress.setText(destination.getAddress());
//        originCity.setText(destination.getCity());
//        originState.setText(destination.getState());
//        originZip.setText(destination.getZip());
//        originPhone.setText(destination.getPhone());
//        originCell.setText(destination.getCell());
//    }
//}
