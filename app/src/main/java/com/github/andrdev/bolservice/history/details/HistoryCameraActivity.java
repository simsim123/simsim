package com.github.andrdev.bolservice.history.details;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.history.details.damages.HistoryDamagesFragment;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;


public class HistoryCameraActivity extends AppCompatActivity {

    String photoType;
    int selectedCarId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        photoType = getIntent().getStringExtra(HistoryDamagesFragment.PHOTO_TYPE);
        selectedCarId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
        if (savedInstanceState == null) {
            showMarkDamagesFragment(photoType);
        }
    }

    public void showMarkDamagesFragment(String photoType) {
        HistoryMarkDamagesFragment fragment =  new HistoryMarkDamagesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HistoryDamagesFragment.PHOTO_TYPE, photoType);
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedCarId);
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
    }

//    public void showVehicleInspectionsFragment() {
//        NewEbolVehicleInspectionFragment fragment =  new NewEbolVehicleInspectionFragment();
//        Bundle bundle = new Bundle();
//        bundle.putInt(NewEbolFragment.SELECTED_CAR_ID, selectedCarId);
//        fragment.setArguments(bundle);
//        getSupportFragmentManager().beginTransaction()
//                .replace(android.R.id.content, fragment).commit();
//
//    }
}
