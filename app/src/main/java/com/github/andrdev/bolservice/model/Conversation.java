package com.github.andrdev.bolservice.model;


public class Conversation {
    long conversationId;
    long interlocutorId;
    String interlocutorFullname;
    Message lastMessage;
}
