package com.github.andrdev.bolservice.model.newEbol;


import com.github.andrdev.bolservice.model.realms.MarkedDamageRealm;

import java.util.List;

public class VehiclePhoto {

    private int id;
    private String photoType;
    private String photoPath;
    private boolean isTemp;
    private String note;
    private List<MarkedDamage> markedDamages;
    private List<MarkedDamage> markedDamagesDelivery;

    public VehiclePhoto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTemp() {
        return isTemp;
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp;
    }

    public List<MarkedDamage> getMarkedDamages() {
        return markedDamages;
    }

    public void setMarkedDamages(List<MarkedDamage> markedDamages) {
        this.markedDamages = markedDamages;
    }

    public List<MarkedDamage> getMarkedDamagesDelivery() {
        return markedDamagesDelivery;
    }

    public void setMarkedDamagesDelivery(List<MarkedDamage> markedDamagesDelivery) {
        this.markedDamagesDelivery = markedDamagesDelivery;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public boolean isLocal() {
        return !photoPath.contains("http");
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }
}
