package com.github.andrdev.bolservice.helpcenter;

import android.os.Bundle;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.HelpCenter;
import com.github.andrdev.bolservice.helpcenter.HelpCenterItemDetailsFragment;
import com.github.andrdev.bolservice.helpcenter.HelpCenterFragment;
import com.github.andrdev.bolservice.BaseDrawerActivity;


public class HelpCenterActivity extends BaseDrawerActivity {

    private static final String TAG_HELP_CENTER = "helpCenter";
    private static final String TAG_HELP_CENTER_ITEM = "helpCenterItem";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showHelpFragment();
        }
    }

    private void showHelpFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new HelpCenterFragment(), TAG_HELP_CENTER).commit();
    }

    public void showHelpCenterItemDetailsFragment(HelpCenter item) {
        HelpCenterItemDetailsFragment fragment = new HelpCenterItemDetailsFragment();
        fragment.setHelpCenter(item);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, TAG_HELP_CENTER_ITEM)
                .addToBackStack(TAG_HELP_CENTER_ITEM).commit();
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 4;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_help_center;
    }
}
