package com.github.andrdev.bolservice.currentEbol;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

public class CurrentFEbolDataProvider implements DataProvider {

    DbHelper dbHelper;

    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void saveOrUpdateEbol(NewEbol newEbol, DataCb<Boolean> callback) {
        dbHelper.updateOrSaveEbol(newEbol, callback::returnData);
    }

    public void removeTempVehicle(DataCb<Boolean> callback) {
        dbHelper.clearTempVehicleInfo(callback::returnData);
    }

    public void getEbol(DataCb<NewEbol> callback) {
        dbHelper.getTempEbol(callback::returnData);
    }

}
