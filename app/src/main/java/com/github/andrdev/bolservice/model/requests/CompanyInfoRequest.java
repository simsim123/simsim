package com.github.andrdev.bolservice.model.requests;


public class CompanyInfoRequest {

    String companyName;
    String address;
    String city;
    String state;
    String zip;
    String phone;
    String fax;
    String email;
    String website;
    String dotNumber;
    String mcNumber;

    public CompanyInfoRequest(String address, String city, String companyName, String dotNumber,
                              String email, String fax, String mcNumber, String phone, String state,
                              String website, String zip) {
        this.address = address;
        this.city = city;
        this.companyName = companyName;
        this.dotNumber = dotNumber;
        this.email = email;
        this.fax = fax;
        this.mcNumber = mcNumber;
        this.phone = phone;
        this.state = state;
        this.website = website;
        this.zip = zip;
    }
}
