package com.github.andrdev.bolservice.login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.dashboard.DashboardActivity;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginFragment extends MvpFragment<LoginView, LoginPresenter> implements LoginView {

    public static final String REGISTER_URL = "http://dbl.perevertajlo.com/register/";

    public static final String RESTORE_PASSWORD_URL = "http://boldirect.com/resetting/request";

    @BindString(R.string.loginFailed)
    String loginFailed;

    @BindString(R.string.pleaseEnterText)
    String errorText;

    @Bind(R.id.progress)
    View progress;

    @Bind(R.id.login)
    EditText login;

    @Bind(R.id.password)
    EditText password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.signIn)
    public void signInClick(View view) {
        if (Utils.hasText(errorText, login, password)) {
            LoginRequest loginRequest = new LoginRequest(Utils.getTextFromEditText(login),
                    Utils.getTextFromEditText(password));
            getPresenter().sendLoginRequest(loginRequest);
        }
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void openDashBoardActivity(){
        Intent intent = new Intent(getActivity(), DashboardActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R.id.signUp)
    public void signUpClick() {
        String url = REGISTER_URL;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    @OnClick(R.id.forgotPassword)
    public void restoreClick() {
        String url = RESTORE_PASSWORD_URL;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void showLoginErrorToast() {
        Toast.makeText(getContext(), loginFailed, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void saveLoginInfo(LoginRequest loginInfo) {
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(BaseDrawerActivity.PREF_LOGIN_KEY, loginInfo.getLogin())
                .putString(BaseDrawerActivity.PREF_PASSWORD_KEY, loginInfo.getPassword()).commit();
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenterImpl();
    }
}
