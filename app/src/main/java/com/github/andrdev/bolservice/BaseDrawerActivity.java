package com.github.andrdev.bolservice;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.github.andrdev.bolservice.login.LoginActivity;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.github.andrdev.bolservice.view.drawer.DrawerWidget;
import com.github.andrdev.bolservice.view.SupSlide;

import butterknife.Bind;
import butterknife.ButterKnife;


public abstract class BaseDrawerActivity extends AppCompatActivity implements DrawerWidget.DrawerCallback{

    public static final String PREF_LOGIN_KEY = "login";
    public static final String PREF_PASSWORD_KEY = "password";
//
//    static List<DrawerLine> drawerLines = new ArrayList<>();
//    static {
//        drawerLines.add(new DrawerLine(R.drawable.car, "Dashboard"));
//        drawerLines.add(new DrawerLine(R.drawable.notebook, "Clients"));
//        drawerLines.add(new DrawerLine(R.drawable.bell, "Notifications"));
//        drawerLines.add(new DrawerLine(R.drawable.friends, "Colleague"));
//        drawerLines.add(new DrawerLine(R.drawable.help, "Help Center"));
//        drawerLines.add(new DrawerLine(R.drawable.logout, "Sign Out"));
//    }
//
//    @Bind(R.id.name)
//    TextView mUserName;
//
//    @Bind(R.id.menuList)
//    RecyclerView mMenuList;

    @Bind(R.id.slidingPanel)
    protected SupSlide mSlidingPanel;

    @Bind(R.id.burger)
    View mBurger;

    @Bind(R.id.drawerView)
    DrawerWidget drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        if(!EbolNetworkWorker2.isLoggedIn()){
            if(PreferenceManager.getDefaultSharedPreferences(this).contains(PREF_LOGIN_KEY)){
                getLoginInfoFromPreferences();
            } else {
                openLoginActivity();
                return;
            }
        }
        ButterKnife.bind(this);
        initToolbar();
        initDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDrawer();
    }

    private void getSavedLoginInfoOrOpenLoginActivity() {
        if(PreferenceManager.getDefaultSharedPreferences(this).contains(PREF_LOGIN_KEY)){
            getLoginInfoFromPreferences();
        } else {
            openLoginActivity();
        }
    }

    private void getLoginInfoFromPreferences() {
        String login = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(PREF_LOGIN_KEY, "");
        String password = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(PREF_PASSWORD_KEY, "");
        if(!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)){
            EbolNetworkWorker2.setAuthenticatedRestWorker(new LoginRequest(login, password));
        }
    }

    public void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(mSlidingPanel.isOpen()){
            mSlidingPanel.closePane();
        } else {
            super.onBackPressed();
        }
    }

    void initDrawer() {
        mSlidingPanel.setParallaxDistance(200);
        drawer.setDrawerCallback(this);
        drawer.setCurrentItemPosition(getCurrentActivityPosition());
//        DrawerAdapter adapter = new DrawerAdapter();
//        adapter.setData(drawerLines);
//        mMenuList.setLayoutManager(new LinearLayoutManager(this));
//        mMenuList.setAdapter(adapter);
//        mMenuList.getAdapter().notifyDataSetChanged();
//        mMenuList.addOnItemTouchListener(new RecyclerItemClickListener(this,
//                (view, position) -> onDrawerRowClick(position)));
//
//        mSlidingPanel.setParallaxDistance(200);
//
//        Realm realm = Realm.getInstance(this);
//        Profile profile = realm.where(Profile.class).findFirst();
//        mUserName.setText(profile.getFullNameFormatted());
//        realm.close();
    }
//
//    private void onDrawerRowClick(int position) {
//        if(getCurrentActivityPosition() == position){
//            closePaneIfOpened();
//            return;
//        }
//        Intent intent = new Intent();
//        switch (position){
//            case 0:
//                intent = new Intent(this, DashboardActivity.class);
//                break;
//            case 1:
//                intent = new Intent(this, ClientsActivity.class);
//                break;
//            case 2:
//                intent = new Intent(this, NotificationsActivity.class);
//                break;
//            case 3:
//                intent = new Intent(this, ColleaguesActivity.class);
//                break;
//            case 4:
//                intent = new Intent(this, HelpCenterActivity.class);
//                break;
//            case 5:
//                intent = new Intent(this, LoginActivity.class);
//                break;
//            default:
//                break;
//        }
//        closePaneIfOpened();
//        startActivity(intent);
//        finish();
//    }

    @Override
    public void removeCurrentItem() {
        finish();
    }

    @Override
    public void removeDrawerFromScreen() {
        closePaneIfOpened();
    }

    private boolean closePaneIfOpened() {
        if(mSlidingPanel.isOpen()){
            mSlidingPanel.closePane();
            return true;
        }
        return false;
    }

    private void initToolbar() {
        mBurger.setOnClickListener((v) -> {
            if (!closePaneIfOpened()) {
                mSlidingPanel.openPane();
            }
        });
    }
//
//    @OnClick({R.id.avatar, R.id.myAccount, R.id.name})
//    void userClick() {
//        if(getCurrentActivityPosition() == 9) {
//            closePaneIfOpened();
//        }
//        Intent intent = new Intent(this, ProfileActivity.class);
//        startActivity(intent);
//        finish();
//    }



    protected abstract int getCurrentActivityPosition();

    protected abstract int getLayout();

}
