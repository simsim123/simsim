package com.github.andrdev.bolservice.newEbol;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.andrdev.bolservice.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolActivity extends AppCompatActivity {

    public static final String EBOL_ID = "ebolId";
    NewFFebolFragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ebol);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showNewEbolFragment();
        } else {
            fragment = (NewFFebolFragment)getSupportFragmentManager().findFragmentById(R.id.fragment);
        }
    }
    void showNewEbolFragment(){
        fragment = new NewFFebolFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, fragment).commit();
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        fragment.saveToDbEbol();
        onBackPressed();
    }

}
