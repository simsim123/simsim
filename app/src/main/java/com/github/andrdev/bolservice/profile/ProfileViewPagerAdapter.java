package com.github.andrdev.bolservice.profile;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.github.andrdev.bolservice.profile.baseInfo.ProfileBaseInfoFragment;
import com.github.andrdev.bolservice.profile.companyInfo.ProfileCompanyInfoFragment;
import com.github.andrdev.bolservice.profile.driverInfo.ProfileDriverInfoFragment;


public class ProfileViewPagerAdapter extends FragmentStatePagerAdapter {

    final static String titles[] = {"Driver Info","Company Info"};

    final Fragment fragments[] = {new ProfileDriverInfoFragment(),
            new ProfileCompanyInfoFragment()};

    public ProfileViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
