package com.github.andrdev.bolservice.view.drawer;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface DrawerView extends MvpView{
    void setUserName(String userName);

    Context getViewContext();
}
