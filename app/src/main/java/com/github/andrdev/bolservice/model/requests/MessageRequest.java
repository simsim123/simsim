package com.github.andrdev.bolservice.model.requests;


public class MessageRequest {

    String message;
    String uid;

    public MessageRequest() {
    }

    public MessageRequest(String message, String uid) {
        this.message = message;
        this.uid = uid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
