package com.github.andrdev.bolservice.view.customer;

import android.location.Address;
import android.location.Location;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

public abstract class CustomerNewEbolPresenter extends MvpBasePresenter<CustomerNewEbolView> {

    abstract void getAddress();

    abstract void permissionsResult(Boolean granted);

    abstract void isGpsTurnedOn();

    abstract void isGpsTurnedOn(Boolean isGpsTurnedOn);

    abstract void getAddressFromGps();

    abstract void getAddressFromLocation(Location location);

    abstract void gpsAddressSuccessOb(List<Address> addresses);
}
