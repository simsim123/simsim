package com.github.andrdev.bolservice.currentEbol;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CurrentEbolActivity  extends AppCompatActivity {

    public static final String EBOL_ID = "ebolId";
    CurrentFEbolFragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ebol);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            showCurrentFEbolFragment();
        } else {
            fragment = (CurrentFEbolFragment)getSupportFragmentManager().findFragmentById(R.id.fragment);
        }
    }
    void showCurrentFEbolFragment(){
        fragment = new CurrentFEbolFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, fragment).commit();
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        fragment.saveToDbEbol();
        onBackPressed();
    }

}
