package com.github.andrdev.bolservice.messages;

import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Message;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;


public class ChatAdapter extends BaseAdapter<Message, ChatAdapter.MessageHolder> {

    int ownerId;
    String otherUserNick;

    @Override
    protected int getLayoutResource(int viewType) {
        if(viewType == 1) {
            return R.layout.list_item_dialog_user;
        } else {
            return R.layout.list_item_dialog_coll;
        }
    }

    @Override
    protected MessageHolder getHolder(View view) {
        return new MessageHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return ownerId == data.get(position).getAuthor()? 1:2;
    }

    public void setOtherUserNick(String otherUserNick) {
        this.otherUserNick = otherUserNick;
    }

    public void setOwnerId(int id) {
        ownerId = id;
    }

    @Override
    public void setData(List<Message> messages) {
        Collections.sort(messages, new MessageComparator());
        this.data = messages;
    }

    protected class MessageHolder extends BaseAdapter.BaseViewHolder<Message> {

        @Bind(R.id.dialogText)
        TextView dialogText;
        @Bind(R.id.nick)
        TextView nick;

        public MessageHolder(View itemView) {
            super(itemView);
        }

        protected void bind(Message message) {
            String authorNick = message.getAuthor() == ownerId ? "Me" : otherUserNick;
            nick.setText(authorNick);
            dialogText.setText(message.getMessageText());
        }
    }

    public class MessageComparator implements Comparator<Message> {
        @Override
        public int compare(Message m1, Message m2) {
            return m1.getCreatedAt().compareTo(m2.getCreatedAt());
        }
    }
}

