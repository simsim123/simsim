package com.github.andrdev.bolservice.view.customer;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.GpsAddressProvider;

import java.util.List;

public class CustomerDataProvider implements DataProvider {

    GpsAddressProvider mapsGpsHandler;

    public GpsAddressProvider getMapsGpsHandler() {
        return mapsGpsHandler;
    }

    public void setMapsGpsHandler(GpsAddressProvider mapsGpsHandler) {
        this.mapsGpsHandler = mapsGpsHandler;
    }

    public void checkForGpsPermission(DataCb<Boolean> callback) {
        mapsGpsHandler.hasGpsPermissions(callback::returnData);
    }

    public void checkGpsIsTurnedOn(DataCb<Boolean> callback) {
        mapsGpsHandler.checkGpsTurnedOn(callback::returnData);
    }

    public void getGpsCoordinates(DataCb<Location> callback) {
        mapsGpsHandler.getCurrentGpsLocationOv(callback::returnData);
    }

    public void getAddressFromCoordinates(Location location, DataCb<List<Address>> callback) {
        mapsGpsHandler.getAddressGJs(location, callback::returnData);
    }

    public void deInit() {
        mapsGpsHandler.deInit();
    }
}
