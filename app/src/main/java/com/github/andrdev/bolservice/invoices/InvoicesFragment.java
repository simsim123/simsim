package com.github.andrdev.bolservice.invoices;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Invoice;

import butterknife.Bind;
import butterknife.OnClick;


public class InvoicesFragment
        extends BaseSearchFragment<Invoice, InvoicesAdapter, BaseSearchView, InvoicesPresenter> {

    @Bind(R.id.pendingInvoices)
    TextView pendingInvoices;

    @Bind(R.id.paidInvoices)
    TextView paidInvoices;

    @Bind(R.id.newInvoices)
    TextView newInvoices;

    int currentInv = 1;
    static final int newInv = 1;
    static final int pendingInv = 2;
    static final int paidInv = 3;

    @NonNull
    @Override
    public InvoicesPresenter createPresenter() {
        return new InvoicesPresenterImpl();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_invoices;
    }


    private void getInvoices(String status) {
        getPresenter().getData(status);
    }

    @Override
    protected void filter(Invoice item, String temp) {
        if (item.getFullname().toLowerCase().contains(temp)
                || (item.getCell()!=null&&item.getCell().toLowerCase().contains(temp))
                || item.getOrderId().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        newInvoicesClick();
    }

    protected void sendDataRequest() {
        switch (currentInv){
            case 1:
                newInvoicesClick();
                break;
            case 2:
                pendingInvoicesClick();
                break;
            case 3:
                paidInvoicesClick();
                break;
            default:
                stopRefresh();
                break;
        }
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        getPresenter().listItemClicked(mData.get(position));
    }

    @Override
    protected InvoicesAdapter getAdapter() {
        return new InvoicesAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {

    }

    @Override
    public void openListItem() {
        ((InvoicesActivity) getActivity()).showInvoiceFragment();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @OnClick(R.id.newInvoices)
    void newInvoicesClick() {
        currentInv = 1;
        pendingInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_middle));
        pendingInvoices.setTextColor(Color.WHITE);

        paidInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_right_but));
        paidInvoices.setTextColor(Color.WHITE);

        newInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_left_sel_but));
        newInvoices.setTextColor(getResources().getColor(R.color.blue));
        getInvoices("new");
    }

    @OnClick(R.id.pendingInvoices)
    void pendingInvoicesClick() {
        currentInv = 2;
        newInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_left_but));
        newInvoices.setTextColor(Color.WHITE);

        paidInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_right_but));
        paidInvoices.setTextColor(Color.WHITE);

        pendingInvoices.setBackgroundColor(Color.WHITE);
        pendingInvoices.setTextColor(getResources().getColor(R.color.blue));
        getInvoices("pending");
    }


    @OnClick(R.id.paidInvoices)
    void paidInvoicesClick() {
        currentInv = 3;
        newInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_left_but));
        newInvoices.setTextColor(Color.WHITE);

        pendingInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_middle));
        pendingInvoices.setTextColor(Color.WHITE);

        paidInvoices.setBackgroundDrawable(getResources().getDrawable(R.drawable.top_right_sel_but));
        paidInvoices.setTextColor(getResources().getColor(R.color.blue));
        getInvoices("paid");
    }

    @OnClick(R.id.newInvoice)
    void newInvoiceClick() {
        ((InvoicesActivity) getActivity()).showNewInvoiceFragment();
    }

    @OnClick(R.id.backIm)
    void onBackClick(View v) {
        getActivity().onBackPressed();
    }
}
