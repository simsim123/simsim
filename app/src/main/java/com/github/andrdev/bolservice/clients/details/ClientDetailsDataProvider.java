package com.github.andrdev.bolservice.clients.details;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Client;

public class ClientDetailsDataProvider implements DataProvider{

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }


    public void getSavedClient(DataCb<Client> successCb) {
        dbHelper.getSavedClient(successCb::returnData);
    }

    public void getIsUserBoss(DataCb<Boolean> successCb) {
        dbHelper.getIsUserBoss(successCb::returnData);
    }
}
