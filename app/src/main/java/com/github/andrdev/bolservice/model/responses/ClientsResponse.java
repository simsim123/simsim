package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Client;

import java.util.List;


public class ClientsResponse {

    List<Client> clients;

    public ClientsResponse() {
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
}
