package com.github.andrdev.bolservice.view.vehicleToolsNe;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.andrdev.bolservice.MvpPercentRelativeLayout;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.view.ToolsSwitch;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VehicleToolsNeWidget extends MvpPercentRelativeLayout<VehicleToolsNeView, VehicleToolsNePresenter>
        implements VehicleToolsNeView {

    @Bind(R.id.keysCount)
    TextView keysCount;
    @Bind(R.id.headRestCount)
    TextView headRestCount;
    @Bind(R.id.remotesCount)
    TextView remotesCount;
    @Bind(R.id.floorMatsCount)
    TextView floorMatsCount;

    @Bind(R.id.radioSw)
    ToolsSwitch radio;
    @Bind(R.id.spareTires)
    ToolsSwitch spareTires;
    @Bind(R.id.manuals)
    ToolsSwitch manuals;
    @Bind(R.id.cargoCovers)
    ToolsSwitch cargoCovers;

    public VehicleToolsNeWidget(Context context) {
        super(context);
        init(context);
    }

    public VehicleToolsNeWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VehicleToolsNeWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_car_tools_ne, this);
        ButterKnife.bind(this, view);
        disableSwitches();
    }


    @Override
    public VehicleToolsNePresenter createPresenter() {
        return new VehicleToolsNePresenter();
    }


    public void setFieldsFromDb(Damages damages) {
        remotesCount.setText(String.valueOf(damages.getRemotes()));
        floorMatsCount.setText(String.valueOf(damages.getFloorMats()));
        headRestCount.setText(String.valueOf(damages.getHeadrest()));
        keysCount.setText(String.valueOf(damages.getKeys()));

        radio.setState(damages.isRadio());
        cargoCovers.setState(damages.isCargoCovers());
        spareTires.setState(damages.isSpareTires());
        manuals.setState(damages.isManuals());
    }


    private void disableSwitches() {
        cargoCovers.setEnabled(false);
        spareTires.setEnabled(false);
        radio.setEnabled(false);
        manuals.setEnabled(false);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
