package com.github.andrdev.bolservice.model.newEbol;


import io.realm.annotations.PrimaryKey;

public class DamageType {

    private int id;
    private String damageShortName;
    private String damageName;
    private boolean state;

    public DamageType() {
    }

    public DamageType(String damageName, String damageShortName, boolean state) {
        this.damageName = damageName;
        this.damageShortName = damageShortName;
        this.state = state;
    }

    public String getDamageName() {
        return damageName;
    }

    public void setDamageName(String damageName) {
        this.damageName = damageName;
    }

    public String getDamageShortName() {
        return damageShortName;
    }

    public void setDamageShortName(String damageShortName) {
        this.damageShortName = damageShortName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean isState() {
        return state;
    }
}
