package com.github.andrdev.bolservice.sign;

import android.content.Context;
import android.graphics.Bitmap;

import com.github.andrdev.bolservice.view.DrawingView;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface SignView extends MvpView{

    Bitmap getSignOnBitmap();

    void backToEbol();

    Context getViewContext();

    String getSignatureType();

    String getEbolId();
}
