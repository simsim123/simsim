package com.github.andrdev.bolservice.model.newEbol;


import java.util.List;

public class MarkedDamage {

    private int id;
    private double yPosition;
    private double xPosition;
    private List<DamageType> damageTypes;
    private boolean isTemp;

    public MarkedDamage() {
    }

    public MarkedDamage(double xPosition, double yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTemp() {
        return isTemp;
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp;
    }

    public double getxPosition() {
        return xPosition;
    }

    public void setxPosition(double xPosition) {
        this.xPosition = xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public void setyPosition(double yPosition) {
        this.yPosition = yPosition;
    }

    public List<DamageType> getDamageTypes() {
        return damageTypes;
    }

    public void setDamageTypes(List<DamageType> damageTypes) {
        this.damageTypes = damageTypes;
    }
}
