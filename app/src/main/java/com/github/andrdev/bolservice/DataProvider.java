package com.github.andrdev.bolservice;

public interface DataProvider {

    void deInit();

    interface DataCb<T>{
        void returnData(T data);
    }

    interface FailedCb<T> {
        void failed(T failed);
    }

    interface NoDataCb {
        void returnFunction();
    }
}
