package com.github.andrdev.bolservice.clients.create;

import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ClientNewPresenter extends MvpBasePresenter<ClientNewView> {

    abstract void createClient(ClientCreateRequest request);

    public abstract void requestFailed(Throwable throwable);

    public abstract void requestSuccess(SimpleResponse simpleResponse);
}
