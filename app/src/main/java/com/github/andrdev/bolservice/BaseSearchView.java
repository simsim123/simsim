package com.github.andrdev.bolservice;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;


public interface BaseSearchView<T> extends MvpView {

    void addData(List<T> data);

    void setData(List<T> data);

    void stopRefresh();

    void showNoDataText();

    void showRequestFailedToast();

    void openListItem();

    Context getViewContext();
}
