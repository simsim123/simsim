package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.DriverFull;

import java.util.List;


public class DriversResponse {

    String status;
    List<DriverFull> drivers;

    public DriversResponse() {
    }

    public List<DriverFull> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverFull> drivers) {
        this.drivers = drivers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
