package com.github.andrdev.bolservice;


import android.Manifest;
import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.github.andrdev.bolservice.model.responses.Places;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.google.android.gms.location.LocationRequest;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class GpsAddressProvider {

    WeakReference<Context> context;
//    GpsHandlerCallback callback;
    Subscription subscription;

    public GpsAddressProvider(Context context) {
        this.context = new WeakReference<>(context);
//        this.callback = callback;
    }

    public void hasGpsPermissions(Callback<Boolean> callback) {
        if(!hasContext()){
            return;
        }
        subscription = RxPermissions.getInstance(context.get())
                .request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::actionDone);
    }

    public void checkGpsTurnedOn(Callback<Boolean> callback) {
        if(!hasContext()){
            return;
        }
        final LocationManager mLocationManager = (LocationManager) context.get()
                .getSystemService(Context.LOCATION_SERVICE);
        subscription = Observable.just(mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::actionDone);
    }

    public void getCurrentGpsLocationOv(Callback<Location> callback) {
        LocationRequest request = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setNumUpdates(1).setInterval(100);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context.get());

        locationProvider.getUpdatedLocation(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        callback::actionDone);
//                .subscribe(new Subscriber<Location>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.d("dreeloc", "onNext: ");
//
//                    }
//
//                    @Override
//                    public void onError(Throwable throwable) {
//                        Log.d("dreeloc", "onNext: " + throwable.getMessage());
//
//                    }
//
//                    @Override
//                    public void onNext(Location location) {
//                        Log.d("dreeloc", "onNext: " + location.toString());
////                        callback::actionDone;
//                    }
//                });
    }

    public void getLocationSUp(Callback<Location> callback) {

        final android.location.LocationManager mLocationManager =
                (LocationManager) context.get().getSystemService(Context.LOCATION_SERVICE);

        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                try {
                    mLocationManager.removeUpdates(this);
                } catch (SecurityException e) {

                }
                callback.actionDone(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };
            //noinspection ResourceType
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000,
                    1, mLocationListener);
    }

    public void getAddressObsVO(Location location, Callback<List<Address>> callback) {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context.get());
        subscription = locationProvider.getReverseGeocodeObservable(location.getLatitude(), location.getLongitude(), 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::actionDone);
    }

    public void getAddressGJs(Location location, Callback<List<Address>> callback) {
        String lat = String.valueOf(location.getLatitude()).replace(',', '.');
        String lon = String.valueOf(location.getLongitude()).replace(',', '.');
        EbolNetworkWorker2.getInstance().requestAddressByCordinates((
                        String.format("%s,%s", lat, lon)),
                (data) -> convertToadress(data, callback));
    }

    private void convertToadress(Places data, Callback<List<Address>> callback) {
        String tempStreet = null;
        String tempNumber = null;
        List<Address> addresses = new ArrayList<>();
        Address adr = new Address(new Locale("en"));
        for(Places.Result.AddressComponent component:data.getResults()[0].getAddress_components()) {
            switch (component.getTypes()[0]) {
                case "street_number":
                        tempNumber = component.getLong_name();
                    break;
                case "route":
                        tempStreet = component.getLong_name();
                    break;
                case "sublocality_level_1":
                case "locality":
                    adr.setAdminArea(component.getLong_name());
                case "sublocality":
                    adr.setSubLocality(component.getLong_name());
                    break;
                case "administrative_area_level_1":
                    adr.setLocality(component.getLong_name());
                    break;
                case "postal_code":
                    adr.setPostalCode(component.getLong_name());
                    break;
            }
        }
        if (tempNumber == null) {
            tempNumber = "";
        }
        if (tempStreet == null) {
            tempStreet = "";
        }
        adr.setThoroughfare(String.format("%s %s", tempNumber, tempStreet));
        addresses.add(adr);
        callback.actionDone(addresses);
    }

    private boolean hasContext() {
        return context.get() != null;
    }


    public void deInit() {
        if(subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }

    public interface Callback<T> {
        void actionDone(T data);
    }

    public interface GpsHandlerCallback {
        void noGpsPermission();
        void addressReceived(Address address);
        void gpsTurnedOff();
    }
}
