package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public class NewEbolEnterVinNumberPresenter extends MvpBasePresenter<NewEbolEnterVinNumberView> {

    NewEbolEnterVinDataProvider dataProvider;


    @Override
    public void attachView(NewEbolEnterVinNumberView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new NewEbolEnterVinDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    public void deInit() {
        dataProvider.deInit();
    }

    public void getData(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getVehicleById(vehicleId, this::vehicleGetSuccess);
    }

    private void vehicleGetSuccess(VehicleInfo info) {
        if (!isViewAttached()) {
            return;
        }
        getView().setData(info);
    }

    public void decodeVin(String vinNumber) {
        dataProvider.decodeVin(vinNumber, this::decodeSuccess, this::decodeFailed);
    }

    public void decodeFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showFailedToast();
    }

    public void decodeSuccess(VinDecodeResponse vinDecodeResponse) {
        if (!isViewAttached()) {
            return;
        }
        if(!vinDecodeResponse.getStatus().equals("true")) {
            getView().showFailedToast();
            return;
        }

        VehicleInfo tempInfo = getView().getVehicleInfo();
        if (tempInfo == null) {
            tempInfo = new VehicleInfo();
            tempInfo.setType("temp");
        }
        tempInfo.setModel(vinDecodeResponse.getModel());
        tempInfo.setVinNumber(vinDecodeResponse.getVin());
        tempInfo.setMake(vinDecodeResponse.getMake());
        tempInfo.setYearOfMake(vinDecodeResponse.getYear());

        dataProvider.saveTempVehicleOrUpdate(tempInfo, this::backToVehicleInfo);
    }

    public void backToVehicleInfo(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().backToVehicleInfo();
    }
}
