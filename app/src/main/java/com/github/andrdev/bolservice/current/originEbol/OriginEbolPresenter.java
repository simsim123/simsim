package com.github.andrdev.bolservice.current.originEbol;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

public abstract class OriginEbolPresenter extends MvpBasePresenter<OriginFEbolView> {
    public abstract void getData();

    public abstract void detailsClick(int vehicleId);

    public abstract void damagesClick(int vehicleId);

    public abstract void customerSignatureClick();

    public abstract void driverSignatureClick();

    public abstract void unableToSignClick();
}
