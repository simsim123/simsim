package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class ChatPresenter extends MvpBasePresenter<ChatView> {
    abstract void getMessages(long collId);

    abstract void sendMessage(String text, String userId);

    abstract void getProfileData();

    abstract public void getOwnerId(Profile profile);
}
