package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;


public class BillingInfoRequestBuilder {
    private String address;
    private String city;
    private String email;
    private String fax;
    private String firstName;
    private String lastName;
    private String phone;
    private String state;
    private String zip;

    public BillingInfoRequestBuilder setAddress(EditText address) {
        if(Utils.hasText(address)){
            this.address = Utils.getTextFromEditText(address);
        }
        return this;
    }

    public BillingInfoRequestBuilder setCity(EditText city) {
        if(Utils.hasText(city)){
            this.city = Utils.getTextFromEditText(city);
        }
        return this;
    }

    public BillingInfoRequestBuilder setEmail(EditText email) {
        if(Utils.hasText(email)){
            this.email = Utils.getTextFromEditText(email);
        }
        return this;
    }

    public BillingInfoRequestBuilder setFax(EditText fax) {
        if(Utils.hasText(fax)){
            this.fax = Utils.getTextFromEditText(fax);
        }
        return this;
    }

    public BillingInfoRequestBuilder setFirstName(EditText firstName) {
        if(Utils.hasText(firstName)){
            this.firstName = Utils.getTextFromEditText(firstName);
        }
        return this;
    }

    public BillingInfoRequestBuilder setLastName(EditText lastName) {
        if(Utils.hasText(lastName)){
            this.lastName = Utils.getTextFromEditText(lastName);
        }
        return this;
    }

    public BillingInfoRequestBuilder setPhone(EditText phone) {
        if(Utils.hasText(phone)){
            this.phone = Utils.getTextFromEditText(phone);
        }
        return this;
    }

    public BillingInfoRequestBuilder setState(EditText state) {
        if(Utils.hasText(state)){
            this.state = Utils.getTextFromEditText(state);
        }
        return this;
    }


    public BillingInfoRequestBuilder setZip(EditText zip) {
        if(Utils.hasText(zip)){
            this.zip = Utils.getTextFromEditText(zip);
        }
        return this;
    }

    public BillingInfoRequest createBillingInfo() {
        return new BillingInfoRequest(address, city, email, fax, firstName, lastName, phone, state, zip);
    }
}