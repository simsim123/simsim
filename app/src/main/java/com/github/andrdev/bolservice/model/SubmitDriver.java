package com.github.andrdev.bolservice.model;


public class SubmitDriver {

    String id;
    String username;
    String fullNameFormatted;
    String firstName;

    String lastName;

    String billingFirstName;

    String billingLastName;

    String billingAddress;

    String billingCity;

    String billingState;

    String billingZip;

    String billingPhone;

    String billingFax;

    String billingEmail;

    String companyName;

    String companyAddress;

    String companyCity;

    String companyState;

    String companyZip;

    String companyPhone;

    String companyFax;

    String companyEma;

    String companyWebsite;

    String companyUsdot;

    String companyMc;

    String driverEmail;

    String driverPhone;

    String usertype;
}
