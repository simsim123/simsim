package com.github.andrdev.bolservice.current.pendingDelivery;


import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.newEbol.trs.NewEbolDataProvider;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class PendingDeliveryPresenter extends MvpBasePresenter<PendingDeliveryView> {

    PendingDeliveryDataProvider dataProvider;
    Set<Integer> vehicleIds;

    @Override
    public void attachView(PendingDeliveryView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        vehicleIds = new HashSet<>();
        dataProvider = new PendingDeliveryDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    private void stopRefresh(Throwable t) {
        getView().stopRefresh();
        getView().showRequestFailedToast();
    }

    private void setData(EbolsResponse ebolsResponse) {
        getView().addData(ebolsResponse.getEbols());
    }

    public void netItemClicked(NewEbol ebol) {
        dataProvider.deleteTempEbol(deleted -> getVehicles(ebol));
    }

    private void getVehicles(NewEbol ebol) {
        dataProvider.getVehicles(String.valueOf(ebol.getSubmitId()),
                response -> getVehicleInfosSuccess(response, ebol));
    }

    private void getVehicleInfosSuccess(VehicleInfoResponse vehicleInfoResponse, NewEbol ebol) {
        ebol.setType("temp");
        ebol.setVehicleInfos(vehicleInfoResponse.getVehicleInfos());
        for (VehicleInfo info : ebol.getVehicleInfos()) {
            vehicleIds.add(info.getCid());
            dataProvider.getVehicleDamages(info.getCid(), damagesResponse ->
                    setDamagesToVehicle(ebol, info, damagesResponse));
        }
    }

    private void setDamagesToVehicle(NewEbol ebol, VehicleInfo info, DamagesResponse damagesResponse) {
        vehicleIds.remove(info.getCid());
        Map<String, VehiclePhoto> photos = damagesResponse.getVehiclePhotos();
        if(photos.containsKey("front")){
            info.getDamages().setFront(photos.get("front"));
        }
        if(photos.containsKey("back")){
            info.getDamages().setBack(photos.get("back"));
        }
        if(photos.containsKey("rightside")){
            info.getDamages().setRightSide(photos.get("rightside"));
        }
        if(photos.containsKey("leftside")){
            info.getDamages().setLeftSide(photos.get("leftside"));
        }
        if(vehicleIds.size() == 0){
            dataProvider.saveEbol(ebol, this::openEbol);
        }
    }

    private void openEbol(boolean saved) {
        getView().openListItem();
    }

    public void getEbols() {
        Log.d("eboolsre", "gg");
        dataProvider.getEbols(this::setData, this::stopRefresh);
    }

    public void newClick() {
        dataProvider.deleteTempEbol(this::openNewEbol);
    }

    private void openNewEbol(boolean deleted) {
        getView().openNewEbol();
    }
}
