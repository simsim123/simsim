package com.github.andrdev.bolservice.colleagues.details;

import android.content.Context;

import com.github.andrdev.bolservice.model.Colleague;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ColleagueDetailsView extends MvpView {
    void initFields();

    void backToColleagues();

    void setData(Colleague colleague);

    void showEditButton();

    Context getViewContext();
}
