package com.github.andrdev.bolservice.newEbol.damages;

import android.view.View;
import android.widget.TextView;


import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.DamageType;
import com.github.andrdev.bolservice.model.realms.DamageTypeRealm;

import butterknife.Bind;

public class DamagesAdapter extends BaseAdapter<DamageType, DamagesAdapter.DamagesHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_damages;
    }

    @Override
    protected DamagesHolder getHolder(View view) {
        return new DamagesHolder(view);
    }

    public void itemClicked(int position) {
        boolean state = data.get(position).getState();
        data.get(position).setState(!state);
        notifyDataSetChanged();
    }

    class DamagesHolder extends BaseAdapter.BaseViewHolder<DamageType> {

        @Bind(R.id.damageType)
        TextView title;
        @Bind(R.id.selectedImage)
        View selectedImage;
        @Bind(R.id.notSelectedImage)
        View notSelectedImage;

        public DamagesHolder(View itemView) {
            super(itemView);
        }

        protected void bind(DamageType damageType) {
            title.setText(damageType.getDamageName());
            if(damageType.getState()) {
                selectedImage.setVisibility(View.VISIBLE);
                notSelectedImage.setVisibility(View.INVISIBLE);
            } else {
                selectedImage.setVisibility(View.INVISIBLE);
                notSelectedImage.setVisibility(View.VISIBLE);
            }
        }
    }
}
