package com.github.andrdev.bolservice.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.ConversationSmall;
import com.github.andrdev.bolservice.model.OrderInfo;
import com.github.andrdev.bolservice.model.Submit;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class HistoryAdapter extends BaseAdapter<NewEbol, HistoryAdapter.HistoryHolder> {


    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_history;
    }

    @Override
    protected HistoryHolder getHolder(View view) {
        return new HistoryHolder(view);
    }

    protected static class HistoryHolder extends BaseAdapter.BaseViewHolder<NewEbol> {

        @Bind(R.id.vehicleId)
        TextView vehicleId;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.vehiclesCount)
        TextView vehiclesCount;
        @Bind(R.id.customerFirst)
        TextView customerFirst;
        @Bind(R.id.customerSecond)
        TextView customerSecond;

        public HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        protected void bind(NewEbol newEbol) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy, HH:mm a");
                Date d = sdf.parse(newEbol.getDate());
                String formattedTime = output.format(d);
                date.setText(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Customer origin = newEbol.getOrigin();
            Customer destination = newEbol.getDestination();
            vehicleId.setText(newEbol.getOrderId());
            String count = vehiclesCount.getContext()
                    .getResources().getQuantityString(R.plurals.vehicles,
                            newEbol.getVehiclesCount(), newEbol.getVehiclesCount());
            vehiclesCount.setText(count);
            customerFirst.setText(String.format("%s, %s, %s, %s, %s", origin.getCustomerName(),
                    origin.getCity(),
                    origin.getState(),
                    origin.getZip(),
                    origin.getPhone()));
            customerSecond.setText(String.format("%s, %s, %s, %s, %s", destination.getCustomerName(),
                    destination.getCity(),
                    destination.getState(),
                    destination.getZip(),
                    destination.getPhone()));
        }
    }
}
