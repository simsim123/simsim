package com.github.andrdev.bolservice.newEbol.vehicleInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.newEbol.vehicleInfo.vin.NewEbolEnterVinNumberFragment;
import com.github.andrdev.bolservice.newEbol.vehicleInfo.vin.NewEbolScanVinFragment;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolVehicleInformationActivity extends AppCompatActivity {

    private static final String TAG_ENTER_VIN = "enterVin";
    private static final String TAG_VEHICLE_INFO = "vehicleInfo";
    public static final String RESPONSE = "response";
    int selectedVehicleId;
    String vin;

    @BindString(R.string.vehicleInfo)
    String vehicleInformation;
    @BindString(R.string.enterVin)
    String enterVin;
    @Bind(R.id.titleTool)
    TextView titleTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_info);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            NewEbolVehicleInfoFragment fragment = new NewEbolVehicleInfoFragment();
            titleTool.setText(vehicleInformation);
            if(getIntent().hasExtra(NewFFebolFragment.SELECTED_VEHICLE_ID)) {

                selectedVehicleId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
                Bundle bundle = new Bundle();
                bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
                fragment.setArguments(bundle);

            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, fragment, TAG_VEHICLE_INFO).commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                showEnterVinNumberFragment();
            } else {
                vin = result.getContents();
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
//                decodeAndSetVin();
                showEnterVinNumberFragment();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if(requestCode == 1919){
            switch (resultCode) {
                case RESULT_OK:
                    vin = data.getStringExtra(NewEbolScanVinFragment.VIN);
                    showEnterVinNumberFragment();
                    return;
                case 319:
                    showEnterVinNumberFragment();
                    return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void decodeAndSetVin() {
        EbolNetworkWorker2.getInstance().decodeVin(vin,
                this::saveData,
                this::donothing);
    }

    private void saveData(VinDecodeResponse vinResp) {
        new DbHelperRxImpl(this).getVehicleById(selectedVehicleId, info->saveInfo(vinResp, info));
    }

    private void saveInfo(VinDecodeResponse vinResp, VehicleInfo vehicleInfo) {

        if (vehicleInfo == null) {
            vehicleInfo = new VehicleInfo();
            vehicleInfo.setType("temp");
        }
        vehicleInfo.setModel(vinResp.getModel());
        vehicleInfo.setVinNumber(vinResp.getVin());
        vehicleInfo.setMake(vinResp.getMake());
        vehicleInfo.setYearOfMake(vinResp.getYear());
        new DbHelperRxImpl(this).saveTempVehicleOrUpdate(vehicleInfo, saved->onBackPressed());
    }

    private void donothing(Throwable throwable) {
    }

    public void showVehicleInformationFragment(String response){
        NewEbolVehicleInfoFragment fragment = new NewEbolVehicleInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(RESPONSE, response);
        if(getIntent().hasExtra(NewFFebolFragment.SELECTED_VEHICLE_ID)) {
            selectedVehicleId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
            bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        }
        fragment.setArguments(bundle);
        titleTool.setText(vehicleInformation);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment, TAG_VEHICLE_INFO).commit();
    }

    public void showEnterVinNumberFragment() {
        NewEbolEnterVinNumberFragment fragment = new NewEbolEnterVinNumberFragment();
        Bundle bundle = new Bundle();
        if (vin != null) {
            bundle.putString(NewEbolScanVinFragment.VIN, vin);
        }
        if (getIntent().hasExtra(NewFFebolFragment.SELECTED_VEHICLE_ID)) {
            selectedVehicleId = getIntent().getIntExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, 0);
            bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        }
        fragment.setArguments(bundle);
        titleTool.setText(enterVin);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, fragment, TAG_ENTER_VIN)
                .addToBackStack(TAG_ENTER_VIN).commitAllowingStateLoss();
    }

    @OnClick(R.id.backIm)
    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_ENTER_VIN);
        if (fragment != null && fragment.isVisible()) {
            titleTool.setText(vehicleInformation);
        }
        super.onBackPressed();
    }
}
