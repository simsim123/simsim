package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Profile;

import java.util.List;


public class SaveProfileResponse {

    String status;
    Profile profile;

    public SaveProfileResponse() {
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
