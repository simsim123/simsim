package com.github.andrdev.bolservice.newEbol.vehicleInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.realms.VehicleInfoRealm;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface NewEbolVehicleInfoView extends MvpView {

    void setFields(VehicleInfo vehicleInfo);

    void openScanVin();

    void openEnterVin();

    void saveFields();

    VehicleInfo getVehicleInfo();

    Context getViewContext();

    void backToEbol();
}
