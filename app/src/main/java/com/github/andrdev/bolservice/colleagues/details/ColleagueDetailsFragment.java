package com.github.andrdev.bolservice.colleagues.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.colleagues.ColleaguesActivity;
import com.github.andrdev.bolservice.model.Colleague;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ColleagueDetailsFragment extends MvpFragment<ColleagueDetailsView, ColleagueDetailsPresenter>
        implements ColleagueDetailsView {

    @Bind(R.id.firstName)
    TextView firstName;

    @Bind(R.id.lastName)
    TextView lastName;

    @Bind(R.id.email)
    TextView email;

    @Bind(R.id.phoneNumber)
    TextView phoneNumber;

    @Bind(R.id.username)
    TextView username;

    @Bind(R.id.titleTool)
    TextView titleTool;

    @Bind(R.id.editColleague)
    TextView editColleague;

    Colleague colleague;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_colleague_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @NonNull
    @Override
    public ColleagueDetailsPresenter createPresenter() {
        return new ColleagueDetailsPresenterImpl();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void initFields() {
        firstName.setText(colleague.getFirstname());
        lastName.setText(colleague.getLastname());
        email.setText(colleague.getDriverEmail());
        phoneNumber.setText(colleague.getDriverPhone());
        username.setText(colleague.getUsername());
        titleTool.setText(colleague.getFullNameFormatted());
    }

    @Override
    public void backToColleagues() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.editColleague)
    void editClick(View view) {
        ((ColleaguesActivity)getActivity()).showColleagueEditFragment();
    }

    @OnClick(R.id.backIm)
    void backClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void setData(Colleague colleague) {
        this.colleague = colleague;
    }

    @Override
    public void showEditButton() {
        editColleague.setVisibility(View.VISIBLE);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
