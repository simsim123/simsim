package com.github.andrdev.bolservice.invoices.create;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;


public abstract class InvoiceNewPresenter extends MvpBasePresenter<InvoiceNewView> {
    abstract void getAddress();

    protected abstract void permissionsResult(Boolean granted);

    protected abstract void isGpsTurnedOn();

    protected abstract void gpsTurnedOn(Boolean gpsTurnedOn);

    protected abstract void getLocation();

    protected abstract void getAddressFromLocation(Location location);

    protected abstract void gpsAddressSuccessOb(List<Address> addresses);

    abstract void createInvoice(InvoiceRequest request);

    protected abstract void requestFailed(Throwable throwable);

    protected abstract void requestSuccess(NewInvoiceResponse simpleResponse);

    abstract void noGpsPermission();

    abstract void addressReceived(Address address);

    abstract void gpsTurnedOff();

}
