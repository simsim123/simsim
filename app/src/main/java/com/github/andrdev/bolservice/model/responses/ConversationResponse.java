package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.Message;

import java.util.List;


public class ConversationResponse {

    String status;
    List<Message> conversation;

    public ConversationResponse() {
    }

    public List<Message> getConversation() {
        return conversation;
    }

    public void setConversation(List<Message> conversation) {
        this.conversation = conversation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
