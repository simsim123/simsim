package com.github.andrdev.bolservice.view.markDamagesNe;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.damages.chooseDamageType.OriginCdtActivity;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.hannesdorfmann.mosby.mvp.layout.MvpRelativeLayout;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MarkDamagesNeWidget extends MvpRelativeLayout<MarkDamagesNeView, MarkDamagesNePresenter>
        implements MarkDamagesNeView, View.OnTouchListener{
    private static final String TAG = "dreeMd";

    Map<String, View> drawedDamages;
    VehiclePhoto photo;
    private double xPosition;
    private double yPosition;
    int damagePixelSize = getPixelSize(35);
    MarkDamagesCreateCallback markCreateCallback;
    MarkDamagesDeliveryCallback markDeliveryCallback;
    int width;
    int height;

    public MarkDamagesNeWidget(Context context) {
        super(context);
        init();
    }

    public MarkDamagesNeWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MarkDamagesNeWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public MarkDamagesNeWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    void init() {
        setOnTouchListener(this);
        drawedDamages = new HashMap<>();

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        height = size.y;
        width = size.x;
    }

    public void setMarkCreateCallback(MarkDamagesCreateCallback callback) {
        this.markCreateCallback = callback;
    }

    public void setMarkDeliveryCallback(MarkDamagesDeliveryCallback callback) {
        this.markDeliveryCallback = callback;
    }

    public void paintDamages() {
        for (final MarkedDamage markedDamage : photo.getMarkedDamages()) {
            int yPos = (int)(height * markedDamage.getyPosition()/100);
            int xPos = (int)(width * markedDamage.getxPosition()/100);
            RelativeLayout.LayoutParams params = getLayoutParams(yPos, xPos);
            TextView textView = getDamageView(markedDamage, params);
            addView(textView);
        }
        for (final MarkedDamage markedDamage : photo.getMarkedDamagesDelivery()) {
            int yPos = (int)(height * markedDamage.getyPosition()/100);
            int xPos = (int)(width * markedDamage.getxPosition()/100);
            RelativeLayout.LayoutParams params = getLayoutParams(yPos, xPos);
            TextView textView = getDeliveryDamageView(markedDamage, params);
            addView(textView);
        }
        invalidate();
    }

    @NonNull
    private RelativeLayout.LayoutParams getLayoutParams(int yPos, int xPos) {
        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(damagePixelSize, damagePixelSize);
        params.topMargin = yPos;
        params.leftMargin = xPos;
        return params;
    }

    private int getPixelSize(int i) {
        Resources r = getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, r.getDisplayMetrics());
    }

    @NonNull
    private TextView getDamageView(final MarkedDamage markedDamage, RelativeLayout.LayoutParams params) {
        String damageCode;
        if(markedDamage.getDamageTypes().size() == 1) {
            damageCode = new ArrayList<>(markedDamage.getDamageTypes()).get(0).getDamageShortName();
        } else {
            damageCode = String.valueOf(new ArrayList<>(markedDamage.getDamageTypes()).size());
        }
        TextView textView = new TextView(getContext());
        textView.setLayoutParams(params);
        textView.setText(damageCode);
        textView.setTextColor(Color.WHITE);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(16);
        textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.krasny));
        textView.setOnClickListener(v -> openDamageActivity(markedDamage));
        return textView;
    }

    @NonNull
    private TextView getDeliveryDamageView(final MarkedDamage markedDamage, LayoutParams params) {
        String damageCode;
        if(markedDamage.getDamageTypes().size() == 1) {
            damageCode = new ArrayList<>(markedDamage.getDamageTypes()).get(0).getDamageShortName();
        } else {
            damageCode = String.valueOf(new ArrayList<>(markedDamage.getDamageTypes()).size());
        }
        double yPos = markedDamage.getyPosition();
        double xPos = markedDamage.getxPosition();
        String key = String.format("%f%f", yPos, xPos);
        if (drawedDamages.containsKey(key)) {
            View view = drawedDamages.remove(key);
            removeView(view);
        }
        TextView textView = new TextView(getContext());
        textView.setLayoutParams(params);
        textView.setText(damageCode);
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(16);
//        textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_orange_d));
        textView.setOnClickListener(v -> openDamageActivityDelivery(markedDamage));
        drawedDamages.put(key, textView);
        return textView;
    }

    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if(markCreateCallback != null) {
                    openChooseDamaTypeActivity(event);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                break;
        }
        return true;
    }

    private void openDamageActivityDelivery(MarkedDamage markedDamage) {
        yPosition = markedDamage.getyPosition();
        xPosition = markedDamage.getxPosition();
        Log.d(TAG, "openDamageActivity: " + xPosition + " " + yPosition);
        Intent intent = new Intent(getContext(), OriginCdtActivity.class);
        intent.putExtra("yPosition", yPosition);
        intent.putExtra("xPosition", xPosition);
        intent.putExtra("action", "delivery");

        markDeliveryCallback.startDamageChooseActivity(intent);
    }

    private void openDamageActivity(MarkedDamage markedDamage) {
        yPosition = markedDamage.getyPosition();
        xPosition = markedDamage.getxPosition();
        Log.d(TAG, "openDamageActivity: "+xPosition+" "+yPosition);
        openChooseDamageActivity();
    }

    private void openChooseDamaTypeActivity(MotionEvent event) {
        yPosition = round((event.getY() - damagePixelSize/2) / (height / 100d), 2);
        xPosition = round((event.getX() - damagePixelSize/2) / (width/ 100d), 2);
//        xPosition = (int)event.getX();
//        yPosition = (int)event.getY();
        openChooseDamageActivity();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void openChooseDamageActivity() {
        Intent intent = new Intent(getContext(), OriginCdtActivity.class);
        intent.putExtra("yPosition", yPosition);
        intent.putExtra("xPosition", xPosition);

        if(markDeliveryCallback != null) {
            markDeliveryCallback.startDamageChooseActivity(intent);
        } else if (markCreateCallback != null) {
            markCreateCallback.startDamageChooseActivity(intent);
        }
    }

    public void deleteDamage() {
        String key = String.format("%f%f", yPosition, xPosition);
        View deletedView = drawedDamages.remove(key);
        if(deletedView != null) {
            deletedView.setVisibility(View.GONE);
            removeView(deletedView);
        }
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public MarkDamagesNePresenter createPresenter() {
        return new MarkDamagesNePresenter();
    }


    public VehiclePhoto getPhoto() {
        return photo;
    }

    public void setPhoto(VehiclePhoto photo) {
        this.photo = photo;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable state = super.onSaveInstanceState();

        SavedState ss = new SavedState(state);
        ss.yPos = yPosition;
        ss.xPos = xPosition;
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(state);
        yPosition = ss.yPos;
        xPosition = ss.xPos;
    }

    public static class SavedState extends BaseSavedState {
        double yPos;
        double xPos;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            yPos = in.readDouble();
            xPos = in.readDouble();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeDouble(yPos);
            out.writeDouble(xPos);
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    public interface MarkDamagesCreateCallback {
        void startDamageChooseActivity(Intent intent);
    }

    public interface MarkDamagesDeliveryCallback {
        void startDamageChooseActivity(Intent intent);
    }
}
