package com.github.andrdev.bolservice.clients;

import android.support.annotation.NonNull;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;


public class ClientsPresenterImpl extends ClientsPresenter {

    ClientsDataProvider dataProvider;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new ClientsDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    @NonNull
    protected DbHelperRxImpl getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getClients("", this::requestSuccess, this::requestFailed);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().showRequestFailedToast();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void requestSuccess(ClientsResponse clientsResponse) {
        if (!isViewAttached()) {
            return;
        }

        if (clientsResponse.getClients() != null && !clientsResponse.getClients().isEmpty()) {
            getView().setData(clientsResponse.getClients());
        } else {
            getView().showNoDataText();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void listItemClicked(Client client) {
        if (!isViewAttached()) {
            return;
        }
        //todo uncomment
        dataProvider.saveClient(client, this::clientSaved);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void clientSaved(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openListItem();
    }
}
