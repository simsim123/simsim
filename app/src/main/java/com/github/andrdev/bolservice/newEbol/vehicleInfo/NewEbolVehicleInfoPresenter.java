package com.github.andrdev.bolservice.newEbol.vehicleInfo;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public class NewEbolVehicleInfoPresenter extends MvpBasePresenter<NewEbolVehicleInfoView> {

    NewEbolVehicleInfoDataProvider dataProvider;

    @Override
    public void attachView(NewEbolVehicleInfoView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new NewEbolVehicleInfoDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    public void deInit() {
        dataProvider.deInit();
    }

    public void getTempVehicle() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getTempVehicle(this::vehicleGetSuccess);
    }

    public void vehicleGetSuccess(VehicleInfo vehicleInfo) {
        if (!isViewAttached()) {
            return;
        }
        if(vehicleInfo != null)
        Log.d("dreety", "saveTempVehicleOrUpdate: " + vehicleInfo.getType());
        getView().setFields(vehicleInfo);
    }


    public void getVehicleById(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getVehicleById(vehicleId, this::vehicleGetSuccess);
    }

    public void nextClick() {
        if (!isViewAttached()) {
            return;
        }
        getView().saveFields();
        VehicleInfo vehicleInfo = getView().getVehicleInfo();
        dataProvider.updateVehicle(vehicleInfo, this::backToEbol);
    }

    public void backToEbol(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().backToEbol();
    }

    public void scanClick() {
        if (!isViewAttached()) {
            return;
        }
        getView().saveFields();
        VehicleInfo vehicleInfo = getView().getVehicleInfo();
        dataProvider.saveTempVehicleOrUpdate(vehicleInfo, this::openScanVin);
    }

    public void openScanVin(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openScanVin();
    }

    public void enterClick() {
        if (!isViewAttached()) {
            return;
        }
        getView().saveFields();
        VehicleInfo vehicleInfo = getView().getVehicleInfo();
        dataProvider.saveTempVehicleOrUpdate(vehicleInfo, this::openEnterVin);
    }

    public void openEnterVin(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openEnterVin();
    }
}
