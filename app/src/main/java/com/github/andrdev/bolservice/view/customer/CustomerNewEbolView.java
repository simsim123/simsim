package com.github.andrdev.bolservice.view.customer;

import android.content.Context;
import android.location.Address;

import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface CustomerNewEbolView extends MvpView {

    Customer getCustomer(Customer customer);

    void setAddressFromPrediction(Address address);

    void buildAlertMessageNoGps();

    Context getViewContext();

    void selectAddressFromGooglePlaces();

    void setCustomerFields(Customer customer);

    void showPlacesOrGpsDialog();
}
