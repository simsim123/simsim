package com.github.andrdev.bolservice.database;

import android.content.Context;
import android.util.Log;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.realms.CustomerRealm;
import com.github.andrdev.bolservice.model.realms.DamageTypeRealm;
import com.github.andrdev.bolservice.model.realms.MarkedDamageRealm;
import com.github.andrdev.bolservice.model.realms.VehiclePhotoRealm;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.realms.DamagesRealm;
import com.github.andrdev.bolservice.model.realms.NewEbolRealm;
import com.github.andrdev.bolservice.model.Profile;
import com.github.andrdev.bolservice.model.realms.VehicleInfoRealm;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;


public class RealmDbData {

    private static final String TAG = "dreedb";
    Realm realm;

    public RealmDbData(Context context) {
        realm = Realm.getInstance(context);
    }

    public boolean saveProfile(Profile profile) {
        realm.beginTransaction();
        realm.where(Profile.class).findAll().clear();
        realm.copyToRealmOrUpdate(profile);
        realm.commitTransaction();
        return true;
    }

    public Profile getSavedProfile() {
        Profile profile = realm.where(Profile.class).findFirst();
//        RealmTransformator.profileRealmToPojo(profile);
        return profile;
    }

    public boolean getIsUserBoss() {
        return realm.where(Profile.class).findFirst().getIsBoss();
    }

    public boolean saveClient(Client client) {
        realm.beginTransaction();
//        RealmTransformator.profileToRealm(profile, realm);
        realm.where(Client.class).findAll().clear();
        realm.copyToRealm(client);
        realm.commitTransaction();
        return true;
    }

    public Client getSavedClient() {
        return realm.where(Client.class).findFirst();
    }

    public boolean saveColleague(Colleague colleague) {
        realm.beginTransaction();
//        RealmTransformator.profileToRealm(profile, realm);
        realm.where(Colleague.class).findAll().clear();
        realm.copyToRealm(colleague);
        realm.commitTransaction();
        return true;
    }

    public Colleague getSavedColleague() {
        return realm.where(Colleague.class).findFirst();
    }

    public boolean saveInvoice(Invoice invoice) {
        realm.beginTransaction();
//        RealmTransformator.profileToRealm(profile, realm);
        realm.where(Invoice.class).findAll().clear();
        realm.copyToRealm(invoice);
        realm.commitTransaction();
        return true;
    }

    public Invoice getSavedInvoice() {
        return realm.where(Invoice.class).findFirst();
    }

    public boolean updateOrSaveEbol(NewEbol ebol) {
        realm.beginTransaction();
        NewEbolRealm newEbolRealm = RealmTransformator.newEbolToRealm(ebol, realm);
        newEbolRealm = realm.copyToRealmOrUpdate(newEbolRealm);
        realm.commitTransaction();
//        NewEbol updatedNewEbol = RealmTransformator.newEbolRealmToPojo(newEbolRealm);
        return true;
    }

    public NewEbol getEbol() {
        NewEbolRealm newEbolRealm = realm.where(NewEbolRealm.class).findFirst();
        return RealmTransformator.newEbolRealmToPojo(newEbolRealm);
    }

    public NewEbol getOrCreateEbol() {
        NewEbolRealm newEbolRealm = realm.where(NewEbolRealm.class).equalTo(
                NewEbolRealm.TYPE, NewFFebolFragment.TEMP).findFirst();
        Log.v("dree", ""+realm.where(NewEbolRealm.class).findAll().size());
        if (newEbolRealm == null) {
            realm.beginTransaction();
            NewEbol newEbol = new NewEbol();
            newEbol.setType(NewFFebolFragment.TEMP);
            newEbolRealm = RealmTransformator.newEbolToRealm(newEbol, realm);
            realm.commitTransaction();
        }
        return RealmTransformator.newEbolRealmToPojo(newEbolRealm);
    }

    public NewEbol getTempEbol() {
        List<NewEbolRealm> realms = realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP).findAll();
        if(!realms.isEmpty()) {
            NewEbolRealm newEbolRealm = realms.get(0);
            return RealmTransformator.newEbolRealmToPojo(newEbolRealm);
        } return null;
    }

    public NewEbol getjEbol() {
        NewEbolRealm newEbolRealm = realm.where(NewEbolRealm.class).findFirst();
        return RealmTransformator.newEbolRealmToPojo(newEbolRealm);
    }

    public boolean deleteTempEbol() {
        realm.beginTransaction();
        realm.clear(NewEbolRealm.class);
        realm.clear(VehicleInfoRealm.class);
        realm.clear(DamagesRealm.class);
        realm.clear(VehiclePhotoRealm.class);
        realm.clear(MarkedDamageRealm.class);
        realm.clear(DamageTypeRealm.class);
        realm.clear(CustomerRealm.class);
        realm.commitTransaction();
        return true;
    }

    public boolean saveVehicle(VehicleInfo info) {
        realm.beginTransaction();
        VehicleInfoRealm vehicleInfoRealm = RealmTransformator.vehicleInfoToRealm(info, realm);
        if(info.getId() < 1) {
            realm.where(VehicleInfoRealm.class).equalTo(VehicleInfoRealm.TYPE, "temp").findAll().clear();
            info.setType("temp");
            info.setId(getId(realm, VehicleInfoRealm.class));
            NewEbolRealm newEbol = realm.where(NewEbolRealm.class)
                    .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP)
                    .findFirst();
            newEbol.getVehicleInfos().add(vehicleInfoRealm);
            realm.copyToRealmOrUpdate(newEbol);
        } else {
            vehicleInfoRealm = RealmTransformator.vehicleInfoToRealm(info, realm);
            realm.copyToRealmOrUpdate(vehicleInfoRealm);
        }
        realm.commitTransaction();
        return true;
    }

    public boolean saveTempVehicleOrUpdate(VehicleInfo info) {
        realm.beginTransaction();
        realm.where(VehicleInfoRealm.class).equalTo(VehicleInfoRealm.TYPE, "temp").findAll().clear();
        VehicleInfoRealm vehicleInfoRealm = RealmTransformator.vehicleInfoToRealm(info, realm);
        realm.commitTransaction();
        return true;
    }

    public boolean updateVehicle(VehicleInfo info) {
        realm.beginTransaction();
        VehicleInfoRealm vehicleInfoRealm;
        if (info.getType() != null && info.getType().equalsIgnoreCase("temp")) {
            info.setType("saved");
            vehicleInfoRealm = RealmTransformator.vehicleInfoToRealm(info, realm);
            NewEbolRealm newEbolRealm = realm.where(NewEbolRealm.class)
                    .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP).findFirst();
            newEbolRealm.getVehicleInfos().add(vehicleInfoRealm);
            NewEbolRealm nre = realm.copyToRealmOrUpdate(newEbolRealm);
            realm.copyToRealmOrUpdate(vehicleInfoRealm);
        } else {
            vehicleInfoRealm = RealmTransformator.vehicleInfoToRealm(info, realm);
        }
        realm.commitTransaction();
        return true;
    }

    public VehicleInfo getTempVehicle() {
        VehicleInfoRealm info = realm.where(VehicleInfoRealm.class)
                .equalTo(VehicleInfoRealm.TYPE, "temp").findFirst();
        return RealmTransformator.vehicleInfoRealmToPojo(info);
    }

    public VehicleInfo getVehicleById(int vehicleId) {
        VehicleInfoRealm info = realm.where(VehicleInfoRealm.class)
                .equalTo(VehicleInfoRealm.ID, vehicleId).findFirst();
        return RealmTransformator.vehicleInfoRealmToPojo(info);
    }

    public boolean clearTempVehicleInfo() {
        realm.beginTransaction();
        realm.where(VehicleInfoRealm.class).equalTo(VehicleInfoRealm.TYPE, "temp").findAll().clear();
        realm.commitTransaction();
        return true;
    }

    public boolean clearVehicleInfoTable() {
        realm.beginTransaction();
        realm.where(VehicleInfoRealm.class).findAll().clear();
        realm.commitTransaction();
        return true;
    }

    public boolean updateDamages(Damages damages, int vehicleId) {
        realm.beginTransaction();
        DamagesRealm damagesRealm;
        if (damages.getId() == 0) {
            damagesRealm = RealmTransformator.damagesToRealm(damages, realm);
            VehicleInfoRealm vehicleInfoRealm = realm.where(VehicleInfoRealm.class)
                    .equalTo(VehicleInfoRealm.ID, vehicleId).findFirst();
            vehicleInfoRealm.setDamages(damagesRealm);
            realm.copyToRealmOrUpdate(vehicleInfoRealm);
        } else {
            damagesRealm = RealmTransformator.damagesToRealm(damages, realm);
            realm.copyToRealmOrUpdate(damagesRealm);
        }
        realm.commitTransaction();
        return true;
    }

    public boolean saveVehiclePhoto(VehiclePhoto vehiclePhoto, int vehicleId) {
        realm.beginTransaction();
        VehiclePhotoRealm vehiclePhotoRealm;
        if (vehiclePhoto.isTemp()) {
            vehiclePhotoRealm = RealmTransformator.vehiclePhotoToRealm(vehiclePhoto, realm);
            VehicleInfoRealm vehicleInfoRealm = realm.where(VehicleInfoRealm.class)
                    .equalTo(VehicleInfoRealm.ID, vehicleId).findFirst();
            DamagesRealm damagesRealm = vehicleInfoRealm.getDamages();

            setPhotoToDamages(vehiclePhotoRealm, damagesRealm);
            realm.copyToRealmOrUpdate(vehicleInfoRealm);
        } else {
            vehiclePhotoRealm = RealmTransformator.vehiclePhotoToRealm(vehiclePhoto, realm);
            realm.copyToRealmOrUpdate(vehiclePhotoRealm);
        }
        realm.commitTransaction();
        return true;
    }

    public void setPhotoToDamages(VehiclePhotoRealm vehiclePhotoRealm, DamagesRealm damagesRealm) {
        VehiclePhotoRealm deletePhoto;
        switch (vehiclePhotoRealm.getPhotoType()){
            case VehiclePhotosWidget.FRONT_PHOTO:
                deletePhoto = damagesRealm.getFront();
                if(deletePhoto != null){
                    deletePhoto.removeFromRealm();
                }
                damagesRealm.setFront(vehiclePhotoRealm);
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                deletePhoto = damagesRealm.getBack();
                if(deletePhoto != null){
                    deletePhoto.removeFromRealm();
                }
                damagesRealm.setBack(vehiclePhotoRealm);
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                deletePhoto = damagesRealm.getRightSide();
                if(deletePhoto != null){
                    deletePhoto.removeFromRealm();
                }
                damagesRealm.setRightSide(vehiclePhotoRealm);
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                deletePhoto = damagesRealm.getLeftSide();
                if(deletePhoto != null){
                    deletePhoto.removeFromRealm();
                }
                damagesRealm.setLeftSide(vehiclePhotoRealm);
                break;
            default:
                break;
        }
    }

    public boolean updateVehiclePhoto(VehiclePhoto vehiclePhoto) {
        realm.beginTransaction();
        VehiclePhotoRealm vehiclePhotoRealm = RealmTransformator.vehiclePhotoToRealm(vehiclePhoto, realm);
        realm.commitTransaction();
        return true;
    }

    public boolean deleteVehiclePhotoById(int photoId) {
        realm.beginTransaction();
        Log.d(TAG, "deleteVehiclePhotoById: " +(realm.where(VehiclePhotoRealm.class)
                .equalTo(VehiclePhotoRealm.ID, photoId).findAll()==null?"null":realm
                .where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.ID, photoId).findAll().size()));
        realm.where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.ID, photoId).findAll().clear();
        Log.d(TAG, "deleteVehiclePhotoById: " + (realm.where(VehiclePhotoRealm.class)
                .equalTo(VehiclePhotoRealm.ID, photoId).findAll() == null ? "null" : realm
                .where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.ID, photoId).findAll().size()));
        realm.commitTransaction();
        return true;
    }

    public boolean deleteTempVehiclePhoto() {
        realm.beginTransaction();
        Log.d(TAG, "deleteVehiclePhotoById: " + (realm.where(VehiclePhotoRealm.class)
                .equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll() == null ? "null" : realm
                .where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll().size()));
        realm.where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll().clear();
        Log.d(TAG, "deleteVehiclePhotoById: " + (realm.where(VehiclePhotoRealm.class)
                .equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll() == null ? "null" : realm
                .where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll().size()));
        realm.commitTransaction();
        return true;
    }

    private <E extends RealmObject> int getId(Realm realm, Class<E> clazz) {
        return realm.where(clazz).max("id").intValue() + 1;
    }

    public void deInit() {
        if (realm != null && !realm.isClosed()) {
            realm.close();
        }
    }

    public List<NewEbol> getEbols() {
        List<NewEbolRealm> ebolRealms = realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, "db").findAll();
        List<NewEbol> ebols = new ArrayList<>();
        for (NewEbolRealm ebolRealm : ebolRealms) {
            ebols.add(RealmTransformator.newEbolRealmToPojo(ebolRealm));
        }
        return ebols;
    }

    public boolean deleteNetTempEbols() {
        realm.beginTransaction();
        realm.where(NewEbolRealm.class).notEqualTo(NewEbolRealm.SUBMIT_ID, 0)
                .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP).findAll().clear();
        realm.commitTransaction();
        return true;
    }

    public boolean saveTempEbols() {
        realm.beginTransaction();
        List<NewEbolRealm> ebols = realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP).findAll();
        for (int i = 0; i < ebols.size(); i++) {
            ebols.get(i).setType("db");
        }
        realm.commitTransaction();
        return true;
    }

    public boolean saveTempDbEbols() {
        realm.beginTransaction();
        List<NewEbolRealm> ebols = realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP)
                .equalTo(NewEbolRealm.SAVED_TO_DB, true).findAll();
        for (int i = 0; i < ebols.size(); i++) {
            ebols.get(i).setType("db");
        }
        realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, NewFFebolFragment.TEMP)
                .equalTo(NewEbolRealm.SAVED_TO_DB, false).findAll().clear();
        realm.commitTransaction();
        return true;
    }

    public boolean saveEbol(NewEbol ebol) {
        realm.beginTransaction();
        NewEbolRealm newEbolRealm = RealmTransformator.newEbolToRealm(ebol, realm);
        newEbolRealm = realm.copyToRealmOrUpdate(newEbolRealm);
        realm.commitTransaction();
        return true;
    }

    public interface SaveCallback{
        void requestSuccess();
//        void requestFailed(Exception exception);
    }

    public interface FailedCallback{
        void requestFailed();
    }

    public interface RequestCallback<T>{
        void requestSuccess(T data);
    }

    public interface SaveClientCallback {
        void clientSaved();
        void saveClientFailed(Exception e);
    }

    public interface SaveColleagueCallback {
        void colleagueSaved();
        void saveColleagueFailed(Exception e);
    }

}
