package com.github.andrdev.bolservice.newEbol.trs;

import android.location.Address;
import android.location.Location;

import com.github.andrdev.bolservice.DataProvider;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;


public abstract class NewEbolPresenter  extends MvpBasePresenter<NewFEbolView> {

    public abstract void getData();

    abstract void saveEbol(DataProvider.DataCb<Boolean> callback);

    public abstract void finishEbolCreation();

    public abstract void addNewVehicle();

    public abstract void detailsClick(int vehicleId);

    public abstract void damagesClick(int vehicleId);

    public abstract void customerSignatureClick();

    public abstract void driverSignatureClick();

    public abstract void unableToSignClick();

    public abstract void getAddress();

    public abstract void permissionsResult(Boolean granted);

    public abstract void isGpsTurnedOn();

    public abstract void isGpsTurnedOn(Boolean gpsTurnedOn);

    public abstract void getLocation();

    public abstract void getAddressFromLocation(Location location);

    public abstract void gpsAddressSuccessOb(List<Address> addresses);

    public abstract void noGpsPermission();

    public abstract void addressReceived(Address address);

    public abstract void gpsTurnedOff();
}
