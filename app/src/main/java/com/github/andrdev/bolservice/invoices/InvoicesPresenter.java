package com.github.andrdev.bolservice.invoices;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.Invoice;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class InvoicesPresenter extends MvpBasePresenter<BaseSearchView> {

    public abstract void getData(String status);

    public abstract void getDataRequestSuccess(InvoicesResponse invoicesResponse);

    public abstract void getDataRequestFailed(Throwable throwable);

    public abstract void listItemClicked(Invoice invoice);
}
