package com.github.andrdev.bolservice.model.requests;


import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;

import rx.Observable;

public class NewDamageRequest {

    String cid;
    String code;
    String title;
    String description;
    String positionTop;
    String positionLeft;
    String side;

    public NewDamageRequest() {
    }

    public NewDamageRequest(MarkedDamage damage, String photoType) {
        Observable.from(damage.getDamageTypes())
                .map(damageType -> code.concat(damageType.getDamageShortName()))
                .subscribe();
        positionTop = String.valueOf(damage.getyPosition());
        positionLeft = String.valueOf(damage.getxPosition());
        side = photoType;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPositionLeft() {
        return positionLeft;
    }

    public void setPositionLeft(String positionLeft) {
        this.positionLeft = positionLeft;
    }

    public String getPositionTop() {
        return positionTop;
    }

    public void setPositionTop(String positionTop) {
        this.positionTop = positionTop;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
