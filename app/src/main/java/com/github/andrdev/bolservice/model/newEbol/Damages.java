package com.github.andrdev.bolservice.model.newEbol;


public class Damages {

    private int id;
    private int cid;
    private boolean radio = false;
    private boolean spareTires = false;
    private int keys;
    private int headrest;
    private boolean manuals = false;
    private boolean cargoCovers = false;
    private int remotes;
    private int floorMats;
    private VehiclePhoto front;
    private VehiclePhoto rightSide;
    private VehiclePhoto leftSide;
    private VehiclePhoto back;
    private VehiclePhoto additionalFirst;
    private VehiclePhoto additionalSecond;

    public Damages() {

    }

    public VehiclePhoto getAdditionalFirst() {
        return additionalFirst;
    }

    public void setAdditionalFirst(VehiclePhoto additionalFirst) {
        this.additionalFirst = additionalFirst;
    }

    public VehiclePhoto getAdditionalSecond() {
        return additionalSecond;
    }

    public void setAdditionalSecond(VehiclePhoto additionalSecond) {
        this.additionalSecond = additionalSecond;
    }

    public VehiclePhoto getBack() {
        return back;
    }

    public void setBack(VehiclePhoto back) {
        this.back = back;
    }

    public boolean isCargoCovers() {
        return cargoCovers;
    }

    public void setCargoCovers(boolean cargoCovers) {
        this.cargoCovers = cargoCovers;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getFloorMats() {
        return floorMats;
    }

    public void setFloorMats(int floorMats) {
        this.floorMats = floorMats;
    }

    public VehiclePhoto getFront() {
        return front;
    }

    public void setFront(VehiclePhoto front) {
        this.front = front;
    }

    public int getHeadrest() {
        return headrest;
    }

    public void setHeadrest(int headrest) {
        this.headrest = headrest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKeys() {
        return keys;
    }

    public void setKeys(int keys) {
        this.keys = keys;
    }

    public VehiclePhoto getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(VehiclePhoto leftSide) {
        this.leftSide = leftSide;
    }

    public boolean isManuals() {
        return manuals;
    }

    public void setManuals(boolean manuals) {
        this.manuals = manuals;
    }

    public boolean isRadio() {
        return radio;
    }

    public void setRadio(boolean radio) {
        this.radio = radio;
    }

    public int getRemotes() {
        return remotes;
    }

    public void setRemotes(int remotes) {
        this.remotes = remotes;
    }

    public VehiclePhoto getRightSide() {
        return rightSide;
    }

    public void setRightSide(VehiclePhoto rightSide) {
        this.rightSide = rightSide;
    }

    public boolean isSpareTires() {
        return spareTires;
    }

    public void setSpareTires(boolean spareTires) {
        this.spareTires = spareTires;
    }
}
