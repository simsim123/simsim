package com.github.andrdev.bolservice.model.realms;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class DamagesRealm extends RealmObject{

    public static final String ID = "id";
    public static final String CID = "cid";
    public static final String RADIO = "radio";
    public static final String SPARE_TIRES = "spareTires";
    public static final String KEYS = "keys";
    public static final String HEADREST = "headrest";
    public static final String MANUALS = "manuals";
    public static final String CARGO_COVERS = "cargoCovers";
    public static final String REMOTES = "remotes";
    public static final String FLOOR_MATS = "floorMats";
    public static final String FRONT = "front";
    public static final String RIGHT_SIDE = "rightSide";
    public static final String LEFT_SIDE = "leftSide";
    public static final String BACK = "back";
    public static final String ADDI_F = "additionalFirst";
    public static final String ADDI_S = "additionalSecond";

    @PrimaryKey
    private int id;
    private int cid;
    private boolean radio = false;
    private boolean spareTires = false;
    private int keys;
    private int headrest;
    private boolean manuals = false;
    private boolean cargoCovers = false;
    private int remotes;
    private int floorMats;
    private VehiclePhotoRealm front;
    private VehiclePhotoRealm rightSide;
    private VehiclePhotoRealm leftSide;
    private VehiclePhotoRealm back;
    private VehiclePhotoRealm additionalFirst;
    private VehiclePhotoRealm additionalSecond;

    public DamagesRealm() {

    }



    public VehiclePhotoRealm getAdditionalFirst() {
        return additionalFirst;
    }

    public void setAdditionalFirst(VehiclePhotoRealm additionalFirst) {
        this.additionalFirst = additionalFirst;
    }

    public VehiclePhotoRealm getAdditionalSecond() {
        return additionalSecond;
    }

    public void setAdditionalSecond(VehiclePhotoRealm additionalSecond) {
        this.additionalSecond = additionalSecond;
    }

    public VehiclePhotoRealm getBack() {
        return back;
    }

    public void setBack(VehiclePhotoRealm back) {
        this.back = back;
    }

    public boolean isCargoCovers() {
        return cargoCovers;
    }

    public void setCargoCovers(boolean cargoCovers) {
        this.cargoCovers = cargoCovers;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getFloorMats() {
        return floorMats;
    }

    public void setFloorMats(int floorMats) {
        this.floorMats = floorMats;
    }

    public VehiclePhotoRealm getFront() {
        return front;
    }

    public void setFront(VehiclePhotoRealm front) {
        this.front = front;
    }

    public int getHeadrest() {
        return headrest;
    }

    public void setHeadrest(int headrest) {
        this.headrest = headrest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKeys() {
        return keys;
    }

    public void setKeys(int keys) {
        this.keys = keys;
    }

    public VehiclePhotoRealm getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(VehiclePhotoRealm leftSide) {
        this.leftSide = leftSide;
    }

    public boolean isManuals() {
        return manuals;
    }

    public void setManuals(boolean manuals) {
        this.manuals = manuals;
    }

    public boolean isRadio() {
        return radio;
    }

    public void setRadio(boolean radio) {
        this.radio = radio;
    }

    public int getRemotes() {
        return remotes;
    }

    public void setRemotes(int remotes) {
        this.remotes = remotes;
    }

    public VehiclePhotoRealm getRightSide() {
        return rightSide;
    }

    public void setRightSide(VehiclePhotoRealm rightSide) {
        this.rightSide = rightSide;
    }

    public boolean isSpareTires() {
        return spareTires;
    }

    public void setSpareTires(boolean spareTires) {
        this.spareTires = spareTires;
    }
}
