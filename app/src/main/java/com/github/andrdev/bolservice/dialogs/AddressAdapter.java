package com.github.andrdev.bolservice.dialogs;

import android.location.Address;
import android.view.View;
import android.widget.TextView;


import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;

import butterknife.Bind;


public class AddressAdapter extends BaseAdapter<Address, AddressAdapter.PredictionHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        return R.layout.list_item_prediction;
    }

    @Override
    protected PredictionHolder getHolder(View view) {
        return new PredictionHolder(view);
    }

    protected class PredictionHolder extends BaseAdapter.BaseViewHolder<Address> {

        @Bind(R.id.mainText)
        TextView mainText;
        @Bind(R.id.secondaryText)
        TextView secondaryText;

        public PredictionHolder(View itemView) {
            super(itemView);
        }

        protected void bind(Address address) {
//            String firstAddress = String.format("%s, %s, %s", address.getThoroughfare(),
//                    address.getLocality(),
//                    address.getAdminArea());
//            mainText.setText(firstAddress);
//            secondaryText.setText(firstAddress + address.getCountryName());
            mainText.setText(address.getThoroughfare());
            secondaryText.setText(address.getCountryName());
        }
    }
}
