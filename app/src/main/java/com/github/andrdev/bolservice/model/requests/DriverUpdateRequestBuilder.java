package com.github.andrdev.bolservice.model.requests;

import android.widget.EditText;

import com.github.andrdev.bolservice.Utils;

public class DriverUpdateRequestBuilder {
    private String did;
    private String driverEmail;
    private String driverPhone;
    private String firstName;
    private String lastName;

    public DriverUpdateRequestBuilder setDid(String did) {
        this.did = did;
        return this;
    }

    public DriverUpdateRequestBuilder setDriverEmail(EditText driverEmail) {
        if(Utils.hasText(driverEmail)){
            this.driverEmail = Utils.getTextFromEditText(driverEmail);
        }
        return this;
    }

    public DriverUpdateRequestBuilder setDriverPhone(EditText driverPhone) {
        if(Utils.hasText(driverPhone)){
            this.driverPhone = Utils.getTextFromEditText(driverPhone);
        }
        return this;
    }

    public DriverUpdateRequestBuilder setFirstName(EditText firstName) {
        if(Utils.hasText(firstName)){
            this.firstName = Utils.getTextFromEditText(firstName);
        }
        return this;
    }

    public DriverUpdateRequestBuilder setLastName(EditText lastName) {
        if(Utils.hasText(lastName)){
            this.lastName = Utils.getTextFromEditText(lastName);
        }
        return this;
    }

    public DriverUpdateRequest createDriverUpdateRequest() {
        return new DriverUpdateRequest(did, driverEmail, driverPhone, firstName, lastName);
    }
}