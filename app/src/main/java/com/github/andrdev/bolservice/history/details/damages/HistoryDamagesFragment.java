package com.github.andrdev.bolservice.history.details.damages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.history.details.HistoryCameraActivity;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.view.vehiclePhotosNe.VehiclePhotosNeWidget;
import com.github.andrdev.bolservice.view.vehicleToolsNe.VehicleToolsNeWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryDamagesFragment extends MvpFragment<HistoryDamagesView, HistoryDamagesPresenter>
        implements HistoryDamagesView {

    public static final String PHOTO_TYPE = "photoType";
    public static final String FRONT_PHOTO = "frontPhoto";
    public static final String BACK_PHOTO = "backPhoto";
    public static final String LEFT_SIDE_PHOTO = "leftSidePhoto";
    public static final String RIGHT_SIDE_PHOTO = "rightSidePhoto";

    @Bind(R.id.vehiclePhotos)
    VehiclePhotosNeWidget vehiclePhotos;

    @Bind(R.id.vehicleTools)
    VehicleToolsNeWidget vehicleTools;

    int selectedVehicleId;
    Damages currentDamages;
    VehicleInfo vi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_origin_damages, container, false);
        ButterKnife.bind(this, view);
        selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getVehicle(selectedVehicleId);
    }

    @Override
    public void setVehicleInfo(VehicleInfo currentCar) {
        ((HistoryCarDamagesActivity)getActivity()).setTitle(currentCar);
        vi = currentCar;
        currentDamages = currentCar.getDamages();
        if (currentDamages != null) {
            setFieldsFromDb(currentDamages);
        }
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    private void setFieldsFromDb(Damages damages) {
        vehicleTools.setFieldsFromDb(damages);
        setCapturedImages(damages);
    }

    private void setCapturedImages(Damages damages) {
        vehiclePhotos.setOpenOnClick(HistoryCameraActivity.class);
        vehiclePhotos.setSelectedVehicleId(selectedVehicleId);
        vehiclePhotos.setFieldsFromDb(damages);
    }

    @OnClick(R.id.save)
    void saveClick(View view) {
        getActivity().onBackPressed();
    }

    @Override
    public HistoryDamagesPresenter createPresenter() {
        return new HistoryDamagesPresenter();
    }
}
