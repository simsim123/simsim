package com.github.andrdev.bolservice.view.vehicleTools;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;

public class VehicleToolsDataProvider implements DataProvider {
    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }
}
