package com.github.andrdev.bolservice.messages;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.ConversationSmall;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

import java.util.List;
import rx.Observable;


public class MessagesPresenterImpl extends MessagesPresenter {

    MessagesDataProvider dataProvider;

    @Override
    public void attachView(BaseSearchView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new MessagesDataProvider();
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getConversations() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getConversationsList(this::setDataAndMArkAsRead,
                this::getMessagesFailed);
    }

    public void getMessagesFailed(Throwable throwable) {
        if (!isViewAttached()) {
            return;
        }
        getView().stopRefresh();
        getView().showRequestFailedToast();
    }

    public void setDataAndMArkAsRead(ConversationsResponse conversationsResponse) {
        if (!isViewAttached()) {
            return;
        }
        getView().stopRefresh();
        if (conversationsResponse.getStatus().equals("true")) {
            getView().setData(conversationsResponse.getConversations());
            markMessagesAsRead(conversationsResponse.getConversations());
        } else {
            getView().showNoDataText();
        }
    }

    @Override
    protected void markMessagesAsRead(List<ConversationSmall> conversations) {
        Observable.from(conversations)
                .filter(conversation -> conversation.getLastMessage().getStatus().equals("new"))
                .forEach((conversation) ->
                        dataProvider.markAsReadMessages(
                                String.valueOf(conversation.getConversationId()),
                                String.valueOf(conversation.getLastMessage().getId())));
    }
}
