package com.github.andrdev.bolservice.notifications;

import com.github.andrdev.bolservice.model.Notification;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;


public interface NotificationsView extends MvpView {

    void getData();

    void setData(List<Notification> data);

    void showFailedToast();
}
