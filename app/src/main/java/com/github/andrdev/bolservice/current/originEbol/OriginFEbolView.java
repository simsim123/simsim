package com.github.andrdev.bolservice.current.originEbol;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginFEbolView extends MvpView {
    void setData(NewEbol ebol);

    void showVehicleDetails(int vehicleId);

    void showDamages(int vehicleId);

    Context getViewContext();

    void openCustomerSignature();

    void openDriverSignature();

    void showUnableToSign();

    NewEbol getEbolToSave();
}
