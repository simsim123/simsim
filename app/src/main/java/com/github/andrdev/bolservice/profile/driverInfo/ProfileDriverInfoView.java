package com.github.andrdev.bolservice.profile.driverInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ProfileDriverInfoView extends MvpView {

    void setProfile(Profile profile);

    void initFields();

    void showFailedToast();

    Context getViewContext();
}
