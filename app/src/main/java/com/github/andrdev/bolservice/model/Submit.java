package com.github.andrdev.bolservice.model;


public class Submit {

    long id;

    String orderId;

    String createdAt;

    String pickupDate;

    String deliveryDate;

    int vehiclesCount;

    String pickupAddressFormatted;

    String deliveryAddressFormatted;

    String status;

//    @DatabaseField(foreign=true,foreignAutoRefresh=true, columnName = "detailed")
    DetailedSubmit detailed;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeliveryAddressFormatted() {
        return deliveryAddressFormatted;
    }

    public void setDeliveryAddressFormatted(String deliveryAddressFormatted) {
        this.deliveryAddressFormatted = deliveryAddressFormatted;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public DetailedSubmit getDetailed() {
        return detailed;
    }

    public void setDetailed(DetailedSubmit detailed) {
        this.detailed = detailed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPickupAddressFormatted() {
        return pickupAddressFormatted;
    }

    public void setPickupAddressFormatted(String pickupAddressFormatted) {
        this.pickupAddressFormatted = pickupAddressFormatted;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getVehiclesCount() {
        return vehiclesCount;
    }

    public void setVehiclesCount(int vehiclesCount) {
        this.vehiclesCount = vehiclesCount;
    }
}
