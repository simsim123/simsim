package com.github.andrdev.bolservice.newEbol.trs;

import android.content.Context;
import android.location.Address;

import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.realms.NewEbolRealm;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface NewFEbolView extends MvpView {

    NewEbol getEbolToSave();

    void buildAlertMessageNoGps();

    void selectAddressFromGooglePlacesOrigin();

    void setAddressFromPredictionOrigin(Address address);

    Context getViewContext();

    void openNewVehicleInfoActivity();

    void setData(NewEbol ebol);

    void saveFields();

    void showVehicleDetails(int vehicleId);

    void showDamages(int vehicleId);

    void openCustomerSignature();

    void openDriverSignature();

    void showUnableToSign();

    void finishEbol();
}
