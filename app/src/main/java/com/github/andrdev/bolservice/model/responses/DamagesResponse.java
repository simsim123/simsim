package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;

import java.util.Map;

public class DamagesResponse {
    Map<String, VehiclePhoto> vehiclePhotos;

    public DamagesResponse() {
    }

    public Map<String, VehiclePhoto> getVehiclePhotos() {
        return vehiclePhotos;
    }

    public void setVehiclePhotos(Map<String, VehiclePhoto> vehiclePhotos) {
        this.vehiclePhotos = vehiclePhotos;
    }
}
