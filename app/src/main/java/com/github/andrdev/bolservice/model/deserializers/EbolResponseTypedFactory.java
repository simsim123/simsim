package com.github.andrdev.bolservice.model.deserializers;

import android.util.Log;

import com.github.andrdev.bolservice.model.Submit;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.SubmitsResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EbolResponseTypedFactory extends CustomizedTypeAdapterFactory<EbolsResponse> {

    public EbolResponseTypedFactory(Class<EbolsResponse> customizedClass) {
        super(customizedClass);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected EbolsResponse deserialize(JsonElement deserialized, Gson gson) {
        SubmitsResponse submitsResponse = null;
        Log.d("eboolsre", deserialized.toString());
        try {
            submitsResponse = gson.getDelegateAdapter(this, TypeToken.get(SubmitsResponse.class))
                    .fromJson(String.valueOf(deserialized));
        } catch (IOException e) {
            e.printStackTrace();
        }
        EbolsResponse ebolsResponse = new EbolsResponse();
        ebolsResponse.setEbols(submitToEbol(submitsResponse.getSubmits()));
        return ebolsResponse;
    }

    List<NewEbol> submitToEbol(List<Submit> submits) {
        List<NewEbol> ebols = new ArrayList<>();
        for (Submit submit : submits) {
            NewEbol ebol = new NewEbol();
            ebol.setSubmitId(submit.getId());
            ebol.setType(submit.getStatus());
//        ebol.setDate(submit.getCreatedAt());
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
                Date d = sdf.parse(submit.getCreatedAt());
                String formattedTime = output.format(d);
                Log.d("dreedatese", formattedTime);
                ebol.setDate(formattedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            SimpleDateFormat month_date = new SimpleDateFormat();
//            ebol.setDate(1505050);
            ebol.setOrderId(submit.getOrderId());
            ebol.setVehiclesCount(submit.getVehiclesCount());
            Customer origin = new Customer();
            origin.setCustomerName(submit.getDetailed().getOriginCustomerName());
            origin.setAddress(submit.getDetailed().getOriginAddress());
            origin.setCity(submit.getDetailed().getOriginCity());
            origin.setState(submit.getDetailed().getOriginState());
            origin.setZip(submit.getDetailed().getOriginZip());
            origin.setPhone(submit.getDetailed().getOriginPhone());
            origin.setCell(submit.getDetailed().getOriginBuyerNumber());
            Customer destination = new Customer();
            destination.setCustomerName(submit.getDetailed().getDestinationCustomerName());
            destination.setAddress(submit.getDetailed().getDestinationAddress());
            destination.setCity(submit.getDetailed().getDestinationCity());
            destination.setState(submit.getDetailed().getDestinationState());
            destination.setZip(submit.getDetailed().getDestinationZip());
            destination.setPhone(submit.getDetailed().getDestinationPhone());
            destination.setCell(submit.getDetailed().getDestinationBuyerNumber());
            ebol.setOrigin(origin);
            ebol.setDestination(destination);
            ebols.add(ebol);
        }
        return ebols;
    }
}
