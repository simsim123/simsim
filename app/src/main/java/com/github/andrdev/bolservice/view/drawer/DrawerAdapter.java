package com.github.andrdev.bolservice.view.drawer;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseAdapter;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.DrawerLine;

import butterknife.Bind;


public class DrawerAdapter extends BaseAdapter<DrawerLine, DrawerAdapter.DrawerHolder> {

    @Override
    protected int getLayoutResource(int viewType) {
        if(viewType == 1) {
            return R.layout.list_item_drawer;
        } else {
            return R.layout.list_item_drawer_dark;
        }
    }

    @Override
    protected DrawerHolder getHolder(View view) {
        return new DrawerHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position%2;
    }

    protected class DrawerHolder extends BaseAdapter.BaseViewHolder<DrawerLine> {

        @Bind(R.id.listText)
        TextView title;
        @Bind(R.id.listIcon)
        ImageView icon;

        public DrawerHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(DrawerLine item) {
            title.setText(item.getTitle());
            icon.setImageResource(item.getIcon());
        }
    }
}