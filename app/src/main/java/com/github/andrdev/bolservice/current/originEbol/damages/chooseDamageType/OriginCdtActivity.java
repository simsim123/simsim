package com.github.andrdev.bolservice.current.originEbol.damages.chooseDamageType;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.damages.OriginDamagesFragment;
import com.github.andrdev.bolservice.current.originEbol.damages.editDamageType.OriginEdtFragment;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;


public class OriginCdtActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            if(getIntent().hasExtra("action")){
                showEditDamageTypes();
            } else {
                showDamageTypeFragment();
            }
        }
    }

    private void showEditDamageTypes() {
        OriginEdtFragment fragment = new OriginEdtFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, getIntent().getIntExtra(
                NewFFebolFragment.SELECTED_VEHICLE_ID, 0));
        bundle.putString(OriginDamagesFragment.PHOTO_TYPE, getIntent().getStringExtra(
                OriginDamagesFragment.PHOTO_TYPE));
        bundle.putDouble("yPosition", getIntent().getDoubleExtra(
                "yPosition", 0));
        bundle.putDouble("xPosition", getIntent().getDoubleExtra(
                "xPosition", 0));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();
    }

    private void showDamageTypeFragment() {
        OriginCdtFragment fragment = new OriginCdtFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, getIntent().getIntExtra(
                NewFFebolFragment.SELECTED_VEHICLE_ID, 0));
        bundle.putString(OriginDamagesFragment.PHOTO_TYPE, getIntent().getStringExtra(
                OriginDamagesFragment.PHOTO_TYPE));
        bundle.putDouble("xPosition", getIntent().getDoubleExtra(
                "xPosition", 0));
        bundle.putDouble("yPosition", getIntent().getDoubleExtra(
                "yPosition", 0));
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();
    }
}

