package com.github.andrdev.bolservice.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.CurrentEbolsActivity;
import com.github.andrdev.bolservice.history.HistoryActivity;
import com.github.andrdev.bolservice.invoices.InvoicesActivity;
import com.github.andrdev.bolservice.messages.MessagesActivity;
import com.github.andrdev.bolservice.model.realms.VehiclePhotoRealm;
import com.github.andrdev.bolservice.model.realms.CustomerRealm;
import com.github.andrdev.bolservice.model.realms.DamageTypeRealm;
import com.github.andrdev.bolservice.model.realms.DamagesRealm;
import com.github.andrdev.bolservice.model.realms.MarkedDamageRealm;
import com.github.andrdev.bolservice.model.realms.NewEbolRealm;
import com.github.andrdev.bolservice.model.realms.VehicleInfoRealm;
import com.github.andrdev.bolservice.newEbol.NewEbolActivity;
import com.google.zxing.client.result.VINResultParser;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;


public class DashBoardFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.plus)
    void plusClick(View view) {
        Realm realm = Realm.getInstance(getContext());
        realm.beginTransaction();
        List<NewEbolRealm> newEbolRealmList = realm.where(NewEbolRealm.class)
                .equalTo(NewEbolRealm.TYPE, "temp").findAll();
        while(newEbolRealmList.listIterator().hasNext()) {
            newEbolRealmList.listIterator().next().setType("locale");
        }
        realm.where(VehicleInfoRealm.class).equalTo(VehicleInfoRealm.TYPE, "temp").findAll().clear();
        realm.where(VehiclePhotoRealm.class).equalTo(VehiclePhotoRealm.IS_TEMP, true).findAll().clear();
        realm.where(MarkedDamageRealm.class).equalTo(MarkedDamageRealm.IS_TEMP, true).findAll().clear();

//        realm.clear(NewEbolRealm.class);
//        realm.clear(VehicleInfoRealm.class);
//        realm.clear(DamagesRealm.class);
//        realm.clear(VehiclePhotoRealm.class);
//        realm.clear(MarkedDamageRealm.class);
//        realm.clear(DamageTypeRealm.class);
//        realm.clear(CustomerRealm.class);
        realm.commitTransaction();
        Intent intent = new Intent(getActivity(), NewEbolActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.current)
    void currentClick(View view) {
        Intent intent = new Intent(getActivity(), CurrentEbolsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.messages)
    void messagesClick(View view){
        Intent intent = new Intent(getActivity(), MessagesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.history)
    void historyClick(View view){
        Intent intent = new Intent(getActivity(), HistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.invoices)
    void invoicesClick(View view){
        Intent intent = new Intent(getActivity(), InvoicesActivity.class);
        startActivity(intent);
    }
}
