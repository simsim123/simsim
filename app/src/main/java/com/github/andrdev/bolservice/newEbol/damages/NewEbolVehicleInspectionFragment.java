package com.github.andrdev.bolservice.newEbol.damages;

import android.support.v4.app.Fragment;


public class NewEbolVehicleInspectionFragment extends Fragment {
//
//
//    @Bind(R.id.parent)
//    RelativeLayout parent;
//    @Bind(R.id.photoResult)
//    ImageView photoResult;
//    @Bind(R.id.vehicleModel)
//    TextView vehicleModel;
//
//    @Bind(R.id.notesView)
//    RelativeLayout notesView;
//
//    @Bind(R.id.notesText)
//    EditText notesText;
//
//    VehicleInfoRealm vehicleInfo;
//    String photoType;
//    int selectedVehicleId;
//    CarPhotoRealm tempPhoto;
//
//    Repo repo;
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_vehicle_inspection, container, false);
//        ButterKnife.bind(this, view);
//        repo = new Repo(getContext());
////
////        photoType = getArguments().getString("phototype");
//        selectedVehicleId = getArguments().getInt(NewEbolFragment.SELECTED_VEHICLE_ID);
//        vehicleInfo = repo.repoVehicleInfo.getVehicleInfoById(selectedVehicleId);
//        Log.d("dreeCPS", "cid" + selectedVehicleId);
//        if(vehicleInfo.getDamages()!=null)
//        Log.d("dreenext", vehicleInfo.getDamages().getFront() == null?"y":"n");
//        if (getArguments() != null && getArguments().containsKey("phototype")) {
//            switch (getArguments().getString("phototype")){
//                case NewEbolDamagesFragment.FRONT_PHOTO:
//                    tempPhoto = vehicleInfo.getDamages().getFront();
//                    break;
//                case NewEbolDamagesFragment.BACK_PHOTO:
//                    tempPhoto = vehicleInfo.getDamages().getBack();
//                    break;
//                case NewEbolDamagesFragment.LEFT_SIDE_PHOTO:
//                    tempPhoto = vehicleInfo.getDamages().getLeftSide();
//                    break;
//                case NewEbolDamagesFragment.RIGHT_SIDE_PHOTO:
//                    tempPhoto = vehicleInfo.getDamages().getRightSide();
//                    break;
//                case NewEbolDamagesFragment.ADDITIONAL_PHOTO_FIRST:
//                    tempPhoto = vehicleInfo.getDamages().getAdditionalFirst();
//                    break;
//                case NewEbolDamagesFragment.ADDITIONAL_PHOTO_SECOND:
//                    tempPhoto = vehicleInfo.getDamages().getAdditionalSecond();
//                    break;
//                default:
//                    tempPhoto = null;
//                    break;
//            }
//        }
//
//        if(tempPhoto == null){
//            tempPhoto = repo.repoCarPhoto.getTempCarPhoto();
//        }
//        photoType = tempPhoto.getPhotoType();
//
//        File file = new File(tempPhoto.getPhotoPath());
//        Picasso picasso = Picasso.with(getContext());
//        picasso.invalidate(file);
//        picasso.load(file).fit().centerCrop().into(photoResult);
//        initFields();
//        return view;
//    }
//
//    private void initFields() {
//            vehicleModel.setText(String.format("%s %s %s", vehicleInfo.getYear(), vehicleInfo.getMake(),
//                    vehicleInfo.getModel()));
////            tempPhoto = realm.where(CarPhoto.class).equalTo("photoType", "tempPhoto").findFirst();
//            Log.d("dree", "mark size" + tempPhoto.getMarkedDamages().size());
//
//            if(tempPhoto.getMarkedDamages().size() != 0) {
//                for (final MarkedDamageRealm markedDamage : tempPhoto.getMarkedDamages()) {
//                    RelativeLayout.LayoutParams params =
//                            new RelativeLayout.LayoutParams(getPixelSize(),getPixelSize());
//                    TextView textView = new TextView(getContext());
//                    int yPosition = markedDamage.getyPosition();
//                    int xPosition = markedDamage.getxPosition();
//                    params.leftMargin = xPosition; //Your X coordinate
//                    params.topMargin = yPosition;
//                    textView.setLayoutParams(params);
//                    String damageCode;
//                    if(markedDamage.getDamageTypes().size() == 1) {
//                        damageCode = new ArrayList<>(markedDamage.getDamageTypes()).get(0).getDamageShortName();
//                    } else {
//                        damageCode = String.valueOf(new ArrayList<>(markedDamage.getDamageTypes()).size());
//                    }
//                    textView.setText(damageCode);
//                    textView.setTextColor(Color.WHITE);
//                    textView.setGravity(Gravity.CENTER);
//                    textView.setTextSize(16);
//                    textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.krasny));
//                    parent.addView(textView);
//                    Log.d("dree", "tvSet");
//                    String key = String.format("%d%d", yPosition, xPosition);
//                }
//
////        parent.invalidate();
//        }
//    }
//
//    int getPixelSize() {
//        Resources r = getResources();
//        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());
//        return px;
//    }
//
//    @OnClick(R.id.continueToNext)
//    void continueToNextClick(View v) {
//        String nextType = "";
//        tempPhoto.setIsTemp(false);
//        repo.repoCarPhoto.updateVehiclePhoto(tempPhoto);
//        if (vehicleInfo.getDamages() == null) {
//            Log.d("dreenext", "newdam");
//            DamagesRealm damagesTemp = new DamagesRealm();
//            repo.repoDamages.createDamages(damagesTemp);
//            vehicleInfo.setDamages(damagesTemp);
//            repo.repoVehicleInfo.updateVehicleInfo(vehicleInfo);
//        }
//        switch (tempPhoto.getPhotoType()) {
//            case NewEbolDamagesFragment.FRONT_PHOTO:
//                nextType = NewEbolDamagesFragment.BACK_PHOTO;
//                vehicleInfo.getDamages().setFront(tempPhoto);
//                break;
//            case NewEbolDamagesFragment.BACK_PHOTO:
//                nextType = NewEbolDamagesFragment.RIGHT_SIDE_PHOTO;
//                vehicleInfo.getDamages().setBack(tempPhoto);
//                break;
//            case NewEbolDamagesFragment.RIGHT_SIDE_PHOTO:
//                nextType = NewEbolDamagesFragment.LEFT_SIDE_PHOTO;
//                vehicleInfo.getDamages().setRightSide(tempPhoto);
//                break;
//            case NewEbolDamagesFragment.LEFT_SIDE_PHOTO:
//                vehicleInfo.getDamages().setLeftSide(tempPhoto);
//                break;
//            case NewEbolDamagesFragment.ADDITIONAL_PHOTO_FIRST:
//                vehicleInfo.getDamages().setAdditionalFirst(tempPhoto);
//                break;
//            case NewEbolDamagesFragment.ADDITIONAL_PHOTO_SECOND:
//                vehicleInfo.getDamages().setAdditionalSecond(tempPhoto);
//                break;
//            default:
//                break;
//        }
//        Log.d("dreenext", vehicleInfo.getDamages().getFront() == null ? "y" : "n");
//        repo.repoDamages.updateDamages(vehicleInfo.getDamages());
//        if (photoType.equals(NewEbolDamagesFragment.LEFT_SIDE_PHOTO)
//                || photoType.equals(NewEbolDamagesFragment.ADDITIONAL_PHOTO_FIRST)
//                || photoType.equals(NewEbolDamagesFragment.ADDITIONAL_PHOTO_SECOND)) {
//            getActivity().onBackPressed();
//        } else {
//            ((NewEbolCameraAcitvity) getActivity()).showCameraFragment(nextType);
//        }
//    }
//
//    @OnClick(R.id.addNotesP)
//    void addNotesPClick(View v){
//        notesView.setVisibility(View.VISIBLE);
//        notesText.setText(tempPhoto.getNote());
//    }
//
//    @OnClick(R.id.saveNote)
//    void saveNoteClick(View v){
//        tempPhoto.setNote(notesText.getText().toString());
//        notesView.setVisibility(View.GONE);
//        repo.repoCarPhoto.updateVehiclePhoto(tempPhoto);
//    }
//
//    @OnClick(R.id.markDamageP)
//    void markDamagePClick(View v){
//        ((NewEbolCameraAcitvity)getActivity()).showMarkDamagesFragment();
//    }
//
//    @OnClick(R.id.retakePhotoP)
//    void retakePhotoPClick(View v){
//        repo.repoCarPhoto.deleteTempVehiclePhoto();
//        ((NewEbolCameraAcitvity)getActivity()).showCameraFragment(photoType);
//    }
//
//    @OnClick(R.id.cancel)
//    void cancelClick(View view) {
//        getActivity().finish();
//    }
}
