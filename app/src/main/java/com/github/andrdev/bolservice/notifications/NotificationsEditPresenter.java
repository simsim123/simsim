package com.github.andrdev.bolservice.notifications;

import com.github.andrdev.bolservice.model.responses.NotificationsMarkResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;


public abstract class NotificationsEditPresenter extends MvpBasePresenter<NotificationsEditView> {
    public abstract void getData();

    public abstract void getDataRequestSuccess(NotificationsResponse notificationsResponse);

    public abstract void getDataRequestFailed(Throwable throwable);

    public abstract void deleteNotifications(List<Long> notificationIds);

    public abstract void markAsReadNotifSuccess(NotificationsMarkResponse notificationsMarkResponse);
}
