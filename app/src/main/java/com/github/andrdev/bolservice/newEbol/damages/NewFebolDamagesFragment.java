package com.github.andrdev.bolservice.newEbol.damages;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.github.andrdev.bolservice.view.vehicleTools.VehicleToolsWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewFebolDamagesFragment extends MvpFragment<NewEbolDamagesView, NewEbolDamagesPresenter>
        implements NewEbolDamagesView {

    int selectedVehicleId;
    VehicleInfo currentVehicle;

    @Bind(R.id.carPhotos)
    VehiclePhotosWidget vehiclePhotos;
    @Bind(R.id.carTools)
    VehicleToolsWidget vehicleTools;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_damages_f, container, false);
        ButterKnife.bind(this, view);
        selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        return view;
    }

    @Override
    public NewEbolDamagesPresenter createPresenter() {
        return new NewEbolDamagesPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        getPresenter().getVehicle(selectedVehicleId);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void setCurrentVehicle(VehicleInfo vehicle) {
        currentVehicle = vehicle;
    }

    @Override
    public void setActivityTitle(String title, String vinNumber) {
        ((NewEbolVehicleDamagesActivity)getActivity()).setTitle(title, vinNumber);
    }

    @Override
    public void setDamages(Damages damages) {
        if(damages != null) {
            vehiclePhotos.setFieldsFromDb(damages);
            vehicleTools.setFieldsFromDb(damages);
        }
        vehiclePhotos.setSelectedVehicleId(selectedVehicleId);
    }


    @OnClick(R.id.save)
    void saveClick(View view) {
        vehicleTools.setToolsToDamages(currentVehicle.getDamages());
        getPresenter().updateVehicle(currentVehicle);
        //todo see where it gets us
//        getPresenter().updateDamages(currentVehicle.getDamages());
    }

    @Override
    public void backToEbol() {
        getActivity().onBackPressed();
    }
}

