package com.github.andrdev.bolservice.sign;

import android.graphics.Bitmap;
import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.io.File;

public class SignPresenter extends MvpBasePresenter<SignView>{

    SignDataProvider dataProvider;

    @Override
    public void attachView(SignView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new SignDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }


    public void signSaveClick() {
        if (!isViewAttached()) {
            return;
        }
        Bitmap bitmap = getView().getSignOnBitmap();
        String ebolId = getView().getEbolId();
        String signType = getView().getSignatureType();

        dataProvider.saveSignatureToFile(bitmap, getFilePath(ebolId, signType), this::saveSignToDb);
    }

    private void saveSignToDb(String signPath) {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getEbol(bol -> saveSignPath(bol, signPath));
    }

    private void saveSignPath(NewEbol ebol, String signPath) {
        if (!isViewAttached()) {
            return;
        }
        String signType = getView().getSignatureType();
        setSignatureToEbol(ebol, signPath, signType);
        Log.v("dreesvsig", "" + (ebol.getId() != 0 ? ebol.getId() : "nyull"));
        Log.v("dreesvsig", ebol.getOrderId()!=null?ebol.getOrderId():"nyull");
        dataProvider.saveOrUpdateEbol(ebol, this::backToEbol);
    }

    private void backToEbol(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().backToEbol();
    }

    private void setSignatureToEbol(NewEbol ebol, String signPath, String signatureType) {
        switch (signatureType) {
            case "customerSign":
                ebol.setCustomerSignPath(signPath);
                ebol.setUnableToSign(false);
                Log.d("dree", signPath);
                break;
            case "driverSign":
                ebol.setDriverSignPath(signPath);
                Log.d("dree", signPath);
                break;
            case "deliveryDriverSign":
                ebol.setDeliveryDriverSignPath(signPath);
                Log.d("dree", "signgdwp");
                break;
            case "deliveryCustomerSign":
                ebol.setDeliveryCustomerSignPath(signPath);
                ebol.setUnableToSign(false);
                Log.d("dree", "signgdwp");
                break;
            default:
        }
    }

    public String getFilePath(String ebolId, String signatureType) {
        File path = new File(getView().getViewContext().getFilesDir()+"/"+ebolId
                + signatureType + ".png");
        return path.toString();
    }

}
