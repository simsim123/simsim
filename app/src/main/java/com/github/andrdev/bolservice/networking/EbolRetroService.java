package com.github.andrdev.bolservice.networking;

import com.github.andrdev.bolservice.model.LoginResponse;
import com.github.andrdev.bolservice.model.responses.VehiclesResponse;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.github.andrdev.bolservice.model.responses.ConversationResponse;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;
import com.github.andrdev.bolservice.model.responses.DriversResponse;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.model.responses.MessagesMarkReadResponse;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.model.responses.NewLocationResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsMarkResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.github.andrdev.bolservice.model.responses.PostMessageResponse;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.model.responses.SubmitsResponse;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.http.QueryMap;


public interface EbolRetroService {

    @FormUrlEncoded
    @POST("/messages/new")
    void postMessage(@FieldMap Map<String, String> map, Callback<PostMessageResponse> cb);

    @FormUrlEncoded
    @POST("/locations/new")
    void locationNew(@FieldMap Map<String, String> map, Callback<NewLocationResponse> cb);

    @FormUrlEncoded
    @POST("/vinNumber/decode")
    void decodeVin(@FieldMap Map<String, String> map, Callback<VinDecodeResponse> cb);

    @FormUrlEncoded
    @POST("/invoices/new")
    void newInvoice(@FieldMap Map<String, String> map, Callback<NewInvoiceResponse> cb);

    @GET("/invoices")
    void getInvoices(@Query("status") String status, Callback<InvoicesResponse> cb);

    @GET("/notifications")
    void getNotifications(Callback<NotificationsResponse> cb);

    @FormUrlEncoded
    @POST("/profile/save")
    void saveProfile(@FieldMap Map<String, String> map, Callback<SaveProfileResponse> cb);

    @FormUrlEncoded
    @POST("/notifications/markAsRead")
    void markAsReadNotifications(@FieldMap Map<String, String> map,
                                 Callback<NotificationsMarkResponse> cb);

    @GET("/clients")
    void getClients(@Query("search") String search, Callback<ClientsResponse> cb);

    @GET("/profile")
    void getProfile(Callback<ProfileResponse> cb);

    @FormUrlEncoded
    @POST("/messages/markAsRead")
    void markAsReadMessages(@FieldMap Map<String, String> map, Callback<MessagesMarkReadResponse> cb);

    @GET("/drivers")
    void getDrivers(Callback<DriversResponse> cb);

    @FormUrlEncoded
    @POST("/drivers/create")
    void createDriver(@FieldMap Map<String, String> map, Callback<CreateDriverResponse> cb);

    @FormUrlEncoded
    @POST("/clients/create")
    void createClient(@FieldMap Map<String, String> map, Callback<SimpleResponse> cb);

    @FormUrlEncoded
    @POST("/clients/delete")
    void deleteClient(@FieldMap Map<String, String> map, Callback<SimpleResponse> cb);

    @FormUrlEncoded
    @POST("/clients/update")
    void updateClient(@FieldMap Map<String, String> map, Callback<SimpleResponse> cb);

    @FormUrlEncoded
    @POST("/drivers/update")
    void updateDriver(@FieldMap Map<String, String> map, Callback<SimpleResponse> cb);

    @FormUrlEncoded
    @POST("/drivers/delete")
    void deleteDriver(@FieldMap Map<String, String> map, Callback<SimpleResponse> cb);

    @GET("/helpCenter")
    void getHelpCenter(Callback<HelpCenterResponse> cb);

    @GET("/submits")
    void getSubmits(@Query("status") String status, Callback<SubmitsResponse> cb);

    @GET("/colleagues")
    void getColleagues (Callback<ColleaguesResponse> cb);

    @GET("/cars")
    void getVehicles(@Query("sid") String sid, Callback<VehiclesResponse> cb);

    @GET("/messages/conversationsList")
    void getConversationsList(Callback<ConversationsResponse> cb);

    @GET("/messages/conversation")
    void getConversation(@Query("cid") long cid, Callback<ConversationResponse> cb);

    @FormUrlEncoded
    @POST("/login")
    void login(@QueryMap Map<String, String> map, @Body Object object, Callback<LoginResponse> cb);
}
