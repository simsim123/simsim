package com.github.andrdev.bolservice.colleagues;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Colleague;


public class ColleaguesFragment extends BaseSearchFragment<Colleague, ColleaguesAdapter,
        BaseSearchView, ColleaguesPresenter> {

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_colleagues;
    }

    @Override
    protected void filter(Colleague item, String temp) {
        if (item.getFullNameFormatted().toLowerCase().contains(temp)
                || item.getDriverPhone().toLowerCase().contains(temp)) {
            mData.add(item);
        }
    }

    @Override
    protected void getData() {
        getPresenter().getData();
    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        getPresenter().listItemClicked(mData.get(position));
    }

    @Override
    protected ColleaguesAdapter getAdapter() {
        return new ColleaguesAdapter();
    }

    @Override
    public void showNoDataText() {

    }

    @Override
    public void showRequestFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void openListItem() {
        ((ColleaguesActivity) getActivity()).showColleagueDetailsFragment();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @NonNull
    @Override
    public ColleaguesPresenter createPresenter() {
        return new ColleaguesPresenterImpl();
    }
}
