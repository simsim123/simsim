package com.github.andrdev.bolservice.newEbol.damages;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.Damages;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

public class NewEbolDamagesDataProvider implements DataProvider {

    private DbHelper dbHelper;

    @Override
    public void deInit() {
        dbHelper.deInit();
    }

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void getVehicleInfoById(int selectedVehicleId, DataCb<VehicleInfo> callback) {
        dbHelper.getVehicleById(selectedVehicleId, callback::returnData);
    }

    public void updateDamages(Damages damages, int vehicleId, DataCb<Boolean> callback) {
        dbHelper.updateDamages(damages, vehicleId, callback::returnData);
    }

    public void updateVehicle(VehicleInfo vehicle, DataCb<Boolean> callback) {
        dbHelper.updateVehicle(vehicle, callback::returnData);
    }
}
