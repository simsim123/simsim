//package com.github.andrdev.ebolservice.networking;
//
//import android.util.Base64;
//import android.util.Log;
//
//import com.github.andrdev.ebolservice.model.LoginRequest;
//import com.github.andrdev.ebolservice.model.LoginResponse;
//import com.github.andrdev.ebolservice.model.requests.ClientCreateRequest;
//import com.github.andrdev.ebolservice.model.requests.MessageRequest;
//import com.github.andrdev.ebolservice.model.responses.CarsResponse;
//import com.github.andrdev.ebolservice.model.requests.ClientUpdateRequest;
//import com.github.andrdev.ebolservice.model.responses.ClientsResponse;
//import com.github.andrdev.ebolservice.model.responses.ColleaguesResponse;
//import com.github.andrdev.ebolservice.model.responses.ConversationResponse;
//import com.github.andrdev.ebolservice.model.responses.ConversationsResponse;
//import com.github.andrdev.ebolservice.model.requests.CreateDriverRequest;
//import com.github.andrdev.ebolservice.model.responses.CreateDriverResponse;
//import com.github.andrdev.ebolservice.model.requests.DriverUpdateRequest;
//import com.github.andrdev.ebolservice.model.responses.DriversResponse;
//import com.github.andrdev.ebolservice.model.responses.HelpCenterResponse;
//import com.github.andrdev.ebolservice.model.requests.InvoiceRequest;
//import com.github.andrdev.ebolservice.model.responses.InvoicesResponse;
//import com.github.andrdev.ebolservice.model.responses.MessagesMarkReadResponse;
//import com.github.andrdev.ebolservice.model.responses.NewInvoiceResponse;
//import com.github.andrdev.ebolservice.model.responses.NewLocationResponse;
//import com.github.andrdev.ebolservice.model.responses.NotificationsMarkResponse;
//import com.github.andrdev.ebolservice.model.responses.NotificationsResponse;
//import com.github.andrdev.ebolservice.model.responses.Places;
//import com.github.andrdev.ebolservice.model.responses.PostMessageResponse;
//import com.github.andrdev.ebolservice.model.responses.Predictions;
//import com.github.andrdev.ebolservice.model.responses.ProfileResponse;
//import com.github.andrdev.ebolservice.model.requests.SaveProfileRequest;
//import com.github.andrdev.ebolservice.model.responses.SaveProfileResponse;
//import com.github.andrdev.ebolservice.model.responses.SimpleResponse;
//import com.github.andrdev.ebolservice.model.responses.SubmitsResponse;
//import com.github.andrdev.ebolservice.model.responses.VinDecodeResponse;
//import com.squareup.okhttp.OkHttpClient;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import retrofit.Callback;
//import retrofit.RequestInterceptor;
//import retrofit.RestAdapter;
//import retrofit.android.AndroidLog;
//import retrofit.client.OkClient;
//
///**
// * Created by taiyokaze on 9/17/15.
// */
//public class EbolNetworkWorker {
//
//    private final static String TEST_URL = "http://dbl.perevertajlo.com/api";
//
//    private final static String LIVE_URL = "http://boldirect.com/api";
//    private final static String GOOGLE_URL = "https://maps.googleapis.com/maps/api";
//    private static EbolNetworkWorker ebolNetworkWorker;
//
//    private static volatile EbolRetroService ebolRetroService;
//    private static volatile GoogleApiRetroService googleApiRetroService;
//
//    private EbolNetworkWorker() {
//        createRestWorker();
//    }
//
//    public static EbolNetworkWorker getInstance() {
//        if (ebolNetworkWorker == null) {
//            synchronized (EbolNetworkWorker.class) {
//                if (ebolNetworkWorker == null) {
//                    ebolNetworkWorker = new EbolNetworkWorker();
//                }
//            }
//        }
//        return ebolNetworkWorker;
//    }
//
//    private void createRestWorker() {
////        RestAdapter retrofit = new RestAdapter.Builder()
////                .setEndpoint(LIVE_URL)
////                .setClient(new OkClient(new OkHttpClient()))
////                .setLogLevel(RestAdapter.LogLevel.FULL).setClient(new OkClient(new OkHttpClient()))
////                .setLog(new AndroidLog("DREE"))
////                .build();
////
////        ebolRetroService = retrofit.create(EbolRetroService.class);
//    }
//
//    public static void setAuthenticatedRestWorker(LoginRequest loginRequest) {
//        final String credentials = loginRequest.getLogin() + ":" + loginRequest.getPassword();
//        RestAdapter retrofit = new RestAdapter.Builder()
//                .setEndpoint(LIVE_URL)
//                .setClient(new OkClient(new OkHttpClient()))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setLog(new AndroidLog("DREE"))
//                .setRequestInterceptor(new RequestInterceptor() {
//                    @Override
//                    public void intercept(RequestInterceptor.RequestFacade request) {
//                        // create Base64 encodet string
//                        String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                        request.addHeader("Authorization", string);
////                        request.addHeader("Accept", "application/json");
//                    }
//                })
//                .build();
//        RestAdapter gRetrofit = new RestAdapter.Builder()
//                .setEndpoint(GOOGLE_URL)
//                .setClient(new OkClient(new OkHttpClient()))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setLog(new AndroidLog("DREE"))
//                .build();
//        String string2 = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//        Log.d("dreeLogin", string2 + " " + credentials);
//        ebolRetroService = retrofit.create(EbolRetroService.class);
//        googleApiRetroService = gRetrofit.create(GoogleApiRetroService.class);
//    }
//
////    public void postMessage(MessageRequest messageRequest, Callback<PostMessageResponse> cb) {
//public void postMessage(String message, String uid, Callback<PostMessageResponse> cb) {
//    Map<String, String> map = new HashMap<>();
//        map.put("message", message);
//        map.put("uid", uid);
//        ebolRetroService.postMessage(map, cb);
//    }
//
//    public void locationNew(String longitude, String latitude, Callback<NewLocationResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("longitude", longitude);
//        map.put("latitude", latitude);
//        ebolRetroService.locationNew(map, cb);
//    }
//
//    public void decodeVin(String vinNumber, Callback<VinDecodeResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("vinNumber", vinNumber);
//        ebolRetroService.decodeVin(map, cb);
//    }
//
//    public void newInvoice(InvoiceRequest invoice, Callback<NewInvoiceResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("amount", invoice.getAmount());
//        map.put("invoiceId", invoice.getInvoiceId());
//        map.put("paymentTerms", invoice.getPaymentTerms());
//        map.put("notes", invoice.getNotes());
//        map.put("fullname", invoice.getFullName());
//        map.put("orderId", invoice.getOrderId());
//        map.put("address", invoice.getAddress());
//        map.put("city", invoice.getCity());
//        map.put("state", invoice.getState());
//        map.put("zip", invoice.getZip());
//        map.put("phone", invoice.getPhone());
//        map.put("cell", invoice.getCell());
//        map.put("vCount", invoice.getvCount());
//        ebolRetroService.newInvoice(map, cb);
//    }
//    public void getInvoices(String status, Callback<InvoicesResponse> cb) {
//        ebolRetroService.getInvoices(status, cb);
//    }
//    public void getNotifications(Callback<NotificationsResponse> cb) {
//        ebolRetroService.getNotifications(cb);
//    }
//    public void getClients(String search, Callback<ClientsResponse> cb) {
//        ebolRetroService.getClients(search, cb);
//    }
//    public void getProfile(Callback<ProfileResponse> cb) {
//        ebolRetroService.getProfile(cb);
//    }
//
//    public void saveProfile(SaveProfileRequest profile, Callback<SaveProfileResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("firstname", profile.getFirstName());
//        map.put("lastname", profile.getLastName());
//        map.put("billingFirstname", profile.getBillingFirstName());
//        map.put("billingLastname", profile.getBillingLastName());
//        map.put("billingAddress", profile.getBillingAddress());
//        map.put("billingCity", profile.getBillingCity());
//        map.put("billingState", profile.getBillingState());
//        map.put("billingZip", profile.getBillingZip());
//        map.put("billingPhone", profile.getBillingPhone());
//        map.put("billingFax", profile.getBillingFax());
//        map.put("billingEmail", profile.getBillingEmail());
//        map.put("companyName", profile.getCompanyName());
//        map.put("companyAddress", profile.getCompanyAddress());
//        map.put("companyCity", profile.getCompanyCity());
//        map.put("companyState", profile.getCompanyState());
//        map.put("companyZip", profile.getCompanyZip());
//        map.put("companyPhone", profile.getCompanyPhone());
//        map.put("companyFax", profile.getCompanyFax());
//        map.put("companyEma", profile.getCompanyEma());
//        ebolRetroService.saveProfile(map, cb);
//    }
//
//    public void markAsReadNotifications(List<Long> notificationIds, Callback<NotificationsMarkResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        String notifications = "[";
//        notifications += notificationIds.get(0).toString();
//        for (int i = 1; i < notificationIds.size(); i++) {
//            notifications+=","+notificationIds.get(0).toString();
//        }
//        notifications+="]";
//        map.put("nids", notifications);
//        ebolRetroService.markAsReadNotifications(map, cb);
//    }
//
//    public void markAsReadMessages(String cid, String mid, Callback<MessagesMarkReadResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("cid", cid);
//        map.put("mid", mid);
//        ebolRetroService.markAsReadMessages(map, cb);
//    }
//
//    public void getDrivers(Callback<DriversResponse> cb) {
//        ebolRetroService.getDrivers(cb);
//    }
//
//    public void createDriver(CreateDriverRequest driver, Callback<CreateDriverResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("username", driver.getUsername());
//        map.put("password", driver.getPassword());
//        map.put("firstname", driver.getFirstName());
//        map.put("lastname", driver.getLastName());
//        map.put("driverEmail", driver.getEmail());
//        map.put("driverPhone", driver.getPhoneNumber());
//        ebolRetroService.createDriver(map, cb);
//    }
//    public void createClient(ClientCreateRequest client, Callback<SimpleResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("customerName", client.getCustomerName());
//        map.put("address", client.getAddress());
//        map.put("city", client.getCity());
//        map.put("tel", client.getTel());
//        map.put("fax", client.getFax());
//        map.put("zip", client.getZip());
//        map.put("email", client.getEmail());
//        map.put("state", client.getState());
//        ebolRetroService.createClient(map, cb);
//    }
//
//    public void deleteClient(String cid, Callback<SimpleResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("cid", cid);
//        ebolRetroService.deleteClient(map, cb);
//    }
//
//    public void updateClient(ClientUpdateRequest client, Callback<SimpleResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("cid", client.getCid());
//        map.put("customerName", client.getCustomerName());
//        map.put("address", client.getAddress());
//        map.put("city", client.getCity());
//        map.put("tel", client.getTel());
//        map.put("fax", client.getFax());
//        map.put("zip", client.getZip());
//        map.put("email", client.getEmail());
//        map.put("state", client.getState());
//        ebolRetroService.updateClient(map, cb);
//    }
//    public void updateDriver(DriverUpdateRequest driver, Callback<SimpleResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("did", driver.getDid());
//        map.put("firstname", driver.getFirstName());
//        map.put("lastname", driver.getLastName());
//        map.put("driverEmail", driver.getDriverEmail());
//        map.put("driverPhone", driver.getDriverPhone());
//        ebolRetroService.updateDriver(map, cb);
//    }
//
//    public void deleteDriver(String did, Callback<SimpleResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("did", did);
//        ebolRetroService.deleteDriver(map, cb);
//    }
//
//    public void getHelpCenter(Callback<HelpCenterResponse> cb) {
//        ebolRetroService.getHelpCenter(cb);
//    }
//
//    public void getSubmits(String status, Callback<SubmitsResponse> cb) {
//        ebolRetroService.getSubmits(status, cb);
//    }
//
//    public void getColleagues(Callback<ColleaguesResponse> cb) {
//        ebolRetroService.getColleagues(cb);
//    }
//
//    public void getVehicles(String sid, Callback<CarsResponse> cb) {
//        ebolRetroService.getVehicles(sid, cb);
//    }
//    public void getConversationsList(Callback<ConversationsResponse> cb) {
//        ebolRetroService.getConversationsList(cb);
//    }
//    public void getConversation(long cid, Callback<ConversationResponse> cb) {
//        ebolRetroService.getConversation(cid, cb);
//    }
//
//
//    public void login(LoginRequest loginRequest, Callback<LoginResponse> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("username", loginRequest.getLogin());
//        map.put("firstname", loginRequest.getPassword());
//        ebolRetroService.login(map, new Object(), cb);
//    }
//
//    public void requestAddressByCordinates(String longLat, Callback<Places> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("latlng", longLat);
//        map.put("location_type", "ROOFTOP");
//        map.put("result_type", "street_address");
//        map.put("key", "AIzaSyAozoN8hLVF5hzLHD6_XtZ_Gjful1N9oas");
//        googleApiRetroService.requestAddressByCordinates(map, new Object(), cb);
//    }
//
//    public void requestAddressAutocomplete(String text, Callback<Predictions> cb) {
//        Map<String, String> map = new HashMap<>();
//        map.put("input", text);
//        map.put("types", "address");
//        map.put("components", "country:us");
////        map.put("result_type", "street_address");
//        map.put("key", "AIzaSyAozoN8hLVF5hzLHD6_XtZ_Gjful1N9oas");
//        googleApiRetroService.requestAddressAutocomplete(map, new Object(), cb);
//    }
//}
//
