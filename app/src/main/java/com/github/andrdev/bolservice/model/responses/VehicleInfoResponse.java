package com.github.andrdev.bolservice.model.responses;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;

import java.util.List;

public class VehicleInfoResponse {

    List<VehicleInfo> vehicleInfos;

    public VehicleInfoResponse() {
    }

    public List<VehicleInfo> getVehicleInfos() {
        return vehicleInfos;
    }

    public void setVehicleInfos(List<VehicleInfo> vehicleInfos) {
        this.vehicleInfos = vehicleInfos;
    }
}
