package com.github.andrdev.bolservice.view.vehicleToolsNe;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class VehicleToolsNePresenter extends MvpBasePresenter<VehicleToolsNeView>{

    VehicleToolsNeDataProvider dataProvider;

    @Override
    public void attachView(VehicleToolsNeView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new VehicleToolsNeDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

}
