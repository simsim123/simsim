package com.github.andrdev.bolservice.history.details.vehicleInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface HistoryViView extends MvpView {
    void setVehicleInfo(VehicleInfo vehicleInfo);

    Context getViewContext();
}
