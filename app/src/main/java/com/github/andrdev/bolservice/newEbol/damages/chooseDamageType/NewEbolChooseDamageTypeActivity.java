package com.github.andrdev.bolservice.newEbol.damages.chooseDamageType;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;


public class NewEbolChooseDamageTypeActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            showInvoicesFragment();
        }
    }

    private void showInvoicesFragment() {
        NewEbolChooseDamageTypeFragment fragment = new NewEbolChooseDamageTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putAll(getIntent().getExtras());
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();
    }
}
