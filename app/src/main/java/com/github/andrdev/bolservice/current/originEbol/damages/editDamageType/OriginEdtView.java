package com.github.andrdev.bolservice.current.originEbol.damages.editDamageType;

import android.content.Context;

import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.hannesdorfmann.mosby.mvp.MvpView;

public interface OriginEdtView extends MvpView {

    void setSaveResultAndFinish();

    void setDeleteResultAndFinish();

    Context getViewContext();

    void setMarkedDamage(MarkedDamage markedDamage);

    void initViews();

    VehiclePhoto getVehiclePhoto();

    void setVehiclePhoto(VehiclePhoto vehiclePhoto);
}
