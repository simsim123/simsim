package com.github.andrdev.bolservice.colleagues.edit;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ColleagueEditDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }

    public void saveColleague(Colleague colleague, DataCb<Boolean> successCb) {
        dbHelper.saveColleague(colleague, successCb::returnData);
    }

    public void getSavedColleague(DataCb<Colleague> successCb) {
        dbHelper.getSavedColleague(successCb::returnData);
    }

    public void updateDriver(DriverUpdateRequest request,
                             DataCb<SimpleResponse> successCb,
                             DataCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().updateDriver(request, successCb::returnData, failedCb::returnData);
    }
}
