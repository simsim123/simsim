package com.github.andrdev.bolservice.view.vehiclePhotos;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface VehiclePhotosView extends MvpView {
    Context getViewContext();
}
