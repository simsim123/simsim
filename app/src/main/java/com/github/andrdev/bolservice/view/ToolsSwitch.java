package com.github.andrdev.bolservice.view;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.andrdev.bolservice.R;


public class ToolsSwitch extends RelativeLayout {

    boolean state;

    View greenYes;
    View redNo;

    public ToolsSwitch(Context context) {
        super(context);
        initViews(context);
    }

    public ToolsSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public ToolsSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.switch_car, this, true);
        greenYes = findViewById(R.id.greenYes);
        redNo = findViewById(R.id.redNo);
        setOnClickListener((v) -> flipSwitch());
    }

    void flipSwitch() {
        if (state) {
            redNo.setVisibility(VISIBLE);
            greenYes.setVisibility(INVISIBLE);
        } else {
            redNo.setVisibility(INVISIBLE);
            greenYes.setVisibility(VISIBLE);
        }
        state = !state;
    }

    public void setState(boolean stateNew) {
        if (stateNew) {
            redNo.setVisibility(INVISIBLE);
            greenYes.setVisibility(VISIBLE);
        } else {
            redNo.setVisibility(VISIBLE);
            greenYes.setVisibility(INVISIBLE);
        }
        state = stateNew;
    }

    public boolean getState() {
        return state;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable state = super.onSaveInstanceState();
        SavedState ss = new SavedState(state);
        ss.state = this.state;
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(state);
        this.state = ss.state;
    }

    public static class SavedState extends BaseSavedState {
        boolean state;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt() == 1;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state ? 1 : 0);
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
