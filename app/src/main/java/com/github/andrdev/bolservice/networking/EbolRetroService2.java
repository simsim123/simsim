package com.github.andrdev.bolservice.networking;

import com.github.andrdev.bolservice.model.LoginResponse;
import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.model.responses.EbolsResponse;
import com.github.andrdev.bolservice.model.responses.NewDamageResponse;
import com.github.andrdev.bolservice.model.responses.VehicleInfoResponse;
import com.github.andrdev.bolservice.model.responses.VehiclesResponse;
import com.github.andrdev.bolservice.model.responses.ClientsResponse;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.github.andrdev.bolservice.model.responses.ConversationResponse;
import com.github.andrdev.bolservice.model.responses.ConversationsResponse;
import com.github.andrdev.bolservice.model.responses.CreateDriverResponse;
import com.github.andrdev.bolservice.model.responses.DriversResponse;
import com.github.andrdev.bolservice.model.responses.HelpCenterResponse;
import com.github.andrdev.bolservice.model.responses.InvoicesResponse;
import com.github.andrdev.bolservice.model.responses.MessagesMarkReadResponse;
import com.github.andrdev.bolservice.model.responses.NewInvoiceResponse;
import com.github.andrdev.bolservice.model.responses.NewLocationResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsMarkResponse;
import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.github.andrdev.bolservice.model.responses.PostMessageResponse;
import com.github.andrdev.bolservice.model.responses.ProfileResponse;
import com.github.andrdev.bolservice.model.responses.SaveProfileResponse;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.model.responses.SubmitsResponse;
import com.github.andrdev.bolservice.model.responses.VinDecodeResponse;
import com.squareup.okhttp.RequestBody;

import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import rx.Observable;


public interface EbolRetroService2 {

    @FormUrlEncoded
    @POST("/locations/new")
    Call<NewLocationResponse> locationNew(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/vinNumber/decode")
    Observable<VinDecodeResponse> decodeVin(@FieldMap Map<String, String> map);

    @GET("api/submits")
    Observable<EbolsResponse> getSubmits(@Query("status") String status);

    @GET("api/cars")
    Observable<VehicleInfoResponse> getVehicles(@Query("sid") String sid);

    @GET("api/cars/new")
    Observable<SimpleResponse> newVehicle(@FieldMap Map<String, String> map);

    @Multipart
    @GET("cars/uploadSide")
    Observable<SimpleResponse> photoUpload(@Query("sid") String sid, @Query("side") String side,
                                           @Part("file\"; filename=\"photo ") RequestBody photo);

    @Multipart
    @POST("share")
    Observable<SimpleResponse> share(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/invoices/new")
    Observable<NewInvoiceResponse> newInvoice(@FieldMap Map<String, String> map);

    @GET("api/invoices")
    Observable<InvoicesResponse> getInvoices(@Query("status") String status);

    @GET("api/notifications")
    Observable<NotificationsResponse> getNotifications();

    @FormUrlEncoded
    @POST("api/notifications/markAsRead")
    Observable<NotificationsMarkResponse> markAsReadNotifications(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/profile/save")
    Observable<SaveProfileResponse> saveProfile(@FieldMap Map<String, String> map);

    @GET("api/profile")
    Observable<ProfileResponse> getProfile();

    @GET("api/clients")
    Observable<ClientsResponse> getClients(@Query("search") String search);

    @FormUrlEncoded
    @POST("api/clients/create")
    Observable<SimpleResponse> createClient(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/clients/update")
    Observable<SimpleResponse> updateClient(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/clients/delete")
    Observable<SimpleResponse> deleteClient(@FieldMap Map<String, String> map);

    @GET("api/colleagues")
    Observable<ColleaguesResponse> getColleagues ();

    @GET("api/drivers")
    Observable<DriversResponse> getDrivers();

    @FormUrlEncoded
    @POST("api/drivers/create")
    Observable<CreateDriverResponse> createDriver(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/drivers/update")
    Observable<SimpleResponse>  updateDriver(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/drivers/delete")
    Observable<SimpleResponse>  deleteDriver(@FieldMap Map<String, String> map);

    @GET("api/helpCenter")
    Observable<HelpCenterResponse> getHelpCenter();

    @GET("api/messages/conversationsList")
    Observable<ConversationsResponse> getConversationsList();

    @GET("api/messages/conversation")
    Observable<ConversationResponse> getConversation(@Query("cid") long cid);

    @FormUrlEncoded
    @POST("api/messages/new")
    Observable<PostMessageResponse> postMessage(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/messages/markAsRead")
    Observable<MessagesMarkReadResponse> markAsReadMessages(@FieldMap Map<String, String> map);

    @GET("api/damages")
    Observable<DamagesResponse> getDamages(@Query("cid") String cid);

    @FormUrlEncoded
    @POST("/damages/create")
    Observable<NewDamageResponse> postDamage(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("/login")
    Call<LoginResponse> login(@QueryMap Map<String, String> map, @Body Object object);
}
