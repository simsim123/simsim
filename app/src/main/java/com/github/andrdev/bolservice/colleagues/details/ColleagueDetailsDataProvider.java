package com.github.andrdev.bolservice.colleagues.details;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Colleague;

public class ColleagueDetailsDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }


    public void getSavedColleague(DataCb<Colleague> successCb) {
        dbHelper.getSavedColleague(successCb::returnData);
    }

    public void getIsUserBoss(DataCb<Boolean> successCb) {
        dbHelper.getIsUserBoss(successCb::returnData);
    }
}
