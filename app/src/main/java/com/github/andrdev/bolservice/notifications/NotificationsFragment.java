package com.github.andrdev.bolservice.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.Notification;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NotificationsFragment extends MvpFragment<NotificationsView, NotificationsPresenter>
        implements NotificationsView {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    
    NotificationsAdapter notificationsAdapter;

    List<Notification> notifications = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        ButterKnife.bind(this, view);
        initRecycler();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }


    @Override
    public void getData() {
        Log.d("dree", "getData");
        getPresenter().getData();
    }

    private void initRecycler() {
        notificationsAdapter = new NotificationsAdapter();
        notificationsAdapter.setData(notifications);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(notificationsAdapter);
    }

    @Override
    public void setData(List<Notification> data) {
        this.notifications = data;
        notificationsAdapter.setData(notifications);
        notificationsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showFailedToast() {
        Toast.makeText(getContext(), "Request Failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public NotificationsPresenter createPresenter() {
        return new NotificationsPresenterImpl();
    }
}
