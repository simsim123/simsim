package com.github.andrdev.bolservice.clients;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.clients.create.ClientNewFragment;
import com.github.andrdev.bolservice.clients.details.ClientDetailsFragment;
import com.github.andrdev.bolservice.clients.edit.ClientEditFragment;
import com.github.andrdev.bolservice.model.Profile;

import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;


public class ClientsActivity  extends BaseDrawerActivity {

    public static final String TAG_CLIENTS_FRAGMENT = "clientsFragment";
    public static final String TAG_CLIENT_NEW_FRAGMENT = "clientNewFragment";
    public static final String TAG_CLIENT_EDIT_FRAGMENT = "clientEditFragment";
    public static final String TAG_CLIENT_DETAILS_FRAGMENT = "clientDetailsFragment";

    @Bind(R.id.newClient)
    TextView newClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showClientsFragment();
        }
        ifBossShowNewButton();
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
    }

    private void ifBossShowNewButton() {
        Realm realm = Realm.getInstance(getApplicationContext());
        boolean userIsBoss = realm.where(Profile.class).findFirst().getIsBoss();
        if (!userIsBoss) {
            newClient.setVisibility(View.INVISIBLE);
        }
        realm.close();
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        return  () -> {
            FragmentManager manager = getSupportFragmentManager();
            if (manager != null) {
                Fragment fragment = manager.findFragmentByTag(TAG_CLIENTS_FRAGMENT);
                if (fragment != null && fragment.isVisible()) {
                    fragment.onResume();
                }
            }
        };
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 1;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_clients;
    }

    public void showClientsFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new ClientsFragment(), TAG_CLIENTS_FRAGMENT).commit();
    }

    @OnClick(R.id.newClient)
    public void showClientNewFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ClientNewFragment(), TAG_CLIENT_NEW_FRAGMENT)
                .addToBackStack(TAG_CLIENT_NEW_FRAGMENT).commit();
    }

    public void showClientEditFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ClientEditFragment(), TAG_CLIENT_EDIT_FRAGMENT)
                .addToBackStack(TAG_CLIENT_EDIT_FRAGMENT).commit();
    }

    public void showClientDetailsFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ClientDetailsFragment(), TAG_CLIENT_DETAILS_FRAGMENT)
                .addToBackStack(TAG_CLIENT_DETAILS_FRAGMENT).commit();
    }
}
