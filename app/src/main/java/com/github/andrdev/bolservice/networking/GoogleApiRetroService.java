package com.github.andrdev.bolservice.networking;

import com.github.andrdev.bolservice.model.responses.NewDamageResponse;
import com.github.andrdev.bolservice.model.responses.PlaceInfo;
import com.github.andrdev.bolservice.model.responses.Places;
import com.github.andrdev.bolservice.model.responses.Predictions;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.QueryMap;
import rx.Observable;


interface GoogleApiRetroService {

    @POST("geocode/json")
    Observable<Places> requestAddressByCordinates(@QueryMap Map<String, String> map);


    @POST("place/autocomplete/json")
    Observable<Predictions> requestAddressAutocomplete(@QueryMap Map<String, String> map);

    @POST("place/details/json")
    Observable<PlaceInfo> requestPlaceInfoByPlaceId(@QueryMap Map<String, String> map);
}
