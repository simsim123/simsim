package com.github.andrdev.bolservice.networking;

import android.support.annotation.NonNull;

import com.github.andrdev.bolservice.model.requests.CarCreateRequest;
import com.github.andrdev.bolservice.model.requests.ClientCreateRequest;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.requests.CreateDriverRequest;
import com.github.andrdev.bolservice.model.requests.DriverUpdateRequest;
import com.github.andrdev.bolservice.model.requests.InvoiceRequest;
import com.github.andrdev.bolservice.model.requests.MessageRequest;
import com.github.andrdev.bolservice.model.requests.NewDamageRequest;
import com.github.andrdev.bolservice.model.requests.SaveProfileRequest;
import com.github.andrdev.bolservice.model.requests.ShareRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UtilsNetRequest {

    @NonNull
    public static Map<String, String> getVinNumberParams(String vinNumber) {
        Map<String, String> map = new HashMap<>();
        map.put("vinNumber", vinNumber);
        return map;
    }

    @NonNull
    public static Map<String, String> getNewInvoiceParams(InvoiceRequest invoice) {
        Map<String, String> map = new HashMap<>();
        map.put("amount", invoice.getAmount());
        map.put("invoiceId", invoice.getInvoiceId());
        map.put("paymentTerms", invoice.getPaymentTerms());
        map.put("notes", invoice.getNotes());
        map.put("fullname", invoice.getFullName());
        map.put("orderId", invoice.getOrderId());
        map.put("address", invoice.getAddress());
        map.put("city", invoice.getCity());
        map.put("state", invoice.getState());
        map.put("zip", invoice.getZip());
        map.put("phone", invoice.getPhone());
        map.put("cell", invoice.getCell());
        map.put("vCount", invoice.getvCount());
        return map;
    }

    @NonNull
    public static Map<String, String> getMarkAsReadNotificationsParams(List<Long> notificationIds) {
        Map<String, String> map = new HashMap<>();
        String notifications = "[";
        notifications += notificationIds.get(0).toString();
        for (int i = 1; i < notificationIds.size(); i++) {
            notifications+=","+notificationIds.get(0).toString();
        }
        notifications+="]";
        map.put("nids", notifications);
        return map;
    }

    @NonNull
    public static Map<String, String> getSaveProfileParams(SaveProfileRequest profile) {
        Map<String, String> map = new HashMap<>();
        map.put("firstname", profile.getFirstName());
        map.put("lastname", profile.getLastName());
        map.put("billingFirstname", profile.getBillingFirstName());
        map.put("billingLastname", profile.getBillingLastName());
        map.put("billingAddress", profile.getBillingAddress());
        map.put("billingCity", profile.getBillingCity());
        map.put("billingState", profile.getBillingState());
        map.put("billingZip", profile.getBillingZip());
        map.put("billingPhone", profile.getBillingPhone());
        map.put("billingFax", profile.getBillingFax());
        map.put("billingEmail", profile.getBillingEmail());
        map.put("companyName", profile.getCompanyName());
        map.put("companyAddress", profile.getCompanyAddress());
        map.put("companyCity", profile.getCompanyCity());
        map.put("companyState", profile.getCompanyState());
        map.put("companyZip", profile.getCompanyZip());
        map.put("companyPhone", profile.getCompanyPhone());
        map.put("companyFax", profile.getCompanyFax());
        map.put("companyEma", profile.getCompanyEma());
        return map;
    }

    @NonNull
    public static Map<String, String> getMarkAsReadMessagesParams(String cid, String mid) {
        Map<String, String> map = new HashMap<>();
        map.put("cid", cid);
        map.put("mid", mid);
        return map;
    }

    @NonNull
    public static Map<String, String> getCreateClientParams(ClientCreateRequest client) {
        Map<String, String> map = new HashMap<>();
        map.put("customerName", client.getCustomerName());
        map.put("address", client.getAddress());
        map.put("city", client.getCity());
        map.put("tel", client.getTel());
        map.put("fax", client.getFax());
        map.put("zip", client.getZip());
        map.put("email", client.getEmail());
        map.put("state", client.getState());
        return map;
    }

    @NonNull
    public static Map<String, String> getUpdateClientParams(ClientUpdateRequest client) {
        Map<String, String> map = new HashMap<>();
        map.put("cid", client.getCid());
        map.put("customerName", client.getCustomerName());
        map.put("address", client.getAddress());
        map.put("city", client.getCity());
        map.put("tel", client.getTel());
        map.put("fax", client.getFax());
        map.put("zip", client.getZip());
        map.put("email", client.getEmail());
        map.put("state", client.getState());
        return map;
    }

    @NonNull
    public static Map<String, String> getDeleteClientParams(String cid) {
        Map<String, String> map = new HashMap<>();
        map.put("cid", cid);
        return map;
    }

    @NonNull
    public static Map<String, String> getCreateDriverParams(CreateDriverRequest driver) {
        Map<String, String> map = new HashMap<>();
        map.put("username", driver.getUsername());
        map.put("password", driver.getPassword());
        map.put("firstname", driver.getFirstName());
        map.put("lastname", driver.getLastName());
        map.put("driverEmail", driver.getEmail());
        map.put("driverPhone", driver.getPhoneNumber());
        return map;
    }

    @NonNull
    public static Map<String, String> getUpdateDriverParams(DriverUpdateRequest driver) {
        Map<String, String> map = new HashMap<>();
        map.put("did", driver.getDid());
        map.put("firstname", driver.getFirstName());
        map.put("lastname", driver.getLastName());
        map.put("driverEmail", driver.getDriverEmail());
        map.put("driverPhone", driver.getDriverPhone());
        return map;
    }

    @NonNull
    public static Map<String, String> getDeleteDriverParams(String did) {
        Map<String, String> map = new HashMap<>();
        map.put("did", did);
        return map;
    }

    @NonNull
    public static Map<String, String> getPostMessageParams(MessageRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("message", request.getMessage());
        map.put("uid", request.getUid());
        return map;
    }

    @NonNull
    public static Map<String, String> getPostDamagesParams(NewDamageRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("cid", request.getCid());
        map.put("code", request.getCode());
        map.put("title", request.getTitle());
        map.put("description", request.getDescription());
        map.put("positionTop", request.getPositionTop());
        map.put("positionLeft", request.getPositionLeft());
        map.put("side", request.getSide());
        return map;
    }

    public static Map<String, String> getCreateCarParams(CarCreateRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("sid", request.getSid());
        map.put("make", request.getMake());
        map.put("yearOfMake", request.getYearOfMake());
        map.put("model", request.getModel());
        map.put("color", request.getColor());
        map.put("mileage", request.getMileage());
        map.put("vinNumber", request.getVinNumber());
        map.put("plateNumber", request.getPlateNumber());
        map.put("radioPresent", request.isRadioPresent());
        map.put("manuals", request.isManuals());
        map.put("spareTire", request.isSpareTire());
        map.put("cargoCover", request.isCargoCover());
        map.put("windscreen", request.isWindscreen());
        map.put("keysCount", request.getKeysCount());
        map.put("remotesCount", request.getRemotesCount());
        map.put("headrestsCount", request.getHeadrestsCount());
        map.put("floorMats", request.getFloorMats());
        return map;
    }

    public static Map<String, String> getShareParams(ShareRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("entityId", request.getEntityId());
        map.put("toEmail", request.getToEmail());
        map.put("type", request.getType());
        return map;
    }
}
