package com.github.andrdev.bolservice.messages;

import android.content.Context;

import com.github.andrdev.bolservice.BaseSearchFragment;
import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.model.ConversationSmall;


public class MessagesFragment
        extends BaseSearchFragment<ConversationSmall, MessagesAdapter, BaseSearchView,
        MessagesPresenter> {


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_messages;
    }

    @Override
    protected void filter(ConversationSmall item, String temp) {
    }

    @Override
    protected void getData() {
        getPresenter().getConversations();
    }

//    @Override
//    protected void sendMessagesCheckRequest() {
//        getPresenter().getConversations();
//    }

    @Override
    protected void recyclerViewItemClicked(int position) {
        ConversationSmall conversation = mData.get(position);
        long conversationId = conversation.getConversationId();
        String personName = conversation.getInterlocutorFullname();
        long interlocutorId = conversation.getInterlocutorId();
        ((MessagesActivity) getActivity()).showChatFragment(personName, conversationId, interlocutorId);
    }

    @Override
    protected MessagesAdapter getAdapter() {
        return new MessagesAdapter();
    }

    @Override
    public void showNoDataText() {
    }

    @Override
    public void showRequestFailedToast() {
    }

    @Override
    public void openListItem() {

    }

    @Override
    public Context getViewContext() {
        return null;
    }

    @Override
    public MessagesPresenter createPresenter() {
        return new MessagesPresenterImpl();
    }
}
