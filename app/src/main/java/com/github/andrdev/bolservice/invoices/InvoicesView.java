package com.github.andrdev.bolservice.invoices;

import android.content.Context;

import com.github.andrdev.bolservice.model.Invoice;
import com.hannesdorfmann.mosby.mvp.MvpView;

import java.util.List;

interface InvoicesView extends MvpView {
    void setData(List<Invoice> invoices);

    void showFailedToast();

    void stopRefresh();

    Context getViewContext();

    void openListItem();

    void showRequestFailedToast();
}
