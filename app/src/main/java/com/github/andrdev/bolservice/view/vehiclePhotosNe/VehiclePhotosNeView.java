package com.github.andrdev.bolservice.view.vehiclePhotosNe;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface VehiclePhotosNeView extends MvpView {
    Context getViewContext();
}
