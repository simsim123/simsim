package com.github.andrdev.bolservice.model.responses;



public class VinDecodeResponse {
        public final String status;
        public final DecodedResult decodedResult;

        public VinDecodeResponse(String status, DecodedResult decodedResult){
            this.status = status;
            this.decodedResult = decodedResult;
        }

    public String getModel() {
        return decodedResult.model.name;
    }

    public String getVin() {
        return decodedResult.vin;
    }

    public String getStatus() {
        return status;
    }

    public String getMake() {
        return decodedResult.make.name;
    }

    public String getYear() {
        return String.valueOf(decodedResult.years[0].year);
    }

    public static final class DecodedResult {
            public final Make make;
            public final Model model;
            public final String vin;
            public final String squishVin;
            public final Year years[];

            public DecodedResult(Make make, Model model, String vin, String squishVin, Year[] years){
                this.make = make;
                this.model = model;
                this.vin = vin;
                this.squishVin = squishVin;
                this.years = years;
            }

            public static final class Make {
                public final long id;
                public final String name;
                public final String niceName;

                public Make(long id, String name, String niceName){
                    this.id = id;
                    this.name = name;
                    this.niceName = niceName;
                }
            }

            public static final class Model {
                public final String id;
                public final String name;
                public final String niceName;

                public Model(String id, String name, String niceName){
                    this.id = id;
                    this.name = name;
                    this.niceName = niceName;
                }
            }

            public static final class Year {
                public final long id;
                public final long year;

                public Year(long id, long year){
                    this.id = id;
                    this.year = year;
                }
            }
        }
    }

