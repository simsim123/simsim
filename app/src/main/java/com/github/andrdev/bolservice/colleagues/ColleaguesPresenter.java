package com.github.andrdev.bolservice.colleagues;

import com.github.andrdev.bolservice.BaseSearchView;
import com.github.andrdev.bolservice.model.Colleague;
import com.github.andrdev.bolservice.model.responses.ColleaguesResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import io.realm.Realm;


public abstract class ColleaguesPresenter extends MvpBasePresenter<BaseSearchView> {

    public abstract void getData();

    public abstract void requestFailed(Throwable throwable);

    public abstract void requestSuccess(ColleaguesResponse colleaguesResponse);

    public abstract void listItemClicked(Colleague colleague);
}
