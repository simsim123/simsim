package com.github.andrdev.bolservice.current.originEbol.vehicleInfo;

import android.os.Bundle;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public class OriginVehicleInfoPresenter extends MvpBasePresenter<OriginVehicleInfoView> {

    OriginVehicleInfoDataProvider dataProvider;

    @Override
    public void attachView(OriginVehicleInfoView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new OriginVehicleInfoDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    public void getVehicleInfo(int vehicleId) {
        dataProvider.getVehicleById(vehicleId, this::setVehicleFields);
    }

    private void setVehicleFields(VehicleInfo vehicleInfo) {
        getView().setVehicleInfo(vehicleInfo);
    }
}
