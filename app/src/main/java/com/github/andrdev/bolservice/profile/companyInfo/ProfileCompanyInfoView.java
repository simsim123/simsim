package com.github.andrdev.bolservice.profile.companyInfo;

import android.content.Context;

import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ProfileCompanyInfoView extends MvpView {

    void setProfile(Profile profile);

    void initFields();

    void showFailedToast();

    Context getViewContext();

}
