package com.github.andrdev.bolservice;

import android.app.Application;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.github.andrdev.bolservice.dashboard.DashboardActivity;
import com.github.andrdev.bolservice.model.requests.LoginRequest;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class Bols extends Application {
    public static Application app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    static void setAuthInfo() {
        String login = PreferenceManager.getDefaultSharedPreferences(app)
                .getString(DashboardActivity.PREF_LOGIN_KEY, "");
        String password = PreferenceManager.getDefaultSharedPreferences(app)
                .getString(DashboardActivity.PREF_PASSWORD_KEY, "");
        if(!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)){
            EbolNetworkWorker2.setAuthenticatedRestWorker(new LoginRequest(login, password));
        }
    }
}
