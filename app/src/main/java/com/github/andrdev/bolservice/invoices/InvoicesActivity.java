package com.github.andrdev.bolservice.invoices;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.invoices.create.InvoiceNewFragment;
import com.github.andrdev.bolservice.invoices.details.InvoiceDetailsFragment;

import butterknife.ButterKnife;


public class InvoicesActivity  extends AppCompatActivity {

    private static final String TAG_INVOICES = "invoices";
    private static final String TAG_INVOICE = "invoice";
    private static final String TAG_NEW_INVOICE = "newInvoice";
    public static final String OPEN_NEW = "openNew";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            if(getIntent().hasExtra(OPEN_NEW)) {
                showNewInvoiceFragment();
                return;
            }
            showInvoicesFragment();
        }
    }

    private void showInvoicesFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new InvoicesFragment(), TAG_INVOICES).commit();
    }

    public void showInvoiceFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new InvoiceDetailsFragment(), TAG_INVOICE)
                .addToBackStack(TAG_INVOICE).commit();
    }

    public void showNewInvoiceFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new InvoiceNewFragment(), TAG_NEW_INVOICE)
                .addToBackStack(TAG_NEW_INVOICE).commit();
    }
}
