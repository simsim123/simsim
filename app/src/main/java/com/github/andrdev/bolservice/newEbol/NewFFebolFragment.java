package com.github.andrdev.bolservice.newEbol;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.github.andrdev.bolservice.dialogs.DatePickerFragment;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.Utils;
import com.github.andrdev.bolservice.model.newEbol.Customer;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
//import com.github.andrdev.bolservice.newEbol.damages.NewEbolCarDamagesActivity;
//import com.github.andrdev.bolservice.newEbol.vehicleInfo.NewEbolVehicleInformationActivity;
import com.github.andrdev.bolservice.newEbol.damages.NewEbolVehicleDamagesActivity;
import com.github.andrdev.bolservice.newEbol.vehicleInfo.NewEbolVehicleInformationActivity;
import com.github.andrdev.bolservice.sign.SignActivity;
import com.github.andrdev.bolservice.view.vehicleInfos.VehicleInfosWidget;
import com.github.andrdev.bolservice.view.customer.CustomerNewEbolBWidget;
import com.github.andrdev.bolservice.view.customer.CustomerNewEbolWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewFFebolFragment extends MvpFragment<NewFFebolView, NewFFebolPresenter>
        implements NewFFebolView {

    public static final String SELECTED_VEHICLE_ID = "selectedVehicleId";
    public static final String TEMP = "temp";

    @Bind(R.id.originOrderId)
    protected EditText originOrderId;

    @Bind(R.id.originDate)
    protected TextView originDate;

    @Bind(R.id.vehiclesHost)
    VehicleInfosWidget vehicleInfos;

    @Bind(R.id.customerA)
    CustomerNewEbolWidget customerA;

    @Bind(R.id.customerB)
    CustomerNewEbolBWidget customerB;

    @Bind(R.id.customerSignCheck)
    View customerSignCheck;

    @Bind(R.id.driverSignCheck)
    View driverSignCheck;

    @Bind(R.id.unableToSignCheck)
    View unableToSignCheck;

    protected NewEbol currentEbol;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_febol, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void onStop() {
        saveEbol();
        super.onStop();
    }

    private void saveEbol() {
        getPresenter().justSaveEbol();
    }

    public void saveToDbEbol() {
        getPresenter().justSaveEbolDb();
    }

    @Override
    public NewEbol getEbolToSaveDb() {
        if (currentEbol == null) {
            currentEbol = new NewEbol();
        }
        String orderId = originOrderId.getText().toString();

        Customer origin = customerA.getCustomer(currentEbol.getOrigin());
        Customer destination = customerB.getCustomer(currentEbol.getDestination());

        currentEbol.setOrderId(orderId);
        currentEbol.setDate(originDate.getText().toString());
        currentEbol.setOrigin(origin);
        currentEbol.setDestination(destination);
        currentEbol.setType("db");
        currentEbol.setIsSavedToDb(true);

        return currentEbol;
    }

    private void getData() {
        getPresenter().getData();
    }

    @Override
    public void setData(NewEbol ebol) {
        currentEbol = ebol;
        showData();
    }

    protected void showData() {
        if (currentEbol == null) {
            return;
        }
        showVehicleInfos();
        setCommonFields(currentEbol);
        showCustomersInfos();
        setSignImages(currentEbol);
    }

    private void showVehicleInfos() {
        vehicleInfos.setVehiclesInfos(currentEbol.getVehicleInfos());
        vehicleInfos.setCallback(new VehicleInfosWidget.VehicleInfosCallback() {
            @Override
            public void detailsClickCb(VehicleInfo vehicleInfo) {
                detailsClick(vehicleInfo);
            }

            @Override
            public void damagesClickCb(VehicleInfo vehicleInfo) {
                damagesClick(vehicleInfo);
            }
        });
        vehicleInfos.showData();
    }

    private void showCustomersInfos() {
        customerA.setFragmentManager(getFragmentManager());
        customerB.setFragmentManager(getFragmentManager());
        if(currentEbol.getDestination() != null) {
            setDestinationCustomerFields(currentEbol.getDestination());
        }
        if(currentEbol.getOrigin() != null) {
            setOriginCustomerFields(currentEbol.getOrigin());
        }
    }

    private void setCommonFields(NewEbol ebol) {
        originOrderId.setText(ebol.getOrderId());
        originDate.setText(ebol.getDate());
    }

    private void setOriginCustomerFields(Customer origin) {
        customerA.setCustomerFields(origin);
    }

    private void setDestinationCustomerFields(Customer destination) {
        customerB.setCustomerFields(destination);
    }

    private void setSignImages(NewEbol savedEbol) {
        setSignImage(savedEbol.getDriverSignPath() != null, driverSignCheck);
        setSignImage(savedEbol.getCustomerSignPath() != null, customerSignCheck);
        setSignImage(savedEbol.isUnableToSign(), unableToSignCheck);
    }

    private void setSignImage(boolean hasSign, View signImage) {
        if (hasSign) {
            signImage.setVisibility(View.VISIBLE);
        } else {
            signImage.setVisibility(View.INVISIBLE);
        }
    }

    protected void detailsClick(VehicleInfo vehInfo) {
        getPresenter().detailsClick(vehInfo.getId());
    }

    @Override
    public void showVehicleDetails(int vehicleId) {
        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
        intent.putExtra(SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    protected void damagesClick(VehicleInfo vehInfo) {
        getPresenter().damagesClick(vehInfo.getId());
    }

    @Override
    public void showDamages(int vehicleId) {
        Intent intent = new Intent(getActivity(), NewEbolVehicleDamagesActivity.class);
        intent.putExtra(SELECTED_VEHICLE_ID, vehicleId);
        startActivity(intent);
    }

    @Override
    public void openNewVehicleInfoActivity() {
        Intent intent = new Intent(getActivity(), NewEbolVehicleInformationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.addCars)
    void plusClick() {
        getPresenter().addNewVehicle();
    }

    @OnClick(R.id.customerSignature)
    void customerSignatureClick() {
        getPresenter().customerSignatureClick();
    }

    @Override
    public void openCustomerSignature() {
        if (Utils.hasText(originOrderId) && canSign()) {
            Intent intent = new Intent(getActivity(), SignActivity.class);
            intent.putExtra("signatureType", "customerSign");
            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
            startActivity(intent);
        }
    }

    private boolean canSign() {
        if(!currentEbol.getVehicleInfos().isEmpty()
                && !TextUtils.isEmpty(currentEbol.getOrigin().getCustomerName())
                || !TextUtils.isEmpty(currentEbol.getOrigin().getAddress())){
            return true;
        }
        return false;
    }

    @OnClick(R.id.driverSignature)
    void driverSignatureClick() {
        getPresenter().driverSignatureClick();
    }

    @Override
    public void openDriverSignature() {
        if (Utils.hasText(originOrderId) && canSign()) {
            Intent intent = new Intent(getActivity(), SignActivity.class);
            intent.putExtra("signatureType", "driverSign");
            intent.putExtra(NewEbolActivity.EBOL_ID, Utils.getTextFromEditText(originOrderId));
            startActivity(intent);
        }
    }

    @OnClick(R.id.unableToSign)
    void unableToSignClick() {
        if(Utils.isNullOrEmpty(currentEbol.getCustomerSignPath())) {
            currentEbol.setUnableToSign(true);
            getPresenter().unableToSignClick();
        }
    }

    @Override
    public void showUnableToSign(){
        if (Utils.hasText(originOrderId) && canSign() &&
                Utils.isNullOrEmpty(currentEbol.getCustomerSignPath())) {
            unableToSignCheck.setVisibility(View.VISIBLE);
            setSignImages(currentEbol);
        }
    }

    @Override
    public void finishEbol() {

    }

    @OnClick(R.id.next)
    void nextClick() {
        currentEbol.setType("pendingPickup");
        getPresenter().finishEbolCreation();
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            intent = new Intent(getContext(), PdfActActivity.class);
        } else {
//            intent = new Intent(getContext(), PdfActPrekk.class);
        }
        if(!Utils.isNullOrEmpty(currentEbol.getDriverSignPath())
                && (!Utils.isNullOrEmpty(currentEbol.getCustomerSignPath()) || currentEbol.isUnableToSign())) {
//        startActivity(intent);
            getActivity().finish();
        }
    }

    @OnClick(R.id.originDate)
    void dateSetClick(View view) {
        hideSoftKeyboard(getActivity());
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setCallback((year, month, day) -> {
            String date = String.format("%s/%s/%s", month + 1, day, year);
            originDate.setText(date);
        });
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public NewEbol getEbolToSave() {
        if (currentEbol == null) {
            currentEbol = new NewEbol();
        }
        String orderId = originOrderId.getText().toString();

        Customer origin = customerA.getCustomer(currentEbol.getOrigin());
        Customer destination = customerB.getCustomer(currentEbol.getDestination());

        currentEbol.setOrderId(orderId);
        currentEbol.setDate(originDate.getText().toString());
        currentEbol.setOrigin(origin);
        currentEbol.setDestination(destination);
        currentEbol.setType(NewFFebolFragment.TEMP);

        return currentEbol;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public NewFFebolPresenter createPresenter() {
        return new NewFFebolPresenterImpl();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

}
