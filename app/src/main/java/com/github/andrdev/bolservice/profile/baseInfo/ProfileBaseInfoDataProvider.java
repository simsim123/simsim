package com.github.andrdev.bolservice.profile.baseInfo;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Profile;

public class ProfileBaseInfoDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }


    public void getSavedProfile(DataCb<Profile> successCb) {
        dbHelper.getSavedProfile(successCb::returnData);
    }
}
