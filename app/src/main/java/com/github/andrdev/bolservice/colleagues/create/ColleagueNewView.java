package com.github.andrdev.bolservice.colleagues.create;

import com.hannesdorfmann.mosby.mvp.MvpView;


public interface ColleagueNewView extends MvpView {
    void backToColleagues();

    void showFailedToast();
}
