package com.github.andrdev.bolservice.currentEbol;

import android.util.Log;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;

public class CurrentFEbolPresenterImpl extends CurrentFEbolPresenter {

    CurrentFEbolDataProvider dataProvider;

    @Override
    public void attachView(CurrentFEbolView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new CurrentFEbolDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

    @Override
    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getEbol(this::setEbol);
    }

    private void setEbol(NewEbol newEbol) {
        if (!isViewAttached()) {
            return;
        }
        Log.v("dree", newEbol.getOrderId()!=null?newEbol.getOrderId():"nyull");
        getView().setData(newEbol);
    }

    @Override
    public void saveEbol(com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSave();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }

    @Override
    public void finishEbolCreation() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openSendActivity);
    }

    private void openSendActivity(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().finishEbol();
    }

    @Override
    public void addNewVehicle() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::addNewVehicleEbolSaved);
    }

    private void addNewVehicleEbolSaved(boolean saved) {
        dataProvider.removeTempVehicle(this::openVehicleCreation);
    }

    private void openVehicleCreation(boolean cleared) {
        if (!isViewAttached()) {
            return;
        }
        getView().openNewVehicleInfoActivity();
    }

    @Override
    public void detailsClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showVehicleDetails(vehicleId));
    }

    @Override
    public void justSaveEbol() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> proceedWithExit());
    }

    @Override
    public void justSaveEbolDb() {
        if (!isViewAttached()) {
            return;
        }
        saveEbolDb((saved) -> proceedWithExit());
    }

    private void saveEbolDb(com.github.andrdev.bolservice.currentEbol.trs.NewEbolDataProvider.DataCb<Boolean> callback) {
        if (!isViewAttached()) {
            return;
        }
        NewEbol newEbol = getView().getEbolToSaveDb();
        dataProvider.saveOrUpdateEbol(newEbol, callback);
    }

    private void proceedWithExit() {
//        getView().proceedWithExit();
    }

    public void showVehicleDetails(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showVehicleDetails(vehicleId);
    }

    @Override
    public void damagesClick(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        saveEbol((saved) -> showDamages(vehicleId));
    }

    public void showDamages(int vehicleId) {
        if (!isViewAttached()) {
            return;
        }
        getView().showDamages(vehicleId);
    }

    @Override
    public void customerSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openCustomerSignature);
    }

    private void openCustomerSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openCustomerSignature();
    }

    @Override
    public void driverSignatureClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::openDriverSignature);
    }

    private void openDriverSignature(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().openDriverSignature();
    }

    @Override
    public void unableToSignClick() {
        if (!isViewAttached()) {
            return;
        }
        saveEbol(this::showUnableToSign);
    }

    public void showUnableToSign(boolean saved) {
        if (!isViewAttached()) {
            return;
        }
        getView().showUnableToSign();
    }
}

