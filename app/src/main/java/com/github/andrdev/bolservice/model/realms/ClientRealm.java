package com.github.andrdev.bolservice.model.realms;

import io.realm.RealmObject;

public class ClientRealm extends RealmObject {
    public static final String ID = "id";
    public static final String CUSTOMER_NAME = "customerName";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String Z_I_P = "zIP";
    public static final String PHONE = "phone";

    private int id;
    private String customerName;
    private String address;
    private String city;
    private String state;
    private String zIP;
    private String phone;

    public ClientRealm(){ }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setCustomerName(String customerName){
        this.customerName = customerName;
    }
    public String getCustomerName(){
        return this.customerName;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress(){
        return this.address;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setState(String state){
        this.state = state;
    }
    public String getState(){
        return this.state;
    }
    public void setZIP(String zIP){
        this.zIP = zIP;
    }
    public String getZIP(){
        return this.zIP;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
}
