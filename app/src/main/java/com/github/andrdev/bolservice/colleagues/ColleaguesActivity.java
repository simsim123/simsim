package com.github.andrdev.bolservice.colleagues;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.colleagues.create.ColleagueNewFragment;
import com.github.andrdev.bolservice.colleagues.details.ColleagueDetailsFragment;
import com.github.andrdev.bolservice.colleagues.edit.ColleagueEditFragment;
import com.github.andrdev.bolservice.model.Profile;

import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;


public class ColleaguesActivity extends BaseDrawerActivity {

    private static final String TAG_COLLEAGUES = "colleagues";
    private static final String TAG_COLLEAGUE_NEW = "colleagueNew";
    private static final String TAG_COLLEAGUE_DETAILS = "colleagueDetails";
    private static final String TAG_COLLEAGUE_EDIT = "colleagueEdit";

    @Bind(R.id.colleagueNew)
    TextView colleagueNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showColleagueFragment();
        }

        showNewButtonWhenUserBoss();
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
    }

    private void showNewButtonWhenUserBoss() {
        Realm realm = Realm.getInstance(getApplicationContext());
        boolean userIsBoss = realm.where(Profile.class).findFirst().getIsBoss();
        if (!userIsBoss) {
            colleagueNew.setVisibility(View.INVISIBLE);
        }
        realm.close();
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        return () -> {
            FragmentManager manager = getSupportFragmentManager();
            if (manager != null) {
                Fragment fragment = manager.findFragmentByTag(TAG_COLLEAGUES);
                if (fragment != null && fragment.isVisible()) {
                    fragment.onResume();
                }
            }
        };
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 3;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_colleagues;
    }

    private void showColleagueFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new ColleaguesFragment(), TAG_COLLEAGUES).commit();
    }

    @OnClick(R.id.colleagueNew)
    void showColleagueNewFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ColleagueNewFragment(), TAG_COLLEAGUE_NEW)
                .addToBackStack(TAG_COLLEAGUE_NEW).commit();
    }

    public void showColleagueDetailsFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ColleagueDetailsFragment(), TAG_COLLEAGUE_DETAILS)
                .addToBackStack(TAG_COLLEAGUE_DETAILS).commit();
    }

    public void showColleagueEditFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ColleagueEditFragment(), TAG_COLLEAGUE_EDIT)
                .addToBackStack(TAG_COLLEAGUE_EDIT).commit();
    }
}
