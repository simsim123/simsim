package com.github.andrdev.bolservice.model.responses;


public final class Predictions {

    public final Prediction predictions[];
    public final String status;

    public Predictions(Prediction[] predictions, String status){
        this.predictions = predictions;
        this.status = status;
    }

    public Prediction[] getPredictions() {
        return predictions;
    }

    public String getStatus() {
        return status;
    }

    public static final class Prediction {
        public final String description;
        public final String id;
        public final Matched_substring matched_substrings[];
        public final String place_id;
        public final String reference;
        public final Term terms[];
        public final String[] types;

        public Prediction(String description, String id, Matched_substring[] matched_substrings, String place_id, String reference, Term[] terms, String[] types){
            this.description = description;
            this.id = id;
            this.matched_substrings = matched_substrings;
            this.place_id = place_id;
            this.reference = reference;
            this.terms = terms;
            this.types = types;
        }

        public String getDescription() {
            return description;
        }

        public String getId() {
            return id;
        }

        public Matched_substring[] getMatched_substrings() {
            return matched_substrings;
        }

        public String getPlace_id() {
            return place_id;
        }

        public String getReference() {
            return reference;
        }

        public Term[] getTerms() {
            return terms;
        }

        public String[] getTypes() {
            return types;
        }

        public static final class Matched_substring {
            public final long length;
            public final long offset;

            public Matched_substring(long length, long offset){
                this.length = length;
                this.offset = offset;
            }
        }

        public static final class Term {
            public final long offset;
            public final String value;

            public Term(long offset, String value){
                this.offset = offset;
                this.value = value;
            }
        }
    }
}