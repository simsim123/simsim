package com.github.andrdev.bolservice.newEbol.vehicleInfo.vin;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.andrdev.bolservice.R;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewEbolScanVinActivity extends AppCompatActivity
        implements CompoundBarcodeView.TorchListener {

    public static final String VIN = "vin";

    private CaptureManager capture;

    @Bind(R.id.zxing_barcode_scanner)
    CompoundBarcodeView barcodeScannerView;

    boolean isLightsOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vin_scan);
        ButterKnife.bind(this);
        capture = new CaptureManager(this, barcodeScannerView);
        barcodeScannerView.setTorchListener(this);

        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @OnClick(R.id.enter)
    void enterClick(View view) {
        setResult(319);
        finish();
    }

    @OnClick(R.id.lights)
    void lightsClick(View view) {
        isLightsOn = !isLightsOn;
        if (isLightsOn) {
            barcodeScannerView.setTorchOn();
        } else {
            barcodeScannerView.setTorchOff();
        }
    }

    @OnClick(R.id.cancel)
    void cancelClick(View view) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }
}
