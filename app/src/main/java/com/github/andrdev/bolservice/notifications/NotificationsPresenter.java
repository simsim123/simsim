package com.github.andrdev.bolservice.notifications;

import com.github.andrdev.bolservice.model.responses.NotificationsResponse;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


public abstract class NotificationsPresenter extends MvpBasePresenter<NotificationsView> {
    public abstract void getData();

    public abstract void getDataRequestSuccess(NotificationsResponse notificationsResponse);

    public abstract void getDataRequestFailed(Throwable throwable);
}
