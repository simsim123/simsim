package com.github.andrdev.bolservice.view.vehicleToolsNe;

import android.content.Context;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface VehicleToolsNeView extends MvpView {
    Context getViewContext();
}
