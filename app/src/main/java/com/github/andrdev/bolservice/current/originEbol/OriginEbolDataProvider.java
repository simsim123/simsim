package com.github.andrdev.bolservice.current.originEbol;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.newEbol.NewEbol;
import com.github.andrdev.bolservice.model.responses.DamagesGetResponse;
import com.github.andrdev.bolservice.model.responses.DamagesResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class OriginEbolDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void deInit() {
        dbHelper.deInit();
    }


    public void getTempEbol(DataCb<NewEbol> callback) {
        dbHelper.getTempEbol(callback::returnData);
    }

    public void getDamages(String carId, DataCb<DamagesResponse> successCb) {
        EbolNetworkWorker2.getInstance().getDamages(carId,
                successCb::returnData);
    }

    public void saveOrUpdateEbol(NewEbol newEbol, DataCb<Boolean> callback) {
        dbHelper.updateOrSaveEbol(newEbol, callback::returnData);
    }
}
