package com.github.andrdev.bolservice.model.responses;

public class PlaceInfo {

    public final Result result;

    public PlaceInfo(Result result){
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public static final class Result {
        public final AddressComponent address_components[];

        public Result(AddressComponent[] address_components) {
            this.address_components = address_components;
        }

        public AddressComponent[] getAddress_components() {
            return address_components;
        }

        public static final class AddressComponent {
            public final String long_name;
            public final String short_name;
            public final String[] types;

            public AddressComponent(String long_name, String short_name, String[] types) {
                this.long_name = long_name;
                this.short_name = short_name;
                this.types = types;
            }

            public String getLong_name() {
                return long_name;
            }

            public String getShort_name() {
                return short_name;
            }

            public String[] getTypes() {
                return types;
            }
        }
    }
}
