package com.github.andrdev.bolservice.notifications;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.github.andrdev.bolservice.BaseDrawerActivity;
import com.github.andrdev.bolservice.R;

import butterknife.OnClick;


public class NotificationsActivity extends BaseDrawerActivity {

    public static final String TAG_NOTIFICATIONS = "notifications";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showNotificationsFragment();
        }
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        return () -> {
            FragmentManager manager = getSupportFragmentManager();
            if (manager != null) {
                Fragment fragment = manager.findFragmentByTag(TAG_NOTIFICATIONS);
                if (fragment != null && fragment.isVisible()) {
                    fragment.onResume();
                }
            }
        };
    }

    @Override
    protected int getCurrentActivityPosition() {
        return 2;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_notifications;
    }

    private void showNotificationsFragment() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new NotificationsFragment(), TAG_NOTIFICATIONS).commit();
    }

    @OnClick(R.id.edit)
    void showNotificationsEditFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new NotificationsEditFragment())
                .addToBackStack(null).commit();
    }
}
