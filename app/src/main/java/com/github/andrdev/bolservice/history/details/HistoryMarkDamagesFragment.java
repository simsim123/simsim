package com.github.andrdev.bolservice.history.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.andrdev.bolservice.R;
import com.github.andrdev.bolservice.current.originEbol.damages.OriginDamagesFragment;
import com.github.andrdev.bolservice.history.details.damages.HistoryDamagesFragment;
import com.github.andrdev.bolservice.model.newEbol.MarkedDamage;
import com.github.andrdev.bolservice.model.newEbol.VehicleInfo;
import com.github.andrdev.bolservice.model.newEbol.VehiclePhoto;
import com.github.andrdev.bolservice.newEbol.NewFFebolFragment;
import com.github.andrdev.bolservice.view.markDamagesNe.MarkDamagesNeWidget;
import com.github.andrdev.bolservice.view.vehiclePhotos.VehiclePhotosWidget;
import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HistoryMarkDamagesFragment
        extends MvpFragment<HistoryMarkDamagesView, HistoryMarkDamagesPresenter>
        implements HistoryMarkDamagesView {

    @Bind(R.id.photoResult)
    ImageView photoResult;
    @Bind(R.id.carModel)
    TextView vehicleModel;
    @Bind(R.id.photoType)
    TextView type;
    @Bind(R.id.photoCounter)
    TextView photoCounter;
    @Bind(R.id.markDamages)
    MarkDamagesNeWidget markDamages;

    String photoType;
    int selectedVehicleId;

    VehicleInfo vehicleInfo;
    VehiclePhoto vehiclePhoto;

    @Bind(R.id.notesView)
    RelativeLayout notesView;

    @Bind(R.id.notesText)
    TextView notesText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mark_damages, container, false);
        ButterKnife.bind(this, view);
        markDamages.setMarkCreateCallback(this::startDamageChooseActivity);
        if(savedInstanceState != null){
            selectedVehicleId = savedInstanceState.getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        } else if (getArguments() != null) {
            selectedVehicleId = getArguments().getInt(NewFFebolFragment.SELECTED_VEHICLE_ID);
        }
        if(getArguments() != null && getArguments().containsKey(OriginDamagesFragment.PHOTO_TYPE)) {
            photoType = getArguments().getString(OriginDamagesFragment.PHOTO_TYPE);
        }
        return view;
    }

    @Override
    public HistoryMarkDamagesPresenter createPresenter() {
        return new HistoryMarkDamagesPresenter();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    private void getData() {
        photoType = getArguments().getString(OriginDamagesFragment.PHOTO_TYPE);
        getPresenter().getData(selectedVehicleId, photoType);
    }

    @Override
    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    @Override
    public void initViews() {
        photoType = vehiclePhoto.getPhotoType();
        loadPhoto(vehiclePhoto.getPhotoPath());
        showViews(photoType);
    }

    public void loadPhoto(String photoPath) {
        if(vehiclePhoto.isLocal()) {
            loadPhotoFromDisk(photoPath);
        } else {
            Picasso.with(getContext()).load(photoPath).into(photoResult);
        }
    }

    private void loadPhotoFromDisk(String photoPath) {
        File file = new File(photoPath);
        Picasso picasso = Picasso.with(getContext());
        picasso.invalidate(file);
        picasso.load(file).fit().centerCrop().into(photoResult);
    }
    private void showViews(String photoType) {
        setPhotoDescription(photoType);
        markDamages.setPhoto(vehiclePhoto);
        markDamages.paintDamages();
    }

    @Override
    public void setVehiclePhoto(VehiclePhoto vehiclePhoto) {
        this.vehiclePhoto = vehiclePhoto;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        super.onSaveInstanceState(bundle);
    }

    public void startDamageChooseActivity(Intent intent) {
        intent.putExtra(OriginDamagesFragment.PHOTO_TYPE, photoType);
        intent.putExtra(NewFFebolFragment.SELECTED_VEHICLE_ID, selectedVehicleId);
        startActivityForResult(intent, 32908);
    }

    private void setPhotoDescription(String photoType) {
        switch (photoType){
            case VehiclePhotosWidget.FRONT_PHOTO:
                type.setText("Driver Front");
                photoCounter.setText("1 of 4");
                break;
            case VehiclePhotosWidget.BACK_PHOTO:
                type.setText("Driver Back");
                photoCounter.setText("2 of 4");
                break;
            case VehiclePhotosWidget.RIGHT_SIDE_PHOTO:
                type.setText("Driver Right");
                photoCounter.setText("3 of 4");
                break;
            case VehiclePhotosWidget.LEFT_SIDE_PHOTO:
                type.setText("Driver Left");
                photoCounter.setText("4 of 4");
                break;
            default:
                type.setVisibility(View.INVISIBLE);
                photoCounter.setVisibility(View.INVISIBLE);
                break;
        }
        vehicleModel.setText(String.format("%s %s %s", vehicleInfo.getYearOfMake(), vehicleInfo.getMake(),
                vehicleInfo.getModel()));
    }

    @OnClick(R.id.addNotesP)
    void addNotesPClick(View v){
        notesView.setVisibility(View.VISIBLE);
        notesText.setText(vehiclePhoto.getNote());
    }


    @OnClick(R.id.saveNote)
    void saveNoteClick(View v){
        vehiclePhoto.setNote(notesText.getText().toString());
        notesView.setVisibility(View.GONE);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void backToDamages() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.cancel)
    void cancelClick(View v) {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.save)
    void saveClick(View v) {
        getActivity().onBackPressed();
    }
}