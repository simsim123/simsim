package com.github.andrdev.bolservice.view.vehiclePhotosNe;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class VehiclePhotosNePresenter extends MvpBasePresenter<VehiclePhotosNeView>{

    VehiclePhotosNeDataProvider dataProvider;

    @Override
    public void attachView(VehiclePhotosNeView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new VehiclePhotosNeDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }

    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }

}
