package com.github.andrdev.bolservice.clients.edit;

import com.github.andrdev.bolservice.DataProvider;
import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.model.Client;
import com.github.andrdev.bolservice.model.requests.ClientUpdateRequest;
import com.github.andrdev.bolservice.model.responses.SimpleResponse;
import com.github.andrdev.bolservice.networking.EbolNetworkWorker2;

public class ClientEditDataProvider implements DataProvider {

    DbHelper dbHelper;

    public void setDbHelper(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public void deInit() {
        dbHelper.deInit();
    }


    public void saveClient(Client client, DataCb<Boolean> successCb) {
        dbHelper.saveClient(client, successCb::returnData);
    }

    public void getSavedClient(DataCb<Client> successCb) {
        dbHelper.getSavedClient(successCb::returnData);
    }

    public void updateClient(ClientUpdateRequest request, DataCb<SimpleResponse> successCb,
                             FailedCb<Throwable> failedCb) {
        EbolNetworkWorker2.getInstance().updateClient(request, successCb::returnData, failedCb::failed);
    }
}
