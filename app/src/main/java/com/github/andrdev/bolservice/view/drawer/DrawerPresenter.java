package com.github.andrdev.bolservice.view.drawer;

import com.github.andrdev.bolservice.database.DbHelper;
import com.github.andrdev.bolservice.database.DbHelperRxImpl;
import com.github.andrdev.bolservice.model.Profile;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

public class DrawerPresenter extends MvpBasePresenter<DrawerView> {

    DrawerDataProvider dataProvider;

    @Override
    public void attachView(DrawerView view) {
        super.attachView(view);
        if(!isViewAttached()){
            return;
        }
        init();
    }

    private void init() {
        dataProvider = new DrawerDataProvider();
        dataProvider.setDbHelper(getDbHelper());
    }

    protected DbHelper getDbHelper() {
        return new DbHelperRxImpl(getView().getViewContext());
    }


    @Override
    public void detachView(boolean retainInstance) {
        deInit();
        super.detachView(retainInstance);
    }

    private void deInit() {
        dataProvider.deInit();
    }


    public void getData() {
        if (!isViewAttached()) {
            return;
        }
        dataProvider.getProfile(this::setProfileName);
    }

    private void setProfileName(Profile profile) {
        if (!isViewAttached()) {
            return;
        }
//        getView().setUserName(profile.getFullNameFormatted());
    }
}
